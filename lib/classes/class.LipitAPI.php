<?php

final class LipitAPI {

    private $api_id;
    private $api_key;
    private $api_token;
    private $code;
    private $msg;
    private $transaction_code;
    private $email;
    private $bookingcode;

    public function __construct() {
        
    }

    /**
     * @return the API_ID
     */
    public function getAPI_ID() {
        return $this->api_id;
    }

    public function setAPI_ID($api_id) {
        $this->api_id = $api_id;
    }

    /**
     * @return the API_KEY
     */
    public function getAPI_KEY() {
        return $this->api_key;
    }

    public function setAPI_KEY($api_key) {
        $this->api_key = $api_key;
    }

    /**
     * @return the API_TOKEN
     */
    public function getAPI_TOKEN() {
        return $this->api_token;
    }

    public function setAPI_TOKEN($api_token) {
        $this->api_token = $api_token;
    }

    /**
     * @return the Code
     */
    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    /**
     * @return the MSG
     */
    public function getMsg() {
        return $this->msg;
    }

    public function setMsg($msg) {
        $this->msg = $msg;
    }

    /**
     * @return the TransactionCode
     */
    public function getTransactionCode() {
        return $this->transaction_code;
    }

    public function setTransactionCode($transaction_code) {
        $this->transaction_code = $transaction_code;
    }

    /**
     * @return the Email
     */
    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    
    /**
     * @return the Booking Code
     */
    public function getBookingCode() {
        return $this->bookingcode;
    }

    public function setBookingCode($bookingcode) {
        $this->bookingcode = $bookingcode;
    }

    /**
     * @return the DoIT
     */
    public function DoIT($url) {

        $fields = array(
            'api_id' => $this->api_id,
            'api_key' => $this->api_key,
            'api_token' => $this->api_token,
            'bookingcode' => $this->bookingcode
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result);
        $this->code = $result->code;
        $this->transaction_code = $result->transaction_code;
    }

    /**
     * @return the Response Massages
     */
    public function Response($code) {
        $arr = array(
            200 => 'Successful.',
            1 => 'Not Successful. [1]',
            2 => 'Please enter the correct image characters. [2]',
            100 => 'API are not Successful :( [100]',
            300 => 'Booking Code is not Empty :( [300]',
            400 => 'Sorry, No Information :( [400] <br/> - Plz Contact: 1900.8899'
        );
        if ($code) {
            return $arr[$code];
        } else {
            return $arr[400];
        }
    }

}

// end of class
#
# EOF
#
?>