<?php
/**
 * Description of class
 * @author Lipit
 */
final class cms_lipit {
    /**
    * @ignore
    */
    public function __construct() {}
    /**
     * Get the complete URL to an admin icon
     *
     * @param string $icon the basename of the desired icon
     * @return string
     */
    public static function Render_Filename($name,$encode,$file_name){
        $newname = "";
        switch($encode){
            case 'md5':
                $newname = md5($file_name);
                break;
            case 'base64':
                $newname = base64_encode($file_name);
                break;
            case 'alias':
                $newname = munge_string_to_url($name,true);
                break;
            default: break;
        }
        return $newname;
    }
}
