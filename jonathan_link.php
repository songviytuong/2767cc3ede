<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('CONFIG_FILE_LOCATION', __DIR__ . '/config.php');
require_once(__DIR__ . '/lib/include.php');
require_once(__DIR__ . '/lib/simple_html_dom.php');

$gCms = CmsApp::get_instance();
$db = $gCms->GetDb();
$config = cmsms()->GetConfig();

$mod = \cms_utils::get_module(MOD_PRODUCTS);
$prefix = $mod->GetPreference('upload_dir', $mod->GetName());

$_file = $config["uploads_url"] . '/html/catalogue.html';
$html = file_get_html($_file);
$content = $html->find(".he-wrap");
$arr = array();
$PATH = $config["uploads_path"] . '/html/';
foreach ($content as $key => $item) {
    $arr[$key]["img"] = $item->find("img", 0)->src;
    $arr[$key]["basename"] = basename($item->find("img", 0)->src);
    file_put_contents($PATH.'/'.$arr[$key]["basename"], file_get_contents($arr[$key]["img"]));
}
echo "<pre>";
var_dump($arr);
