<?php

define('CONFIG_FILE_LOCATION', __DIR__ . '/config.php');
require_once(__DIR__ . '/lib/include.php');

$gCms = CmsApp::get_instance();
$db = $gCms->GetDb();
$config = cmsms()->GetConfig();

$type = $_REQUEST["type"];

if ($type && isset($type) == 'pagetime') {
    $value = $_REQUEST["time"];
    $ex_query = 'INSERT INTO ' . cms_db_prefix() . 'statistics (`type`,`value`,`started`,`finished`) VALUES (?,?,NOW(),?)';
    $db->Execute($ex_query, array($type, $value,''));
} elseif($type && isset($type) == 'navinfo'){
    $value = serialize($_REQUEST);
    $ex_query = 'INSERT INTO ' . cms_db_prefix() . 'statistics (`type`,`value`,`started`,`finished`) VALUES (?,?,NOW(),?)';
    //$db->Execute($ex_query, array($type, $value));
} 

$human = $_GET["logHuman"];
if($human && isset($human) == 1){
    $value = "Human Value";
    $ex_query = 'INSERT INTO ' . cms_db_prefix() . 'statistics (`type`,`value`,`started`,`finished`) VALUES (?,?,NOW(),?)';
    $db->Execute($ex_query, array('logHuman', $_REQUEST["r"],''));
}
