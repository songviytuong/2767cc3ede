<?php
define('CONFIG_FILE_LOCATION', __DIR__ . '/config.php');
require_once(__DIR__ . '/lib/include.php');
require_once(__DIR__ . '/lib/simple_html_dom.php');
$gCms = CmsApp::get_instance();
$db = $gCms->GetDb();
$config = cmsms()->GetConfig();
$mod = \cms_utils::get_module(MOD_PRODUCTS);
$prefix = $mod->GetPreference('upload_dir', $mod->GetName());
//$query = 'SELECT id,sku,source FROM ' . cms_db_prefix() . 'module_products';
$query = 'SELECT
c.product_id,
c.collection_id,
p.id,
p.sku,
p.source
FROM
cms_module_products AS p
INNER JOIN cms_module_products_prodtocollect AS c ON c.product_id = p.id
WHERE
c.collection_id = 0';

$tmp = $db->GetAll($query);

$key_data = isset($_GET['item']) ? $_GET['item'] : -1 ;
if($key_data==-1){
    echo "not found item";
    return;
}
$item = isset($tmp[$key_data]) ? $tmp[$key_data] : '' ;
$key = $key_data;
if ($item != '' && $item["sku"] != "") {
    $PATH = $config["uploads_path"] . '/' . $prefix . '/product_' . $item["id"];
    $FILE = $PATH . '/'. $item["sku"].'.html';
    $insert = array();
    if (!file_exists($FILE)){
        $datadetails = login_and_get_data($item["source"]);
        $yes = str_get_html($datadetails);
        if($yes){
            $status = $yes->save($FILE);
        }

    } else {
        $html = file_get_html($FILE);
        $insert["product_name"] = trim($html->find('.product_name', 0)->innertext);
        $desc = trim($html->find('//div/p[2]/', 0)->innertext);
        if (!$desc) {
            $desc = trim($html->find('//div/p[3]/', 0)->innertext);
        } else {
            $desc = "Coming soon...";
        }
        $insert["description"] = $desc;
        $collection = explode("collection_id=", trim($html->find('//div/p[10]/a', 0)->href));
        $insert["collecton_id"] = (int) $collection[1];
        $insert["sku"] = $item["sku"];
        $insert["id"] = $item["id"];

        $update = array(
            'product_name' => $insert["product_name"],
            'product_name_en' => $insert["product_name"],
            'details' => $insert["description"],
            'details_en' => $insert["description"],
            'id' => $item["id"]
        );

//      var_dump($update);
//      echo "<pre>";
//      var_dump($insert);
//      exit();
        
        if ($insert["collecton_id"]) {
            $update_query = 'UPDATE ' . cms_db_prefix() . 'module_products_prodtocollect SET collection_id = ? WHERE product_id = ?';
            $db->Execute($update_query, array($insert["collecton_id"], $insert["id"]));
        }
//        $update_query = 'UPDATE ' . cms_db_prefix() . 'module_products SET product_name = ?, product_name_en = ?, details = ?, details_en = ? WHERE id = ?';
//        $db->Execute($update_query,$update);
//        $insert_query = 'INSERT INTO ' . cms_db_prefix() . 'module_products_prodtocollect (product_id, collection_id) VALUES (?,?)';
//        $db->Execute($insert_query, array($item["id"], $insert["collecton_id"]));
    }
    echo "<img src='assets/images/spinner.gif'/> <small>ID: ".$item["id"]." - SKU: ". $item["sku"]."</small>";
    $key++;
    echo "<meta http-equiv='refresh' content='0;URL=\"http://gallery.dev/jonathan_getDetails.php?item=".$key."\"' />";
} else {
    echo "Done.";
}



function login_and_get_data($link = '') {
    if ($link == '') {
        echo "Link empty !";
        return;
    }
    
    $username = "khanhkts@me.com";
    $password = "29031978";

    //login form action url
    $url="http://www.jonathancharlesfurniture.com/catalogue/index.php?route=trade/login"; 
    $postinfo = "email=".$username."&password=".$password;

    $cookie_file_path = "/cookie.txt";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
    //set the cookie the site has for certain features, this is optional
    curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
    curl_setopt($ch, CURLOPT_USERAGENT,
        "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
    curl_exec($ch);

    //page with the content I want to grab
    curl_setopt($ch, CURLOPT_URL, $link);
    //do stuff with the info with DomDocument() etc
    $html = curl_exec($ch);
    curl_close($ch);
    return $html;
    
}