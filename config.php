<?php
$config['dbms'] = 'mysqli';
$config['db_hostname'] = 'localhost';
$config['db_username'] = 'root';
$config['db_password'] = 'a7GKBst7qYxKoTBH';
$config['db_name'] = 'local_jonathan';

$config['root_url'] = 'http://' . $_SERVER['HTTP_HOST'];
$config['db_prefix'] = 'cms_';
$config['timezone'] = 'Asia/Ho_Chi_Minh';
$config['php_memory_limit'] = '32M';
$config['admin_dir'] = 'admin';
$config['default_upload_permission'] = '664';
$config['use_smarty_php_tags'] = '';
$config['auto_alias_content'] = true;
$config['url_rewriting'] = 'mod_rewrite';
$config['page_extension'] = '/';
$config['use_hierarchy'] = true;
$config['query_var'] = 'page';
$config['internal_pretty_urls'] = 'mod_rewrite';
$config['assume_mod_rewrite'] = true;
$config['permissive_smarty'] = 1;
$config['output_compression'] = '1';
$config['themes_url'] = $config['root_url'].'/assets';
$config['debug'] = false;

$config['url'] = $config['root_url'].'/getBackData?showtemplate=false';
$config['api_id'] = 8888;
$config['api_key'] = 'user8888';
$config['api_token'] = 'pass8888';

/// ErrorLogger
require_once('E:\xampp\htdocs\public_html\jonathan\modules\ErrorLogger/ErrorLogger.handler.php');

?>