<?php
#-------------------------------------------------------------------------
# Plugin: cookie_consent
# Author: Rolf Tjassens
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2012 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The plugin's homepage is: http://dev.cmsmadesimple.org/projects/cookie_consent
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

function smarty_cms_function_cookie_consent($params, &$smarty) {

/**
 * Check for presence consent cookie
 */
	$cookie_name = 'cookie_consent';
	if (isset($_COOKIE[$cookie_name])) {
		return false;
	}

/**
 * Default parameter settings
 */
	$action			= isset($params['action']) ? $params['action'] : '';
	$decline_text	= isset($params['decline_text']) ? $params['decline_text'] : '';
	$lang			= isset($params['lang']) ? $params['lang'] : '';

/**
 * +++++ Head action +++++
 */
	if ( (isset($action)) && ($action == 'head') ) {
		echo '
			<style type="text/css">
				#cookie_consent { 
					background: #333;
					position: absolute;
					float: left;
					z-index: 10000000000000;
					width: 100%;
					padding: 5px 0;
				}
				#cookie_consent p {
					text-align: center;
					color: #ccc;
					font-size: 12px;
					margin: 0;
				}
				#cookie_consent a.more_info {
					margin: 0 5px;
					color: #ccc;
					text-decoration: underline;
					cursor: pointer;
				}
				#cookie_consent a.decline_cookies {
					margin: 0 5px;
					color: #f33;
					text-decoration: underline;
					cursor: pointer;
				}
				a.page_decline_cookies {
					color: #f33;
					text-decoration: underline;
					cursor: pointer;
				}
				#cookie_consent a.accept_cookies {
					margin: 0 5px;
					color: #6c0;
					font-weight: bold;
					text-decoration: underline;
					cursor: pointer;
				}
			</style>
			';
		return false;
	} // +++++ End head action +++++

/**
 * +++++ Body action +++++
 */
	if ( (isset($action)) && ($action == 'body') ) {
		if ( $lang == 'nl' ) {
			echo '
				<div id="cookie_consent"><p>Om u beter van dienst te kunnen zijn gebruikt deze website cookies <a class="accept_cookies">Accepteer cookies</a><a href="cookies" class="more_info">Meer informatie</a><!--<a class="decline_cookies">Weiger cookies</a>--></p></div>
				';
		}
		elseif ( $lang == 'de' ) {
			echo '
				<div id="cookie_consent"><p>Um einen besseren Service bieten zu k&ouml;nnen nutzt diese Website Cookies <a class="accept_cookies">Cookies akzeptieren</a><a href="cookies" class="more_info">Mehr info</a><!--<a class="decline_cookies">Cookies ablehnen</a>--></p></div>
				';
		}
		elseif ( $lang == 'fr' ) {
			echo '
				<div id="cookie_consent"><p>Afin d&prime;am&eacute;liorer votre exp&eacute;rience sur ce site, nous utilisons des cookies <a class="accept_cookies">Accepter les cookies</a><a href="cookies" class="more_info">Plus d&prime;informations</a><!--<a class="decline_cookies">Refuser les cookies</a>--></p></div>
				';
		}
        elseif ( $lang == 'pt' ) {
            echo '
                <div id="cookie_consent"><p>A fim de lhe prestar um melhor servi&ccedil;o, este website usa cookies <a class="accept_cookies">Aceitar cookies</a><a href="cookies" class="more_info">mais informa&ccedil;&otilde;es</a><!--<a class="decline_cookies">Declinar cookies</a>--></p></div>
                ';
        }
		elseif ( $lang == 'xx' ) {
			echo '
				<div id="cookie_consent"><p>xxxx <a class="accept_cookies">xxxx</a><a href="cookies" class="more_info">xxxx</a><!--<a class="decline_cookies">xxxx</a>--></p></div>
				';
		}
		else {
			echo '
				<div id="cookie_consent"><p>To be of better service to you this website makes use of cookies <a class="accept_cookies">Accept cookies</a><a href="cookies" class="more_info">More info</a><!--<a class="decline_cookies">Decline cookies</a>--></p></div>
				';
		}

		// Strip http(s)://www from url
		// http://stackoverflow.com/questions/9364242/how-to-remove-http-www-and-slash-from-url-in-php
		$root_url = cmsms()->config['root_url'];
		$root_url = trim($root_url, '/');
		if (!preg_match('#^http(s)?://#', $root_url)) {
			$root_url = 'http://' . $root_url;
		}
		$urlParts = parse_url($root_url);
		$domain = preg_replace('/^www\./', '', $urlParts['host']);

		echo '
			<script type="text/javascript" src="lib/cookie_consent/jquery.cookie.js"></script>
			<script type="text/javascript">
				$(".decline_cookies").click(function () {
				// Hide message bar
				$("#cookie_consent").toggle("slow");
				// Hide page button
				$(".page_decline_cookies").toggle("slow");
				// Set cookie we can not use cookies at the website :o(
				$.cookie("'.$cookie_name.'", "no", { domain: "'.$domain.'", path: "/", expires: 365 });
				});
				$(".accept_cookies").click(function () {
				// Hide message bar
				$("#cookie_consent").toggle("slow");
				// Hide page button
				$(".page_decline_cookies").toggle("slow");
				// Set cookie we can use cookies at the website :o)
				$.cookie("'.$cookie_name.'", "yes", { domain: "'.$domain.'", path: "/", expires: 1095 });
				// Reload page
				location.reload();
				});
			</script>
			';
	return false;
	} // +++++ End body action +++++

/**
 * +++++ Decline_text +++++
 */
	if ( (isset($decline_text)) && ($decline_text != '') ) {
		echo '<a class="page_decline_cookies">'.$decline_text.'</a>';
		echo '
			<script type="text/javascript">
				$(".page_decline_cookies").click(function () {
				// Hide message bar
				$("#cookie_consent").toggle("slow");
				// Hide page button
				$(".page_decline_cookies").toggle("slow");
				// Set cookie we can not use cookies at the website :o(
				$.cookie("'.$cookie_name.'", "no", { domain: "'.$domain.'", path: "/", expires: 365 });
				});
			</script>
			';
		return false;
		} // +++++ End decline_text +++++
} // End Function


/**
 * Help text
 */
function smarty_cms_help_function_cookie_consent() {
	?>
	<h3>What does this do?</h3>
	<p>The plugin adds a cookie consent bar to your website. A visitor can accept or decline cookies. Depending on the visitors choice the - in example - analytics code can be added to the site or not...</p>
	<br />
	<p>The choice of the visitor is stored in a cookie named <em>cookie_consent</em> and will have the content 'yes' or 'no'. The 'yes' cookie will be stored for 3 years, the 'no' cookie for 1 year. When expired the question will be asked again...</p>
	<br />
	<p>The tag does not work for the CMSMS session cookie because it is a technical cookie and is needed for the operation of the website, therefor allowed. The cookie_consent tag does not delete existing (third party) cookies.</p>

	<h3>Dependencies</h3>
	<ul>
	<li>JQuery library</li>
	<li>JQuery cookie file</li>
	</ul>

	<h3>How do I use it?</h3>
	<p><b>JQuery cookie file</b></p>
	<p>Download the latest <em>jquery.cookie.js</em> file <a href="https://github.com/carhartl/jquery-cookie/archive/master.zip">HERE</a> (or download the ZIP file from <a href="https://github.com/carhartl/jquery-cookie" target="_blank">https://github.com/carhartl/jquery-cookie</a>)<br />
	Create a new folder /lib/cookie_consent/ and put the jquery.cookie.js file in it.</p>
	<br />
	<p><b>HTML Template</b></p>
	<p>Add to your template the tags with the matching parameters:</p>
	<br />
	<p style="background: #fff; border: #ccc solid 1px; padding: 10px;">
	<br />
	** Required JQuery call **<br />
	<br />
	&#123;cookie_consent action='head'&#125; <<< Adds default plugin stylesheet (optional)<br />
	&lt;/head&gt;<br />
	<br />
	&lt;body&gt;<br />
	&#123;cookie_consent action='body'&#125;<br />
	</p>
	<br />
	<p>You can use the <em>lang</em> parameter to change the language of the message.<br />
	Default is English, available languages are Dutch(nl), German(de), French(fr) and Portuguese(pt).</p>
	<br />
	<p style="background: #fff; border: #ccc solid 1px; padding: 10px;">
	&#123;cookie_consent action='body' lang='nl'&#125;<br />
	</p>
	<br />
	<p>The <em>xx</em> option in the plugin file can be changed/used for another language.</p>
	<br />
	<p><b>Message bar</b></p>
	<p>If you want you can change the content of the message in the plugin file (be carefull)<br />
	By default the decline cookies button is not shown. You can remove the quotes in the plugin file to let it show...</p>
	<br />
	<p><b>Don't forget to use in your template or page:</b></p>
	<p style="background: #fff; border: #ccc solid 1px; padding: 10px;">
	&#123;if $smarty.cookies.cookie_consent == 'yes'&#125;<br />
	** Analytics code **<br />
	&#123;/if&#125;
	</p>
	<br />
	<p style="background: #fff; border: #ccc solid 1px; padding: 10px;">
	&#123;if $smarty.cookies.cookie_consent == 'no'&#125;<br />
	&lt;p&gt;This function can not work without cookies&lt;/p&gt;<br />
	&#123;/if&#125;
	</p>
	<br />
	<p><b>Cookie policy page</b></p>
	<p>At the bottom of your cookie policy page (page alias is 'cookies') you can set this tag to display the decline cookies link:</p>
	<br />
	<p style="background: #fff; border: #ccc solid 1px; padding: 10px;">
	&#123;cookie_consent decline_text='Decline cookies'&#125;
	</p>

	<h3>What parameters does it take?</h3>
	<ul>
	<li><b>action=&quot;&quot;</b><br />
	Available values are:
	<ul>
	<li><b>head</b> (optional) - Adds default plugin stylesheet</li>
	<li><b>body</b> (required) - Adds HTML and JQuery to the page</li>
	</ul>
	</li>
	<li><b>lang=&quot;&quot;</b><br />
	The language of the message. (Available values are nl, de, fr, pt)<br />
	The option <em>xx</em> can be changed/used for another language...</li>
	<li><b>decline_text=&quot;&quot;</b><br />
	The text of the decline cookie link to display at your cookie policy page.</li>
	</ul>

	<?php
} // End Function


/**
 * About text
 */
function smarty_cms_about_function_cookie_consent() {
	?>
	<p><b>Plugin author: Rolf Tjassens</b> &lt;rolf [at] cmsmadesimple [dot] org&gt;</p>

	<p><b>Version:</b> 1.1</p>
	<p><b>Change History:</b></p>
	<p>29-09-2012 - Rolf Tjassens - Initial release</p>
	<p>16-11-2012 - Rolf Tjassens - Release 1.1<p>
		<ul>
			<li>Added Portoguese language</li>
			<li>Added reload page when accepting cookies</li>
		</ul>
	<?php
} // End Function
?>