<?php

function smarty_function_cms_api_request($params, &$smarty) {
    $gCms = CmsApp::get_instance();
    $config = $gCms->GetConfig();

    $LipitAPI = new LipitAPI();

    $url = isset($params['url']) ? $params['url'] : $config['url'];
    $api_id = isset($params['api_id']) ? $params['api_id'] : $config['api_id'];
    $api_key = isset($params['api_key']) ? $params['api_key'] : $config['api_key'];
    $api_token = isset($params['api_token']) ? $params['api_token'] : $config['api_token'];

    $captcha_verify = $params['captcha'];

    if ($captcha_verify != $_COOKIE['code_security']) {
        echo json_encode(array('code' => 1, 'msg' => $LipitAPI->Response(2)));
        exit();
    }

    $bookingcode = $params['bookingcode'];

    $LipitAPI->setAPI_ID($api_id);
    $LipitAPI->setAPI_KEY($api_key);
    $LipitAPI->setAPI_TOKEN($api_token);
    $LipitAPI->setAPI_KEY($api_key);
    $LipitAPI->setBookingCode($bookingcode);
    $LipitAPI->DoIT($url);
    $CodeResult = $LipitAPI->getCode();
    if ($CodeResult == 200) {
        #Done
        $msg = $LipitAPI->getTransactionCode();
        $msg = "<img src='" . $config["root_url"] . "/images/down.png'/> <a href='" . $config["root_url"] . "/uploads/images/_brochure/item" . $msg[0] . "/" . $msg[1] . "'>[Download] - Your information Booking !</a><br/><div class='brochureinfo'><small>".$msg[4]."</small></div>";
    } else {
        $msg = $LipitAPI->Response($CodeResult);
    }

    echo json_encode(array(
        'code' => $CodeResult,
        'msg' => $msg,
        'captcha' => $captcha_verify
    ));
}

?>