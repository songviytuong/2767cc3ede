<?php

function smarty_function_replace_key($params, &$smarty)
{
    $config = CmsApp::get_instance()->GetConfig();
    $cont = isset($params['cont']) ? $params['cont'] : "" ;
    $changeto = isset($params['changeto']) ? $params['changeto'] : "changeto" ;
    $bytext = isset($params['bytext']) ? $params['bytext'] : "bytext" ;
    
    require_once(SMARTY_PLUGINS_DIR . 'shared.mb_str_replace.php');
    
    $gCms = CmsApp::get_instance();
    $hm = $gCms->GetHierarchyManager();
    $db = $gCms->GetDb();
    
    $q = "  SELECT 
                mr.item_id as id,
                mr.url,
                mr.title,
                fvLink.link,
                fvText.text
            FROM cms_module_lisereplace_item mr
            LEFT OUTER JOIN (SELECT item_id,fielddef_id,value as link FROM ".CMS_DB_PREFIX."module_lisereplace_fieldval Where fielddef_id = (SELECT fielddef_id FROM `".CMS_DB_PREFIX."module_lisereplace_fielddef` Where `alias` = '$changeto')) as fvLink on (fvLink.item_id = mr.item_id)
            LEFT OUTER JOIN (SELECT item_id,fielddef_id,value as text FROM ".CMS_DB_PREFIX."module_lisereplace_fieldval Where fielddef_id = (SELECT fielddef_id FROM `".CMS_DB_PREFIX."module_lisereplace_fielddef` Where `alias` = '$bytext')) as fvText on (fvText.item_id = mr.item_id)
            WHERE mr.active = 1";
    
    $rSearch = $db->Execute( $q );
    $rReplace = $db->Execute( $q );
    if( !$rSearch && !$rReplace ) {
        // @todo: throw an exception here
        echo 'DB error: '. $db->ErrorMsg()."<br/>";
    }
    $arr = array();
    while ($rSearch && $row = $rSearch->FetchRow()){
        $arr["key"] .= $row['title'].",";
    }
    foreach($arr as $value){
        $search = $value;
    }
    
    $brr = array();
    while ($rReplace && $row = $rReplace->FetchRow()){
        $brr["change"] .= "<a href='".$row['link']."' title='".$row['text']."' data-source='".$row['title']."'>".$row['text']."</a>,";
    }
    foreach($brr as $value){
        $replace = $value;
    }
    $i = 0;
    if (Smarty::$_MBSTRING) {
        require_once(SMARTY_PLUGINS_DIR . 'shared.mb_str_replace.php');
        $ar = explode(",", substr($search, 0,-1));
        $br = explode(",", substr($replace, 0,-1));
        $cnt = count($ar);
        for($i=0; $i<$cnt;){
            $cont = smarty_mb_str_replace($ar[$i], $br[$i], $cont);
            $i++;
        }
    }
    $out = str_replace($ar[$i], $br[$i], $cont);
    if (isset($params['assign'])) {
        $smarty->assign(trim($params['assign']), $out);
        return;
    }
    return $out;
}

function smarty_cms_about_function_replace_key() {
?>
    <p>Author: Lee Peace songviytuong@gmail.com;</p>
    <p>Change History:</p>
    <ul>
        <li>Version: 1.0</li>
        <li>{replace_key cont=$mystring changeto=$aliaschangto bytext=$aliasbytext}</li>
    </ul>
<?php
}
?>