<?php
function smarty_function_cms_doit($params, &$smarty) {
    $gCms = CmsApp::get_instance();
    $config = $gCms->GetConfig();

    $adult = isset($params["adult"]) ? $params["adult"] : "";
    $child = isset($params["child"]) ? $params["child"] : "";
    $fullname = isset($params["fullname"]) ? $params["fullname"] : "";
    $phone = isset($params["phone"]) ? $params["phone"] : "";
    $email = isset($params["email"]) ? $params["email"] : "";
    $enquiry = isset($params["enquiry"]) ? $params["enquiry"] : "";

    $description = isset($params["description"]) ? $params["description"] : "";
    $security = isset($params["security"]) ? $params["security"] : "";

    $mytoken = substr(md5("LEEPEACE"),0,10);
    if ($mytoken != $security) {
        $msg = "Token has Failed: " . $mytoken;
    } else {
        $msg = $adult . $child . munge_string_to_url($fullname) . $phone . $email . $enquiry;

        #Save to Database
        #Fields: title_en, alias, position, active, create_time
        #item_id = lastid, fielddef_id = 3 (email), 

        $db = \cms_utils::get_db();

        $timeStamp = time();
        $timeNow = date("Y-m-d H:i:s", $timeStamp);

        // Préparation des valeurs
        $options['title_en'] = (empty($options['title_en'])) ? $fullname : $options['title_en'];
        $options['alias'] = (empty($options['alias'])) ? munge_string_to_url($fullname) : $options['alias'];
        $options['create_time'] = (empty($options['create_time'])) ? $timeNow : $options['create_time'];
        $options['modified_time'] = (empty($options['modified_time'])) ? $timeNow : $options['modified_time'];
        $options['active'] = (empty($options['active'])) ? 0 : $options['active'];
        $options['owner'] = (empty($options['owner'])) ? 1 : $options['owner'];

        // Préparation du SET
        $set = '';
        foreach ($options as $key => $val):
            $set .= $key . ' = ?';
            end($options);
            if ($key != key($options)):
                $set .= ', ';
            endif;
        endforeach;

        $query = "
        INSERT INTO " . CMS_DB_PREFIX . "module_lisebooking_item
        SET {$set}
        ";

        $result = $db->Execute($query, $options);

        if (!$result):
            $return->message = $db->ErrorMsg();
            return $return;
        endif;

//        $field_arr = array(
//            '3' => "Email",
//            '4' => "Booking",
//            '8' => "Phone"
//        );

        $last = $db->Insert_ID();

        $query_email = "INSERT INTO " . CMS_DB_PREFIX . "module_lisebooking_fieldval SET item_id = " . $last . ", fielddef_id = 3, value = '$email'";
        $db->Execute($query_email);

        $query_booking = "INSERT INTO " . CMS_DB_PREFIX . "module_lisebooking_fieldval SET item_id = " . $last . ", fielddef_id = 4, value = '$timeNow'";
        $db->Execute($query_booking);

        $query_phone = "INSERT INTO " . CMS_DB_PREFIX . "module_lisebooking_fieldval SET item_id = " . $last . ", fielddef_id = 8, value = '$phone'";
        $db->Execute($query_phone);
    }
    
    $token_checkout = substr(md5($last.$fullname),0,10);
    $url = cmsms()->config['root_url']."/check-out/?_token=$token_checkout&bid=$last";
    
    echo json_encode(array(
        'msg' => $msg,
        'url' => $url
    ));
}

?>