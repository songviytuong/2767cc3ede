<?php
#-------------------------------------------------------------------------
# Plugin: cache_remote_file
# Author: Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The plugin's homepage is: http://dev.cmsmadesimple.org/projects/cacheremotefile
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

function smarty_cms_function_cache_remote_file($params, &$smarty) {

	/* Default parameter settings */
	$source_url				= isset($params['url']) ? $params['url'] : '';
	$time					= isset($params['time']) ? $params['time'] : '1';
	$cache_directory		= 'tmp/cache_remote_file';
	$cache_file				= $cache_directory . '/crf_' . md5($source_url) . '.php';

	/* Language strings */
	$lang_cantcreatedir 	= '<p style="color:red">ERROR - cache_remote_file tag - Cannot create the required cache directory</p>';
	$lang_urlparammissing 	= '<p style="color:red">ERROR - cache_remote_file tag - The required URL parameter is missing in your tag</p>';
	$lang_sourceoffline 	= '<p style="color:red">ERROR - cache_remote_file tag - The source URL is offline or incorrect, can not create cache file</p>';

	/* Check if the cache directory is available. If not, create it */
	if (! is_dir($cache_directory) ) {
		@mkdir($cache_directory);
		
		if  ( isset($cache_directory) && !file_exists($cache_directory.'/index.html') ) {
			file_put_contents($cache_directory.'/index.html', '<!- Cache directory for cache_remote_file tag -->');
		}
		
		if ( isset($cache_directory) && is_dir($cache_directory) && file_exists($cache_directory.'/index.html')) {
			/* Put mention into the Admin log */
			audit('','cache_remote_file tag','Created cache directory: '. $cache_directory);
		} else {
			echo $lang_cantcreatedir;
			return false;
		}
	}
	
	/* Check for the URL parameter in the cache_remote_file tag */	
	if ( isset($source_url) && $source_url == '' ) {
		echo $lang_urlparammissing;
		return false;
	}
	
	/* Depending on the presence of the cache file do some actions... */
	if ( !file_exists($cache_file) ) {
		/* Cache file is not present */
		
		$file_content  = @file_get_contents($source_url);
		
		if ($file_content === false) {
			echo $lang_sourceoffline;
			return false;
		} else { 
			file_put_contents( $cache_file, $file_content );
		}
	
	} else {
		/* Cache file is present */
		
		$time_left = ( (@filemtime($cache_file)) - (time() - 3600 * $time) );
		
		if ( $time_left < 0 ) {
		
			$file_content  = @file_get_contents($source_url);
			
			if ($file_content === false) { 
				touch($cache_file);
			} else { 
				file_put_contents( $cache_file, $file_content );
			}
		}
	}

return @file_get_contents($cache_file);

} // End Function


/**
 * Help text
 */
function smarty_cms_help_function_cache_remote_file() {
	?>
	<h3>What does this do?</h3>
	<p>This plugin is an extended - but more reliable - version of the PHP file_get_contents function.<br />
	Change the content at one site and the others will follow when the buffer time has elapsed.</p>
	<br />
	<p>At first the tag will automatically create a new folder named (by default): /tmp/cache_remote_file.<br />
	Because the tag caches the content for one hour, it won't waste any bandwidth. The tag has several reliability checks, so even when the source website is offline for a long period the content in the client website remains available.<br />
	The clear cache function of CMSMS will not delete these cached files! The files can only be removed manually in the CMSMS File Manager or using FTP!</p>
	<br />
	<p>Some examples where to use this tag:<br />
		<ul>
			<li>Copyright or disclaimer texts</li>
			<li>Analytics code</li>
			<li>Navigation</li>
			<li>News headlines</li>
			<li>etc.</li>
		</ul>
	</p>


	<h3>Dependencies</h3>
	<p>This plugin requires write permission in the CMSMS /tmp folder. If necessary you can change the directory path in the top of the code of the file function.cache_remote_file.php at the default parameter settings. Change the value for the parameter <i>$cache_directory</i>.<br />
	In the server settings "allow_url_fopen" should be allowed.</p>


	<h3>How do I use it?</h3>
	<p>Create at the source website in the Design Manager module a Core::Page template (named i.e. blank) with <b>only</b> the <code>&#123;content&#125;</code> tag in it.<br />
	Put the text or code you want to share between websites in a hidden page (this means not in the menu) with the blank template attached.<br />
	Perhaps it is necessary to switch off the WYSIWYG editor in the page options tab.<br />
	Opening the page will only show the text or code with no styling.</p>
	<p>You can retrieve and cache this text or code from the source website just by adding in your page or template:<br />
	<code>&#123;cache_remote_file url='http://www.website.com/index.php?page=page_alias'&#125;</code> Don't use a <i>pretty url</i> in the url parameter!</p>
	<br />
	<p><b>Note:</b> All image or page paths in the content need to have the masters root url in it! Otherwise you will get an Error 404.</p>


	<h3>What parameters does it take?</h3>
	<p><b>url=''</b> (Required)<br />
	The url of the source file/page at the source website.</p>
	<p><b>time=''</b><br />
	Number of <b>hours</b> the remote content will be stored in a local cache file. When this period has elapsed the tag will try to renew the cache file with the content present in the source file. Default value is 1.</p>
	<br />

	<?php
} //End Function


/**
 * About text
 */
function smarty_cms_about_function_cache_remote_file() {
	?>
	<p><b>Plugin author: Rolf Tjassens</b> &lt;rolf [at] cmsmadesimple [dot] org&gt;</p>

	<p><b>Version:</b> 1.1</p>
	<br />
	<p><b>Change History:</b></p>
	<p><b>Version:</b> 1.1</p>
	<p>14-02-2016 - Improved source file not available function</p>
	<p><b>Version:</b> 1.0</p>
	<p>03-08-2012 - Initial release</p>
	<?php
} //End Function
?>