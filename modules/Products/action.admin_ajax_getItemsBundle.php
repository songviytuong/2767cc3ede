<?php

if (!isset($gCms))
    exit;
if (!$this->CheckPermission('Modify Products'))
    return;

global $mle, $mleblock;
$theme = cms_utils::get_theme_object();

$sku = trim($_REQUEST["term"]);
$pid = trim($_REQUEST["pid"]);

$db = cmsms()->GetDb();
$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_products WHERE sku = ?';
$row = $db->GetRow($query, array($sku));
$id = $row['id'];
$product_name = ($row['product_name' . $mleblock]) ? $row['product_name' . $mleblock] : $row['product_name'];
$price = $row['price'];
$create_date = $row['create_date'];
$modified_date = $row['modified_date'];
$edit_link = $this->CreateLink('m1_', 'editproduct', $returnid, $theme->DisplayImage('icons/system/edit.gif', $this->ModLang('edit'), '', '', 'systemicon'), array(
    'compid' => $row["id"]
        ));
$delete_link = $this->CreateLink('m1_', 'admin_delete_bundle', $returnid, $theme->DisplayImage('icons/system/delete.gif', $this->ModLang('delete'), '', '', 'systemicon'), array(
    'mode' => 'delete',
    'pid' => $pid,
    'item_id' => $row["id"]
));

$tmpl = <<<EOT
"<tr id="item_$id" class="row2" style="cursor: auto;">
            <td style="cursor: auto;">$id</td>
            <td style="cursor: auto;">$sku</td>
            <td style="cursor: auto;">$product_name</td>
            <td style="cursor: auto;">$price</td>
            <td style="cursor: auto;">$create_date</td>
            <td style="cursor: auto;">$modified_date</td>
            <td style="cursor: auto;"></td>
            <td style="cursor: auto;">$edit_link</td>
            <td class="init-ajax-delete" style="cursor: auto;">$delete_link</td>
        </tr>"
EOT;

$existing = 'SELECT id FROM ' . cms_db_prefix() . 'module_products_bundle WHERE product_id =? AND item_id = ? ';
$row_existing = $db->GetRow($existing, array($pid,$id));

$rest = array();

if (!$row_existing["id"] && $id) {
    $insert = 'INSERT INTO ' . cms_db_prefix() . 'module_products_bundle SET product_id =?, item_id = ?';
    $db->Execute($insert, array($pid, $id));

    $rest["msg"] = "Insert";
    $rest["data"] = $tmpl;
} else {
    $rest["msg"] = "Not Found or Exist";
}

header("Content-type:application/json; charset=utf-8");
$handlers = ob_list_handlers(); 
for ($cnt = 0; $cnt < sizeof($handlers); $cnt++) { ob_end_clean(); }

// give it back via ajax
\cge_utils::send_ajax_and_exit($rest);

#
# EOF
#
?>