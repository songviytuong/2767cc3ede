<?php

if( !isset($gCms) ) exit;
if (!$this->CheckPermission('Modify Products') ) return;

//
// Setup
//

$pagelimits = array('2'=>2,'5'=>5,'25'=>25,'100'=>100,'500'=>500);
$sortitems = array();
$sortitems[$this->Lang('productname')] = 'product_name';
$sortitems[$this->Lang('price')] = 'price';
$sortitems[$this->Lang('weight')] = 'weight';
$sortitems[$this->Lang('sku')] = 'sku';
$sortitems[$this->Lang('createddate')] = 'p.create_date';
$sortitems[$this->Lang('modifieddate')] = 'p.modified_date';
$sortorders = array();
$sortorders[$this->Lang('ascending')] = 'asc';
$sortorders[$this->Lang('descending')] = 'desc';
$uid = get_userid(false);
$bare_filter = array('productname'=>'','sku'=>'','hierarchy'=>'','children'=>0,'translates'=>-1,'seo_include'=>-1,'pagelimit'=>25,'sortby'=>'p.position',
		     'sortorder'=>'asc','categories'=>array(),'excludecats'=>0,'status'=>'published','custom_fields'=>array());
$filter = $bare_filter;
$tmp = cms_userprefs::get('products_filter');
if( $tmp ) {
    $tmp = unserialize($tmp);
    $filter = array_merge($bare_filter,$tmp);
}

// $hierarchy = '';
// $children  = 0;
// $pagelimit = 25;
// $sortby    = 'create_date';
// $sortorder = 'desc';
$pagenumber = 1;
$fields_viewable = array();
$field_names = array();

if( isset($params['reset']) ) {
    $filter = $bare_filter;
    cms_userprefs::remove('products_filter');
    unset($_SESSION['products_sel_pagenum']);
}

//
// Get preferences
//

//
// Handle Get parameters
//
if( isset($_SESSION['products_sel_pagenum']) ) $pagenumber = (int)$_SESSION['products_sel_pagenum'];
if( isset($params['pagenumber']) ) $pagenumber = (int)$params['pagenumber'];
$pagenumber = max(1,$pagenumber);
if( $pagenumber > 1 ) $_SESSION['products_sel_pagenum'] = $pagenumber;

//
// Handle form submit
//
if( isset($params['submit']) ) {
    $filter['productname'] = trim($params['input_productname']);
    $filter['sku'] = trim($params['input_sku']);
    if( $filter['productname'] ) $filter['sku'] = ''; // one, or the other.. but not both.
    $filter['hierarchy'] = trim($params['input_hierarchy']);
    $filter['children'] = (int)$params['input_children'];
    $filter['translates'] = (int)$params['input_translates'];
    $filter['seo_include'] = (int)$params['input_seo_include'];
    $filter['pagelimit'] = (int)$params['input_pagelimit'];
    $filter['pagelimit'] = max(1,min(500,$filter['pagelimit']));
    $filter['sortby'] = trim($params['input_sortby']);
    $filter['sortorder'] = trim($params['input_sortorder']);
    $filter['status'] = trim($params['input_status']);
    $filter['custom_fields'] = array();
    if( isset($params['custom_fields']) ) $filter['custom_fields'] = $params['custom_fields'];
    $filter['categories'] = array();
    if( isset($params['categories']) ) $filter['categories'] = $params['categories'];
    $filter['excludecats'] = (int)cge_utils::get_param($params,'input_excludecats');

    cms_userprefs::set('products_filter',serialize($filter));
    unset($_SESSION['products_sel_pagenum']);
    $pagenumber = 1;
}

#Start MLE
global $hls, $hl, $mleblock;
$thisURL = $_SERVER['SCRIPT_NAME'] . '?';
foreach ($_GET as $key => $val)
    if ('hl' != $key)
        $thisURL .= $key . '=' . $val . '&amp;';
if (isset($hls)) {
    $langList = ' &nbsp; &nbsp; ';
    foreach ($hls as $key => $mle) {
        $langList .= ($hl == $key) ? $mle['flag'] . ' ' : '<a href="' . $thisURL . 'hl=' . $key . '">' . $mle['flag'] . '</a> ';
    }
}
$smarty->assign('langList', $langList);
#End MLE

//
// Begin the form
//
$tpl = $this->CreateSmartyTemplate('productlist.tpl');
$tmp = $this->GetCategories();
if( is_array($tmp) && count($tmp) ) {
    $category_list = array();
    foreach( $tmp as $one ) {
        $category_list[$one->id] = $one->name;
    }
    $tpl->assign('category_list',$category_list);
}

$fielddefs = product_utils::get_fielddefs();
foreach( $fielddefs as $onedef ) {
    switch( $onedef->type ) {
    case 'textbox':
        $sortitems[$onedef->prompt] = 'F::'.$onedef->id;
        $sortitems[$onedef->prompt.' ('.$this->Lang('numeric').')'] = 'F::'.$onedef->id.'::N';
        break;
    case 'dropdown':
        $sortitems[$onedef->prompt] = 'F::'.$onedef->id;
        break;
    }
}

$all_fields = product_ops::get_fields();
if( is_array($all_fields) ) {
    for( $i = 0; $i < count($all_fields); $i++ ) {
        switch( $all_fields[$i]['type'] ) {
        case 'textarea':
            break;
        default:
            $fields_viewable[$all_fields[$i]['id']] = $all_fields[$i]['prompt'];
            $field_names[$all_fields[$i]['id']] = $all_fields[$i]['name'];
            break;
        }
    }
    if( count($fields_viewable) ) {
        // now trim down the custom fields
        // to make sure that something hasn't been deleted.
        $tmp = array();
        foreach( $filter['custom_fields'] as $fid ) {
            if( in_array($fid,array_keys($fields_viewable)) ) $tmp[] = $fid;
        }
        $filter['custom_fields'] = $tmp;
    }
    else {
        $filter['custom_fields'] = array();
    }
}
$all_fields = cge_array::to_hash($all_fields,'name');

$filterinuse = ($filter == $bare_filter)?FALSE:TRUE;
$tpl->assign('formstart',$this->CGCreateFormStart($id,'defaultadmin'));
$tpl->assign('formend',$this->CreateFormEnd());
if( count($fields_viewable) ) {
    $tpl->assign('fields_viewable',$fields_viewable);
    $tpl->assign('field_names',$field_names);
}
$tpl->assign('filter',$filter);
$tpl->assign('input_hierarchy',$this->CreateHierarchyDropdown($id,'input_hierarchy',$filter['hierarchy']));
$tpl->assign('input_children',$this->CreateInputYesNoDropdown($id,'input_children',$filter['children']));
$tpl->assign('input_sortby',$this->CreateInputDropdown($id,'input_sortby',$sortitems,-1,$filter['sortby']));
$tpl->assign('input_sortorder',$this->CreateInputDropdown($id,'input_sortorder',$sortorders,-1,$filter['sortorder']));
$tpl->assign('input_pagelimit',$this->CreateInputDropdown($id,'input_pagelimit',$pagelimits,-1,$filter['pagelimit']));
$tpl->assign('input_excludecats',$this->CreateInputYesNoDropdown($id,'input_excludecats',$filter['excludecats']));

$tpl->assign('input_translates',$this->CreateInputYesNoDropdown($id,'input_translates',$filter['translates'],'',1));
$tpl->assign('input_seo_include',$this->CreateInputYesNoDropdown($id,'input_seo_include',$filter['seo_include'],'',1));

$statuses = array($this->Lang('published')=>'published',
		  $this->Lang('draft')=>'draft',
		  $this->Lang('disabled')=>'disabled');
$smarty->assign('statustext',$this->Lang('status'));
$smarty->assign('input_status', $this->CreateInputDropdown($id,'input_status',$statuses,-1,$filter['status']));
//
// Build the query
//
$fields = array();
$fields[] = 'DISTINCT p.*';
$prefix1 = "SELECT <FIELDS> FROM ".cms_db_prefix()."module_products p ";
$prefix2 = "SELECT count(DISTINCT p.id) AS count FROM ".cms_db_prefix().'module_products p ';
$where = array();
$joins = array();
$qparms = array();

if( $filter['productname'] ) {
    $tmp = $filter['productname'];
    $tmp = str_replace('*','%',$tmp);
    if( strpos('%',$tmp) === FALSE ) $tmp = $tmp . '%';
    $tmp = str_replace('%%','%',$tmp);
    $where[] = 'p.product_name LIKE ?';
    $qparms[] = $tmp;
}
else if( $filter['sku'] ) {
    $tmp = $filter['sku'];
    $tmp = str_replace('*','%',$tmp);
    if( strpos('%',$tmp) === FALSE ) $tmp = $tmp . '%';
    $tmp = str_replace('%%','%',$tmp);
    $joins[] = 'LEFT JOIN '.cms_db_prefix().'module_products_attribs PA ON p.id = PA.product_id';
    $where[] = '(p.sku LIKE ? OR PA.sku LIKE ?)';
    $qparms[] = $tmp;
    $qparms[] = $tmp;
}

if( $filter['status'] ) {
    $tmp = $filter['status'];
    $where[] = 'p.status = ?';
    $qparms[] = $tmp;
}

if( $filter['translates'] == 1) {
    $where[] = 'p.product_name_vi IS NOT NULL';
} else {
    $where[] = 'p.product_name_vi IS NULL';
}

if( $filter['seo_include'] == 1) {
    $joins[] = 'LEFT JOIN '.cms_db_prefix().'module_liseseo_item S ON p.sku = S.title_en';
    $joins[] = 'LEFT JOIN '.cms_db_prefix().'module_liseseo_fieldval AS V ON S.item_id = V.item_id';
    $where[] = 'S.title_en IS NOT NULL';
    $where[] = 'V.fielddef_id = 1 AND V.`value` IS NOT NULL';
    
} elseif( $filter['seo_include'] == 0 ){
    $joins[] = 'LEFT JOIN '.cms_db_prefix().'module_liseseo_item S ON p.sku = S.title_en';
    $joins[] = 'LEFT JOIN '.cms_db_prefix().'module_liseseo_fieldval AS V ON S.item_id = V.item_id';
    $where[] = 'S.alias IS NULL';
}

if( count($filter['custom_fields']) && count($fields_viewable) ) {
    for( $j = 0; $j < count($filter['custom_fields']); $j++ ) {
        $fid = $filter['custom_fields'][$j];
        $fields[] = "Fv{$j}.value AS 'Fld__{$field_names[$fid]}'";
        $joins[] = 'LEFT OUTER JOIN '.cms_db_prefix()."module_products_fieldvals Fv{$j} ON Fv{$j}.product_id = p.id AND Fv{$j}.fielddef_id = ?";
        $qparms[] = $filter['custom_fields'][$j];
    }
}

if( !empty($filter['hierarchy']) ) {
    $tquery = 'SELECT long_name FROM '.cms_db_prefix().'module_products_hierarchy WHERE id = ?';
    $long_name = $db->GetOne($tquery,array($filter['hierarchy']));

    $joins[] = 'LEFT JOIN '.cms_db_prefix().'module_products_prodtohier ph ON ph.product_id = p.id';
    $joins[] = 'LEFT JOIN '.cms_db_prefix().'module_products_hierarchy h ON h.id = ph.hierarchy_id';
    $where[] = 'h.long_name LIKE ?';
    if( $filter['children'] ) {
        $qparms[] = $long_name.'%';
    }
    else {
        $qparms[] = $long_name;
    }
}

if( count($filter['categories']) > 0 ) {
    $joins[] = 'LEFT OUTER JOIN '.cms_db_prefix().'module_products_product_categories pc ON p.id = pc.product_id';
    if( $filter['excludecats'] ) {
        $where[] = 'COALESCE(pc.category_id,-1) NOT IN ('.implode(',',$filter['categories']).')';
    }
    else {
        $where[] = 'pc.category_id IN ('.implode(',',$filter['categories']).')';
    }
}

// handle funky custom field sort orders
if( startswith($filter['sortby'],'F::') ) {
    list($junk,$fid,$numeric) = explode('::',$filter['sortby'],3);
    $joins[] = 'LEFT JOIN '.cms_db_prefix().'module_products_fieldvals fv ON fv.product_id = p.id';
    $where[] = 'fv.fielddef_id = ?';
    $qparms[] = $fid;
    if( $numeric ) {
        $order = " ORDER BY CAST(fv.value AS DECIMAL(12,3)) {$filter['sortorder']}";
    } else {
        $order = " ORDER BY fv.value {$filter['sortorder']}";
    }
}
else {
    $order = " ORDER BY {$filter['sortby']} {$filter['sortorder']}";
}

$str = implode(' ',$joins);
if( count($where) ) {
    $str .= ' WHERE ' . implode(' AND ',$where) . $order;
}
else {
    $str .= $order;
}

$query1 = str_replace('<FIELDS>',implode(',',$fields),$prefix1) . $str;
$query2 = $prefix2 . $str;

//
// Setup start element, and count pages
//
$totalcount = $db->GetOne($query2,$qparms);
$pagecount = (int)($totalcount/$filter['pagelimit']);
if( ($totalcount % $filter['pagelimit']) != 0 ) $pagecount++;
$startelement = ($pagenumber-1) * $filter['pagelimit'];

//
// Begin the output
//
$tpl->assign('totalproducts',$db->GetOne('SELECT COUNT(id) FROM '.cms_db_prefix().'module_products'));
$tpl->assign('pagenumber',$pagenumber);
$tpl->assign('pagecount',$pagecount);
$tpl->assign('totalrows',$totalcount);
if( $pagenumber > 1 ) {
    $parms = array('pagenumber'=>1);
    $tpl->assign('firstpage_url', $this->CreateURL($id,'defaultadmin','',$parms));
    $parms = array('pagenumber'=>$pagenumber -1);
    $tpl->assign('prevpage_url', $this->CreateURL($id,'defaultadmin','',$parms));
}
if( $pagenumber < $pagecount ) {
    $parms = array('pagenumber'=>$pagenumber + 1);
    $tpl->assign('nextpage_url', $this->CreateURL($id,'defaultadmin','',$parms));
    $parms = array('pagenumber'=>$pagecount);
    $tpl->assign('lastpage_url', $this->CreateURL($id,'defaultadmin','',$parms));
}
$entryarray = array();
$dbresult = $db->SelectLimit($query1,$filter['pagelimit'],$startelement,$qparms);
$theme = cms_utils::get_theme_object();

$name = "Exchange Custom";
$priceExchange = $db->GetRow('SELECT value FROM ' . cms_db_prefix() . 'module_customgs WHERE name=?', array($name));
$priceExchange = ($priceExchange) ? $priceExchange["value"] : 1;

$discount = "Discount";
$priceDiscount = $db->GetRow('SELECT value FROM ' . cms_db_prefix() . 'module_customgs WHERE name=?', array($discount));
$priceDiscount = ($priceDiscount) ? $priceDiscount["value"] : 1;

while ($dbresult && $row = $dbresult->FetchRow()) {
    foreach( $row as $key => $value ) {
        if( startswith($key,'Fld__') ) {
            unset($row[$key]);
            $key = substr($key,strlen('Fld__'));
            $row[$key] = $value;

            $tmp = @unserialize($value);
            if( $tmp !== FALSE ) {
                $row[$key] = product_utils::get_displayable_fieldval($key,$tmp);
            }
            else {
                $row[$key] = product_utils::get_displayable_fieldval($key,$value);
            }
        }
    }
    $row['product_name'] = !empty($row['product_name'.$mleblock]) ? $row['product_name'.$mleblock] : $row['product_name'];
            
    $pr = ($priceExchange != 1 && $mleblock != '_en') ?  $row['price'] * $priceExchange * $priceDiscount/100 : $row['price'];
    $row['price'] = number_format($pr);
    $row['position'] = $row['position'];
    $row['edit_url'] = $this->CreateURL($id,'editproduct',$returnid, array('compid'=>$row['id']));
    $row['editlink'] = $this->CreateLink($id, 'editproduct', $returnid,
                                         $theme->DisplayImage('icons/system/edit.gif', $this->Lang('edit'),'','','systemicon'),
                                         array('compid'=>$row['id']));
    $row['copylink'] = $this->CreateLink($id, 'admin_copyproduct', $returnid,
                                         $theme->DisplayImage('icons/system/copy.gif', $this->Lang('copy_product'),'','','systemicon'), array('compid'=>$row['id']));
    $row['deletelink'] = $this->CreateLink($id, 'deleteproduct', $returnid,
                                           $theme->DisplayImage('icons/system/delete.gif', $this->Lang('delete'),'','','systemicon'), array('compid'=>$row['id']),
                                           $this->Lang('areyousure_deleteproduct'));
    
    $row["source"] = $row['source'];
    
    $gSEO = $db->GetRow('SELECT item_id FROM ' . cms_db_prefix() . 'module_liseseo_item WHERE title="'.$row['sku'].'"');
    $urlext='?'.CMS_SECURE_PARAM_NAME.'='.$_SESSION[CMS_USER_KEY];
    $seo_id = ($gSEO) ? $gSEO["item_id"] : "";
    
    if (!$seo_id) {
        $row["ajax"] = true;
        $row["importseo"] = $this->CreateLink($id, 'admin_ajax_importseo', $returnid, $theme->DisplayImage('icons/system/back.gif', $this->Lang('import_seo'), '', '', 'systemicon'), array('compid' => $row['id']));
    } else {
        $module = \cms_utils::get_module('LISESEO');
        $gTitle = $db->GetRow('SELECT value FROM ' . cms_db_prefix() . 'module_liseseo_fieldval WHERE fielddef_id in (1,2,3,4) and item_id =' . $seo_id);
        $seoValue = ($gTitle) ? $gTitle["value"] : "";
        if ($seoValue) {
            $row["importseo"] = $module->CreateLink($id, 'admin_edititem', $returnid, $theme->DisplayImage('icons/system/accept.gif', $this->Lang('import_seo'), '', '', 'systemicon'), array('item_id' => $seo_id, 'target' => '_blank'));
        } else {
            $row["importseo"] = $module->CreateLink($id, 'admin_edititem', $returnid, $theme->DisplayImage('icons/system/run.gif', $this->Lang('import_seo'), '', '', 'systemicon'), array('item_id' => $seo_id, 'target' => '_blank'));
        }
    }

    $help = '&nbsp;'.cms_admin_utils::get_help_tag(array('key'=>'help_content_type','title'=>"Notes"));
    
    $uquery = "SELECT username FROM ".cms_db_prefix()."users WHERE user_id = ?";
    $uname = $db->GetOne($uquery,array(($row['owner']) ? $row['owner'] : $uid));
    $row['owner'] = $uname;
    $entryarray[] = $row;
    
}
if($this->CheckPermission('Products Remove')){
    $tpl->assign('mode', 'master');
}
$tpl->assign('items', $entryarray);
$tpl->assign('itemcount', count($entryarray));

$tpl->assign('importlink',
             $this->CreateImageLink($id,'importproducts',$returnid,$this->Lang('import_from_csv'), 'icons/system/import.gif',array(),'','',false));

$tpl->assign('exportlink',
             $this->CreateImageLink($id,'exportcsv',$returnid,$this->Lang('export_to_csv'), 'icons/system/export.gif',array(),'','',false));
$tpl->assign('addlink',
             $this->CreateLink($id, 'addproduct', $returnid,
                               $theme->DisplayImage('icons/system/newfolder.gif', $this->Lang('addproduct'),'','','systemicon'),
                               array('hierarchy'=>$filter['hierarchy']), '', false, false, '') .' '.
             $this->CreateLink($id, 'addproduct', $returnid, $this->Lang('addproduct'),
                               array('hierarchy'=>$filter['hierarchy']), '', false, false, 'class="pageoptions"'));

$tpl->assign('username', $uname);
$tpl->assign('idtext',$this->Lang('id'));
$tpl->assign('producttext', $this->Lang('product'));
$tpl->assign('pricetext', $this->Lang('price'));
$tpl->assign('weight_units',product_ops::get_weight_units());
$tpl->assign('weighttext',$this->Lang('weight'));
$tpl->assign('formstart2',$this->CreateFormStart($id,'admin_bulkaction',$returnid));
$tpl->assign('formend2',$this->CreateFormEnd());
$tpl->assign('filterinuse',$filterinuse);
$bulkactions = array();
$bulkactions['delete'] = $this->Lang('delete');
$bulkactions['setdraft'] = $this->Lang('setdraft');
$bulkactions['setdisabled'] = $this->Lang('setdisabled');
$bulkactions['setpublished'] = $this->Lang('setpublished');
if( product_utils::have_quantity_field() ) {
    $bulkactions['editquantity'] = $this->Lang('editquantity');
}
$tpl->assign('bulkactions',$bulkactions);

// Display template
$tpl->display();

// EOF
?>
