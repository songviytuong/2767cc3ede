{*
#CMS - CMS Made Simple
#(c)2004-6 by Ted Kulp (ted@cmsmadesimple.org)
#This project's homepage is: http://cmsmadesimple.org
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#$Id$
*}

{if empty($hidden)}
<h3>{$mod->Lang('addcategory')}</h3>
{else}
<h3>{$mod->Lang('edit_category')}</h3>
{/if}

{$startform}
	<div class="pageoverflow">
		<p class="pagetext">*{$mod->Lang('name')}:</p>
		<p class="pageinput">
		  <input name="{$actionid}name" value="{$name}" size="80" maxlength="255"/>
		</p>
	</div>
	<div class="pageoverflow">
		<p class="pageinput">
		  <input type="submit" name="{$actionid}submit" value="{$mod->Lang('submit')}"/>
		  <input type="submit" name="{$actionid}cancel" value="{$mod->Lang('cancel')}"/>
		</p>
	</div>
{$endform}
