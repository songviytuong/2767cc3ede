<script type="text/javascript">
    $(document).ready(function () {
        initAjaxColor();
    {if isset($compid)}
        $(document).on('click', '#editoptions', function () {
            if (!confirm('{$mod->Lang('confirm_editoptions')}'))
                return false;
            window.location = '{module_action_link module=Products action='admin_edit_attribs' prodid=$compid jsfriendly=1}';
            return false;
        });

        $(document).on('click', '#timelines', function () {
            if (!confirm('{$mod->Lang('confirm_editoptions')}'))
                return false;
            window.location = '{module_action_link module=Products action='admin_edit_timelines' prodid=$compid jsfriendly=1}';
            return false;
        });
    {/if}

        if ($.fancybox)
            $('.fancybox').fancybox();
        
        $('span.cdautocomplete > input').autocomplete({
            minLength: 2,
            source: function (request, response) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '{module_action_link module='CompanyDirectory' action='ajax_selcompany' jsfriendly=1 pagelimit=20}&showtemplate=false',
                    data: {
                        addid: 1,
                        term: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            }
        });
        
        $('.productcolor[data-productcolor]').one("click",function(){
            if (confirm('Are you sure?')) {
                addcolor($(this));
            }
        });
        
        function initAjaxColor(){
            $('.removecolor').click(function(e){
                e.preventDefault();
                removecolor($(this));
            });
        }
        
        function addcolor(obj){
            var id = obj.attr('data-productcolor');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '{module_action_link module='Products' action='admin_ajax_addColor' jsfriendly=1}&showtemplate=false',
                data: {
                    pid: {$compid|default:0},
                    cid: id
                },
                beforeSend: function(){
                    obj.empty().append('<div class="ajax-loading ajax-loader-type-16"></div>');
                },
                success: function (res) {
                    $('.color'+id).remove();
                    $('.colors_active').append(res['data']);
                    initAjaxColor();
                }
            });
        }
        
        function removecolor(obj){
            if (confirm('Are you sure?')) {
                var id = obj.parent().attr('data-productcolor');
                $('.activecolor.color'+id).remove();
            }
        }
        
        $('div.bundle > input').autocomplete({
            minLength: 2,
            source: function (request, response) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '{module_action_link module='Products' action='admin_ajax_getItemsBundle' jsfriendly=1}&showtemplate=false',
                    data: {
                        pid: {$compid|default:0},
                        term: request.term
                    },
                    beforeSend: function(){
                        $('span.result').text('Loading...');
                    },
                    success: function (res) {
                        window.setTimeout(function () {
                            $('span.result').text(res['msg']);
                            $('tbody').append(res['data']);
                            if(res['msg'] == 'Insert'){
                                $('input[name=bundle]').val('');
                            }
                            initAjax();
                            initAjaxEvents();
                        },500);
                        
                    }
                });
            }
        });
        
        setTimeout(function(){
            $('#product_price').blur();
        },100);
        
        $('#product_price').blur(function () {
            $.ajax({
                url: '{module_action_url action='admin_ajax_getcurrency' forajax=1}',
                method: 'POST',
                data: {
                  '{$actionid}price': $('#product_price').val()
                },
                success: function(res) {
                   if (res) {
                        $('.VND').text(res);
                    } else {
                        $('.VND').text('= ? VND');
                    }
            }
            });
        });
        
        $('#product_name').blur(function () {
            $.ajax({
                url: '{module_action_url action='admin_ajax_exists' forajax=1}',
                method: 'POST',
                type: 'JSON',
                data: {
                  '{$actionid}product_name': $('#product_name').val(),
                  'aAction':'product_name'
                },
                success: function(res) {
                    if (res) {
                        $('.exists_product').html(res['msg']);
                    }
                }
            });
        });
        
        $('#sku').blur(function () {
            $.ajax({
                url: '{module_action_url action='admin_ajax_exists' forajax=1}',
                method: 'POST',
                type: 'JSON',
                data: {
                  '{$actionid}sku': $('#sku').val(),
                  'aAction':'sku'
                },
                success: function(res) {
                    if (res) {
                        $('.exists_sku').html(res['msg']);
                        if (res['url']) {
                            window.location = res['url'];
                            return false;
                        }
                    }
                }
            });
        });

    });
</script>

{$langList}

{$startform}
{if isset($compid)}
<h3>{$mod->Lang('edit_product')}&nbsp;({($sku) ? $sku : $compid})</h3>
{else}
<h3>{$mod->Lang('addproduct')}</h3>
{/if}

<div class="pageoverflow">
  <p class="pageinput">{$hidden}
    <input type="submit" name="{$actionid}submit" value="{$mod->Lang('submit')}"/>
    {if $allowedtravel == 1}<input type="submit" id="timelines" value="{$mod->Lang('timelines')}"/>{/if}
    <input type="submit" name="{$actionid}apply" value="{$mod->Lang('apply')}"/>
    <input type="submit" name="{$actionid}cancel" value="{$mod->Lang('cancel')}"/>
    {$seo}
  </p>
  {$listimg}
  
</div>

{cge_start_tabs}
{cge_tabheader name='main' label=$mod->Lang('product_info')}
{if $customfieldscount gt 0}{cge_tabheader name='fields' label=$mod->Lang('fields')}{/if}
{cge_tabheader name='advanced' label=$mod->Lang('advanced')}
{cge_tabheader name='bundle' label=$mod->Lang('bundle_tabs')}
{cge_tabheader name='colors' label=$mod->Lang('colors_tabs')}

{cge_tabcontent_start name='main'}
<div class="c_full">
    <div class="grid_7">
        {capture "val"}{$uploads_path}/_products/product_{$compid}/{$sku}-1-160x165.jpg{/capture}
        {assign var="printad" value={$smarty.capture.val}} 
        {if file_exists($printad)}
            <div class="c_full">
                <div class="grid_2 text-right">&nbsp;</div>
                <div class="grid_10">
                    <img src="{$uploads_url}/_products/product_{$compid}/{$sku}-1-160x165.jpg" alt=""/>
                </div>
            </div>
        {/if}

        <div class="c_full">
            <div class="grid_2 text-right">{$mod->Lang('name')}:</div>
            <div class="grid_10">
                <input type="text" id="product_name" name="{$actionid}product_name" size="50" value="{$product_name}" maxlength="255"/>
                <small class="exists_product"></small>
            </div>
        </div>
        <div class="c_full">
            <div class="grid_2 text-right">{$mod->Lang('price')}:</div>
            <div class="grid_10">
                <input type="text" id="product_price" name="{$actionid}price" value="{$price}" size="10" maxlength="12"/> {$currency_symbol} <span class="VND"></span>
            </div>
        </div>

        <div class="c_full">
            <div class="grid_2 text-right">{$detailstext}:</div>
            <div class="grid_10">{$inputdetails}</div>
        </div>
        {if $allowedtravel == 1}
            <br/>
            {* Package Includes *}
            <div class="c_full">
                <div class="grid_2 text-right">{$pack_includes_text}:</div>
                <div class="grid_10">{$pack_includes}</div>
            </div>

            {* Package Excludes *}
            <div class="c_full">
                <div class="grid_2 text-right">{$pack_excludes_text}:</div>
                <div class="grid_10">{$pack_excludes}</div>
            </div>
        {/if}
    </div>

  <div class="grid_5">{* right column *}
      <div class="c_full">
    <p class="grid_2 text-right">{if $mod->GetPreference('skurequired')}*{/if}{$mod->Lang('sku')}:</p>
    <p class="grid_10">
        {if $mode == "editor"}
            {assign var="readonly" value="readonly"}
        {/if}
      <input type="text" id="sku" name="{$actionid}sku" value="{$sku}" maxlength="25" {$readonly|default:''}/>
      <small class="exists_sku"></small>
      </p>
  </div>
    

    {* the hierarchy stuff *}
    {if count($hierarchy_items)}
      <div class="c_full">
        <p class="grid_2 text-right">{$mod->Lang('hierarchy_position')}:</p>
        <p class="grid_10">
            {if $mode == "editor"}
                {assign var="disabled" value="disabled"}
            {/if}
            <select name="{$actionid}hierarchy" id="hierarchy">
                {html_options options=array_flip($hierarchy_items) selected=$hierarchy_pos}
            </select>
	</p>
      </div>
      <div class="clearb"></div>
    {/if}
    
    {* the hierarchy stuff *}
    {if count($collection_items)}
      <div class="c_full">
        <p class="grid_2 text-right">{$mod->Lang('collection_position')}:</p>
        <p class="grid_10">
            <select name="{$actionid}collection" id="collection">
                {html_options options=array_flip($collection_items) selected=$collection_pos}
            </select>
	</p>
      </div>
      <div class="clearb"></div>
    {/if}

    {* categories *}
    {if isset($all_categories)}
        {assign var="size" value=$all_categories|@count}
      <div class="c_full">
        <div class="grid_2 text-right">{$mod->Lang('categories')}:</div>
        <div class="grid_10">
            {if $mode == "editor"}
                {assign var="disabled" value="disabled"}
            {/if}
            <select name="{$actionid}categories[]" id="categories" multiple="multiple" size={$size}>
                {html_options options=$all_categories selected=$sel_categories}
            </select>
	</div>
      </div>
    {/if}
    
    
    {* status field *}
    <div class="c_full">
      <div class="grid_2 text-right">{$statustext}:</div>
      <div class="grid_10">{$inputstatus}</div>
    </div>
    
    <div class="c_full">
      <div class="grid_2 text-right">{$taxabletext}:</div>
      <div class="grid_10">{$inputtaxable}</div>
    </div>
    
  </div>

  <div class="clearb"></div>
</div>

{* display custom fields *}
{if $customfieldscount gt 0}
{cge_tabcontent_start name='fields'}
  {foreach from=$customfields item=customfield}
    <div class="c_full">
      <p class="grid_2 text-right">{if isset($customfield->prompt)}{$customfield->prompt}{else}{$customfield->name}{/if}:</p>
      <p class="grid_10">
        {if isset($customfield->value)}
          {if $customfield->type == 'image' && isset($customfield->image) && isset($customfield->thumbnail)}
            <a href="{$customfield->image}" class="fancybox"><img src="{$customfield->thumbnail}" alt="{$customfield->value}"/></a>
          {elseif $customfield->type != 'textarea' && $customfield->type != 'dimensions' && $customfield->type != 'subscription'}
	     <!--{$mod->Lang('current_value')}:&nbsp;{$customfield->value}<br/-->
          {/if}
          {if isset($customfield->delete)}{$mod->Lang('delete')}&nbsp;{$customfield->delete}<br/>{/if}
        {/if}
        {if isset($customfield->hidden)}{$customfield->hidden}{/if}{$customfield->input_box}
        {if isset($customfield->attribute)}<br/>{$customfield->attribute}{/if}
      </p>
    </div>
  {/foreach}
  <div class="clearb"></div>
{/if}

{cge_tabcontent_start name='advanced'}
<script type="text/javascript">
$(document).ready(function(){
  // variables
  var pid = {$compid|default:0};
  var manual_urlslug = pid;
  var finished_setup = 0;
  var ajax_timeout;
  var ajax_xhr;

  // setup cursor for ajax stuff
  $('form').ajaxStart(function() {
    $('*').css('cursor','progress');
  });

  $('form').ajaxStop(function() {
    $('*').css('cursor','auto');
  });
  
  

  function _ajax_getslug() {
     if( typeof ajax_xhr != 'undefined' && ajax_xhr != 0 ) ajax_xhr.abort();
     var $name = $('#product_name');
     var form = $name.closest('form');
     var pname = $name.val();
     ajax_xhr = $.ajax({
        url: '{module_action_url action='admin_ajax_getslug' forajax=1}',
	method: 'POST',
	data: {
	  '{$actionid}pid':  pid,
	  '{$actionid}name': pname,
	  '{$actionid}hier': $('#hierarchy').val(),
	  '{$actionid}cats': $('#categories').val(),
	},
	success: function( res ) {
	   $('#urlslug').val(res);
	   ajax_xhr = 0;
	}
     })
  }

  function ajax_getslug() {
     if( !finished_setup ) return;
     if( manual_urlslug) return;
     if( ajax_timeout != undefined ) clearTimeout(ajax_timeout);
     ajax_timeout = setTimeout(_ajax_getslug,500);
  }

  $('#urlslug').keyup(function(){
     manual_urlslug = 0;
     if( $(this).val() != '' ) manual_urlslug = 1;
  });

  $('#product_name').keyup(function(){
     // ajax call to get a url slug given a template
     ajax_getslug();
  });

  $('#hierarchy,#categories').change(function(){
     // ajax call to get a url slug given a template
     ajax_getslug();
  })

  finished_setup = 1;
})
</script>

  
  <!--div class="c_full">
    <p class="grid_2 text-right">{$mod->Lang('weight')} ({$weightunits}):</p>
    <p class="grid_10">
      <input type="text" name="{$actionid}weight" value="{$weight}" size="8" maxlength="12"/>
      <br/>{$mod->Lang('info_decimal_units')}
    </p>
  </div-->
  <div class="c_full">
    <p class="grid_2 text-right">{$mod->Lang('url_slug')}:</p>
    <p class="grid_10">
      <input type="text" id="urlslug" name="{$actionid}urlslug" value="{$urlslug}" size="40"/>
      <br/>{$mod->Lang('info_url_slug')}
    </p>
  </div>
  <div class="c_full">
    <p class="grid_2 text-right">{$mod->Lang('url_alias')}:</p>
    <p class="grid_10">{$inputalias}<br/>{$mod->Lang('info_url_alias')}</p>
  </div>
  <div class="clearb"></div>
  
  {cge_tabcontent_start name='bundle'}
  <div class="bundle"><input name="bundle" placeholder="Add Bundle by SKU"/> <span class="result"></span></div>
  <table cellspacing="0" class="pagetable">
    <thead>
      <tr>
	<th class="pageicon">Item ID</th>
	<th>SKU</th>
	<th>Product Name</th>
	<th>Price</th>
	<th>Create Date</th>
	<th>Modified Date</th>
	<th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
	<th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
        <th class="pageicon {literal}{sorter: false}{/literal}">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$items item=entry}
        <tr id="item_{$entry.id}" class="{cycle values='row1,row2'}">
            <td>{$entry.id}</td>
            <td>{$entry.sku}</td>
            <td>{$entry.product_name}</td>
            <td>{$entry.price}</td>
            <td>{$entry.create_date}</td>
            <td>{$entry.modified_date}</td>
            <td></td>
            <td>{$entry.edit_link}</td>
            <td class="init-ajax-delete">{$entry.delete_link}</td>
        </tr>
      {/foreach}
    </tbody>
  </table>
  <div class="clearb"></div>
  bold italic underline strikethrough superscript subscript | cmsms_linker link image media | anchor code fullscreen
{cge_tabcontent_start name='colors'}
<style>
    .activecolor {
        display: inline-block;
        height: 20px;
        width: 3%;
        border: solid 1px #d7d7d7;
        padding: 1px;
        background: #fff;
        cursor: pointer !important;
    }
    .productcolor {
        display: inline-block;
        height: 20px;
        width: 3%;
        border: solid 1px #d7d7d7;
        padding: 1px;
        background: #fff;
        cursor: pointer !important;
    }
    .productcolor:hover{
        border: solid 1px red;
    }
    .productcolor.selected{
        border: solid 1px red;
    }
    .removecolor{
        cursor: help !important;
    }
</style>
<div class="clearb">List Color:</div>
<div class="c_full">
{foreach from=$colors item=entry}
    <div class="productcolor color{$entry.id}" data-productcolor="{$entry.id}" style="background-color:{$entry.code}"></div>
{/foreach}
</div>
<div class="clearb"><br/>Active Color:</div>
<div class="c_full colors_active">
{foreach from=$colors_active item=entry}
    <div class="activecolor color{$entry.id}" data-productcolor="{$entry.id}" style="background-color:{$entry.code}">
    {admin_icon icon='erase.png' width='16px' class='removecolor' title='Remove'}
    </div>
{/foreach}
</div>
<div class="clearb"></div>
  
{cge_end_tabs}

<div class="pageoverflow">
  <p class="pageinput">
    <input type="submit" name="{$actionid}submit" value="{$mod->Lang('submit')}"/>
    <input type="submit" name="{$actionid}apply" value="{$mod->Lang('apply')}"/>
    <input type="submit" name="{$actionid}cancel" value="{$mod->Lang('cancel')}"/>
  </p>
</div>
{$endform}
