{if ($details)}
    {foreach $details as $detail}
    {$detail->name}<br/>
    {$detail->description}<br/>
    {/foreach}
{else}
<ul>
{foreach from=$items item=entry}
<li class="item-2"><a href="{$entry->summary_url}" title="{$entry->name}">{$entry->name|truncate:30}</a></li>
{/foreach}
</ul>
{/if}