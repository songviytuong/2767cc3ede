<?php

if (!isset($gCms))
    exit();
if (!$this->CheckPermission('Modify Products'))
    return;
if (!isset($params['prodid']))
    return;

$this->SetCurrentTab('products');

#Start MLE
global $hls, $hl, $mleblock;
$thisURL = $_SERVER['SCRIPT_NAME'] . '?';
foreach ($_GET as $key => $val)
    if ('hl' != $key)
        $thisURL .= $key . '=' . $val . '&amp;';
if (isset($hls)) {
    $langList = ' &nbsp; &nbsp; ';
    foreach ($hls as $key => $mle) {
        $langList .= ($hl == $key) ? $mle['flag'] . ' ' : '<a href="' . $thisURL . 'hl=' . $key . '">' . $mle['flag'] . '</a> ';
    }
}
$smarty->assign('langList', $langList);
#End MLE

$skel_row = array(
    'id' => '',
    'product_id' => (int) $params['prodid'],
    'title_en' => '',
    'title_my' => '',
    'description_en' => '',
    'description_my' => '',
    'meals' => '',
    'vehicle' => '',
);
$timelines = array($skel_row);

if (isset($params['cancel'])) {
    $this->SetMessage($this->Lang('operation_cancelled'));
    $this->RedirectTotab($id);
}
if (isset($params['deleteall'])) {
    products_timelines::delete_by_product($params['prodid']);
    $this->SetMessage($this->Lang('msg_options_deleted', $params['prodid']));
    $this->RedirectTotab($id);
}
if (isset($params['copyattribs'])) {
    if (!isset($params['copyfrom']) || $params['copyfrom'] < 1) {
        echo $this->ShowErrors($this->Lang('error_missingparam'));
    } else {
        $timelines = products_timelines::load_by_product((int) $params['copyfrom']);
        echo $this->ShowMessage($this->Lang('msg_optionscopied'));
    }
}
if (isset($params['submit']) || isset($params['apply'])) {
    global $mleblock;
    try {
        //products_timelines::delete_by_product($params['prodid']);
        if (isset($params['prodid'])) {
            // we have some options.
            $timelines = products_timelines::load_from_form($params);
            foreach ($timelines as $timeline) {
                $timeline->save();
            }
        }
        if (!isset($params['apply'])) {
            $this->SetMessage($this->Lang('msg_options_saved'));
            $this->RedirectToTab($id);
        } else {
            $this->Redirect($id, 'admin_edit_timelines', '', array('prodid' => $params['prodid']));
        }
        
    } catch (CmsException $e) {
        echo $this->ShowErrors($e->GetMessage());
    }
} else {
    $tmp = products_timelines::load_by_product($params['prodid']);
    if (is_array($tmp) && count($tmp)) {
        $timelines = $tmp;
    }
}

// get a list of products (except this one) with products.
$query = 'SELECT DISTINCT P.id,P.product_name FROM ' . cms_db_prefix() . 'module_products_timelines A
          LEFT JOIN ' . cms_db_prefix() . 'module_products P
          ON A.product_id = P.id WHERE A.product_id != ?
          ORDER BY product_name ASC';
$tmp = $db->GetArray($query, array((int) $params['prodid']));

if (is_array($tmp) && count($tmp)) {
    $t2 = array();
    foreach ($tmp as $rec) {
        $t2[$rec['id']] = $rec['product_name'];
    }
    $smarty->assign('products_with_timelines', $t2);
}

#Get List of Meals
$mealsSQL = "SELECT code,name FROM " . cms_db_prefix() . "module_products_meals";
$resultMeals = $db->GetArray($mealsSQL);
if (is_array($resultMeals) && count($resultMeals)) {
    $arrMeals = array();
    foreach ($resultMeals as $rec) {
        $arrMeals[$rec['code']] = $rec['name'];
    }
    $smarty->assign('timelines_with_meals', $arrMeals);
}

#Get List of Vehicles
$vehiclesSQL = "SELECT code,name FROM " . cms_db_prefix() . "module_products_vehicles";
$resultVehicles = $db->GetArray($vehiclesSQL);
if (is_array($resultVehicles) && count($resultVehicles)) {
    $arrVehicles = array();
    foreach ($resultVehicles as $rec) {
        $arrVehicles[$rec['code']] = $rec['name'];
    }
    $smarty->assign('timelines_with_vehicles', $arrVehicles);
}
$product = product_ops::get_product((int) $params['prodid'], TRUE);
$smarty->assign('product', $product);
$smarty->assign('formstart', $this->CGCreateFormStart($id, 'admin_edit_timelines', '', array('prodid' => $params['prodid'])));
$smarty->assign('formend', $this->CreateFormEnd());
$smarty->assign('skel_row', $skel_row);
$smarty->assign('timelines', $timelines);
$smarty->assign('mleblock', $mleblock);

echo $this->ProcessTemplate('admin_edit_timelines.tpl');
#
# EOF
#
?>