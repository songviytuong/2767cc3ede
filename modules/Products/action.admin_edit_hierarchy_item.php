<?php  /* -*- Mode: PHP; c-set-style: linux; tab-width: 4; c-basic-offset: 4 -*- */
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: Products (c) 2008-2014 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow users to create, manage
#  and display products in a variety of ways.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# Visit the CMSMS Homepage at: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE
if( !isset($gCms) ) exit;
if( !$this->CheckPermission('Modify Products') ) exit;

$this->SetCurrentTab('hierarchy');
if( !isset($params['hierarchy_id']) ) {
    $this->SetError($this->Lang('error_missingparam'));
    $this->RedirectToTab($id);
}
$hierarchy_id = (int)$params['hierarchy_id'];

if( isset($params['cancel']) )  {
    // we're cancelling
    $this->RedirectToTab($id);
}

#
# Defaults
#
$parent = -1;
$name = '';
$alias = '';
$extra1 = '';
$extra2 = '';
$description = '';
$destdir = product_utils::get_hierarchy_upload_path();


#
# Get the values from the database
#
$query = 'SELECT * FROM '.cms_db_prefix().'module_products_hierarchy WHERE id = ?';
$row = $db->GetRow( $query, array( $hierarchy_id) );
$parent = $row['parent_id'];
$name = $row['name'];
$alias = $row['alias'];
$extra1 = $row['extra1'];
$extra2 = $row['extra2'];
$image = $row['image'];
$image_parent = $row['image_parent'];
$description = $row['description'];

#
# Form Action
if( isset($params['submit']) ) {
    $error = 0;
    $name = trim($params['name']);
    if ($params['alias']) {
        $alias = trim($params['alias']);
    } else {
        $alias = munge_string_to_url(trim($params['name']));
    }

    $extra1 = trim($params['extra1']);
    $extra2 = trim($params['extra2']);
    $parent = (int) $params['parent'];
    $description = trim($params['description']);

    if ($parent == $params['hierarchy_id']) {
        $error = 1;
        echo $this->ShowErrors($this->Lang('error_invalidparent'));
    }

    if (!$error && empty($name)) {
        $error = 1;
        echo $this->ShowErrors($this->Lang('error_noname'));
    }

    if (!$error) {
        $query = 'SELECT id FROM ' . cms_db_prefix() . 'module_products_hierarchy WHERE parent_id = ? AND name = ? AND id != ?';
        $tmp = $db->GetOne($query, array($parent, $name, $hierarchy_id));
        if ($tmp) {
            $error = 1;
            echo $this->ShowErrors($this->Lang('error_nameused'));
        }
    }

    if (!$error) {
        // handle image delete first
        if (isset($params['deleteimg'])) {
            $srcname = cms_join_path($destdir, $image);
            $destname = cms_join_path($destdir, 'thumb_' . $image);
            $previewname = cms_join_path($destdir, 'preview_' . $image);
            @unlink($srcname);
            @unlink($destname);
            @unlink($previewname);
            $image = '';
        }
        
        if (isset($params['deleteimg_parent'])) {
            $srcname = cms_join_path($destdir, $image_parent);
            $destname = cms_join_path($destdir, 'thumb_' . $image_parent);
            $previewname = cms_join_path($destdir, 'preview_' . $image_parent);
            @unlink($srcname);
            @unlink($destname);
            @unlink($previewname);
            $image_parent = '';
        }

        // Handle file upload
        $attr = 'default';
        $errors = array();
        $res = $this->HandleUploadedImage($id, 'file', $destdir, $errors, '', $attr, true, 'md5');
        $res_parent = $this->HandleUploadedImage($id, 'file_parent', $destdir, $errors, '', $attr, true, 'md5');
        if ($res === FALSE) {
            echo $this->ShowErrors($errors);
        } else {
            if ($res != cg_fileupload::NOFILE) {
                // image upload succeeded
                $image = $res;
                if ($res === TRUE)
                    $image = '';
            }
            
            if ($res_parent != cg_fileupload::NOFILE) {
                // image upload succeeded
                $image_parent = $res_parent;
                if ($res_parent === TRUE)
                    $image_parent = '';
            }

            $item_order = $row['item_order'];
            if ($row['parent_id'] != $parent) {
                // changed to a different parent.
                // decrement all of the siblings with the larger item_order
                $query = 'UPDATE ' . cms_db_prefix() . 'module_products_hieriarchy SET item_order = item_order - 1
                          WHERE parent_id = ? AND item_order > ?';
                $db->Execute($query, array($row['parent_id'], $item_order));

                // find a new item order
                $query = 'SELECT COALESCE(MAX(item_order),0)+1 FROM ' . cms_db_prefix() . 'module_products_hierarchy
                          WHERE parent_id = ?';
                $item_order = $db->GetOne($query, array($parent));
            }
            $query = 'UPDATE ' . cms_db_prefix() . 'module_products_hierarchy
                      SET name = ?, alias = ?, image = ?, image_parent = ?, item_order = ?, parent_id = ?, description = ?,
                          extra1 = ?, extra2 = ?
                      WHERE id = ?';
            $db->Execute($query, array($name, $alias, $image, $image_parent, $item_order, $parent, $description, $extra1, $extra2, $hierarchy_id));

            $this->UpdateHierarchyPositions();
            $this->RedirectToTab($id);
        }
    }
}


#
# Build the form
#
$hierarchy_items = $this->BuildHierarchyList();
$smarty->assign('hierarchy_id',$hierarchy_id);
$smarty->assign('hierarchy_items',$hierarchy_items);
$smarty->assign('parent',$parent);
$smarty->assign('name',$name);
$smarty->assign('alias',$alias);
$smarty->assign('extra1',$extra1);
$smarty->assign('extra2',$extra2);
$smarty->assign('image',$image);
$smarty->assign('image_parent',$image_parent);
$image_url = product_utils::get_hierarchy_upload_url()."/$image";
$image_parent_url = product_utils::get_hierarchy_upload_url()."/$image_parent";
$smarty->assign('image_url',$image_url);
$smarty->assign('image_parent_url',$image_parent_url);
$smarty->assign('description',$description);

$smarty->assign('formstart',
                $this->CGCreateFormStart($id,'admin_edit_hierarchy_item',$returnid,
                                         $params,'false','post','multipart/form-data'));
$smarty->assign('formend',$this->CreateFormEnd());

echo $this->ProcessTemplate('admin_add_hierarchy_item.tpl');


#
# EOF
#
?>