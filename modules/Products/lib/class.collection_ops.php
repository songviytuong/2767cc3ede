<?php

class collection_ops
{
    private static $_allcollection;

    private static function _update_collection_counts(&$data,$parent_id = -1)
    {
        $n = 0;
        foreach( $data as &$rec ) {
            if( !isset($rec['direct_count']) ) continue;
            if( $rec['parent_id'] != $parent_id ) continue;
            $rec['count'] = $rec['direct_count'];
            $rec['count'] += self::_update_collection_counts($data,$rec['id']);
            $n += $rec['count'];
        }
        return $n;
    }

    public static function get_all_collection_info($count = FALSE, $all = TRUE)
    {
        if( !is_array(self::$_allcollection) ) self::$_allcollection = array();

        $db = cmsms()->GetDb();
        if( !$count ) {
            $key = 'nocount';

            if( !isset(self::$_allcollection[$key]) ) {
                $query = 'SELECT * FROM '.cms_db_prefix().'module_products_collections ORDER BY `item_order` ASC';
                $tmp = $db->GetArray($query);
                if( is_array($tmp) && count($tmp) ) $tmp = cge_array::to_hash($tmp,'item_order');
                self::$_allcollection[$key] = $tmp;
            }
        }
        else {
            // all is only applicable here.
            $key = 'count';
            if( !isset(self::$_allcollection[$key]) ) {
                $db = cmsms()->GetDb();
                $query = 'SELECT * FROM '.cms_db_prefix().'module_products_collections ORDER BY `item_order` ASC';
                $tmp = $db->GetAll($query);
                if( is_array($tmp) && count($tmp) ) $tmp = cge_array::to_hash($tmp,'item_order');
                self::_update_collection_counts($tmp);
                self::$_allcollection[$key] = $tmp;
            }
        }
        return self::$_allcollection[$key];
    }


    public static function get_collection_info($collection_id)
    {
        $allinfo = self::get_all_collection_info();
        if( is_array($allinfo) && count($allinfo) ) {
            if( isset($allinfo[$collection_id]) ) return $allinfo[$collection_id];
        }
    }


    public static function get_collection_path($hier_id)
    {
        $out = array();
        while( $hier_id > 0 ) {
            $out[] = $hier_id;

            $hier_info = self::get_collection_info($hier_id);
            if( !$hier_info ) break;

            $hier_id = $hier_info['parent_id'];
            if( $hier_id < 0 ) break;
        }

        return array_reverse($out);
    }


    public static function get_breadcrumb($id,$hier_id,$hierpage,$delim = ' &raquo; ')
    {
        $collection_path = self::get_collection_path($hier_id);
        if( !$collection_path ) return FALSE;

        $module = cms_utils::get_module(MOD_PRODUCTS);
        $tmp = array();
        foreach( $collection_path as $one ) {
            $info = collection_ops::get_collection_info($one);
            $link = $module->CreatePrettyLink($id,'collection',$hierpage,$info['name'],array('parent'=>$info['id']));
            $tmp[] = $link;
        }
        if( $delim != '' ) return implode($delim,$tmp);
        return $tmp;
    }

    public static function get_flat_list($full = TRUE,$id = 'prod',$returndid = '')
    {
        if( !function_exists('__hiertree_cb') ) {
            function __hiertree_cb(&$row)
            {
                global $id,$returnid,$entryarray;
                $mod = cms_utils::get_module(MOD_PRODUCTS);
                if( is_object($mod) ) {
                    $row['edit_url'] = $mod->CreateURL($id,'admin_edit_collection_item', $returnid, array('collection_id'=>$row['id']));
                    $row['depth'] = count(explode('.', $row['collection'])) - 1;
                    $row['alias'] = $row["alias"];
                    $row['edit_link'] = $mod->CreateImageLink($id,'admin_edit_collection_item', $returnid,
                                                              $mod->Lang('edit_collection_item'),
                                                              'icons/system/edit.gif',
                                                              array('collection_id'=>$row['id']));
                    $row['delete_link'] = $mod->CreateImageLink($id,'admin_delete_collection_item', $returnid,
                                                                $mod->Lang('delete_collection_item'),
                                                                'icons/system/delete.gif',
                                                                array('collection_id'=>$row['id']),'',
                                                                $mod->Lang('confirm_delete_collection_node'));

                    $entryarray[] = $row;
                }
            }

            function __hiertree_flatten($tree,&$entryarray)
            {
                if( is_array($tree) && count($tree) ) {
                    foreach( $tree as $one ) {
                        $copy = $one;
                        unset($copy['children']);
                        $entryarray[] = $copy;
                        if( isset($one['children']) ) __hiertree_flatten($one['children'],$entryarray);
                    }
                }
            } // function.
        }

        $entryarray = array();
        $cb = '';
        if( $full ) $cb = '__hiertree_cb';
        $tree = product_utils::collection_get_tree(-1,0,$cb);
        __hiertree_flatten($tree,$entryarray);
        return $entryarray;
        
    }
} // end of class

#
# EOF
#
?>
