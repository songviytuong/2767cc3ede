<?php 
final class products_bundle implements ArrayAccess
{
    public static function delete_product_bundle($params){
        $db = cmsms()->GetDb();
        $query = 'DELETE FROM '.cms_db_prefix().'module_products_bundle
              WHERE item_id = ? and product_id = ?';
        $dbr = $db->Execute($query,$params);
        if( !$dbr ) return FALSE;
        return TRUE;
    }
    
    public static function getItemByTerm($sku){
        $db = cmsms()->GetDb();
        $query = 'SELECT * FROM '.cms_db_prefix().'module_products
                  WHERE sku = ?';
        $dbr = $db->GetRow($query,$sku);
        return $dbr;
    }

    public function offsetExists($offset) {
        
    }

    public function offsetGet($offset) {
        
    }

    public function offsetSet($offset, $value) {
        
    }

    public function offsetUnset($offset) {
        
    }

} // end of class

#
# EOF
#
?>