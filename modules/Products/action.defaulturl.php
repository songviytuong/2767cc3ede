<?php

if (!isset($gCms)) exit;

if(isset($params['categoryid']) || isset($params['hierarchyid']))
  {
    $this->DoAction('default',$id,$params,$returnid);
  }
else if(isset($params['collectid']))
{
    $this->DoAction('default',$id,$params,$returnid);
//  $this->DoAction('detailscollection',$id,$params,$returnid);
}
else if( isset($params['parent']) )
  {
    $this->DoAction('hierarchy',$id,$params,$returnid);
  }
else
  {
    $this->DoAction('details', $id, $params, $returnid);
  }

# vim:ts=4 sw=4 noet
?>
