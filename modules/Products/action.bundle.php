<?php 

$thetemplate = $this->GetPreference(PRODUCTS_PREF_DFLTBUNDLE_TEMPLATE);
$thetemplate = \cge_param::get_string($params,'bundletemplate',$thetemplate);
$bundle_id = \cge_param::get_int($params,'bundleid');
//
//$sortby = 'C.name';
//$sortorder = 'asc';
$resultpage = \cge_param::get_string($params,'resultpage');
//$pagelimit = \cge_param::get_string($params,'pagelimit');

if( !empty($resultpage) ) {
    $manager = $gCms->GetHierarchyManager();
    $node = $manager->sureGetNodeByAlias($resultpage);
    if (isset($node)) {
        $content = $node->GetContent();
        if (isset($content)) $resultpage = $content->Id();
    }
    else {
        $node = $manager->sureGetNodeById($resultpage);
        if (!isset($node)) $resultpage = '';
    }
}

//
//$tmp = strtolower(\cge_param::get_string($params,'sortby'));
//switch( $tmp ) {
//case 'id':
//case 'name':
//	$sortby = 'C.'.$tmp;
//	break;
//}
//$tmp = strtolower(\cge_param::get_string($params,'sortorder'));
//switch( $tmp ) {
//case 'asc':
//case 'desc':
//	$sortorder = $tmp;
//	break;
//}
//
//
//$query = "SELECT * FROM ".cms_db_prefix()."module_products_collections C";
//$where = array();
//$qparms = array();
//if( $collect_id ) {
//    $where[] .= "C.id = ?";
//    $qparms[] = $collect_id;
//}
//
//if($pagelimit) {
//    $pagelimit = " LIMIT 0, $pagelimit";
//}
//
//if( count($where) ) $query .= ' WHERE '.implode(' AND ',$where);
//$query .= " GROUP BY C.id ORDER BY $sortby $sortorder $pagelimit";
////var_dump($query);exit();
//$collections = $db->GetArray($query,$qparms);
//if( !$collections ) return;
//
//$results = array();
//for( $i = 0; $i < count($collections); $i++ ) {
//    $row =& $collections[$i];
//
//    $obj = new StdClass();
//    foreach( $row as $k => $v ) {
//        $obj->$k = $v;
//    }
//    
//    $params['collectid'] = $obj->id;
//    
//    if(!$collect_id){
//    $obj->detail_url = $this->CreatePrettyLink($id,'collection',($resultpage!='')?$resultpage:$returnid,
//                                                   '',$params,'',true);
//    }
//    $params['collectname'] = $obj->name;
//    $obj->summary_url = $this->CreatePrettyLink($id,'default',($resultpage!='')?$resultpage:$returnid,
//                                                '',$params,'',true);
//    
//    $results[] = $obj;
//}
$query = 'SELECT * FROM cms_module_products_bundle B INNER JOIN cms_module_products P ON P.id = B.item_id WHERE B.product_id = ?';
$bundles = $db->GetArray($query,array($bundle_id));
if( !$bundles ) return;

$results = array();
for( $i = 0; $i < count($bundles); $i++ ) {
    $row =& $bundles[$i];

    $obj = new StdClass();
    foreach( $row as $k => $v ) {
        $obj->$k = $v;
    }
    
    $pretty_url = '';
    $parms = array();
    if( $this->_notpretty != '' ) $parms['notpretty'] = $this->_notpretty;
    if( product_utils::can_do_pretty('details',$parms) ) $pretty_url = product_ops::pretty_url($row["id"],$resultpage);
    
    $module = cms_utils::get_module('Products');
    $obj->detail_url = $module->create_url('prod','details',($resultpage!='')?$resultpage:$returnid,$parms,false,true,$pretty_url);
    
    $results[] = $obj;
}

$tpl = $this->CreateSmartyTemplate($thetemplate,'bundle_');
$tpl->assign('bundles',$results);
$tpl->display();

// EOF
?>
