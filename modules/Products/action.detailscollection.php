<?php

if (!isset($gCms))
    exit;
$thetemplate = \cge_param::get_string($params, 'collectiontemplate', $thetemplate = $this->GetPreference(PRODUCTS_PREF_DFLTCOLLECTION_TEMPLATE));
$cache_id = '|ps' . md5(serialize($params));
$tpl = $this->CreateSmartyTemplate($thetemplate, 'collection_', $cache_id);

if (!$tpl->isCached()) {
    $detailsarray = array();
    // setup the query
    try {
        $details_query = "SELECT * FROM cms_module_products_collections WHERE id =" . $params["collectid"];
        $details = $db->GetArray($details_query);

        for ($i = 0; $i < count($details); $i++) {
            $row = & $details[$i];

            $obj = new StdClass();
            foreach ($row as $k => $v) {
                $obj->$k = $v;
            }

            $detailsarray[] = $obj;
        }
        $tpl->assign('details', $detailsarray);
    } catch (Exception $e) {
        echo $this->DisplayErrorMessage($e->GetMessage());
    }
}

$tpl->display();

#
# EOF
#
?>