<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: Products (c) 2008-2014 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow users to create, manage
#  and display products in a variety of ways.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# Visit the CMSMS Homepage at: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE
if (!isset($gCms)) exit;
if (!$this->CheckPermission('Modify Products')) return;
$this->SetCurrentTab('products');

#Start MLE
global $hls, $hl, $mleblock;
$thisURL = $_SERVER['SCRIPT_NAME'] . '?';
foreach ($_GET as $key => $val)
    if ('hl' != $key)
        $thisURL .= $key . '=' . $val . '&amp;';
if (isset($hls)) {
    $langList = ' &nbsp; &nbsp; ';
    foreach ($hls as $key => $mle) {
        $langList .= ($hl == $key) ? $mle['flag'] . ' ' : '<a href="' . $thisURL . 'hl=' . $key . '">' . $mle['flag'] . '</a> ';
    }
}
$smarty->assign('langList', $langList);
#End MLE


//
// A utility function
//
function get_field_def(&$fielddefs,$id)
{
    foreach( $fielddefs as $onedef ) {
        if( $onedef->id == $id ) {
            return $onedef;
        }
    }
    return false;
}


if (isset($params['cancel'])) $this->RedirectToTab($id);

$product_name = \cge_param::get_string($params,'product_name');
$price = \cge_param::get_float($params,'price');
$weight = \cge_param::get_float($params,'weight');
$sku = \cge_param::get_string($params,'sku');
$alias = \cge_param::get_string($params,'alias');
$details = \cge_param::get_html($params,'details');
$status = \cge_param::get_string($params,'status',$this->GetPreference('default_status','published'));
$taxable = \cge_param::get_bool($params,'taxable',$this->GetPreference('default_taxable',1));
$urlslug = \cge_param::get_string($params,'urlslug');


$userid = get_userid();
$fielddefs = product_utils::get_fielddefs(true);

if (isset($params['submit']) || isset($params['apply'])) {
    $errors = array();
    try {
        // check product name
        if( empty($product_name) ) throw new \RuntimeException($this->Lang('nonamegiven'));
        // check for duplicate name
        $query = 'SELECT id FROM '.cms_db_prefix().'module_products WHERE product_name = ?';
        if( (int) $db->GetOne($query,array($product_name)) > 0 ) throw new \Runtimeexception($this->Lang('err_product_nameused'));

        // handle an empty alias
        if( empty($alias) ) {
            $alias = product_ops::generate_alias($product_name);
        }
        else {
            $alias = munge_string_to_url($alias);
        }

        // check for duplicate alias
        if( product_ops::check_alias_used($alias) ) throw new \RuntimeException($this->Lang('error_product_aliasused'));

        // check for empty sku
        if( empty($sku) && $this->GetPreference('skurequired',0) ) throw new \RuntimeException($this->Lang('err_sku_required'));

        // check for duplicate sku
        if( !empty($sku) ) {
            if( product_ops::check_sku_used($sku) ) throw new \RuntimeException($this->Lang('error_product_skuused'));
        }

        // validate the urlslug (which is optional)
        if( $urlslug != '' ) {
            $urlslug = trim($urlslug,'/');

            // check for invalid chars.
            $translated = munge_string_to_url($urlslug,false,true);
            if( strtolower($translated) != strtolower($urlslug) ) throw new Exception($this->Lang('error_invalidurlslug'));

            // make sure this url isn't taken.
            $url = trim($urlslug," /\t\r\n\0\x08");
            cms_route_manager::load_routes();
            $route = cms_route_manager::find_match($urlslug);
            if( $route ) {
                // we're adding an article, not editing... any matching route is bad.
                throw new Exception($this->Lang('error_invalidurl'));
            }
        }

        // insert the product record
        $query = 'INSERT INTO '.cms_db_prefix().'module_products (product_name, product_name'.$mleblock.', price, details, create_date, modified_date, taxable, status, weight, sku, alias, url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';
        $dbr = $db->Execute($query, array($product_name, $product_name, $price, $details, trim($db->DBTimeStamp(time()), "'"), trim($db->DBTimeStamp(time()), "'"), $taxable, $status, $weight, $sku, $alias, $urlslug));
        if( !$dbr ) die('ERROR: '.$db->sql.'<br/>'.$db->ErrorMsg());
        $cid = $db->Insert_ID();

        // insert the prodtohier record
        $query = 'INSERT INTO '.cms_db_prefix().'module_products_prodtohier (product_id,hierarchy_id) VALUES (?,?)';
        $db->Execute( $query, array( $cid, (int)$params['hierarchy']) );

        // Handle custom fields
        if (isset($_REQUEST[$id.'customfield'])) {
            foreach ($_REQUEST[$id.'customfield'] as $k=>$v) {
                if (startswith($k, 'field-')) {
                    // get the field index
                    $fid = substr($k, 6);

                    // get the field type
                    $def = get_field_def($fielddefs,$fid);
                    if( !$def ) {
                        die('could not get field def for '.$fid);
                        continue;
                    }

                    // handle the upload (if any)
                    switch( $def->type ) {
                    case 'file':
                        $destdir = product_utils::get_product_upload_path($cid);
                        cge_dir::mkdirr($destdir);
                        if( !is_dir($destdir) ) die('directory still does not exist');
                        $handler = new cg_fileupload($id,$destdir);
                        $handler->set_accepted_filetypes($this->GetPreference('allowed_filetypes'));
                        $res = $handler->handle_upload('customfield','','field-'.$fid);
                        $err = $handler->get_error();
                        if( !$res && $err != cg_fileupload::NOFILE ) {
                            $errors[] = sprintf("%s %s: %s",$this->Lang('field'),$def->name,$this->GetUploadErrorMessage($err));
                        }
                        else if( !$res ) {
                            $v = null;
                        }
                        else {
                            $v = $res;
                        }
                        break;

                    case 'image':
                        $attr = 'default'; // use default value for wmlocation
                        if( isset($_REQUEST[$id.'customfield_attr']) && isset($_REQUEST[$id.'customfield_attr'][$k]) ) {
                            $attr = $_REQUEST[$id.'customfield_attr'][$k];
                        }
                        $destdir = product_utils::get_product_upload_path($cid);
                        $res = $this->HandleUploadedImage($id,'customfield',$destdir,$errors,'field-'.$fid,$attr);
                        if( $res === FALSE ) {
                            $v = null;
                        }
                        else if( $res === TRUE ) {
                            true;
                        }
                        else {
                            $v = $res;
                        }
                        break;

                    case 'quantity':
                        $v = (int)$v;
                        $v = max(0,$v);
                        break;

                    case 'subscription':
                    case 'dimensions':
                        if( is_array($v) ) $v = serialize($v);
                        break;

                    case 'companydir':
                        $v = (int)$v;
                        if( $v < 1 ) $v = null;
                        break;

                    case 'textbox':
                    case 'checkbox':
                    case 'textarea':
                    case 'dropdown':
                        break;

                    default:
                        die("unknown field type: ".$def->type);
                        break;
                    }

                    // commit it.
                    if( !is_null($v) && $v !== '' ) {
                        $query = 'INSERT INTO '.cms_db_prefix().'module_products_fieldvals (product_id, fielddef_id, value, create_date, modified_date) VALUES (?,?,?,?,?)';
                        $db->Execute($query, array($cid, $fid, $v, trim($db->DBTimeStamp(time()), "'"), trim($db->DBTimeStamp(time()), "'")));
                    }
                }
            }
        }

        // handle category stuff
        if (isset($params['categories'])) {
            foreach ($params['categories'] as $v) {
                $query = 'INSERT INTO '.cms_db_prefix().'module_products_product_categories
                  (product_id, category_id, create_date, modified_date) VALUES (?,?,?,?)';
                $db->Execute($query, array($cid, $v, trim($db->DBTimeStamp(time()), "'"), trim($db->DBTimeStamp(time()), "'")));
            }
        }

        //Update search index
        $module = cms_utils::get_search_module();
        if ($module != FALSE) {
            if( $status == 'published' ) {
                $module->AddWords($this->GetName(), $cid, 'product', implode(' ', $this->GetSearchableText($cid) ));
            }
        }

        if( count($errors) ) throw new \RuntimeException($this->Lang('info_fieldproblems'));
        // all done.
        if (!isset($params['apply'])) {
            $this->RedirectToTab($id);
        } else {
            $this->Redirect($id, 'editproduct', '', array('compid' => $cid));
        }
    }
    catch( \Exception $e ) {
        // handle errors.
        if( count($errors) ) echo $this->ShowErrors($errors);
        echo $this->ShowErrors($e->GetMessage());
    }
}

$fieldarray = array();
if (count($fielddefs) > 0) {
    $subscribe_opts = array();
    $subscribe_opts[-1] = $this->Lang('none');
    $subscribe_opts['monthly'] = $this->Lang('subscr_monthly');
    $subscribe_opts['quarterly'] = $this->Lang('subscr_quarterly');
    $subscribe_opts['semianually'] = $this->Lang('subscr_semianually');
    $subscribe_opts['yearly'] = $this->Lang('subscr_yearly');
    $subscribe_opts = array_flip($subscribe_opts);

    $expire_opts = array();
    $expire_opts[$this->Lang('none')] = -1;
    $expire_opts[$this->Lang('expire_six_months')] = '6';
    $expire_opts[$this->Lang('expire_one_year')] = '12';
    $expire_opts[$this->Lang('expire_two_year')] = '24';

    $wmopts = array();
    $wmopts[$this->Lang('none')] = 'none';
    $wmopts[$this->Lang('default')] = 'default';
    $wmopts[$this->Lang('align_ul')] = '0';
    $wmopts[$this->Lang('align_uc')] = '1';
    $wmopts[$this->Lang('align_ur')] = '2';
    $wmopts[$this->Lang('align_ml')] = '3';
    $wmopts[$this->Lang('align_mc')] = '4';
    $wmopts[$this->Lang('align_mr')] = '5';
    $wmopts[$this->Lang('align_ll')] = '6';
    $wmopts[$this->Lang('align_lc')] = '7';
    $wmopts[$this->Lang('align_lr')] = '8';

    foreach ($fielddefs as $fielddef) {
        $field = new stdClass();

        $value = '';
        if (isset($fielddef->value)) $value = $fielddef->value;

        if (isset($_REQUEST[$id.'customfield']['field-'.$fielddef->id])) $value = $_REQUEST[$id.'customfield']['field-'.$fielddef->id];
        $field->id = $fielddef->id;
        $field->name = $fielddef->name;
        $field->prompt = $fielddef->prompt;
        $field->type = $fielddef->type;
        switch ($fielddef->type) {
        case 'dimensions':
            $field->prompt .= '&nbsp;('.product_ops::get_length_units().')';
            $field->input_box =
                $this->Lang('abbr_length').':&nbsp'.
                $this->CreateInputText($id,'customfield[field-'.$fielddef->id.'][length]',$value,3,3).
                $this->Lang('abbr_width').':&nbsp'.
                $this->CreateInputText($id,'customfield[field-'.$fielddef->id.'][width]', $value,3,3).
                $this->Lang('abbr_height').':&nbsp'.
                $this->CreateInputText($id,'customfield[field-'.$fielddef->id.'][height]', $value,3,3);
            break;
        case 'checkbox':
            $field->input_box = '<input type="hidden" name="' . $id . 'customfield[field-'.$fielddef->id.']' . '" value="false" />'.$this->CreateInputCheckbox($id, 'customfield[field-'.$fielddef->id.']', 'true', $value == 'true');
            break;
        case 'textarea':
            $field->input_box = $this->CreateTextArea(true, $id, $value, 'customfield[field-'.$fielddef->id.']');
            break;
        case 'dropdown':
            $field->input_box = $this->CreateInputDropdown($id, 'customfield[field-'.$fielddef->id.']', $fielddef->options, -1, $value );
            break;
        case 'file':
            $field->input_box = $this->CreateFileUploadInput($id,'customfield[field-'.$fielddef->id.']','',50);
            $field->hidden = $this->CreateInputHidden($id,'customfield[field-'.$fielddef->id.']','');
        case 'image':
            $field->input_box = $this->CreateFileUploadInput($id,'customfield[field-'.$fielddef->id.']','',50);
            $field->hidden = $this->CreateInputHidden($id,'customfield[field-'.$fielddef->id.']','');
            break;
        case 'subscription':
            $field->input_box = $this->Lang('subscr_payperiod').':&nbsp;';
            $field->input_box .= $this->CreateInputDropdown($id,'customfield[field-'.$fielddef->id.'][payperiod]',
                                                            $subscribe_opts, -1, $value);
            $field->input_box .= '<br/>'.$this->Lang('subscr_delperiod').':&nbsp;';
            $field->input_box .= $this->CreateInputDropdown($id,'customfield[field-'.$fielddef->id.'][delperiod]',
                                                            $subscribe_opts, -1, $value);
            $field->input_box .= '<br/>'.$this->Lang('subscr_expiry').':&nbsp;';
            $field->input_box .= $this->CreateInputDropdown($id,'customfield[field-'.$fielddef->id.'][expire]',
                                                            $expire_opts, -1, $value);
            break;

        case 'quantity':
            $field->input_box = $this->CreateInputText($id, 'customfield[field-'.$fielddef->id.']', $value, 4, 4);
            break;

        case 'companydir':
            $cdmod = cms_utils::get_module('CompanyDirectory','1.19');
            if( $cdmod ) {
                $field->input_box = $this->CreateInputText($id,'customfield[field-'.$fielddef->id.']',$value,10,10,
                                                           "placeholder=\"{$this->Lang('cd_autocomplete')}\" title=\"{$this->Lang('title_cdautocomplete')}\"");
                $field->input_box = '<span class="cdautocomplete">'.$field->input_box.'</span>';
            }
            break;

        case 'textbox':
        default:
            $field->input_box = $this->CreateInputText($id, 'customfield[field-'.$fielddef->id.']', $value, 30, 255);
            break;
        }
        $fieldarray[] = $field;
    }
}

$categories = $this->GetCategories();
$catarray = array();
if( is_array($categories) && count($categories) ) {
    foreach ($categories as $fielddef) {
        $catarray[$fielddef->name] = $fielddef->id;
    }
}

#Display template
$smarty->assign('product_name',$product_name);
$smarty->assign('price',$price);
$smarty->assign('weight',$weight);
$smarty->assign('sku',$sku);
$smarty->assign('startform', $this->CreateFormStart($id, 'addproduct', $returnid, 'post', 'multipart/form-data'));
$smarty->assign('endform', $this->CreateFormEnd());
$smarty->assign('currency_symbol',product_ops::get_currency_symbol());
$smarty->assign('weightunits',product_ops::get_weight_units());

$smarty->assign('inputalias',$this->CreateInputText($id,'alias',$alias,40,255));
$smarty->assign('detailstext', $this->Lang('details'));
$smarty->assign('inputdetails', $this->CreateTextArea(true, $id, $details, 'details', '', '', '', '', '80', '5'));

if( count($catarray) > 0 ) {
    $n = count($catarray)/4;
    $n = min($n,20);
    $n = max($n,5);
    $smarty->assign('all_categories',array_flip($catarray));
    $smarty->assign('selcatarray',array());
}

$mode = "editor";
if($this->CheckPermission('Products Remove')){
    $mode = "master";
}
$smarty->assign('mode', $mode);

$smarty->assign('taxabletext',$this->Lang('taxable'));
$smarty->assign('inputtaxable', $this->CreateInputCheckbox($id,'taxable',1,$taxable));

$hierarchy_items = $this->BuildHierarchyList();
$smarty->assign('hierarchy_items',$hierarchy_items);
$smarty->assign('hierarchy_pos',isset($params['hierarchy'])?$params['hierarchy']:-1);

$statuses = array($this->Lang('published')=>'published', $this->Lang('draft')=>'draft', $this->Lang('disabled')=>'disabled');
$smarty->assign('statustext',$this->Lang('status'));
$smarty->assign('inputstatus',$this->CreateInputDropdown($id,'status',$statuses,-1,$status));
$smarty->assign('hidden', '');
$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));
$smarty->assign('customfields', $fieldarray);
$smarty->assign('customfieldscount', count($fieldarray));
$smarty->assign('urlslug',$urlslug);

echo $this->ProcessTemplate('editproduct.tpl');

?>