<?php  /* -*- Mode: PHP; c-set-style: linux; tab-width: 4; c-basic-offset: 4 -*- */
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: Products (c) 2008-2014 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow users to create, manage
#  and display products in a variety of ways.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# Visit the CMSMS Homepage at: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

if (!isset($gCms)) exit;
if (!$this->CheckPermission('Modify Products')) return;

// initialization.
$this->SetCurrentTab('Products');
if( !isset($params['compid']) )
  {
    $this->SetError($this->Lang('error_missingparam'));
    $this->RedirectToTab($id);
    return;
  }
$compid = (int)$params['compid'];
if( $compid <= 0 )
  {
    $this->SetError($this->Lang('error_missingparam'));
    $this->RedirectToTab($id);
    return;
  }
$fielddefs = product_ops::get_fields();

//
// read all the data
//
$query = 'SELECT * FROM '.cms_db_prefix().'module_products WHERE Id = ?';
$product = $db->GetRow($query,array($compid));
if( !is_array($product) )
  {
    $this->SetError($this->Lang('error_productnotfound'));
    $this->RedirectToTab($id);
    return;
  }

// get the categories
$query = 'SELECT * FROM '.cms_db_prefix().'module_products_product_categories WHERE product_id = ?';
$product_categories = $db->GetArray($query,array($compid));

// get the field values...
$query = 'SELECT * FROM '.cms_db_prefix().'module_products_fieldvals WHERE product_id = ?';
$product_fieldvals = $db->GetArray($query,array($compid));

// get the attribute sets
$attribs = products_attrib::load_by_product($compid);

// get the hierarchy
$query = 'SELECT hierarchy_id FROM '.cms_db_prefix().'module_products_prodtohier WHERE product_id = ?';
$hierarchy_id = $db->GetOne($query,array($compid));

//
// alter data
//
if( $product['alias'] )
  {
    $product['alias'] = product_ops::generate_alias($product['product_name']);
  }

// get a new name for the product.
$product['product_name'] = $this->Lang('prefix_copyof').' '.$product['product_name'];
$suffix = '';
$query = 'SELECT id FROM '.cms_db_prefix().'module_products WHERE product_name = ?';
while( $suffix < 100 )
  {
    $tname = $product['product_name'].$suffix;
    $tmp = $db->GetOne($query,array($tname));
    if( !$tmp )
      {
	$product['product_name'] = $tname;
	break;
      }
    if( !$suffix ) $suffix = 1;
    $suffix++;
  }

// make sure that the sku is set to 'something' on copying a product.
$new_sku = '';
if( $product['sku'] ) {
  $idx = '';
  if( ($p = strpos($product['sku'],'_copy')) !== FALSE ) {
    if( $p !== FALSE ) $idx = (int)substr($product['sku'],$p+strlen('_copy'));
    $idx = max(2,$idx+1);
  }
  $new_sku = $product['sku'] . '_copy' . $idx;
  while( $idx < 100 ) {
    if( !product_ops::check_sku_used($new_sku) ) {
      $product['sku'] = $new_sku;
      break;
    }
    if( $idx == '' ) $idx = 1;
    $idx++;
    $new_sku = $product['sku'] . '_copy' . $idx;
  }
}
// clear the sku's for the attributes
if( $new_sku ) {
  $idx = 1;
  foreach( $attribs as &$one ) {
    $one['sku'] = $product['sku']."-$idx";
    $idx++;
  }
}

//
// save data
// note: transactions would be really really nice here.
//
try {
  $query = 'INSERT INTO '.cms_db_prefix()."module_products
            (product_name, details, price, create_date, modified_date, taxable, status,
             weight, sku, alias)
            VALUES (?,?,?,NOW(),NOW(),?,?,?,?,?)";
  $dbr = $db->Execute($query,
		      array($product['product_name'],$product['details'],$product['price'],
			    $product['taxable'],$product['status'],$product['weight'],
			    $product['sku'],$product['alias']));
  if( !$dbr )
    throw new CmsException($this->Lang('error_dberror').' -- '.$db->sql.' -- '.$db->ErrorMsg());

  $new_id = $db->Insert_Id();

  if( is_array($product_categories) && count($product_categories) ) {
    $query = 'INSERT INTO '.cms_db_prefix()."module_products_product_categories
              (product_id,category_id,create_date,modified_date) VALUES (?,?,NOW(),NOW())";
    foreach( $product_categories as $cat ) {
      $db->Execute($query,array($new_id,$cat['category_id']));
    }
  }

  if( is_array($product_fieldvals) && count($product_fieldvals) ) {
    $query = 'INSERT INTO '.cms_db_prefix()."module_products_fieldvals
              (product_id,fielddef_id,value,create_date,modified_date)
              VALUES (?,?,?,NOW(),NOW())";
    foreach( $product_fieldvals as $fv ) {
      $db->Execute($query,array($new_id,$fv['fielddef_id'],$fv['value']));
    }
  }

  if( is_array($attribs) && count($attribs) ) {
    $n = count($attribs);
    for( $i = 0; $i < $n; $i++ ) {
      $new = clone $attribs[$i];
      $new['product_id'] = $new_id;
      $new->save();
    }
  }

  if( $hierarchy_id > 0 ) {
    $query = 'INSERT INTO '.cms_db_prefix().'module_products_prodtohier
              (product_id,hierarchy_id) VALUES (?,?)';
    $db->Execute($query,array($new_id,$hierarchy_id));
  }

  //
  // copy files.
  //
  $srcdir = product_utils::get_product_upload_path($compid);
  $destdir = product_utils::get_product_upload_path($new_id);
  if( is_dir($srcdir) ) {
      $files = glob($srcdir.'/*');
      if( is_array($files) && count($files) ) {
          @mkdir($destdir);
          foreach( $files as $onefile ) {
              $fn = basename($onefile);
              copy($onefile,cms_join_path($destdir,$fn));
          }
      }
  }
}
catch( CmsException $e ) {
  $this->SetError($e->GetMessage());
  $this->RedirectToTab($id);
}

//
// all done.. redirect to edit form
//
$this->SetMessage($this->Lang('msg_productcopied'));
$this->Redirect($id,'editproduct','',array('compid'=>$new_id));

#
# EOF
#
?>