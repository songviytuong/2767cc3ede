<?php

if (!isset($gCms)) exit;
if (!$this->CheckPermission('Delete Products Bundle')) return;

$items = array();
$mode = $params["mode"];
if(isset($mode)) {
    $items['item_id'] = (int)$params['item_id'];
    $items['pid'] = (int)$params['pid'];
}
products_bundle::delete_product_bundle($items);
$handlers = ob_list_handlers(); 
for ($cnt = 0; $cnt < sizeof($handlers); $cnt++) { ob_end_clean(); }

echo $this->lang('deleted');
exit();

#
# EOF
#
?>