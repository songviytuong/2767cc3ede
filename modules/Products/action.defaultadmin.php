<?php

if (!isset($gCms)) exit;

#The tabs
echo $this->StartTabHeaders();

if ($this->CheckPermission('Modify Products')) {
    echo $this->SetTabHeader('products', $this->Lang('products'));
}

if ($this->CheckPermission('Products Report Users')) {
    echo $this->SetTabHeader('report_users', $this->Lang('report_users'));
}

if ($this->CheckPermission('Modify Templates')) {
    echo $this->SetTabHeader('fielddefs', $this->Lang('fielddefs'));
    echo $this->SetTabHeader('hierarchy', $this->Lang('product_hierarchy'));
    echo $this->SetTabHeader('categories', $this->Lang('categories'));
    echo $this->SetTabHeader('collections', $this->Lang('collections'));
    echo $this->SetTabHeader('collections_template', $this->Lang('collections_templates'));
    echo $this->SetTabHeader('bundle_template', $this->Lang('bundle_templates'));

    echo $this->SetTabHeader('summary_template', $this->Lang('summarytemplates'));
    echo $this->SetTabHeader('detail_template', $this->Lang('detailtemplates'));
    echo $this->SetTabHeader('byhierarchy_template', $this->Lang('byhierarchytemplates'));
    echo $this->SetTabHeader('categorylist_template', $this->Lang('categorylisttemplates'));
    echo $this->SetTabHeader('search_template', $this->Lang('searchtemplates'));
    echo $this->SetTabHeader('default_templates', $this->Lang('defaulttemplates'));
}

if ($this->CheckPermission('Modify Site Preferences')) {
  echo $this->SetTabHeader('prefs',$this->Lang('preferences'));
}
echo $this->EndTabHeaders();

#The content of the tabs
echo $this->StartTabContent();
if ($this->CheckPermission('Modify Products')) {
  echo $this->StartTab('products', $params);
  include(__DIR__.'/function.admin_productstab.php');
  echo $this->EndTab();
}

if ($this->CheckPermission('Products Report Users')) {
  echo $this->StartTab('report_users', $params);
  include(__DIR__.'/function.admin_report_users_stab.php');
  echo $this->EndTab();
}

if( $this->CheckPermission( 'Modify Templates' ) ) {
  echo $this->StartTab('fielddefs', $params);
  include(__DIR__.'/function.admin_fielddefs_tab.php');
  echo $this->EndTab();

  echo $this->StartTab('hierarchy', $params);
  include(__DIR__.'/function.admin_hierarchy_tab.php');
  echo $this->EndTab();

  echo $this->StartTab('categories', $params);
  include(__DIR__.'/function.admin_categories_tab.php');
  echo $this->EndTab();
  
  echo $this->StartTab('collections', $params);
  include(__DIR__.'/function.admin_collections_tab.php');
  echo $this->EndTab();
  
  echo $this->StartTab('bundle', $params);
  include(__DIR__.'/function.admin_bundle_tab.php');
  echo $this->EndTab();
  
  include(__DIR__.'/function.admin_templatestabs.php');
}

if( $this->CheckPermission('Modify Site Preferences') ) {
  echo $this->StartTab('prefs',$params);
  include(__DIR__.'/function.admin_prefstab.php');
  echo $this->EndTab();
}

echo $this->EndTabContent();

# vim:ts=4 sw=4 noet
?>