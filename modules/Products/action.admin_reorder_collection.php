<?php 

if( !isset($gCms) ) exit;
if( !$this->CheckPermission('Modify Products') ) exit;

//
// initialize
//
function orderhier_create_flatlist($tree,$parent_id = -1)
{
  $data = array();
  $order = 1;
  foreach( $tree as &$node )
    {
      if( is_array($node) && count($node) == 2 )
	{
	  $pid = (int)substr($node[0],strlen('hier_'));
	  $data[] = array('id'=>$pid,'parent_id'=>$parent_id,'order'=>$order);
	  if( isset($node[1]) && is_array($node[1]) )
	    {
	      $data = array_merge($data,orderhier_create_flatlist($node[1],$pid));
	    }
	}
      else
	{
	  $pid = (int)substr($node,strlen('hier_'));
	  $data[] = array('id'=>$pid,'parent_id'=>$parent_id,'order'=>$order);
	}
      $order++;
    }
  return $data;
}

//
// setup the data
//
$this->SetCurrentTab('collections');
$tree = product_utils::collection_get_tree();

//
// handle form submission_
//
if( isset($params['cancel']) )
  {
    $this->SetMessage($this->Lang('operation_cancelled'));
    $this->RedirectToTab($id);
    return;
  }
else if( isset($params['submit']) )
  {
    $params['orderdata'] = json_decode($params['orderdata']);

    // 1. Massage the order data from the form.
    $data = orderhier_create_flatlist($params['orderdata']);

    // 2. Update the data array with info from the tree.
    $query = 'SELECT * FROM '.cms_db_prefix().'module_products_hierarchy ORDER by hierarchy';
    $dbr = $db->GetArray($query);
    $dbr = cge_array::to_hash($dbr,'id');

    // 3. Update the database
    $query = 'UPDATE '.cms_db_prefix().'module_products_hierarchy SET parent_id = ?, item_order = ?, hierarchy = ?, long_name = ? WHERE id = ?';
    foreach( $data as $rec )
      {
	$dbr = $db->Execute($query,array($rec['parent_id'],$rec['order'],'','',$rec['id']));
      }
    product_utils::update_hierarchy_positions();

    // and get out of here.
    $this->SetMessage($this->Lang('operation_complete'));
    $this->RedirectToTab($id);
    return;
  }

//
// give it to smarty
//
$smarty->assign('formstart',$this->CGCreateFormStart($id,'admin_reorder_collection'));
$smarty->assign('formend',$this->CreateFormEnd());
$smarty->assign('tree',$tree);
$smarty->assign('depth',0);

// display the form.
echo $this->ProcessTemplate('admin_reorder_collection.tpl');

#
# EOF
#
?>