<?php 

if( !isset($gCms) ) exit;
if( !$this->CheckPermission('Modify Products') ) exit;
$this->SetCurrentTab('collections');
if( !isset($params['collectid']) ) {
    $this->SetError($this->Lang('error_missingparam'));
    $this->RedirectToTab($id);
}
$collection_id = (int)$params['collectid'];

if( isset($params['cancel']) )  {
    // we're cancelling
    $this->RedirectToTab($id);
}

#
# Defaults
#
$name = '';
$alias = '';
$description = '';
$destdir = product_utils::get_collection_upload_path();


#
# Get the values from the database
#
$query = 'SELECT * FROM '.cms_db_prefix().'module_products_collections WHERE id = ?';
$row = $db->GetRow( $query, array( $collection_id) );
$name = $row['name'];
$alias = $row['alias'];
$image = $row['image'];
$description = $row['description'];

#
# Form Action
if( isset($params['submit']) ) {
    $error = 0;
    $name = trim($params['name']);
    if ($params['alias']) {
        $alias = trim($params['alias']);
    } else {
        $alias = munge_string_to_url(trim($params['name']));
    }

    $description = trim($params['description']);

    if (!$error && empty($name)) {
        $error = 1;
        echo $this->ShowErrors($this->Lang('error_noname'));
    }

    if (!$error) {
        $query = 'SELECT id FROM ' . cms_db_prefix() . 'module_products_collections WHERE name = ? AND id != ?';
        $tmp = $db->GetOne($query, array($name, $collection_id));
        if ($tmp) {
            $error = 1;
            echo $this->ShowErrors($this->Lang('error_nameused'));
        }
    }

    if (!$error) {
        // handle image delete first
        if (isset($params['deleteimg'])) {
            $srcname = cms_join_path($destdir, $image);
            $destname = cms_join_path($destdir, 'thumb_' . $image);
            $previewname = cms_join_path($destdir, 'preview_' . $image);
            @unlink($srcname);
            @unlink($destname);
            @unlink($previewname);
            $image = '';
        }

        // Handle file upload
        $attr = 'default';
        $errors = array();
        $res = $this->HandleUploadedImage($id, 'file', $destdir, $errors, '', $attr, true, 'md5');
        if ($res === FALSE) {
            echo $this->ShowErrors($errors);
        } else {
            if ($res != cg_fileupload::NOFILE) {
                // image upload succeeded
                $image = $res;
                if ($res === TRUE)
                    $image = '';
            }
            
//            $query = 'SELECT COALESCE(MAX(item_order),0)+1 FROM ' . cms_db_prefix() . 'module_products_collections';
//            $item_order = $db->GetOne($query);
                
            $query = 'UPDATE ' . cms_db_prefix() . 'module_products_collections
                      SET name = ?, alias = ?, image = ?, description = ? WHERE id = ?';
            $db->Execute($query, array($name, $alias, $image, $description, $collection_id));
            $this->RedirectToTab($id);
        }
    }
}


#
# Build the form
#
$smarty->assign('collection_id',$collection_id);
$smarty->assign('name',$name);
$smarty->assign('alias',$alias);
$smarty->assign('image',$image);
$image_url = product_utils::get_collection_upload_url()."/$image";
$smarty->assign('image_url',$image_url);
$smarty->assign('description',$description);

$smarty->assign('formstart',
                $this->CGCreateFormStart($id,'admin_edit_collection_item',$returnid,
                                         $params,'false','post','multipart/form-data'));
$smarty->assign('formend',$this->CreateFormEnd());

echo $this->ProcessTemplate('admin_add_collection_item.tpl');


#
# EOF
#
?>