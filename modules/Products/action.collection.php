<?php 

$thetemplate = $this->GetPreference(PRODUCTS_PREF_DFLTCOLLECTION_TEMPLATE);
$thetemplate = \cge_param::get_string($params,'collectiontemplate',$thetemplate);
$collect_id = \cge_param::get_int($params,'collectid');
$sortby = 'C.name';
$sortorder = 'asc';
$resultpage = \cge_param::get_string($params,'resultpage');
$pagelimit = \cge_param::get_string($params,'pagelimit');

if( !empty($resultpage) ) {
    $manager = $gCms->GetHierarchyManager();
    $node = $manager->sureGetNodeByAlias($resultpage);
    if (isset($node)) {
        $content = $node->GetContent();
        if (isset($content)) $resultpage = $content->Id();
    }
    else {
        $node = $manager->sureGetNodeById($resultpage);
        if (!isset($node)) $resultpage = '';
    }
}

$tmp = strtolower(\cge_param::get_string($params,'sortby'));
switch( $tmp ) {
case 'id':
case 'name':
	$sortby = 'C.'.$tmp;
	break;
}
$tmp = strtolower(\cge_param::get_string($params,'sortorder'));
switch( $tmp ) {
case 'asc':
case 'desc':
	$sortorder = $tmp;
	break;
}


$query = "SELECT * FROM ".cms_db_prefix()."module_products_collections C";
$where = array();
$qparms = array();
if( $collect_id ) {
    $where[] .= "C.id = ?";
    $qparms[] = $collect_id;
}

if($pagelimit) {
    $pagelimit = " LIMIT 0, $pagelimit";
}

if( count($where) ) $query .= ' WHERE '.implode(' AND ',$where);
$query .= " GROUP BY C.id ORDER BY $sortby $sortorder $pagelimit";
//var_dump($query);exit();
$collections = $db->GetArray($query,$qparms);
if( !$collections ) return;

$results = array();
for( $i = 0; $i < count($collections); $i++ ) {
    $row =& $collections[$i];

    $obj = new StdClass();
    foreach( $row as $k => $v ) {
        $obj->$k = $v;
    }
    
    $params['collectid'] = $obj->id;
    
    if(!$collect_id){
    $obj->detail_url = $this->CreatePrettyLink($id,'collection',($resultpage!='')?$resultpage:$returnid,
                                                   '',$params,'',true);
    }
    $params['collectname'] = $obj->name;
    $obj->summary_url = $this->CreatePrettyLink($id,'default',($resultpage!='')?$resultpage:$returnid,
                                                '',$params,'',true);
    
    $results[] = $obj;
}

$tpl = $this->CreateSmartyTemplate($thetemplate,'collection_');
$tpl->assign('items',$results);
$tpl->display();

// EOF
?>
