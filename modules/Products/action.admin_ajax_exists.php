<?php
if (!isset($gCms)) exit;
if (!$this->CheckPermission('Modify Products')) return;
$rest = array();

$aAction = $_POST["aAction"];
$aList = $_POST["aList"];

$where = \cge_param::get_string($params,$aAction);
$where = ($where) ? $where : $_POST["sku"];

$exists_query = $db->GetRow('SELECT id,'.$aAction.' FROM ' . cms_db_prefix() . 'module_products WHERE '.$aAction.'=?', array($where));
$exists = ($exists_query) ? $exists_query[$aAction] : "";
if($exists != ""){
    $rest["msg"] = "<font color='red'>(*) ".$this->Lang('exists_'.$aAction)."</font>";
    if($aAction == "sku" || $aList == "productlist"){
        $rest["msg"] = "<font color='red'>(*) ".$this->Lang('exists_'.$aAction)."</font>";
        $rest["msg_loading"] = "<font color='orange'>Loading...</font>";
        $rest["url"] = cms_html_entity_decode($this->CreateUrl($id, 'editproduct', '', array('compid' => $exists_query["id"])));
    }
}else{
    if($aList == "productlist"){
        $rest["msg_loading"] = "<font color='red'>Coming...</font>";
        $rest["url"] = cms_html_entity_decode($this->CreateUrl($id, 'addproduct','', array()));
    }else{
        $rest["msg"] = "";
    }
}
// give it back via ajax
\cge_utils::send_ajax_and_exit($rest);

#
# EOF
#
?>