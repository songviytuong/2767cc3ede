<?php

if (!isset($gCms))
    exit;
if (!$this->CheckPermission('Modify Products'))
    return;

$theme = cms_utils::get_theme_object();

$pid = trim($_REQUEST["pid"]);
$cid = trim($_REQUEST["cid"]);

$db = cmsms()->GetDb();
$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_products_colors WHERE id = ?';
$row = $db->GetRow($query, array($cid));

$data_productcolor = $row["id"];
$stylecolor = $row["code"];
$icon = '<img src="themes/OneEleven/images/icons/system/erase.png" width="16px" class="removecolor" title="Remove" alt="erase.png">';

$tmpl = <<<EOT

   <div class="activecolor color$data_productcolor" data-productcolor="$data_productcolor" style="background-color:$stylecolor">$icon</div>
        
EOT;

$rest = array();

if ($row) {
    $insert = $db->Execute("INSERT INTO " . cms_db_prefix() . "module_products_prodtocolor (product_id,color_id) VALUES ($pid,$cid)");
    if($insert){
        $rest["data"] = $tmpl;
    }
} else {
    
}

header("Content-type:application/json; charset=utf-8");
$handlers = ob_list_handlers(); 
for ($cnt = 0; $cnt < sizeof($handlers); $cnt++) { ob_end_clean(); }

// give it back via ajax
\cge_utils::send_ajax_and_exit($rest);

#
# EOF
#
?>