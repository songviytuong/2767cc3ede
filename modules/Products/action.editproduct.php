<?php

if (!isset($gCms)) exit;
$this->SetCurrentTab('products');

#Start MLE
global $hls, $hl, $mleblock;
$thisURL = $_SERVER['SCRIPT_NAME'] . '?';
foreach ($_GET as $key => $val)
    if ('hl' != $key)
        $thisURL .= $key . '=' . $val . '&amp;';
if (isset($hls)) {
    $langList = ' &nbsp; &nbsp; ';
    foreach ($hls as $key => $mle) {
        $langList .= ($hl == $key) ? $mle['flag'] . ' ' : '<a href="' . $thisURL . 'hl=' . $key . '">' . $mle['flag'] . '</a> ';
    }
}
$smarty->assign('langList', $langList);
#End MLE

//
// A utility function
//
function get_field_def(&$fielddefs,$id) {
  foreach( $fielddefs as $onedef ) {
    if( $onedef->id == $id ) return $onedef;
  }
  return false;
}

function products_delete_uploaded_file($dir,$filename)
{
  if( empty($filename) ) return;

  $filename = basename($filename);
  @unlink(cms_join_path($dir,$filename));
  @unlink(cms_join_path($dir,'thumb_'.$filename));
  @unlink(cms_join_path($dir,'preview_'.$filename));
}

if (!$this->CheckPermission('Modify Products')) {
  echo $this->ShowErrors($this->Lang('needpermission', array('Modify Products')));
  return;
}

if (isset($params['cancel'])) $this->RedirectToTab($id);

$compid = \cge_param::get_int($params,'compid');
$product_name = \cge_param::get_string($params,'product_name');
$price = \cge_param::get_float($params,'price');
$weight = \cge_param::get_float($params,'weight');
$sku = \cge_param::get_string($params,'sku');
$alias = \cge_param::get_string($params,'alias');
$details = \cge_param::get_html($params,'details');
$pack_includes = \cge_param::get_html($params,'pack_includes');
$pack_excludes = \cge_param::get_html($params,'pack_excludes');
$status = \cge_param::get_string($params,'status',$this->GetPreference('default_status','published'));
$taxable = \cge_param::get_bool($params,'taxable',$this->GetPreference('default_taxable',1));
$urlslug = \cge_param::get_string($params,'urlslug');
$hierarchy_pos = \cge_param::get_int($params,'hierarchy',-1);
$collection_pos = \cge_param::get_int($params,'collection',-1);
$origname = \cge_param::get_string($params,'origname');

if(false) {
    $ids = array();
    $PATH = $config["uploads_path"] . '/_products/product_' . $compid;
    $SOURCE = "http://www.jonathancharlesfurniture.com/catalogue/image/cache";
    for ($i = 1; $i < 15; $i++) {
        $FILENAME = "493719" . "-" . $i . "-1400x1200.jpg";
        if (!file_exists($PATH . '/' . $FILENAME)) {
            $file_headers = @get_headers($SOURCE . '/' . $FILENAME);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                continue;
            } else {
                $ids[] = $i;
            }
        }
    }
    $listimg = implode(",", $ids);
    $smarty->assign('listimg', $listimg);
}

$fieldarray = array();
$userid = get_userid();
$fielddefs = $this->GetFieldDefsForProduct($compid,true);
if (isset($params['seo'])){
    $seo_module = cms_utils::get_module('LISESEO');
    $gSEO = $db->GetRow('SELECT item_id FROM ' . cms_db_prefix() . 'module_liseseo_item WHERE title="'.$sku.'"');
    $seo_id = ($gSEO) ? $gSEO["item_id"] : "";
    if($seo_id) {
        $seo_module->Redirect('m1_','admin_edititem', '', array('item_id'=>$seo_id));
    }
}
if (isset($params['submit']) || isset($params['apply'])) {
    $errors = array();
    try {
        // check product name
        if( empty($product_name) ) throw new \RuntimeException($this->Lang('nonamegiven'));
        $query = 'SELECT id FROM '.cms_db_prefix().'module_products WHERE product_name = ? AND id != ?';
        if( (int) $db->GetOne($query,array($product_name,$compid)) ) {
            throw new \Runtimeexception($this->Lang('err_product_nameused'));
        }

        // handle an empty alias
        if( empty($alias) ) {
            $alias = product_ops::generate_alias($product_name);
        }
        else {
            $alias = munge_string_to_url($alias);
        }

        // check for duplicate alias
        if( product_ops::check_alias_used($alias,$compid) ) throw new \RuntimeException($this->Lang('error_product_aliasused'));

        // check for empty sku
        if( empty($sku) && $this->GetPreference('skurequired',0) ) throw new \RuntimeException($this->Lang('err_sku_required'));

        // check for duplicate sku
        if( !empty($sku) ) {
            //if( product_ops::check_sku_used($sku,$compid) ) throw new \RuntimeException($this->Lang('error_product_skuused'));
        }
        
        #if Travel
        $str_travel = "";
        $str_travel_content = "";
        $travel = $this->GetPreference('allowedtravel');
        if($travel == 1){
            $str_travel = "pack_includes'.$mleblock.' = ?,pack_excludes'.$mleblock.' = ?, ";
            $str_travel_content = '"$pack_includes","$pack_excludes", ';
        }
        
        //$details = strip_tags($details);
            
        // update the original record
        $query = 'UPDATE '.cms_db_prefix().'module_products SET product_name'.$mleblock.' = ?, '.$str_travel.' price = ?, details'.$mleblock.' = ?, modified_date = '.$db->DBTimeStamp(time()).',taxable = ?, status = ?,
                      weight = ?, sku = ?, alias = ?, url = ?, owner = ? WHERE id = ?';
        
        $test = $db->Execute($query, array("$product_name",$str_travel_content.$price, "$details",  $taxable, $status, $weight, $sku, $alias, $urlslug, $userid, $compid));

        // Update the hierarchy stuff
        $query = 'DELETE FROM '.cms_db_prefix().'module_products_prodtohier WHERE product_id = ?';
        $db->Execute( $query, array( $compid ) );
        $query = 'INSERT INTO '.cms_db_prefix().'module_products_prodtohier
              (product_id,hierarchy_id) VALUES (?,?)';
        $db->Execute( $query, array( $compid, $hierarchy_pos ) );
        
        // Update the collection stuff
        $query = 'DELETE FROM '.cms_db_prefix().'module_products_prodtocollect WHERE product_id = ?';
        $db->Execute( $query, array( $compid ) );
        $query = 'INSERT INTO '.cms_db_prefix().'module_products_prodtocollect
              (product_id,collection_id) VALUES (?,?)';
        $db->Execute( $query, array( $compid, $collection_pos ) );

        // Update custom fields
        $deleted_items = array();
        $db->Execute('DELETE FROM '.cms_db_prefix().'module_products_fieldvals WHERE product_id = ?', array($compid));

        $destdir = product_utils::get_product_upload_path($compid);

        if (isset($_REQUEST[$id.'customfield'])) {
            foreach ($_REQUEST[$id.'customfield'] as $k=>$v) {
                // handle file deletions
                if (startswith($k, 'deletefield-')) {

                    // get the field index
                    $fid = substr($k, strlen('deletefield-'));

                    // get the field type
                    $def = get_field_def($fielddefs,$fid);
                    if( !isset($def->value) ) continue;
                    if( !$def ) die('could not get field def for '.$fid);

                    $destdir = product_utils::get_product_upload_path($compid);

                    switch( $def->type ) {
                    case 'file':
                    case 'image':
                        // delete the file
                        products_delete_uploaded_file($destdir,$def->value);
                        $deleted_items[] = $fid;
                        break;
                    }
                }
            }

            foreach ($_REQUEST[$id.'customfield'] as $k=>$v) {
                // handle new values (or hidden values)
                if (startswith($k, 'field-')) { // else

                    // get the field index
                    $fid = substr($k, 6);

                    if( in_array($fid,$deleted_items) ) $v = null;

                    // get the field type
                    $def = get_field_def($fielddefs,$fid);
                    if( !$def ) {
                        die('could not get field def for '.$fid);
                        continue;
                    }

                    // handle the upload (if any)
                    switch( $def->type ) {
                    case 'file':
                        if( isset($def->value)) {
                            $str = "field-$fid";
                            if( isset($_FILES[$id.'customfield']['name'][$str]) && isset($_FILES[$id.'customfield']['size'][$str]) &&
                                $_FILES[$id.'customfield']['size'][$str] > 0 ) {
                                products_delete_uploaded_file($destdir,$def->value);
                            }
                        }
                        $destdir = product_utils::get_product_upload_path($compid);
                        cge_dir::mkdirr($destdir);
                        if( !is_dir($destdir) ) die('directory still does not exist');
                        $handler = new cg_fileupload($id,$destdir);
                        $handler->set_accepted_filetypes($this->GetPreference('allowed_filetypes'));
                        $res = $handler->handle_upload('customfield','','field-'.$fid);
                        $err = $handler->get_error();
                        if( !$res && $err != cg_fileupload::NOFILE ) {
                            $errors[] = sprintf("%s %s: %s",$this->Lang('field'),$def->name, $this->GetUploadErrorMessage($err));
                        }
                        else if( !$res && !empty($v) ) {
                            true;
                        }
                        else if( !$res ) {
                            $v = null;
                        }
                        else {
                            $v = $res;
                        }
                        break;

                    case 'image':
                        if( isset($def->value) ) {
                            $str = "field-$fid";
                            if( isset($_FILES[$id.'customfield']['name'][$str]) && isset($_FILES[$id.'customfield']['size'][$str]) &&
                                $_FILES[$id.'customfield']['size'][$str] > 0 ) {
                                products_delete_uploaded_file($destdir,$def->value);
                            }
                        }
                        $attr = 'default'; // use default value for wmlocation
                        if( isset($_REQUEST[$id.'customfield_attr']) && isset($_REQUEST[$id.'customfield_attr'][$k]) ) {
                            $attr = $_REQUEST[$id.'customfield_attr'][$k];
                        }
                        $destdir = product_utils::get_product_upload_path($compid);
                        $res = $this->HandleUploadedImage($id,'customfield',$destdir,$errors,'field-'.$fid,$attr);
                        if( $res === FALSE ) {
                            $v = null;
                        }
                        else if( $res === TRUE ) {
                            true;
                        }
                        else {
                            $v = $res;
                        }
                        break;

                    case 'quantity':
                        $v = (int)$v;
                        $v = max(0,$v);
                        break;

                    case 'subscription':
                    case 'dimensions':
                        if( is_array($v) ) $v = serialize($v);
                        break;

                    case 'textbox':
                    case 'checkbox':
                    case 'textarea':
                    case 'dropdown':
                        break;

                    case 'companydir':
                        $v = (int)$v;
                        if( $v < 1 ) $v = null;
                        break;

                    default:
                        die("unknown type: ".$def->type);
                        break;
                    }

                    if( !is_null($v) && $v !== '' ) {
                        // commit it if there is a valid value
                        $query = 'INSERT INTO '.cms_db_prefix().'module_products_fieldvals (product_id, fielddef_id, value, create_date, modified_date) VALUES (?,?,?,?,?)';
                        $db->Execute($query, array($compid, $fid, $v, trim($db->DBTimeStamp(time()), "'"), trim($db->DBTimeStamp(time()), "'")));
                    }
                }
            }
        }

        // Update categories
        $db->Execute('DELETE FROM '.cms_db_prefix().'module_products_product_categories WHERE product_id = ?', array($compid));
        if (isset($params['categories'])) {
            foreach ($params['categories'] as $v) {
                $query = 'INSERT INTO '.cms_db_prefix().'module_products_product_categories (product_id, category_id, create_date, modified_date) VALUES (?,?,?,?)';
                $db->Execute($query, array($compid, $v, trim($db->DBTimeStamp(time()), "'"), trim($db->DBTimeStamp(time()), "'")));
            }
        }

        //Update search index
        $module = cms_utils::get_search_module();
        if ($module != FALSE) {
            $module->DeleteWords($this->GetName(), $compid, 'product');
            if( $status == 'published' ) {
                $module->AddWords($this->GetName(), $compid, 'product', implode(' ', $this->GetSearchableText($compid) ) );
            }
        }

        if( count($errors) ) throw new \RuntimeException($this->Lang('info_fieldproblems'));
        
        // all done.
        Events::SendEvent('Products','ProductSave', array('proid' => &$compid, 'module' => 'Products', 'modified_date' => trim($db->DBTimeStamp(time()), "'")));
        if (!isset($params['apply'])) {
            $this->RedirectToTab($id);
        } else {
            $this->Redirect($id, 'editproduct', '', array('compid' => $compid));
        }
    }
    catch( \Exception $e ) {
        // handle errors.
        if( count($errors) ) echo $this->ShowErrors($errors);
        echo $this->ShowErrors($e->GetMessage());
    }
}
else {
    // load the product from the database
    $query = 'SELECT * FROM '.cms_db_prefix().'module_products WHERE id = ?';
    $row = $db->GetRow($query, array($compid));

    if ($row) {
        $product_name = !empty($row['product_name'.$mleblock]) ? $row['product_name'.$mleblock] : $row['product_name'];
        $price = (float)$row['price'];
        $weight = (float)$row['weight'];
        $sku = $row['sku'];
        $alias = $row['alias'];
        $details = !empty($row['details'.$mleblock]) ? $row['details'.$mleblock] : $row['details'];
        $origname = $row['product_name'.$mleblock];
        $pack_includes = $row['pack_includes'.$mleblock];
        $pack_excludes = $row['pack_excludes'.$mleblock];
        $taxable = $row['taxable'];
        $status = $row['status'];
        $urlslug = $row['url'];

        $query_hierarchy = 'SELECT hierarchy_id FROM '.cms_db_prefix().'module_products_prodtohier
              WHERE product_id = ?';
        $hierarchy_pos = $db->GetOne($query_hierarchy, array( $compid) );
        
        $query_collect = 'SELECT collection_id FROM '.cms_db_prefix().'module_products_prodtocollect
              WHERE product_id = ?';
        $collection_pos = $db->GetOne($query_collect, array( $compid) );
    }
}

$fieldarray = array();
if (count($fielddefs) > 0) {
  $subscribe_opts = array();
  $subscribe_opts[-1] = $this->Lang('none');
  $subscribe_opts['monthly'] = $this->Lang('subscr_monthly');
  $subscribe_opts['quarterly'] = $this->Lang('subscr_quarterly');
  $subscribe_opts['semianually'] = $this->Lang('subscr_semianually');
  $subscribe_opts['yearly'] = $this->Lang('subscr_yearly');
  $subscribe_opts = array_flip($subscribe_opts);

  $expire_opts = array();
  $expire_opts[$this->Lang('none')] = -1;
  $expire_opts[$this->Lang('expire_six_months')] = '6';
  $expire_opts[$this->Lang('expire_one_year')] = '12';
  $expire_opts[$this->Lang('expire_two_year')] = '24';

  $wmopts = array();
  $wmopts[$this->Lang('none')] = 'none';
  $wmopts[$this->Lang('default')] = 'default';
  $wmopts[$this->Lang('align_ul')] = '0';
  $wmopts[$this->Lang('align_uc')] = '1';
  $wmopts[$this->Lang('align_ur')] = '2';
  $wmopts[$this->Lang('align_ml')] = '3';
  $wmopts[$this->Lang('align_mc')] = '4';
  $wmopts[$this->Lang('align_mr')] = '5';
  $wmopts[$this->Lang('align_ll')] = '6';
  $wmopts[$this->Lang('align_lc')] = '7';
  $wmopts[$this->Lang('align_lr')] = '8';

  foreach ($fielddefs as $fielddef) {
    $field = new stdClass();

    $value = '';
    if (isset($fielddef->value)) $value = $fielddef->value;

    if (isset($_REQUEST[$id.'customfield']['field-'.$fielddef->id])) {
      $value = $_REQUEST[$id.'customfield']['field-'.$fielddef->id];
    }

    $field->id = $fielddef->id;
    $field->name = $fielddef->name;
    $field->type = $fielddef->type;
    if( isset($fielddef->value) && !empty($fielddef->value) ) $field->value = $fielddef->value;
    $field->prompt = $fielddef->prompt;
    switch ($fielddef->type) {
    case 'dimensions':
      if( !is_array($value) ) $value = array('length'=>0,'width'=>0,'height'=>0);
      $field->prompt .= '&nbsp;('.product_ops::get_length_units().')';
      $field->input_box =
	$this->Lang('abbr_length').':&nbsp'.
	$this->CreateInputText($id,'customfield[field-'.$fielddef->id.'][length]',
			       $value['length'],3,3).
	$this->Lang('abbr_width').':&nbsp'.
	$this->CreateInputText($id,'customfield[field-'.$fielddef->id.'][width]',
			       $value['width'],3,3).
	$this->Lang('abbr_height').':&nbsp'.
	$this->CreateInputText($id,'customfield[field-'.$fielddef->id.'][height]',
			       $value['height'],3,3);
      break;

    case 'checkbox':
      $field->input_box =
	'<input type="hidden" name="' . $id . 'customfield[field-'.$fielddef->id.']' . '" value="false" />' .
	$this->CreateInputCheckbox($id, 'customfield[field-'.$fielddef->id.']', 'true', $value );
      break;

    case 'textarea':
      $field->input_box = $this->CreateTextArea(true, $id, $value, 'customfield[field-'.$fielddef->id.']');
      break;

    case 'dropdown':
      $field->input_box = $this->CreateInputDropdown($id, 'customfield[field-'.$fielddef->id.']',
						     $fielddef->options, -1, $value );
      break;

    case 'file':
      $field->delete = $this->CreateInputCheckbox($id,'customfield[deletefield-'.$fielddef->id.']',
						  1,0);
      $field->input_box = $this->CreateFileUploadInput($id,'customfield[field-'.$fielddef->id.']','',50);
      $field->hidden = $this->CreateInputHidden($id,'customfield[field-'.$fielddef->id.']',$value);
      break;

    case 'image':
        if ($value) {
            $destdir = product_utils::get_product_upload_path($compid);
            $url = product_utils::get_product_upload_url($compid);
            $fn = cms_join_path($destdir,'thumb_'.$value);
            if( file_exists($fn) ) $field->image = "{$url}/{$value}";
            $fn = cms_join_path($destdir,'thumb_'.$value);
            if( file_exists($fn) ) $field->thumbnail = "{$url}/thumb_{$value}";
            $fn = cms_join_path($destdir,'preview_'.$value);
            if( file_exists($fn) ) $field->preview = "{$url}/preview_{$value}";
        }
        $field->delete = $this->CreateInputCheckbox($id,'customfield[deletefield-'.$fielddef->id.']',  1,0);
        $field->input_box = $this->CreateFileUploadInput($id,'customfield[field-'.$fielddef->id.']','',50);
        $field->hidden = $this->CreateInputHidden($id,'customfield[field-'.$fielddef->id.']',$value);
        break;

    case 'subscription':
      if( !is_array($value) ) $value = array('payperiod'=>-1,'delperiod'=>-1,'expire'=>1);
      if( !isset($value['payperiod']) ) $value['payperiod'] = -1;
      if( !isset($value['delperiod']) ) $value['delperiod'] = -1;
      if( !isset($value['expire']) ) $value['expire'] = -1;
      $field->input_box = $this->Lang('subscr_payperiod').':&nbsp;';
      $field->input_box .= $this->CreateInputDropdown($id,'customfield[field-'.$fielddef->id.'][payperiod]',
						      $subscribe_opts, -1, $value['payperiod']);
      $field->input_box .= '<br/>'.$this->Lang('subscr_delperiod').':&nbsp;';
      $field->input_box .= $this->CreateInputDropdown($id,'customfield[field-'.$fielddef->id.'][delperiod]',
						      $subscribe_opts, -1, $value['delperiod']);
      $field->input_box .= '<br/>'.$this->Lang('subscr_expiry').':&nbsp;';
      $field->input_box .= $this->CreateInputDropdown($id,'customfield[field-'.$fielddef->id.'][expire]',
						      $expire_opts, -1, $value['expire']);
      break;

    case 'quantity':
      $field->input_box = $this->CreateInputText($id, 'customfield[field-'.$fielddef->id.']', $value, 4, 4);
      break;

    case 'companydir':
      $cdmod = cms_utils::get_module('CompanyDirectory','1.19');
      if( $cdmod ) {
	$field->input_box = $this->CreateInputText($id,'customfield[field-'.$fielddef->id.']',$value,10,10,
						   "placeholder=\"{$this->Lang('cd_autocomplete')}\" title=\"{$this->Lang('title_cdautocomplete')}\"");
	$field->input_box = '<span class="cdautocomplete">'.$field->input_box.'</span>';
      }
      break;

    case 'textbox':
    default:
      $field->input_box = $this->CreateInputText($id, 'customfield[field-'.$fielddef->id.']', $value, 30, 255);
      break;
    }

    $fieldarray[] = $field;
  }
}

$allcategories = $this->GetCategories();
$catarray = array();
if( is_array($allcategories) && count($allcategories) ) {
  foreach( $allcategories as $one ) {
    $catarray[$one->name] = $one->id;
  }
}
$selcategories = $this->GetCategoriesForProduct($compid);
$selcatarray = array();
if( is_array($selcategories) && count($selcategories) ) {
  foreach( $selcategories as $one ) {
    if( !$one->value ) continue;
    $selcatarray[$one->name] = $one->id;
  }
}

$mode = "editor";
if($this->CheckPermission('Products Remove')){
    $mode = "master";
}
$smarty->assign('mode', $mode);

#Display template
$smarty->assign('product_name',$product_name);
$smarty->assign('price',$price);
$smarty->assign('weight',$weight);
$smarty->assign('sku',$sku);
$smarty->assign('urlslug',$urlslug);
$smarty->assign('startform', $this->CreateFormStart($id, 'editproduct', $returnid, 'post', 'multipart/form-data'));
$smarty->assign('endform', $this->CreateFormEnd());
$smarty->assign('currency_symbol',product_ops::get_currency_symbol());
$smarty->assign('inputprice', $this->CreateInputText($id, 'price', sprintf("%.2F",$price), 10, 12));
$smarty->assign('weightunits',$this->GetPreference('products_weightunits'));
$smarty->assign('inputalias',$this->CreateInputText($id,'alias',$alias,40,255));
$smarty->assign('detailstext', $this->Lang('details'));
$smarty->assign('pack_includes_text', $this->Lang('pack_includes_text'));
$smarty->assign('pack_excludes_text', $this->Lang('pack_excludes_text'));
$smarty->assign('inputdetails', $this->CreateTextArea(true, $id, $details, 'details', '', '', '', '', '80', '5'));
$smarty->assign('pack_includes', $this->CreateTextArea(true, $id, $pack_includes, 'pack_includes', '', '', '', '', '80', '5'));
$smarty->assign('pack_excludes', $this->CreateTextArea(true, $id, $pack_excludes, 'pack_excludes', '', '', '', '', '80', '5'));
if( count($catarray) > 0 ) {
    $n = count($catarray)/4;
    $n = min($n,20);
    $n = max($n,5);
    $smarty->assign('all_categories',array_flip($catarray));
    $smarty->assign('sel_categories',$selcatarray);
}
$smarty->assign('taxabletext',$this->Lang('taxable'));
$smarty->assign('inputtaxable',	$this->CreateInputCheckbox($id,'taxable',1,$taxable));

$collection_items = $this->BuildCollectionList();
$smarty->assign('collection_items',$collection_items);
$smarty->assign('collection_pos',$collection_pos);

$hierarchy_items = $this->BuildHierarchyList();
$smarty->assign('hierarchy_items',$hierarchy_items);
$smarty->assign('hierarchy_pos',$hierarchy_pos);

if ($this->CheckPermission('Allowed Travel')) {
    $smarty->assign('allowedtravel', $this->GetPreference('allowedtravel'));
}

$entryarray = array();
$qparms = array();

$theme = cms_utils::get_theme_object();

$query = "SELECT * FROM " . cms_db_prefix() . "module_products_bundle WHERE product_id = ".$compid;
$dbresult = $db->GetAll($query,$qparms);

foreach ($dbresult as $row) {
    $query_details = "SELECT * FROM " . cms_db_prefix() . "module_products WHERE id = ".$row["item_id"];
    $data = $db->GetRow($query_details);
    $row['id'] = $data["id"];
    $row['sku'] = $data["sku"];
    $row['price'] = $data["price"];
    $row['create_date'] = $data["create_date"];
    $row['modified_date'] = $data["modified_date"];
    $row['product_name'] = !empty($data['product_name'.$mleblock]) ? $data['product_name'.$mleblock] : $data['product_name'];
    
    $row['edit_link'] = $this->CreateLink($id, 'editproduct', $returnid, $theme->DisplayImage('icons/system/edit.gif', $this->ModLang('edit'), '', '', 'systemicon'),array(
        'compid' => $data["id"]
    ));
    $row['delete_link'] = $this->CreateLink($id, 'admin_delete_bundle', $returnid, $theme->DisplayImage('icons/system/delete.gif', $this->ModLang('delete'), '', '', 'systemicon'), array(
        'mode' => 'delete',
        'pid' => $compid,
        'item_id' => $data['id']
    ));
    $entryarray[] = $row;
}



#Add: Color
$entryColor = array();
$colorresult = $db->GetArray("SELECT * FROM " . cms_db_prefix() . "module_products_colors");
function cmp($a, $b){
    return strcmp($a["name"], $b["name"]);
}
usort($colorresult, "cmp");

$arr = array();
$datacolor = $db->GetArray("SELECT color_id FROM " . cms_db_prefix() . "module_products_prodtocolor");
foreach($datacolor as $rowcolor){
        $arr[] = $rowcolor["color_id"];
}
//echo "<pre>";
//var_dump($arr); exit();

foreach($colorresult as $color){
    if(!in_array($color["id"], $arr)){
        $entryColor[] = $color;
    } else {
        $entryColor_active[] = $color;
    }
}
$smarty->assign('colors', $entryColor);
$smarty->assign('colors_active', $entryColor_active);

$statuses = array($this->Lang('published')=>'published',
		  $this->Lang('draft')=>'draft',
		  $this->Lang('disabled')=>'disabled');
$smarty->assign('statustext',$this->Lang('status'));
$smarty->assign('inputstatus', $this->CreateInputDropdown($id,'status',$statuses,-1,$status));

$smarty->assign('idtext',$this->Lang('id'));
$smarty->assign('compid',$compid);

$config = cmsms()->GetConfig();
$smarty->assign('uploads_url',$config['uploads_url']);
$smarty->assign('uploads_path',$config['uploads_path']);

if($this->CheckPermission('Modify Templates')){
    $smarty->assign('mode', 'master');
}
$smarty->assign('hidden',
		$this->CreateInputHidden($id, 'compid', $compid).
		$this->CreateInputHidden($id, 'origname', $origname));
$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));
if($this->CheckPermission('Products Import SEO')){
$smarty->assign('seo', $this->CreateInputSubmit($id, 'seo', $this->lang('seo'),'',$config["admin_dir"]."/themes/OneEleven/images/icons/system/run.gif",$this->lang('seo_confirm')));
}
$smarty->assign('customfields', $fieldarray);
$smarty->assign('customfieldscount', count($fieldarray));

$smarty->assign('starttabheaders',$this->StartTabHeaders());
$smarty->assign('tabheader_main',$this->SetTabHeader('main',$this->Lang('product_info')));
$smarty->assign('tabheader_fields',$this->SetTabHeader('fields',$this->Lang('fields')));
$smarty->assign('tabheader_advanced',$this->SetTabHeader('advanced',$this->Lang('advanced')));
$smarty->assign('tabheader_bundle',$this->SetTabHeader('bundle_tabs',$this->Lang('bundle_tabs')));
$smarty->assign('endtabheaders',$this->EndTabHeaders());
$smarty->assign('starttabcontent',$this->StartTabContent());
$smarty->assign('tab_main',$this->StartTab('main'));
$smarty->assign('tab_fields',$this->StartTab('fields'));
$smarty->assign('tab_advanced',$this->StartTab('advanced'));
$smarty->assign('tab_bundle',$this->StartTab('bundle_tabs'));
$smarty->assign('endtab',$this->EndTab());
$smarty->assign('endtabcontent',$this->EndTabContent());

$smarty->assign('items', $entryarray);

echo $this->ProcessTemplate('editproduct.tpl');

?>