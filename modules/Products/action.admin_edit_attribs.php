<?php  /* -*- Mode: PHP; c-set-style: linux; tab-width: 4; c-basic-offset: 4 -*- */
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: Products (c) 2008-2014 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow users to create, manage
#  and display products in a variety of ways.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# Visit the CMSMS Homepage at: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE
if( !isset($gCms) ) exit();
if( !$this->CheckPermission('Modify Products') ) return;
if( !isset($params['prodid']) ) return;

$this->SetCurrentTab('products');
$skel_row = array('id'=>'','product_id'=>(int)$params['prodid'],'text'=>'','adjustment'=>0,'sku'=>'','qoh'=>0,'notes'=>'');
$attribs = array($skel_row);

if( isset($params['cancel']) ) {
  $this->SetMessage($this->Lang('operation_cancelled'));
  $this->RedirectTotab($id);
}
if( isset($params['deleteall']) ) {
  products_attrib::delete_by_product($params['prodid']);
  $this->SetMessage($this->Lang('msg_options_deleted',$params['prodid']));
  $this->RedirectTotab($id);
}
if( isset($params['copyattribs']) ) {
  if( !isset($params['copyfrom']) || $params['copyfrom'] < 1 ) {
	echo $this->ShowErrors($this->Lang('error_missingparam'));
  }
  else {
	$attribs = products_attrib::load_by_product((int)$params['copyfrom']);
	echo $this->ShowMessage($this->Lang('msg_optionscopied'));
  }
}
if( isset($params['submit']) ) {
    try {
        products_attrib::delete_by_product($params['prodid']);
        if( isset($params['sku']) && isset($params['text']) ) {
            // we have some options.
            $attribs = products_attrib::load_from_form($params);
            foreach( $attribs as $attrib ) {
                $attrib->save();
            }
        }

        $this->SetMessage($this->Lang('msg_options_saved'));
        $this->RedirectToTab($id);
    }
    catch( CmsException $e ) {
        echo $this->ShowErrors($e->GetMessage());
    }
}
else {
  $tmp = products_attrib::load_by_product($params['prodid']);
  if( is_array($tmp) && count($tmp) ) {
	$attribs = $tmp;
  }
}


// get a list of products (except this one) with products.
$query = 'SELECT DISTINCT P.id,P.product_name FROM '.cms_db_prefix().'module_products_attribs A
          LEFT JOIN '.cms_db_prefix().'module_products P
          ON A.product_id = P.id WHERE A.product_id != ?
          ORDER BY product_name ASC';
$tmp = $db->GetArray($query,array((int)$params['prodid']));
if( is_array($tmp) && count($tmp) ) {
  $t2 = array();
  foreach( $tmp as $rec ) {
	$t2[$rec['id']] = $rec['product_name'];
  }
  $smarty->assign('products_with_attribs',$t2);
}

$product = product_ops::get_product((int)$params['prodid'],TRUE);
$smarty->assign('product',$product);
$smarty->assign('sku_is_required',$this->GetPreference('skurequired',0));
$smarty->assign('formstart',$this->CGCreateFormStart($id,'admin_edit_attribs','',array('prodid'=>$params['prodid'])));
$smarty->assign('formend',$this->CreateFormEnd());
$smarty->assign('skel_row',$skel_row);
$smarty->assign('attribs',$attribs);

echo $this->ProcessTemplate('admin_edit_attribs.tpl');
#
# EOF
#
?>