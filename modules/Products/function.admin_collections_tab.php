<?php 

if( !isset($gCms) ) exit;
if( !$this->CheckPermission('Modify Products') ) exit;

#Put together a list of current categories...
$entryarray = array();

$query = "SELECT * FROM ".cms_db_prefix()."module_products_collections ORDER BY item_order ASC";
$dbresult = $db->Execute($query);

$rowclass = 'row1';
$theme = cms_utils::get_theme_object();
while ($dbresult && $row = $dbresult->FetchRow()) {
    $onerow = new stdClass();
    $onerow->id = $row['id'];
    $onerow->alias = $row['alias'];
    $onerow->name = $this->CreateLink($id, 'admin_edit_collection_item', $returnid, $row['name'], array('collectid'=>$row['id']));
    $onerow->copylink = $this->CreateLink($id, 'copycollection', $returnid, $theme->DisplayImage('icons/system/copy.gif', $this->Lang('copy'),'','','systemicon'), array('catid'=>$row['id']));
    $onerow->editlink = $this->CreateLink($id, 'editcollection', $returnid, $theme->DisplayImage('icons/system/edit.gif', $this->Lang('edit'),'','','systemicon'), array('catid'=>$row['id']));
    $onerow->deletelink = $this->CreateLink($id, 'deletecollection', $returnid, $theme->DisplayImage('icons/system/delete.gif', $this->Lang('delete'),'','','systemicon'), array('catid'=>$row['id']), $this->Lang('areyousure'));
    $onerow->rowclass = $rowclass;
    $entryarray[] = $onerow;
    ($rowclass=="row1"?$rowclass="row2":$rowclass="row1");
  }
  
$smarty->assign('entries',$entryarray);
$smarty->assign('add_collection_link', $this->CreateImageLink($id,'admin_add_collection_item',
				       $returnid,
				       $this->Lang('add_collection_item'),
				       'icons/system/newobject.gif',
				       array(),'','',false));

if( count($entryarray) > 1 )
  {
    $smarty->assign('reorder_link',
		    $this->CreateImageLink($id,'admin_reorder_collection',
					   $returnid,
					   $this->Lang('reorder_collection'),
					   'icons/system/reorder.gif',
					   array(),'','',false));
  }

echo $this->ProcessTemplate('admin_collection_tab.tpl');
#
# EOF
#
?>