<?php
$lang['actiondelete']='Bir veya daha fazla &ouml;ğe sil';
$lang['actions']='Eylemler';
$lang['advancedhelp']='Lets you gain access to whole cmsms filesystem, not just /uploads/ (if allowed)';
$lang['afilecontainsillegalchars']='Dosya adında boşlık veya ge&ccedil;ersiz karakterler var (&#039;,&quot;,+,*,\,/,&amp;,$). Y&uuml;kleme iptal edildi';
$lang['afileistoobig']='Dosya &ccedil;ok b&uuml;y&uuml;k. Y&uuml;kleme iptal edildi';
$lang['and']='ve';
$lang['bytes']='bayt';
$lang['cancel']='İptal';
$lang['change_working_folder']='&Ccedil;alışan klas&ouml;r&uuml; değiştir';
$lang['confirm_unpack']='Bu arşiv dosyasını a&ccedil;mak istiyor musunuz?';
$lang['confirmdeleteselected']='Se&ccedil;ili dosyaları silmek istediğinize emin misiniz?';
$lang['confirmselected']='Emin misiniz?';
$lang['confirmsingledelete']='Emin misiniz?';
$lang['confirmsingledirdelete']='Klas&ouml;r&uuml; silmek istediğinize emin misiniz?';
$lang['copiedto']='kopyalandı';
$lang['copy']='Kopyala';
$lang['copy_destdir']='Hedef Dizin';
$lang['copy_destname']='Hedef Dosya adı';
$lang['copyfailed']='Kopyalama işlemi başarısız oldu %s';
$lang['copyselected']='Se&ccedil;ili dosyaları kopyala';
$lang['copysuccess']='&Ouml;ğeler başarıyla kopyalandı';
$lang['couldnotcopy']='kopyalanamadı';
$lang['couldnotmove']='taşınamadı';
$lang['create']='Oluştur';
$lang['create_thumbnails']='Y&uuml;kleme sırasında k&uuml;&ccedil;&uuml;k resimleri oluştur';
$lang['createnewdir']='Yeni klas&ouml;r oluştur';
$lang['createthumbnail']='K&uuml;&ccedil;&uuml;kresim oluştur <em>(yada yeniden oluştur)</em>';
$lang['currentpath']='Ge&ccedil;erli yol:';
$lang['delete']='Sil';
$lang['deleteselected']='Se&ccedil;ili dosyaları sil';
$lang['deleteselectedcancelled']='Se&ccedil;ili dosyaların silinmesinden vazge&ccedil;ildi.';
$lang['deletesuccess']='&Ouml;ğeler Başarıyla silindi ';
$lang['dirchmodfailmulti']='Changing permissions on the directory failed, some of it&#039;s content may have gotten new permissions, though.';
$lang['dirchmodsuccessmulti']='Changing permissions on the directory and it&#039;s content was successful';
$lang['direxists']='Klas&ouml;r zaten var';
$lang['dirtreedeletefail']='Bu klas&ouml;r&uuml; silerken bir hata oluştu. Yine de i&ccedil;indekiler silinmiş olabilir.';
$lang['dirtreedeletesuccess']='Klas&ouml;r i&ccedil;erikleri başarıyla silindi.';
$lang['enableadvanced']='Gelişmiş modu a&ccedil;?';
$lang['eventhelp_OnFileUploaded']='<h4>Parameters:</h4>
<ul>
<li>&quot;file&quot; - The complete file specification to the uploaded file.</li>
<li>&quot;thumb&quot; - If generated, the complete file specification to the generated thumbnail.</li>
</ul>';
$lang['file']='dosya';
$lang['filedate']='Tarih';
$lang['filedeletefail']=' bir hata nedeniyle silinmedi';
$lang['filedeletesuccess']='silindi';
$lang['fileexistsdest']='%s zaten hedef de var';
$lang['fileinfo']='Dosya Bilgisi';
$lang['filename']='Dosya Adı';
$lang['fileno']='Dosya no.';
$lang['filenotfound']='Dosya bulunamadı';
$lang['fileoutsideuploads']='Uploads klas&ouml;r&uuml; dışındaki dosyaları değiştirmek i&ccedil;in izin verilemez! (Bu İşlem Gelişmiş Dosya Y&ouml;netimi iznini gerektirir)';
$lang['fileowner']='Dosya Sahibi';
$lang['fileperms']='İzinler';
$lang['files']='dosyalar';
$lang['filescopiedfailed']='%s dosya(lar) kopyalarken hata oluştu';
$lang['filescopiedsuccessfully']='%s dosya(lar)  başarı ile kopyalandı';
$lang['filesdeletedfiled']='%s dosya(lar) silerken hata oluştu';
$lang['filesdeletedsuccessfully']='%s dosya(lar) başarı ile silindi';
$lang['filesize']='Boyut';
$lang['filesmovedfailed']='%s dosya(lar)  taşırken hata oluştu';
$lang['filesmovedsuccessfully']='%s dosya(lar)  başarı ile taşındı';
$lang['fileview']='Dosya G&ouml;r&uuml;n&uuml;m&uuml;';
$lang['folder']='Klas&ouml;r';
$lang['friendlyname']='Dosya Y&ouml;neticisi';
$lang['group']='Grup';
$lang['help']='		<h3>What does this do?</h3>
		<p>This module provides file management functions</p>
		<h3>How do I use it?</h3>
		<p>Select it from the content-menu from in the admin. </p>';
$lang['iconsize']='Ikon size';
$lang['ie_upload_message']='S&uuml;r&uuml;kle ve Bırak, veya birden fazla dosya y&uuml;kleme Internet Explorer kullanılamaz. Bu &ouml;zellikleri etkinleştirmek i&ccedil;in Firefox veya Chrome gibi daha yeni tarayıcılar kullanılmalıdır.';
$lang['imsure']='Eminim';
$lang['insufficientpermission']='Yetersiz izin %s';
$lang['internalerror']='Internal error (meaning something didn&#039;t make sense at all, please report what you did)';
$lang['invaliddestname']='Belirtilen hedef adı boş veya ge&ccedil;ersiz';
$lang['invalidmovedir']='Belirtilen hedef dizin ge&ccedil;ersiz';
$lang['invalidnewdir']='Name of new directory cannot contain chars like /, \ and &#039;..&#039; ';
$lang['itemstocopy']='Bu &ouml;ğeleri kopyala';
$lang['itemstomove']='Bu &ouml;ğeleri Taşı';
$lang['largeicons']='B&uuml;y&uuml;k ikonlar';
$lang['morethanonefiledirselected']='Sadece bir dosya veya dizin bu eylem i&ccedil;in işaretlenmiş olmalıdır.';
$lang['move']='Taşı';
$lang['move_destdir']='Hedef Dizin';
$lang['movedto']='taşındı';
$lang['movefailed']='Taşıma işlemi başarısız oldu %s';
$lang['moveselected']='Se&ccedil;ili dosyaları taşı';
$lang['movesuccess']='&Ouml;ğeler başarıyla taşındı';
$lang['namealreadyexists']='Dosya adı zaten var';
$lang['newdir']='Yeni klas&ouml;r';
$lang['newdirname']='Yeni klas&ouml;r oluştur:';
$lang['newdirsuccess']='Klas&ouml;r başarıyla oluşturuldu';
$lang['newname']='Yeni Dosya Adı:';
$lang['newpermissions']='Yeni İzinler:';
$lang['newunsupportedarchive']='%s desteklenmeyen arşiv bi&ccedil;imi';
$lang['nodestinationdirs']='Operasyon i&ccedil;in herhangi ge&ccedil;erli bir hedef dizin bulunamadı';
$lang['nofilesselected']='Dosya se&ccedil;ilmedi';
$lang['none']='Hi&ccedil;';
$lang['nothinguploaded']='Hi&ccedil;birşey y&uuml;klemedi';
$lang['notwritable']='Yazılamaz';
$lang['others']='Diğerleri';
$lang['owner']='Sahip';
$lang['permission']='Usage of the the File Manager module';
$lang['permissionadvanced']='Advanced usage of the the File Manager module';
$lang['postletexists']='Warning! Please removed the modules/FileManager/postlet directory completely, as this has been obsolete for many, many versions.';
$lang['prompt_copy']='Bir veya daha fazla &ouml;ğe kopyala';
$lang['prompt_dropfiles']='Dosya y&uuml;klemek i&ccedil;in buraya s&uuml;r&uuml;kle bırak';
$lang['prompt_move']='&Ouml;ğeleri Başka Bir Dizine Taşı';
$lang['refresh']='yenile';
$lang['rename']='Yeniden Adlandır';
$lang['renamecancelled']='Yeniden adlandırma iptal edildi';
$lang['renameerror']='Hata! &Ouml;ğe yeniden adlandırılamadı';
$lang['renamefailure']='Dosya adı değitirme hatası';
$lang['renamesuccess']='Dosya adı değiştirildi';
$lang['return']='Geri D&ouml;n';
$lang['savesettings']='Ayarları Kaydet';
$lang['selecttargetdir']='Hedef se&ccedil;imi taşı/kopyala i&ccedil;in';
$lang['setpermissions']='İzinleri Belirle';
$lang['settings']='Ayarlar';
$lang['settingsconfirmsingledelete']='Tek dosya silinmesini onayla?';
$lang['settingssaved']='Ayarlar Kaydedildi';
$lang['showhiddenfiles']='Gizli Dosyaları G&ouml;ster';
$lang['showthumbnails']='K&uuml;&ccedil;&uuml;kresimleri g&ouml;ster';
$lang['singledirdeletefail']='Klas&ouml;r silmeye &ccedil;alışırken bir hata oluştu (Klas&ouml;r boş mu?)';
$lang['singledirdeletesuccess']='Klas&ouml;r başarıyla silindi';
$lang['singlefiledeletefail']='Dosyayı silmeye &ccedil;alışırken bir hata oluştu';
$lang['singlefiledeletesuccess']='Dosya başarıyla silindi';
$lang['smallicons']='K&uuml;&ccedil;&uuml;k ikonlar';
$lang['space']='boşluk';
$lang['subdir']='altdizin';
$lang['subdirs']='Alt dizinler';
$lang['switchtofileview']='Dosya g&ouml;r&uuml;n&uuml;m&uuml;ne ge&ccedil;';
$lang['thousanddelimiter']='Binlik ayracı';
$lang['thumberror']='K&uuml;&ccedil;&uuml;k resim oluşturma sorunu';
$lang['thumbnail']='K&uuml;&ccedil;&uuml;k resim oluştur';
$lang['thumbsuccess']='Thumbnail Başarıyla oluşturuldu';
$lang['unknown']='Bilinmeyen';
$lang['unknownfileaction']='İ&ccedil; hata: bilinmeyen dosya eylemi';
$lang['unpack']='A&ccedil;';
$lang['unpackafterupload']='Y&uuml;klemeden sonra sıkıştırılmış dosyayı a&ccedil; (Sadece tgz ve zip dosyaları)';
$lang['unpacksuccess']='başarıyla a&ccedil;ıldı';
$lang['uploadboxes']='Y&uuml;kleme kutusu sayısı';
$lang['uploaderstandard']='Standard html input-metedu (allows unpacking)';
$lang['uploadfail']='y&uuml;kleme hatası';
$lang['uploadmethod']='Y&uuml;kleme metodu';
$lang['uploadnewfile']='Yeni dosya(lar) y&uuml;kle:';
$lang['uploadsuccess']='y&uuml;klendi';
$lang['uploadview']='Dosya Y&uuml;kle';
$lang['writable']='Yazılabilir';
$lang['writeprotected']='Dosya Korumalı';
?>