<?php

$lang['confirm_change_pwd'] = 'Warning: After changing the pass phrase, you need to update it in the SimpleSiteMgr module!!';

$lang['friendlyname'] = 'Simple Site Info';

$lang['moddescription']	= 'SimpleSiteInfo gathers website information in support of the SimpleSiteMgr module.';

$lang['create_new_pwd'] = 'Create New Password';

$lang['postinstall'] = 'SimpleSiteInfo has been installed';
$lang['postuninstall'] = 'SimpleSiteInfo has been removed';
$lang['postupgrade'] = 'SimpleSiteInfo has been upgraded';
$lang['prompt_current_pwd'] = 'Current Pass Phrase';
$lang['pwdchanged'] = 'Pass Phrase Changed.<br /><b>Don\'t forget to change it in the SimpleSiteMgr module at the master website!</b>';

?>