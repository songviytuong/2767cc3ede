{$startform}

	<p class="information">{$moddescription}</p>
	
	<div class="pageoverflow">
		<p class="pagetext">{$prompt_current_pwd}:</p>
		<p class="pageinput">{$current_pwd}</p>
	</div>

	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">
			{if isset($idfield)}{$idfield}{/if}
			<input class="cms_submit" name="m1_submit" id="m1_submit" value="{$create_new_pwd}" type="submit" onclick="return confirm('{$confirm_change_pwd}')" />
		</p>
	</div>

{$endform}