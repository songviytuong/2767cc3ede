<script type="text/javascript" src="../modules/SimpleSiteMgr/js/accordion.js"></script>

{strip}
	<style type="text/css">
		body { overflow-y: scroll }
		#options-all { margin: 10px 20px 10px 15px; font-weight:bold; color:#000 }
		#options-all span { float:right }
		#options-all a,	#options-site a { text-decoration:none; color:#000 }
		#options-all a:hover, #options-site a:hover { color:#6A9AB7 }
		#options-site { margin:10px 0px; font-weight:bold; color:#000 }
		#acc { width:98%; list-style:none; color:#033; margin:0 auto 20px }
		#acc .acc-section { overflow:hidden; background:#transparent }
		#acc .acc-content { padding:15px; border:1px solid #ccc; border-top:none; background:#fff; }
		h4#header { color:#000;border:1px solid #385C72; padding:6px 6px 6px; font-weight:bold; margin:15px 1% 0 1%; cursor:default; background: transparent url(../modules/SimpleSiteMgr/images/header_over.gif) repeat-x left top; display: table; width: 98% }
		h4 { color:#000;border:1px solid #385C72; padding:6px 6px 6px; font-weight:bold; margin:15px 0 0 0; cursor:pointer; background: transparent url(../modules/SimpleSiteMgr/images/header.gif) repeat-x left top; display: table; width: 100% }
		h4:hover { background: transparent url(../modules/SimpleSiteMgr/images/header_over.gif) repeat-x left top; }
		h4 span { display: table-cell; width: 30% }
		.error { padding: 10px 10px 10px 40px }
		.font-red { color: #A21111 }
		.systemicon { padding-top: 1px }
		.site-warning { padding: 5px; border: #f00 solid 1px; background: #fcddde; color: #f00; font-weight: bold; }
	</style>
{/strip}

{if !empty($feed_error)}<div class="error">{$warning_feed_error}</div>{/if}

<div id="options-all">
	{$prompt_latest_version} {$latest_cmsms_version}<span>{$updateall} | <a href="javascript:accordion.pr(1)">{$expand_all}</a> | <a href="javascript:accordion.pr(-1)">{$collapse_all}</a></span>
</div>

<h4 id="header">
	<span>{$header_name}</span>
	<span>CMSMS {$header_update_status}</span>
	<span>{$header_php_version}</span>
</h4>

<div id="acc">
{foreach from=$sites item=entry}
	<div>
		{if $entry.wrong_info_version == ''}
			<h4>
			<span>{$entry.site_name}&nbsp;{$entry.site_warning}</span>
			<span class="font-red">{$entry.upgrade_status}{if !empty($entry.upgradable)} CMS Made Simple {$entry.site_cms_version}{/if}</span>
			<span>PHP {$entry.site_php_version}</span>
			</h4>
		{else}
			<h4>{$entry.site_name} - {$entry.wrong_info_version}</h4>
		{/if}
		
		<div class="acc-section">
			<div class="acc-content">
 				<div id="options-site">{$entry.updatesite}{if $entry.wrong_info_version == ''} | <a href="{$entry.site_admin_url}" target="_blank">{$open_site_admin}</a>{/if}</div>
				{if $entry.wrong_info_version == ''}
				
				{if !empty($entry.site_warning)}
					<p class="site-warning">{$prompt_site_warning}: {$entry.site_config_writable} {$entry.site_installer_present} {$entry.site_maintenance_mode}</p>
				{/if}
				
					<div class="c_full">
						<div class="grid_6">
							<h3>{$core_modules|upper}</h3>
							<table class="pagetable">
								<thead>
									<tr>
										<th>{$theader_module}</th>
										<th style="text-align:center">{$theader_installed}</th>
									</tr>
								</thead>
								<tbody>
									{foreach from=$entry.coreitems item=core}
										<tr class="{cycle values='row1,row2'}">
											<td>{$core.module_name}</td>
											<td style="text-align:center">{$core.module_version}</td>
										</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
						<div class="grid_6">
							<h3>{$addon_modules|upper}</h3>
							<table class="pagetable">
								<thead>	
									<tr>
										<th style="width:100px">{$theader_module}</th>
										<th style="width:100px;text-align:center;">{$theader_installed}</th>
										<th style="width:100px;text-align:center;">{$theader_forge}</th>
									</tr>
								</thead>
								<tbody>
									{foreach from=$entry.addonitems item=addon}
										<tr class="{cycle values='row1,row2'}">
											<td>{$addon.module_name}</td>
											<td style="text-align:center">{$addon.module_version}</td>
											<td style="text-align:center;color:{$addon.updatable|default:black}">{$addon.latest_version}</td>
										</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
						<div class="clear"></div>
					</div>
				{/if}
			</div>
		</div>
	</div>
{/foreach}
</div>

{literal}
	<script type="text/javascript">
		var accordion=new TINY.accordion.slider("accordion");
		accordion.init("acc","h4",false,-1,"selected"); 
	</script>
{/literal}