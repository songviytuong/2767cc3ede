{$addsite}

<table cellspacing="0" class="pagetable">
	<thead>
		<tr>
			<th>{$header_name}</th>
			<th>{$header_url}</th>
			<th>{$header_filedate}</th>
			<th>{$header_fileupdate}</th>
			<th />
			<th />
		</tr>
	</thead>
	<tbody>
		{foreach from=$items item=entry}
			<tr class="{cycle values='row1,row2'}">
				<td>{$entry.site_name}</td>
				<td>{$entry.site_url}</td>
				<td>{$entry.site_filedate}</td>
				<td>{$entry.site_fileupdate}</td>
				<td>{$entry.editsite}</td>
				<td>{$entry.delete}</td>
			</tr>
		{/foreach}
	</tbody>
</table>

{$addsite}