{$startform}

	<div class="pageoverflow">
		<p class="pagetext">*{$prompt_site_name}:</p>
		<p class="pageinput">{$input_site_name}</p>
	</div>
	
	<div class="pageoverflow">
		<p class="pagetext">*{$prompt_site_url}:</p>
		<p class="pageinput">{$input_site_url}</p>
	</div>
	
	<div class="pageoverflow">
		<p class="pagetext">*{$prompt_site_filepwd}:</p>
		<p class="pageinput">{$input_site_filepwd}</p>
	</div>
	
	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{if isset($idfield)}{$idfield}{/if}{$submit}{$cancel}</p>
	</div>

{$endform}