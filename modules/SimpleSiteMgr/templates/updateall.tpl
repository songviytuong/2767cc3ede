{$startform}

	<h4>{$label_sites_to_be_updated}</h4>

	<table class="pagetable">
		<thead>
			<tr>
				<th>{$prompt_site_name}</th>
				<th>{$prompt_site_url}</th>
				<th>{$prompt_site_filepwd}</th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$items item=entry}
				<tr class="{cycle values='row1,row2'}">
					<td>{$entry.site_name}</td>
					<td>{$entry.site_url}</td>
					<td>{$entry.site_filepwd}</td>
			   </tr>
			{/foreach}
		</tbody>
	</table>

	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{if isset($idfield)}{$idfield}{/if}{$update}{$cancel}</p>
	</div>

{$endform}