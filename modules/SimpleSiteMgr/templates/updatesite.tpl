{$startform}

	<h4>{$label_site_to_be_updated}</h4>
	
	<table class="pagetable">
		<thead>
			<tr>
				<th>{$prompt_site_name}</th>
				<th>{$prompt_site_url}</th>
				<th>{$prompt_site_filepwd}</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{$label_site_name}</td>
				<td>{$label_site_url}</td>
				<td>{$label_site_filepwd}</td>
			</tr>
		</tbody>
	</table>

	<div class="pageoverflow">
		<p class="pagetext">&nbsp;</p>
		<p class="pageinput">{if isset($idfield)}{$idfield}{/if}{$update}{$cancel}</p>
	</div>
	
{$endform}