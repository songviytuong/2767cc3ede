<?php
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

if (isset($params['cancel'])) 
	$this->Redirect($id, 'defaultadmin', $returnid, array('active_tab' => 'listsites'));

$site_id = (isset($params['site_id']) ? $params['site_id'] : '');

$errors = array();
$fileparts = array();
$site_modules = array();
$core_modules = $this->core_module_list();

$db =& $this->GetDb();

if (isset($params['update'])) {

	$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites WHERE site_id = ' . $site_id;
	$dbresult =& $db->Execute($query);

	if(!$dbresult) {

		$errors[] = $this->Lang('nosuchid', array($site_id));
		$site_id = '';

	} else {
	
		$row 			= $dbresult->FetchRow();
		$site_name		= $row['site_name'];
		$site_url		= $row['site_url'];
		$site_filepwd	= $row['site_filepwd'];
		
		$local_key = substr( $site_filepwd, 20 );
		
		@touch( $site_url . '/index.php?mact=SimpleSiteInfo,cntnt01,update_info_file,0&cntnt01key=' . $local_key );
	
	}
	
	$full_file = $site_url . '/tmp/templates_c/SimpleSiteInfo^'.md5($local_key).'.dat';
	
	$filetemp  = @file_get_contents($full_file);
	
	if ($filetemp === false) {
	
		$errors[] = $this->Lang('no_site_file');
		
		$query = 'UPDATE '.cms_db_prefix().'module_simplesitemgr_sites 
				SET
					site_maintenance_mode=?
				WHERE
					site_id=' . $site_id;

			$result = $db->Execute($query, array('1'));

			if(!$result) $errors[] = 'SQL ERROR: ' . $db->ErrorMsg() . '(with ' . $db->sql . ')';	
	
	} else {

		$filedata = $this->NB_Decryption($filetemp, $site_filepwd);
		$fileparts = explode('*', $filedata);

		if($fileparts[0] != 'CMSMS') {

 			$errors[] = $this->Lang('wrong_password');

		} else {

			$filetime = date("F d Y H:i:s", $this->GetRemoteFileDate($full_file));
			$time = date("F d Y H:i:s", time());

			$query = 'UPDATE '.cms_db_prefix().'module_simplesitemgr_sites 
				SET
					site_filedate=?,
					site_fileupdate=?,
					site_cms_version=?,
					site_admin_url=?,
					site_php_version=?,
					site_module_list=?,
					site_ssi_version=?,
					site_config_writable=?,
					site_installer_present=?,
					site_maintenance_mode=?
				WHERE
					site_id=' . $site_id;

			$result = $db->Execute($query, array($filetime, $time, $fileparts[2], $fileparts[1], $fileparts[3], $fileparts[4], $fileparts[5], $fileparts[6], $fileparts[7], $fileparts[8]));

			if(!$result) {
			
				$errors[] = 'SQL ERROR: ' . $db->ErrorMsg() . '(with ' . $db->sql . ')';
				
			} else {
			
				$site_modules = $this->GetArrays('|', ',', $fileparts[4]);
				$query = 'DELETE FROM ' . cms_db_prefix() . 'module_simplesitemgr_site_modules WHERE site_id=?';
				$result = $db->Execute($query, array($site_id));

				foreach($site_modules as $module=>$version) {
					if (in_array($module, $core_modules)) {	
						$core = "1"; 
					} else {
						$core = "0";
					}
					$query = 'INSERT INTO '.cms_db_prefix().'module_simplesitemgr_site_modules (
						site_id,
						core,
						module_name,
						module_version)
					VALUES(?,?,?,?)';
					$result = $db->Execute($query, array($site_id, $core, $module, $version));
					if(!$result) { $errors[] = 'SQL ERROR: ' . $db->ErrorMsg() . '(with ' . $db->sql . ')';	}						
				}
				$message = $this->Lang('siteupdated');
				$this->Redirect($id, 'defaultadmin', $returnid, array('active_tab' => 'listsites', 'message' => $message));
			}
		}
	}

} else {

	$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites WHERE site_id = ' . $site_id;
	$dbresult =& $db->Execute($query);

	if(!$dbresult) {

		$errors[] = $this->Lang('nosuchid', array($site_id));
		$site_id = '';

	} else {

		$row 			= $dbresult->FetchRow();
		$site_name		= $row['site_name'];
		$site_url		= $row['site_url'];
		$site_filepwd	= $row['site_filepwd'];

	}
}

if(count($errors))
	echo $this->ShowErrors($errors);

if(isset($site_id))
	$smarty->assign('idfield', $this->CreateInputHidden($id, 'site_id', $site_id));

$smarty->assign('formid',$id);
$smarty->assign('startform', $this->CreateFormStart($id, 'admin_updatesite', $returnid));
$smarty->assign('endform', $this->CreateFormEnd());

$smarty->assign('prompt_site_name', $this->Lang('prompt_site_name'));
$smarty->assign('prompt_site_url', $this->Lang('prompt_site_url'));
$smarty->assign('prompt_site_filepwd', $this->Lang('prompt_site_filepwd'));

$smarty->assign('label_site_name', $site_name);
$smarty->assign('label_site_url', $site_url);
$smarty->assign('label_site_to_be_updated', $this->lang('sitetobeupdated'));
$smarty->assign('label_site_filepwd', $site_filepwd);

$smarty->assign('update', $this->CreateInputSubmit($id, 'update', $this->lang('update')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));

echo $this->ProcessTemplate('updatesite.tpl');

?>