<?php
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

if (isset($params['cancel'])) 
	$this->Redirect($id, 'defaultadmin', $returnid, array('active_tab' => 'manage'));

$db =& $this->GetDb();

$errors = array();

$site_id = (isset($params['site_id']) ? $params['site_id'] : '');
$site_name = (isset($params['site_name']) ? $params['site_name'] : '');
$site_url = (isset($params['site_url']) ? $params['site_url'] : '');
$site_filepwd = (isset($params['site_filepwd']) ? $params['site_filepwd'] : '');

if(isset($params['submit'])) {

		if($site_name != '' && $site_url != '' && $site_filepwd != '') {

			if (!preg_match("/http(s?):/i", $site_url)) {
				$site_url = "http://" . $site_url;
			}

			$query = 'UPDATE '.cms_db_prefix().'module_simplesitemgr_sites 
				SET
					site_name=?,
					site_url=?,
					site_filepwd=?
				WHERE
					site_id=' . $site_id;

			$result = $db->Execute($query, array($site_name, $site_url, $site_filepwd));

			if(!$result) {
				$errors[] = 'SQL ERROR: ' . $db->ErrorMsg() . '(with ' . $db->sql . ')';
			} else {
				$messsage = $this->Lang('siteedited');
				if(!isset($params['apply'])) {
					$this->Redirect($id, 'defaultadmin', $returnid, array('active_tab' => 'manage', 'message' => $messsage));
				} else {
					echo $this->ShowMessage($message);
				}
			
			}

		} else {

			if($params['site_name'] == '')
				$errors[] = $this->Lang('no_site_name');

			if($params['site_url'] == '')
				$errors[] = $this->Lang('no_site_url');

			if($params['site_filepwd'] == '')
				$errors[] = $this->Lang('no_site_filepwd');

		}

} else {

	$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites WHERE site_id = ' . $site_id;

	$dbresult =& $db->Execute($query);

	if(!$dbresult) {

		$errors[] = $this->Lang('nosuchid', array($site_id));
		$site_id = '';

	} else {

		$row 			= $dbresult->FetchRow();
		$site_name		= $row['site_name'];
		$site_url		= $row['site_url'];
		$site_filepwd	= $row['site_filepwd'];

	}

}

if(count($errors))
	echo $this->ShowErrors($errors);

if(isset($site_id))
	$smarty->assign('idfield', $this->CreateInputHidden($id, 'site_id', $site_id));

$smarty->assign('formid',$id);
$smarty->assign('startform', $this->CreateFormStart($id, 'admin_editsite', $returnid));
$smarty->assign('endform', $this->CreateFormEnd());

$smarty->assign('prompt_site_name', $this->Lang('prompt_site_name'));
$smarty->assign('prompt_site_url', $this->Lang('prompt_site_url'));
$smarty->assign('prompt_site_filepwd', $this->Lang('prompt_site_filepwd'));

$smarty->assign('input_site_name', $this->CreateInputText($id, 'site_name', $site_name, $size='70', $maxlength='255'));
$smarty->assign('input_site_url', $this->CreateInputText($id, 'site_url', $site_url, $size='70', $maxlength='255'));
$smarty->assign('input_site_filepwd', $this->CreateInputText($id, 'site_filepwd', $site_filepwd, $size='24', $maxlength='24'));

$smarty->assign('submit', $this->CreateInputSubmit($id, 'submit', lang('submit')));
$smarty->assign('cancel', $this->CreateInputSubmit($id, 'cancel', lang('cancel')));

echo $this->ProcessTemplate('editsite.tpl');

?>