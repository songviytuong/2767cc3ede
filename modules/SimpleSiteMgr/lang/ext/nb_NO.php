<?php
$lang['friendlyname'] = 'Simple Site Manager';
$lang['moddescription'] = 'SimpleSiteMgr is used to monitor the versions of CMSMadeSimple and associated modules on sites you manage.';
$lang['postinstall'] = 'SimpleSiteMgr has successfully been installed.';
$lang['postuninstall'] = 'SimpleSiteMgr has successfully been removed.';
$lang['uninstalled'] = 'SimpleSiteMgr Uninstalled.';
$lang['installed'] = 'SimpleSiteMgr version %s installed.';
$lang['upgraded'] = 'SimpleSiteMgr upgraded to version %s.';
$lang['modsimplesitemgr'] = 'SimpleSiteMgr: Use Module';
$lang['listsites'] = 'Site Listings';
$lang['manage'] = 'Manage Sites';
$lang['needpermission'] = 'You don&#039;t have permission to manage this module';
$lang['areyousure'] = 'Are you sure?';
$lang['addsite'] = 'Add Site';
$lang['siteadded'] = 'The site was successfully added';
$lang['editsite'] = 'Edit Site';
$lang['siteedited'] = 'The site was successfully edited.';
$lang['deletesite'] = 'Delete Site';
$lang['sitedeleted'] = 'The site was successfully deleted';
$lang['updatesite'] = 'Update Site';
$lang['update'] = 'Update';
$lang['updateall'] = 'Update All Sites';
$lang['siteupdated'] = 'The site was successfully updated';
$lang['sitesupdated'] = 'The sites were successfully updated';
$lang['sitetobeupdated'] = 'Confirm Update for:';
$lang['sitestobeupdated'] = 'Confirm Updates for:';
$lang['header_name'] = 'Site Name';
$lang['header_url'] = 'URL';
$lang['header_filedate'] = 'File Date';
$lang['header_fileupdate'] = 'Updated On';
$lang['prompt_site_name'] = 'Site Name';
$lang['prompt_site_url'] = 'Site Base URL';
$lang['prompt_file_url'] = 'Version File URL';
$lang['prompt_site_filepwd'] = 'Version File Pass Phrase';
$lang['no_site_name'] = 'Site Name missing!';
$lang['no_site_url'] = 'Site Base URL missing!';
$lang['no_site_file'] = 'Version File missing!';
$lang['no_site_pwd'] = 'Pass Phrase missing!';
$lang['wrong_password'] = 'Version File Pass Phrase incorrect or missing!';
$lang['gotosite'] = 'Site Admin';
$lang['installed_version'] = 'Installed Version:';
$lang['latest_version'] = 'Latest Version:';
$lang['core_modules'] = 'Core Modules';
$lang['addon_modules'] = 'Additional Modules';
$lang['uptodate_color'] = 'black';
$lang['notuptodate_color'] = 'red';
$lang['theader_module'] = 'Module';
$lang['theader_installed'] = 'Installed Version';
$lang['theader_forge'] = 'Forge Version';
$lang['wrong_info_version'] = 'The version file was created with an older version of the SimpleSiteInfo module.';
$lang['nosuchid'] = 'No such ID';
$lang['changelog'] = '<dl>
	<dt>0.1</dt>
		<dd>Test version published for the first time.</dd>
	<dt>0.2</dt>
		<dd>Fixed typos and cosmetics.</dd>
	<dt>0.2.1</dt>
		<dd>Corrected uploaded files.</dd>
	<dt>0.3</dt>
		<dd><ul>
			<li>Corrected CMS version variables.</li>
			<li>Removed the event handlers from SimpleSiteMgr - SimpleSiteInfo must now be installed to create event handlers.</li>
			<li>Limited Pass Phrase length.</li>
		</ul></dd>
	<dt>0.4</dt>
		<dd><ul>
			<li>Changed update processing.</li>
			<li>Added Update All.</li>
			<li>Fixed https handling.</li>
			<li>Changed the encryption/decryption functions to accomodate the Update All action.</li>
		</ul></dd>
	<dt>0.5</dt>
		<dd>Added link to admin pages of the sites.</dd>
	<dt>1.0</dt>
		<dd><ul>
			<li>Sites requiring module updates are highlighted in red.</li>
			<li>Pass Phrase limited to 24 characters.</li>
		</ul></dd>
	<dt>2.0</dt>
		<dd>Moved the nuSOAP processing from the SimpleSiteInfo module to this module, which updates the Forge version info dynamically and allows for better multi language use and customisation.</dd>
</dl>';
$lang['help'] = '<h3>About</h3>
<p>This module provides a way to monitor the status of external CMSMS sites, showing the currently installed 
version of CMSMadeSimple, as well as any installed modules.</p>

<h3>Getting Started</h3>
<p>In order to use this module to it&#039;s fullest, you need to install the &quot;SimpleSiteInfo&quot; module at each site you wish to monitor. 
Once that is installed, logout of the remote site to create the site info file. Now you can log back in to this site and add the 
remote site to your list of sites to monitor, on the &quot;Manage Sites&quot; tab of this module.</p>
<p><b>NOTE:</b>If you wish to include this site in the list of sites monitored, you must also install the &quot;SimpleSiteInfo&quot; module here.</p>
<h3>Usage</h3>
<p>This is currently only an Admin module, with no frontend output. As such, there are no parameters to pass.</p>
<p>There is an &quot;Update All&quot; link, which will obtain the site info files from all monitored sites. However, if there are a lot of sites, you may
 experience timeouts if your PHP max_execution_time and/or memory_limit are set too low.</p> 
<p>On sites that have out of date modules, the site names are highlighted in red.</p>
<h3>Support</h3>
<p>This module does not contain any commercial support. If you have problems, ask in the
<a href="http://forum.cmsmadesimple.org">forums</a>, the 
<a href="irc://irc.freenode.net/cms"><abbr title="Internet Relay Chat">IRC</abbr> chat</a> or write an email to
<a href="mailto:nmcgran@telus.net?subject=SimpleSiteMgr">me</a>.</p>
<p>Special thanks to Rolf for all of your help in testing these modules.</p>';
$lang['utmz'] = '156861353.1292071197.3458.79.utmcsr=forum.cmsmadesimple.org|utmccn=(referral)|utmcmd=referral|utmcct=/index.php/topic,49989.0/topicseen.html';
$lang['utma'] = '156861353.179052623084110100.1210423577.1292701741.1292707001.3467';
$lang['qca'] = '1210971690-27308073-81952832';
$lang['utmc'] = '156861353';
$lang['utmb'] = '156861353.2.10.1292707001';
?>