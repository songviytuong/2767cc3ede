<?php

$lang['addon_modules'] = 'Additional Modules';
$lang['addsite'] = 'Add Site';
$lang['areyousure']	= 'Are you sure?';

$lang['collapse_all']	= 'Collapse All';
$lang['core_modules'] = 'Installed Core Modules';

$lang['deletesite']	= 'Delete Site';

$lang['editsite'] = 'Edit Site';
$lang['expand_all']	= 'Expand All';

$lang['friendlyname'] = 'Simple Site Manager';

$lang['header_filedate'] = 'File Date';
$lang['header_fileupdate'] = 'Updated On';
$lang['header_name'] = 'Site Name';
$lang['header_php_version']	= 'PHP Version';
$lang['header_update_status']	= 'Update Status';
$lang['header_url']	= 'URL';

$lang['prompt_latest_version'] = 'Latest CMS Made Simple Version:';
$lang['listsites'] = 'Site Listings';

$lang['manage']	= 'Manage Sites';
$lang['moddescription'] = 'SimpleSiteMgr is used to monitor the versions of CMS Made Simple and associated modules on sites you manage.';

$lang['no_site_file'] = 'Website information is not available! Is the site off-line or in Maintenance Mode?';
$lang['no_site_name'] = 'Site Name missing!';
$lang['no_site_pwd'] = 'Pass Phrase missing!';
$lang['no_site_url'] = 'Site URL missing!';
$lang['nosuchid'] = 'No such ID';

$lang['open_site_admin'] = 'Open Site Admin';

$lang['postinstall'] = 'SimpleSiteMgr has been installed';
$lang['postuninstall'] = 'SimpleSiteMgr has been removed';
$lang['prompt_config_writable'] = 'The config.php file is writable!';
$lang['prompt_installer_present'] = 'The Installer file is still present!';
$lang['prompt_maintenance_enabled'] = 'Website information was not available! Is the site off-line or in Maintenance Mode?';
$lang['prompt_site_filepwd'] = 'Pass Phrase';
$lang['prompt_site_name'] = 'Site Name';
$lang['prompt_site_url'] = 'Site URL';
$lang['prompt_site_warning'] = 'Warning';

$lang['siteadded'] = 'The site was added';
$lang['sitedeleted'] = 'The site was deleted';
$lang['siteedited']	= 'The site was edited';
$lang['sitestobeupdated'] = 'Confirm Updates for:';
$lang['sitesupdated'] = 'Site information is updated';
$lang['sitetobeupdated'] = 'Confirm Update for:';
$lang['siteupdated'] = 'The site information is updated';

$lang['theader_forge'] = 'Forge Version';
$lang['theader_installed'] = 'Installed Version';
$lang['theader_module'] = 'Module';

$lang['update'] = 'Update';
$lang['updateall'] = 'Update Information of all Websites';
$lang['updatesite'] = 'Update Site Information';

$lang['warning_feed_error']	= 'The latest release information can\'t be retrieved from the CMS Made Simple Forge... Upgrade information shown below can be incorrect...';
$lang['wrong_info_version'] = 'No or wrong SimpleSiteInfo version detected, check if the site uses the last version of the module and Update Site Information here!';
$lang['wrong_password'] = 'Pass Phrase incorrect or missing!';

?>