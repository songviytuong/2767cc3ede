<ul>
	<li><b>Version 3.2</b> (2016-12-24)<br />
		- Fix for removed mcrypt_ecb function in PHP7.</li>
	<li><b>Version 3.1</b> (2016-02-11)<br />
		- Add more website checks, writable config file, presence installer file and maintenance mode.<br />
		- The data read from the remote website is created a few seconds before, so up-to-date.</li>
	<li><b>Version 3.0</b> (2016-01-28)<br />
		Complete rewrite of the module!<br />
		It is NOT possible to upgrade an older release of the module. You must uninstall the old module and install the last version from the Forge.</li>
	<li><b>Version 1.0</b> (2010-09-24)<br />
		First stable release</li>
</ul>