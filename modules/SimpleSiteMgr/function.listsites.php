<?php
#-------------------------------------------------------------------------
# Module: SimpleSiteMgr
# Author: Noel McGran, Rolf Tjassens
#-------------------------------------------------------------------------
# CMS Made Simple is (c) 2004 - 2011 by Ted Kulp (wishy@cmsmadesimple.org)
# CMS Made Simple is (c) 2011 - 2016 by The CMSMS Dev Team
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/simplesitemgr
#-------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#-------------------------------------------------------------------------

if ( !cmsms() ) exit;

// CMSMS Version Check
if ( !function_exists('get_latest_cms_version') ) {
  function get_latest_cms_version() {
    $lastrun = cms_siteprefs::get('_mastersite_lastcheck');
    $lastver = cms_siteprefs::get('_mastersite_lastver');
    if ( $lastrun < time() - 24 * 3600 ) {
      $req = new cms_http_request();
      $req->setTimeout(10);
      $req->setReferrer('');
      $req->execute(CMS_DEFAULT_VERSIONCHECK_URL);
      if ( $req->getStatus() == 200 ) $latest_version = trim($req->getResult());
      $lastver = substr( $latest_version,15 );
      cms_siteprefs::set('_mastersite_lastcheck',time());
      cms_siteprefs::set('_mastersite_lastver',$lastver);
    }
    return $lastver;
  }
}

$latest_cmsms_version = get_latest_cms_version();

// Get module information from the CMS Made Simple Forge
$xml = @simplexml_load_file('http://dev.cmsmadesimple.org/project/latest_files.rss');

$allmoduledetails = array();
$feed_error = '';

if (!empty($xml)) {

	$i = '';
	
	foreach($xml->channel->item as $item) {

		// Get the required information from the RSS feed
		$forge_module_name = substr(strrchr($item->link, "/"), 1);
		$forge_module_version = substr(strrchr($item->title, " "), 1);
		
		$allmoduledetails[$i]['name'] = $forge_module_name;
		$allmoduledetails[$i]['version'] = $forge_module_version;
		
		$i++;
	}
	
} else {
	$feed_error = 1;
}

// Main Query
$query = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_sites ORDER BY site_name ASC';
$dbresult =& $db->Execute($query);

$sites = Array();
asort($sites);

while($dbresult && $row = $dbresult->FetchRow()) {
	$coreitems = Array();
	$addonitems = Array();
	
	$i = count($sites);
	$sites[$i]['site_id'] 				= $row['site_id'];
	$sites[$i]['site_name'] 			= $row['site_name'];
	$sites[$i]['site_admin_url'] 		= $row['site_admin_url'];
	$sites[$i]['site_php_version'] 		= $row['site_php_version'];
	$sites[$i]['site_module_list'] 		= $row['site_module_list'];
	
	//$sites[$i]['site_ssi_version'] 		= $row['site_ssi_version'];
	
	$site_warning = '0';
	$sites[$i]['site_config_writable'] = '';
	if ($row['site_config_writable'] == '1') { 
		$sites[$i]['site_config_writable'] = $this->Lang('prompt_config_writable');
		$site_warning = '1';
	}
	
	$sites[$i]['site_installer_present'] = '';
	if ($row['site_installer_present'] == '1') { 
		$sites[$i]['site_installer_present'] = $this->Lang('prompt_installer_present');
		$site_warning = '1';
	}
	
	$sites[$i]['site_maintenance_mode']	= '';
	if ($row['site_maintenance_mode'] == '1') {
		$sites[$i]['site_maintenance_mode'] = $this->Lang('prompt_maintenance_enabled');
		$site_warning = '1';
	}
	
	$admintheme = cms_utils::get_theme_object();
	$sites[$i]['site_warning'] = '';
	if ( $site_warning == '1' ) $sites[$i]['site_warning'] = $admintheme->DisplayImage('icons/Notifications/1.gif',$this->Lang('prompt_site_warning'),'','','systemicon');
	
	if ( $row['site_ssi_version'] < $this->MinimumInfoVersion() ) {
		$sites[$i]['site_admin_url'] = "";
		$sites[$i]['site_module_list'] = "";
		$sites[$i]['wrong_info_version'] = $this->Lang('wrong_info_version');
	} else {
		$sites[$i]['wrong_info_version'] = '';
		
		$site_cms_version = preg_replace('/\s+/', '', $row['site_cms_version']); // remove redundant space... :/
		$sites[$i]['site_cms_version'] = $site_cms_version;
		
		if(version_compare($site_cms_version, $latest_cmsms_version) < 0) {
			$sites[$i]['upgradable'] = 'yes';
			$sites[$i]['upgrade_status'] = $admintheme->DisplayImage('icons/extra/false.gif',$this->Lang('prompt_site_warning'),'','','systemicon');
		} else {
			$sites[$i]['upgradable'] = '';
			$sites[$i]['upgrade_status'] = $admintheme->DisplayImage('icons/extra/green.gif','','','','systemicon');
		}
		
		// Core Modules Query
		$query2 = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_site_modules 
					WHERE site_id = ' . $row['site_id'] . '
					AND core = 1
					ORDER BY module_name ASC';
		$dbresult2 =& $db->Execute($query2);
		while($dbresult2 && $row2 = $dbresult2->FetchRow()){
			$j = count($coreitems);
			$coreitems[$j]['module_name'] = $row2['module_name'];
			$coreitems[$j]['module_version'] = $row2['module_version'];
		}
		$sites[$i]['coreitems']	= $coreitems;
		
		// Add-on Modules Query
		$query3 = 'SELECT * FROM ' . cms_db_prefix() . 'module_simplesitemgr_site_modules 
					WHERE site_id = ' . $row['site_id'] . '
					AND core = 0
					ORDER BY module_name ASC';
		$dbresult3 =& $db->Execute($query3);
		while($dbresult3 && $row3 = $dbresult3->FetchRow()){
			$k = count($addonitems);
			$addonitems[$k]['module_name'] 	= $row3['module_name'];
			$addonitems[$k]['module_version'] 	= $row3['module_version'];
			$addonitems[$k]['latest_version'] = '';		
			
			foreach($allmoduledetails as $module) {
			
				if( strtolower($module['name']) == strtolower($addonitems[$k]['module_name']) ) {
				
					if(version_compare($addonitems[$k]['module_version'], $module['version']) < 0) {
						$addonitems[$k]['latest_version'] = $module['version'];		
						$addonitems[$k]['updatable'] = 'red';
					}
				}
			}
		}
		$sites[$i]['addonitems'] = $addonitems;
	}
	$sites[$i]['updatesite'] = $this->CreateLink($id,'admin_updatesite',$returnid,$this->Lang('updatesite'),array('site_id' => $row['site_id'])); 
}

$smarty->assign('sites', $sites);
if (!empty($feed_error)) $smarty->assign('feed_error', $feed_error);
$smarty->assign('warning_feed_error', $this->Lang('warning_feed_error'));
$smarty->assign('prompt_latest_version', $this->Lang('prompt_latest_version'));
$smarty->assign('latest_cmsms_version', $latest_cmsms_version);

$smarty->assign('updateall', $this->CreateLink($id,'admin_updateall',$returnid,$this->Lang('updateall'))); 
$smarty->assign('collapse_all', $this->Lang('collapse_all'));
$smarty->assign('expand_all', $this->Lang('expand_all'));
$smarty->assign('prompt_site_warning', $this->Lang('prompt_site_warning'));

$smarty->assign('core_modules', $this->Lang('core_modules'));
$smarty->assign('addon_modules', $this->Lang('addon_modules'));

$smarty->assign('header_name', $this->Lang('header_name'));
$smarty->assign('header_update_status', $this->Lang('header_update_status'));
$smarty->assign('header_php_version', $this->Lang('header_php_version'));
$smarty->assign('theader_module', $this->Lang('theader_module'));
$smarty->assign('theader_installed', $this->Lang('theader_installed'));
$smarty->assign('theader_forge', $this->Lang('theader_forge'));

$smarty->assign('open_site_admin', $this->Lang('open_site_admin'));

echo $this->ProcessTemplate('listsites.tpl');

?>