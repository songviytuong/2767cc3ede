{$formstart}
<input type="hidden" name="{$actionid}cg_activetab" value="handling">{* a little trick for the tab *}
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('handling_module')}:</p>
  <p class="pageinput">
    <select name="{$actionid}handling_module">
    {html_options options=$handling_modules selected=$handling_module}
    </select>
    <br/>{$mod->Lang('info_handling_module')}
  </p>
</div>


<div class="pageoverflow">
  <p class="pagetext"></p>
  <p class="pageinput">
    <input type="submit" name="{$actionid}handling_submit" value="{$mod->Lang('submit')}"/>
  </p>
</div>
{$formend}