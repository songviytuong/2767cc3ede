<?php

class cgecomm_sourcetranslator_products extends cg_ecomm_sourcetranslator
{

    /** deprecated, use by sku **/
    public function get_product_info($product_id)
    {
        $products = cge_utils::get_module('Products');
        $raw_info = $products->get_product_info($product_id);
        if( !$raw_info ) return FALSE;
        return $this->_translate_product($raw_info);
    }

    private function _translate_product($raw_info)
    {
        if( $raw_info['status'] != 'published' ) return FALSE;

        $mod = cge_utils::get_module('CGEcommerceBase');
        $dimensions_fld = $mod->GetPreference('ship_dimensions');

        $obj = new cg_ecomm_productinfo();
        $obj->set_product_id($raw_info['id']);
        $name = '';
        if( isset($raw_info['product_name']) ) $name = $raw_info['product_name'];
        if( !$name && isset($raw_info['name']) ) $name = $raw_info['name'];
        $obj->set_name($name);

        $obj->set_weight($raw_info['weight']);
        $obj->set_sku($raw_info['sku']);
        $obj->set_price($raw_info['price']);
        $obj->set_name($raw_info['product_name']);
        $obj->set_taxable($raw_info['taxable']);
        $obj->set_type(cg_ecomm_productinfo::TYPE_PRODUCT);

        // get fields.
        if( isset($raw_info['fields']) ) {
            // attempt to find subscription info
            $dimensions_set = FALSE;
            foreach( $raw_info['fields'] as &$fld ) {
                if( $fld->type == 'dimensions' && $fld->id == $dimensions_fld && $dimensions_set == FALSE ) {
                    // get the dimensions.
                    $obj->set_dimensions($fld->value['length'],$fld->value['width'],$fld->value['height']);
                    $dimensions_set = TRUE;
                    continue;
                }

                if( $fld->type == 'quantity' ) {
                    $obj->set_qoh($fld->value);
                    continue;
                }

                if( $fld->type == 'subscription' ) {
                    $subscr = new cg_ecomm_productinfo_subscription;
                    $subscr->set_payperiod($fld->value['payperiod']);
                    $subscr->set_deliveryperiod($fld->value['delperiod']);
                    $subscr->set_expiry($fld->value['expire']);
                    $obj->set_subscription($subscr);
                    continue;
                }
            }
        }

        // get attributes.
        if( isset($raw_info['attributes']) ) {
            foreach( $raw_info['attributes'] as $option ) {
                $attr = new cg_ecomm_productinfo_option;
                $attr->id = $option['id'];
                $attr->sku = $option['sku'];
                $attr->text = $option['text'];
                $attr->qoh = $option['qoh'];
                $attr->adjustment = $option['adjustment'];
                $obj->add_option($attr);
            }
        }

        return $obj;
    }

    public function get_product()
    {
        if( $this->get_source() != 'Products' ) throw new CmsInvalidDataException(__CLASS__.' asked to retrieve info for another module');

        $products = cge_utils::get_module('Products');
        if( !$products ) return FALSE;

        $raw_product = null;
        if( ($tmp = $this->get_product_id()) > 0 ) {
            $raw_product = product_ops::get_product($tmp);
        }
        else if( ($tmp = $this->get_sku()) != '' ) {
            $query = new products_query();
            $query['sku'] = $tmp;
            $rs = $query->execute();
            if( $rs->totalrows == 0 ) {
                // main SKU not found, get by option's sku
                $query = new products_query();
                $query['optsku'] = $tmp;
                $rs = $query->execute();
                if( $rs->totalrows == 0 ) {
                    return FALSE;
                }
                else if( $rs->totalrows > 1 ) {
                    // throw exception?
                    return FALSE;
                }
            }
            $raw_product = $rs->get_product(FALSE,TRUE);
        }

        if( !is_array($raw_product) ) return FALSE;

        return $this->_translate_product($raw_product);
    }
} // class

?>