<?php

abstract class cg_ecomm_sourcetranslator
{
  private $_params;

  public function __construct($params)
  {
    if( is_array($params) && count($params) ) {
      $this->_params = $params;
    }
  }

  public function get_source()
  {
    if( is_array($this->_params) && isset($this->_params['source']) ) {
      return $this->_params['source'];
    }
  }

  public function get_sku()
  {
    if( is_array($this->_params) && isset($this->_params['sku']) ) {
      return $this->_params['sku'];
    }
  }

  public function get_product_id()
  {
    if( is_array($this->_params) && isset($this->_params['product_id']) ) {
      return $this->_params['product_id'];
    }
  }

  /** deprecated **/
  abstract public function get_product_info($product_id);

  abstract public function get_product();
}

?>