<?php
/**
* Advanced Config For Captcha Module
*/
/**
* This is an advanced configuration file
* Don't mess with this unless you know what you are doing
* or explicitly told to do so by a developer
*/

/**
* plugins directory
* 
* @var bool
*/
$cmcfg['plugins_dir']     = 'plugins';

?>