<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
/**
 * recaptcha
 * @see class.captchalib.php
 * @see CaptchaLib
 * @package Captcha
*/
namespace Captcha;
/**
 * @extends CaptchaLib
*/
class recaptcha extends \Captcha\cm_plugin_base
{
  
  private $_reCaptcha;
  private $_hl;
	/**
	* Class constructor
	*/
	function __construct()
	{
		// call the parent constructor
		parent::__construct();
		//$this->setPath('recaptcha-php');
    $this->setIncludeFiles( array('recaptchalib.php') );
    $this->setFriendlyName('reCaptcha');	
    $options[] = new plugin_option('site_key' , 'string', '');
    $options[] = new plugin_option('secret' , 'string', '');
    foreach($options as $opt) $this->AddOption($opt);
	}
	
	function getCaptcha($options = array())
	{
		$this->load();
    $mod = cmsms()->GetModuleInstance('Captcha');
    
    if( !is_object($mod) ) return false;
    $hl_data_file = cms_join_path( 
                                    $mod->GetModulePath(),
                                    'data',
                                    'hl.data'
                                  );
                                  
    require($hl_data_file);
    
    if(empty($options['site_key']))
    {
      $mod->Audit(0, 'recaptcha plugin', 'site_key is missing'); 
      return false; 
    }
    
    $curlang = \CmsNlsOperations::get_current_language();

    if( !in_array($curlang, $hl) )
    {
      if( strlen($curlang) == 2)
      {
        $curlang = 'en';
      }
      else
      {
        $curlang = substr($curlang, 0, 2);
        if( !in_array($curlang, $hl) ) $curlang = 'en';
      }
    }

    $html = '<div class="g-recaptcha" data-sitekey="' . $options['site_key'] . '"></div>
      <script type="text/javascript"
          src="https://www.google.com/recaptcha/api.js?hl=' . $curlang .'">
      </script>';
    
    return $html;
	}
  
  function checkAvailability() 
  {
    $this->setIsAvailable(TRUE);
    return TRUE;
  }
	
	function checkCaptcha($input, $options = array()) 
	{
		$this->load();
    if(empty($options['secret'])) 
    {
      $mod->Audit(0, 'recaptcha plugin', 'secret is missing'); 
      return false; 
    }
    
    $this->_reCaptcha = new \ReCaptcha($options['secret']);
        
    if ($_POST["g-recaptcha-response"]) 
    {
      $resp = $this->_reCaptcha->verifyResponse(
                                                  $_SERVER["REMOTE_ADDR"],
                                                  $_POST["g-recaptcha-response"]
                                                );
      
      return $resp->success;
    }
    
    return false;
	}
	
}
?>
