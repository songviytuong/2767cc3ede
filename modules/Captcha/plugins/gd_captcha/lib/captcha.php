<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
/***
* File : captcha.php
* Description : Creating a captha image and
* store the text in a session variable
* Author : Kiran Paul V.J. aka kiranvj aka human
* License : Freeware
* Last update : 17-Aug-2007
*/

if (isset($_GET['sess']))
session_name($_GET['sess']);

// Initialize session data
session_start();
// all images in this file is of PNG format,
// there is not specific reason for that.
// this line is used to set the header of the page
// setting the header to image/png means this page
// contians data of image->PNG type
header("Content-type: image/png");

// create a new image resource from a file
$captchaImage = imagecreatefrompng("captcha.png")
or die("Cannot Initialize new GD image stream");

//Loads a new font from a file
$captchaFont = imageloadfont("anonymous.gdf");

// Create the captcha text with some manipulation
$captchaText = substr(md5(uniqid('')),-9,9);

// stores the captha text in a session variable
$_SESSION['session_captchaText'] = $captchaText;

// Allocating color for captcha text to be used
// in imagestring function
$captchaColor = imagecolorallocate($captchaImage,0,0,0);

// drawing the string
imagestring($captchaImage,$captchaFont,15,5,$captchaText,$captchaColor);

// Outputs the captha image in PNG format.
// You can change the image format using
// imagejpeg,imagegif ,imagewbmp etc.
imagepng($captchaImage);

// frees memory
imagedestroy($captchaImage);

?>