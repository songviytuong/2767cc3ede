<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
/**
 * hn_captcha
 * @see class.captchalib.php
 * @see CaptchaLib
 * @package Captcha
*/
namespace Captcha;

/**
 * @extends CaptchaLib
*/
class phpcaptcha extends \Captcha\cm_plugin_base
{
	/**
	* Class constructor
	*/
	function __construct()
	{
		// call the parent constructor
		parent::__construct();
    $this->setFriendlyName('PHP Captcha');
		$this->setIncludeFiles( array('php-captcha.inc.php') );
    
		$options[] = new plugin_option('width'          , 'int'   , '200');
		$options[] = new plugin_option('height'         , 'int'   , '60');
		$options[] = new plugin_option('display_shadow' , 'bool'  , '0');
		$options[] = new plugin_option('use_color'      , 'bool'  , '1');
		$options[] = new plugin_option('owner_text'     , 'string', '');
		$options[] = new plugin_option('alt'            , 'string', 'Visual Captcha');
    
    foreach($options as $opt) $this->AddOption($opt);
	}
	
	function getCaptcha($options = array())
	{
		$this->load($options);
		$imagefile = cms_join_path($this->getImagePath(), 'php-captcha.jpg');
    
		$this->object->Create($imagefile);
		
		$url = $this->getImageUrl() . '/php-captcha.jpg';
		
		return '<img src="'. $url. '" width="'. $options['width'] . '" height="' . $options['height'] . '" alt="' . $options['alt'] . '" />';
	}
	
	function checkCaptcha($input, $options = array()) 
	{
		$this->load($options);
		return \PhpCaptcha::Validate($input);
	}
	
	function load($options = array())
	{
		// Call the parent class load method (includes necessary files)
		parent::load();
		
		$aFonts = array(
			cms_join_path($this->getFontPath(), 'FreeSans.ttf'), 
			cms_join_path($this->getFontPath(), 'FreeSerif.ttf')
		);
    
		if(!isset($this->object))
		{
			$this->object = new \PhpCaptcha($aFonts, $options['width'], $options['height']);
			$this->object->DisplayShadow($options['display_shadow'] == '1' ? true : false);
			$this->object->UseColour($options['use_color'] == '1' ? true : false);
			if ($options['owner_text'] != '')
			{
				$this->object->SetOwnerText($options['owner_text']);
			}
		}
	}
  
  function NeedsInputField()
  {
    return TRUE;
  }          
}
?>