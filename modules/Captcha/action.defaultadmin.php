<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckAccess($id, $params, $returnid) ) return;

$active_tab = isset($params['active_tab']) ? $params['active_tab'] : 'general';

$plugins = $this->getAvailableLibs(); 
$active_lib = $this->getActiveLib();

$image_path = $active_lib->getImagePath(); 

if( empty($plugins) )
{
  // No Captcha libraries are available
  $this->addMessage('No Captcha libraries available');
}

$tabs_obj = new stdClass();
$tabs_obj->StartTabsHeaders   = $this->StartTabHeaders();
$tabs_obj->EndTabHeaders      = $this->EndTabHeaders();
$tabs_obj->StartTabContent    = $this->StartTabContent();
$tabs_obj->EndTabContent      = $this->EndTabContent();

# the tabs
$tabs_obj->tabs = array();

# general tab
$tab = new stdClass();
$tab->name = 'general';
$tab->friendlyname = $this->Lang('title_general');
$tab->TabHeader = $this->SetTabHeader( $tab->name, $tab->friendlyname, ($active_tab == $tab->name) );
$tab->Start = $this->StartTab($tab->name, $params);
$tab->FormStart = $this->CreateFormStart($id, 'save_general_admin', $returnid);
$tab->FormEnd = $this->CreateFormEnd();
$tab->FormSubmit = $this->CreateInputSubmit( $id, 'submit', lang('submit') );

$tab->inputs = array();

# active lib fieldset
$input = new stdClass();
$input->type = 'fieldset_start';
$input->label = $this->Lang('legend_active_lib');
$input->data = '';
$tab->inputs[] = $input;

$input = new stdClass();
$input->type = 'dropdown';
$input->name = 'select_captchalib';
$input->label = $this->Lang('legend_active_lib');
$input->data = \Captcha\utils::build_plugins_dropdown_array();
$input->selected = $active_lib->name;
$tab->inputs[] = $input;

$input = new stdClass();
$input->type = 'string';
$input->label = '';

$test = $active_lib->getOptions();

if( !empty($test) )
{
  $linkparms = array('active_tab' => $active_lib->getName() );
  $input->data = $this->CreateLink($id, 'defaultadmin', $returnid, 'options', $linkparms);
}
else
{
  $input->data = '';
}

$tab->inputs[] = $input;

$input = new stdClass();
$input->type = 'fieldset_end';
$input->label = '';
$input->data = '';
$tab->inputs[] = $input;
# active lib fieldset end


// Enable PEAR Captcha selectbox
if (isset($plugins['pear']))
{
  $input = new stdClass();
  $input->type = 'fieldset_start';
  $input->label = $this->Lang('legend_pear');
  $input->data = '';
  $tab->inputs[] = $input;
  
  $input = new stdClass();
  $input->type = 'checkbox';
  $input->label = $this->Lang('label_enable_pear');
  $input->name = 'enable_pear';
  $input->data = $this->getPreference('enable_pear', 1);
  $tab->inputs[] = $input;
  
  $input = new stdClass();
  $input->type = 'fieldset_end';
  $input->label = '';
  $input->data = '';
  $tab->inputs[] = $input;
}


# cache fieldset
$input = new stdClass();
$input->type = 'fieldset_start';
$input->label = $this->Lang('legend_cache');
$input->data = '';
$tab->inputs[] = $input;

$input = new stdClass();
$input->type = 'string';
$input->label = $this->Lang('cache_directory') . ': ';
$input->data = $image_path;
$tab->inputs[] = $input;

$input = new stdClass();
$input->type = 'string';
$input->label = $this->Lang(
                            'msg_cache_overview', 
                            $this->getNumberOfCachedImages($image_path)
                          );
$input->data = '';
$tab->inputs[] = $input;

$input = new stdClass();
$input->type = 'checkbox';
$input->label = $this->Lang('label_clear_cache');
$input->name = 'clear_cache';
$input->data = 0;
$tab->inputs[] = $input;

$input = new stdClass();
$input->type = 'fieldset_end';
$input->label = '';
$input->data = '';
$tab->inputs[] = $input;

# cache fieldset end

#available fonts //<- not sure why it was removerd from the template... to revisit at some point (JM)

$tab->End = $this->EndTab();

$tabs_obj->tabs[] = $tab;
# end of general

# plugins tabs
foreach($plugins as $lib)
{
  $options = $lib->getOptions();
  
  if (! empty($options))
  {
    if($lib->getName() == 'pear' && $this->GetPreference('enable_pear') == '0') continue;
    
    $tab = new stdClass();
    $tab->name = $lib->getName();
    $tab->friendlyname = $lib->getFriendlyName();
    $tab->TabHeader = $this->SetTabHeader( $tab->name, $tab->friendlyname, ($active_tab == $tab->name) );
    
    $tab->Start = $this->StartTab($tab->name, $params);
    $formparams = array( 'libname' => $lib->getName() );
    $tab->FormStart = $this->CreateFormStart($id, 'save_captcha_options', $returnid, 'post', '', false, '', $formparams);
    $tab->inputs = array();
    
    foreach($options as $option)
    {
      $input = new stdClass();
      $input->label = $option->getName();
      $input->name = $option->getName();
      
      switch ($option->type)
      {
        case 'string':
        case 'int':
          $input->type = 'text';
          $input->name = 'options[' . $option->getName() . ']';
          $input->data = $this->GetPreference('captchalib_' . $lib->getName() . '_' . $option->getName(), $option->getDefault());
          break;
        case 'bool':
          $input->type = 'checkbox';
          $input->name = 'options[' . $option->getName() . ']';
          $input->data = $this->GetPreference('captchalib_' . $lib->getName() . '_' . $option->getName(), $option->getDefault());
          break;
      }
      
      $tab->inputs[] = $input;
    }
    
    $tab->FormEnd = $this->CreateFormEnd();
    $tab->FormSubmit = $this->CreateInputSubmit( $id, 'submit', lang('submit') );
    $tab->End = $this->EndTab();

    $tabs_obj->tabs[] = $tab;
  }
}

$this->SetError( $this->getErrors() );  

$smarty->assign('tabs_obj', $tabs_obj);

echo $this->ProcessTemplate('defaultadmin.tpl');
?>