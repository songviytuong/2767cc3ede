<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission('Modify Site Preferences') ) return;

if (isset($params['cancel']))
{
  $this->Redirect($id, 'defaultadmin', $returnid, $params);
} 
$this->errors = array_merge($this->errors, \Captcha\utils::get_GD_errors() );
$plugins = $this->getAvailableLibs(); 
$libname = $params['libname'];
$submitted_options = $params['options'];
$plugin = $plugins[$libname]; 

if (! isset($params['reset']))
{
	$changes = false;
	
	foreach ($submitted_options as $option_name => $value)
	{
		$option = $plugin->options[$option_name];
		$default = $option->getDefault();
		$pref_name = 'captchalib_' . $libname . '_' . $option_name;
		$current = $this->GetPreference($pref_name, $default);
		
		switch ($option->getType())
		{
			case 'int':
			case 'string':
				
				if ($current != $default)
				{ // there's a pref value in the db
					if ($value != $current)
					{
						if ($value == $default)
						{
							$this->RemovePreference($pref_name);
							$changes = true;
						}
						else
						{
							$this->SetPreference($pref_name, $value);
							$changes = true;
						}
					}
				}
				else
				{
					if ($value != $default)
					{
						$this->SetPreference($pref_name, $value);
						$changes = true;
					}
				}
				break;
			case 'bool':
				if ($current == '0')
				{
					$this->setPreference($pref_name, '1');
					$changes = true;
				}
				break;
		}
	}
	
	foreach ($plugin->getOptions() as $liboption)
	{
		if ($liboption->getType() == 'bool')
		{
			$default = $liboption->getDefault();
			$pref_name = 'captchalib_' . $libname . '_' . $liboption->getName();
			$current = $this->GetPreference($pref_name, $default);

			if (!isset($params['options'][$liboption->name]) && $current == '1')
			{
				if ($default == '0')
				{
					$this->RemovePreference($pref_name);
				}
				elseif ($default == '1')
				{
					$this->SetPreference($pref_name, '0');
				}
				$changes = true;
			}
		}
	}
	
}

if (isset($params['reset']))
{
	foreach($lib->options as $option)
	{
		$pref_name = 'captchalib_' . $libname . '_' . $option->getName();
		$this->RemovePreference($pref_name);
	}
	$this->addMessage($this->Lang('msg_restored_defaults'));
}
elseif ($changes)
{
	$this->addMessage($this->Lang('msg_changes'));
}
else
{
	$this->addMessage($this->Lang('msg_changes'));
}

$this->SetMessage( $this->getMessages() );
$this->SetError( $this->getErrors() ); 

$params = array('active_tab' => $libname);
$this->Redirect($id, 'defaultadmin', $returnid, $params);

?>