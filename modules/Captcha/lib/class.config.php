<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
/**
* Captcha config class
* uses Singleton design patterns
* Implements ArrayAccess
*
* @version 1.1
* @author Jo Morg
*/
namespace Captcha;
class config implements \ArrayAccess
{
  
  /**
  * @access private
  */
  private static $_data = array();
   
  /**
  * The instance
  * @access private
  */
  private static $_instance;
  
  /**
  * CMSMS own config
  * @access private
  */
  private static $_CMSMS_CFG = NULL;
  
  /**
  * Private constructor to prevent it being created directly
  * @access private
  */
  private function __construct(){}
    
  /**
  * GetInstance method used to access the object
  * @access public
  * @return 
  */
  public static function GetInstance()
  {
    if( !isset(self::$_instance) )
    {
      self::$_instance = new self;
      self::$_CMSMS_CFG = \cmsms()->GetConfig();
      $rootdir = dirname(  dirname(  dirname( dirname(__FILE__) ) ) );
      $file = cms_join_path( dirname( dirname(__FILE__) ), 'includes', 'cm_cnf.inc' );
      
      if( file_exists($file) )
      {
        include($file); 
        self::$_data = $cmcfg;
      }
      
    }
    
    return self::$_instance;
  }
  
  /**
  * prevent cloning of the object: issues an E_USER_ERROR if this is attempted
  */
  public function __clone()
  {
    trigger_error( 'Cloning the Config is not permitted', E_USER_ERROR );
  }
  
  /**
  * Get a data by key
  *
  * @param string The key data to retrieve
  * @access public
  */
  public function &__get ($key) 
  {
    if(self::$_CMSMS_CFG->offsetExists($key)) return self::$_CMSMS_CFG[$key];
    return self::$_data[$key];
  }

  /**
  * Assigns a value to the specified data
  *
  * @param string The data key to assign the value to
  * @param mixed  The value to set
  * @access public
  */
  public function __set($key, $value) 
  {
    trigger_error( 'Read Only', E_USER_ERROR );
  }
  
  public function offsetExists($key) 
  {
    return ( isset(self::$_data[$key]) || self::$_CMSMS_CFG->offsetExists($key) );
  }

   public function offsetGet($key) 
   {
     if(self::$_CMSMS_CFG->offsetExists($key)) return self::$_CMSMS_CFG[$key];
     
      if(isset(self::$_data[$key])) 
      {
         return self::$_data[$key];
      }

      return null;
   }

   public function offsetSet($key, $value) 
   {
      trigger_error( 'Read Only', E_USER_ERROR );
   }

   public function offsetUnset($key) 
   {
      trigger_error( 'Read Only', E_USER_ERROR );
   }
   
   public static function get($key)
  {
    if(self::$_CMSMS_CFG->offsetExists($key)) return self::$_CMSMS_CFG[$key];
    
    if( self::exists($key) )
    {
      return self::$_data[$key];
    }
  }

  public static function set($key, $value)
  {
    trigger_error( 'Read Only', E_USER_ERROR );
  }

  public static function erase($key)
  {
    trigger_error( 'Read Only', E_USER_ERROR );
  }
}
?>