<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
namespace Captcha;
/**
 * A PHP Class representing a Captcha module plugin
 * Base class for different available captcha PHP libraries
 * @package Captcha
*/
class cm_plugin_base 
{
	
	/**
	* Can be used to keep a reference to a captcha library class object
	*/
	var $object;
	
	/**
	* The shortname of the Captcha Library
	* Must be unique for every library and a class.captchalib_{shortname}.php file must be present containing a 
	* @var string The name of the Captcha Library
	*/
	var $name;
	
	/**
	* @var string	The friendly name of the Captcha Library
	*/
	var $friendlyname;
	
	/**
	* @var array	Array of file names that need to be included
	*/
	var $includefiles;
	
	/**
	* @var string	The (include) path (if needed) for the Captcha Library
	*/
	var $path;
    
    /**
    * @var array   CMS Config
    */
    var $cmsconfig;
	
	/**
	* @var string	The path to write captcha images to if needed
	*/
	var $imagepath;
	
	/**
	* @var string	The url to the captcha images directory
	*/
	var $image_url;
	
	/**
	* @var string	The folder where fonts are stored
	*/
	var $fontpath;
	
	/**
	* @var bool	Whether the Captcha Library is available on the system
	*/
	var $is_available;
	/**
	* @var bool	Whether the Captcha Library is enabled (true) or disabled (false)
	*/
	var $is_enabled;
	
	var $options;
	
	/**
	* The class constructor
	*/
	function __construct() 
	{
		$this->object = NULL;
    $tmp = explode( '\\', get_called_class() );
    $this->setName($tmp[count($tmp) - 1]);
    $mod = \cmsms()->GetModuleInstance('Captcha');
    $this->cmsconfig = \Captcha\config::GetInstance();
		$this->setFriendlyName( utils::friendly_from_class_name( $this->name) );
    $path = cms_join_path(
                            $mod->GetModulePath(),
                            $this->cmsconfig['plugins_dir'],
                            $this->getName()
                          );
                          
		$this->setPath($path);    
    $relative_temp = str_replace($this->cmsconfig['root_path'], '', TMP_CACHE_LOCATION);
    $relative = str_replace(DIRECTORY_SEPARATOR, '/', $relative_temp);
    $this->setImagePath(TMP_CACHE_LOCATION);
    $this->setImageUrl($this->cmsconfig['root_url'] . $relative);
		$this->setIncludeFiles( array() );
		$this->setIsAvailable(FALSE);
		$this->setIsEnabled(TRUE);
		$this->options = array();
	}


/* Start Get and Set methods */

	/**
	* Set method for the is_available property
	*/    
	function setIsAvailable($bool)
	{
	  $this->is_available = $bool;
	}
	
	/**
	* Get method for the is_available property
	* @return bool
	*/    
	function getIsAvailable()
	{
		return $this->is_available;
	}
	
	/**
	* Set method for the is_enabled property
	*/    
	function setIsEnabled($bool)
	{
	$this->is_enabled = $bool;
	}
	
	/**
	* Get method for the is_enbled property
	* @return bool
	*/    
	function getIsEnabled()
	{
		return $this->is_enabled;
	}
	
	/**
	* Set method for the name property
	* @param string name
	*/
	function setName($name)
	{
		$this->name = $name;
	}
	
	/**
	* Get method for the name property
	* @return string
	*/    
	function getName()
	{
		return $this->name;
	}
	
	/**
	* Set method for the friendlyname property
	* @param string Friendly name
	*/
	function setFriendlyName($name)
	{
		$this->friendlyname = $name;
	}
	
	/**
	* Get method for the friendlyname property
	* @return string Friendly name
	*/    
	function getFriendlyName()
	{
		return $this->friendlyname;
	}
	
	/**
	* Set method for the includefile property
	* @param string filename
	*/
	function setIncludeFiles($files_array)
	{
		$this->includefiles = $files_array;
	}
	
	/**
	* Get method for the includefile property
	* @return string
	*/
	function getIncludeFiles()
	{
		return $this->includefiles;
	}
	
	/**
	* Set method for the path property
	* @param string path
	*/
	function setPath($path)
	{
		$this->path = $path;
	}
	
	/**
	* Get method for the path property
	* @return string
	*/
	function getPath()
	{
		return $this->path;
	}
	
	/**
	* Set method for the imagepath property
	* @param string path
	*/
	function setImagePath($path)
	{
		$this->imagepath = $path;
	}
	
	/**
	* Get method for the imagepath property
	* @return string
	*/
	function getImagePath()
	{
		return $this->imagepath;
	}
	
	
	/**
	* Set method for the image_url property
	* @param string url
	*/
	function setImageUrl($url)
	{
		$this->image_url = $url;
	}
	
	/**
	* Get method for the image_url property
	* @return string
	*/
	function getImageUrl()
	{
		return $this->image_url;
	}
	
	/**
	* Set method for the fontpath property
	* @param string path
	*/
	function setFontPath($path)
	{
		$this->fontpath = $path;
	}
	
	/**
	* Get method for the imagepath property
	* @return string
	*/
	function getFontPath()
	{
		return $this->fontpath;
	}

/* End Get and Set methods */

	/**
	* Alias for the get method getIsAvailable()
	* @return bool
	* @see getIsAvailable()
	*/
	function isAvailable()
	{
		return $this->getIsAvailable();
	}
	
	/**
	* Alias for the get method getIsEnabled()
	* @return bool
	* @see getIsEnabled()
	*/
	function isEnabled()
	{
		return $this->getIsEnabled();
	}
	
	/**
	* "Negative" Alias for the get method getIsEnabled()
	* @return bool
	* @see getIsEnabled()
	*/
	function isDisabled()
	{
		return ( !$this->getIsEnabled() );
	}
	
	/**
	* Enables the Captcha library (within the module/application)
	* @see setIsEnabled()
	*/
	function enable()
	{
		$this->setIsEnabled(TRUE);
	}
	
	/**
	* Disables the Captcha library (within the module/application)
	* @see setIsEnabled()
	*/
	function disable()
	{
		$this->setIsEnabled(FALSE);
	}
	
	function &getOptions()
	{
		return $this->options;
	}
  
	function setOptions($options)
	{
		$this->options = $options;
	}
	
	function addOption(&$option)
	{
		$this->options[$option->name] = $option;
	}


/* functions below should be overridden by child classes */

	/**
	* Checks availabiltity of the Captcha Library.
	* Is supposed to be overridden by a child class.
	* @return bool	Whether the library is available on the system
  * @deprecated - to be removed at any time
	*/
	function checkAvailability() 
  {
    $missing_files = FALSE;
    
    foreach ($this->getIncludeFiles() as $includefile)
    {
      $location = cms_join_path(
                                  $this->getPath(),
                                  'lib', 
                                  $includefile
                                );
      
      if ( !is_readable($location) ) 
      {
        $missing_files = TRUE;
        break;
      }
    }
    
    $this->setIsAvailable(!$missing_files);
    return !$missing_files;
	}
	
	/**
	* Includes necessary library (class) files
	*/
	function load()
	{
		foreach ($this->getIncludeFiles() as $includefile)
		{
			$location = cms_join_path(
                                  $this->getPath(),
                                  'lib', 
                                  $includefile
                                );
                                  
			require_once($location);
		}
	}
	
	/**
	* returns the HTML to show the Captcha image
	*/
	function getCaptcha( $options=array() ) 
  {
		return '';
	}
	
	/**
	* Checks the user input.
	* @param string 
	* @return bool
	*/
  function checkCaptcha($input)
  {
    return FALSE;
  }	
  
  /**
  * return whether the plugin needs an input field on the form
  * note: this may be deprecated anytime soon if the template system get's in place 
  */
  function NeedsInputField()
  {
    return FALSE;
  }    
}

?>