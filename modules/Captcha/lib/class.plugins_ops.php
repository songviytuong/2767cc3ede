<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
namespace Captcha;

class plugins_ops
{
  private static $_plugins = array();
  private static $_loaded_plugins = array();
  private static $_plugins_dir;
  private static $_config;
  private static $_cm;
  private static $_initialized = FALSE;
  
  private static function _initialize()
  {
    if(!self::$_initialized)
    {
      self::$_config = \Captcha\config::GetInstance();
      self::$_cm = \cmsms()->GetModuleInstance('Captcha');
      self::$_initialized = TRUE; 
    }
  }
  
  public static function get_plugins_dir()
  {
    self::_initialize(self::$_config['plugins_dir']);
    
    if( empty(self::$_plugins_dir) )
    {

      self::$_plugins_dir = cms_join_path(
                                            self::$_cm->GetModulePath(),
                                            self::$_config['plugins_dir'] 
                                          ); 
      
      try
      {
        if ( !is_dir(self::$_plugins_dir) ) throw new Exception('Captcha library directory doesn\'t exists');
      }
      catch (Exception $e) 
      {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
        self::$_plugins_dir = NULL;
      }
    }
    
    return self::$_plugins_dir;
    
  }
  
  public static function scanDir($dir = './', $sort = false)
  {
    $dir_open = opendir($dir);
    
    if (! $dir_open)
    {
      return false;
    }
    
    $files = array();
    while ($dir_content = readdir($dir_open))
    {
      if( preg_match( "/^\./", $dir_content)) // ignore dot files
      {
        continue;
      }
      $files[] = $dir_content;
    }
    
    if(is_array($files))
    {
      $sort == true ? rsort($files, SORT_STRING) : sort($files, SORT_STRING);
    }
    
    return $files;
  }
  
  private static function _build_list($dir = '')
  {
    if( empty($dir) ) $dir = self::get_plugins_dir();
     
    if( empty(self::$_plugins) )
    { 
      $tmp = self::scanDir($dir);
            
      foreach($tmp as $one)
      {
        $path = cms_join_path($dir, $one);
        if( !is_dir($path) ) continue;
        $fn = cms_join_path($path, 'cm_plugin.' . $one . '.php');
        if( !is_readable($fn) ) continue;
        self::$_plugins[$one] = $fn;
      }
    }
    
    return !empty(self::$_plugins);
  }
  
  public static function exists($name)
  {
    return array_key_exists($name, self::$_plugins);
  }  
  
  public static function is_loaded($name)
  {
    return array_key_exists($name, self::$_loaded_plugins);
  }
  
  public static function list_plugins($dir = '')
  {
    try
    {
      if (! self::_build_list($dir) ) throw new \Exception('No Captcha library class files found');
    }
    catch (Exception $e) 
    {
      echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
    
    return self::$_plugins;
  }
  
  public static function load_plugin($name)
  {
    if( empty(self::$_plugins) )  self::load();
    if( !self::exists($name) )    return;
    if( self::is_loaded($name) )  return;
        
    require_once self::$_plugins[$name];
    $class = "\\Captcha\\$name";
    self::$_loaded_plugins[$name] = new $class();
    $fn = cms_join_path(self::$_cm->GetModulePath(), 'fonts');
    self::$_loaded_plugins[$name]->setFontPath($fn);

    // PEAR library availability
    if(
       self::$_loaded_plugins[$name]->getName() == 'pear' 
       && self::$_cm->getPreference('enable_pear') == FALSE
      )
    {
      self::$_loaded_plugins[$name]->disable();
    }
    
    self::$_loaded_plugins[$name]->checkAvailability();
  }
  
  static function load($all = FALSE) 
  {
    if( self::_build_list() )
    {
      foreach(array_keys(self::$_plugins ) as $one)
      {
        self::load_plugin($one);
        
        if(!$all)
        {
          self::$_loaded_plugins[$one]->checkAvailability();
           
          if(
              !self::$_loaded_plugins[$one]->isAvailable() ||
              !self::$_loaded_plugins[$one]->isEnabled()
            )
            {
              unset(self::$_loaded_plugins[$one]);
            }  
        }

      }
    }     
  }
  
  static function get_loaded()
  {
    return self::$_loaded_plugins;
  } 
}
?>