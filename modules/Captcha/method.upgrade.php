<?php
#-------------------------------------------------------------------------
# Module: Captcha - Enables CAPTCHA support for use in other modules.
#-------------------------------------------------------------------------
# Version 0.5.5
# maintained by Fernando Morgado AKA Jo Morg & Mark Reed (mark) AKA DrCss
# since 2013
#-------------------------------------------------------------------------
# Original Author: Dick Ittmann
#-------------------------------------------------------------------------
#
# Captcha is a CMS Made Simple module that enables the web developer to create
# multiple lists throughout a site. It can be duplicated and given friendly
# names for easier client maintenance.
#
#-------------------------------------------------------------------------
# BEGIN_LICENSE
#-------------------------------------------------------------------------
#
# This file is part of Captcha
# Captcha program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Captcha program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
# END_LICENSE
#-------------------------------------------------------------------------
switch($oldversion)
{
  case '0.1-beta1':
    // create a preference for the font dir
    $this->SetPreference('font_path', cms_join_path($this->GetModulePath(), 'fonts'));
  case '0.1':
  case '0.1.1':
    if($this->GetPreference('active_lib') == 'b2evo')
    {
      $this->SetPreference('active_lib', 'hncaptcha');
    }
  case '0.2':
  case '0.2.1':  
  case '0.5.2':
    if($this->GetPreference('active_lib') == 'hncaptcha')
    {
      $this->SetPreference('active_lib', 'hn_captcha');
    }
    
    \Captcha\utils::DeleteFilesFromFileList($newversion);
}
  
// put mention into the admin log
$this->Audit( 0, $this->Lang('friendlyname'), $this->Lang( 'upgraded', $this->GetVersion() ) );
?>
