<ul>
  <li>
    <p><b>Version 0.5.5</b></p>
    <ul>
      <li>Fixes an issue with the loading of Captcha libraries;</li>
    </ul> 
  </li> 
  <li>
    <p><b>Version 0.5.4</b></p><br>
      <div class="warning">
        <p><strong>NOTE:</strong> From this release on <strong>Captcha</strong> isn't supporting <strong>CMS Made Simple versions of branch 1.x</strong>. It may work, but there will be no support if it doesn't.</p>
      </div>
    <ul>
      <li>Fixes BR #11262;</li>
      <li>Implemented FR #11052: NeedsInputField() delegated to plugins, old method was a quick fix but a poor one at that;</li>
      <li>A few stability fixes;</li>
    </ul> 
  </li> 
  <li>
    <p><b>Version 0.5.3</b><br>
      <div class="warning">
        <p><strong>NOTE:</strong> unless there are critical issues, this will be the last release of <strong>Captcha</strong> to support <strong>CMS Made Simple versions of branch 1.x</strong>.</p>
      </div>
    </p>
    <ul>
      <li>The code base has been updated in terms of efficiency;</li>
      <li>The code base has been cleaned up and updated to support PHP versions up to 7.x;</li>
      <li>Minimum PHP version required is now 5.3.0;</li>
      <li>Changed the mechanism used for different CAPTCHAS libraries:
        <ul>
          <li>these are now plugins;</li>
          <li>there is an easier support to 3rd party plugins;</li>
          <li>this change also paved the way to an API to future support of CAPTCHAS implemented in 3rd party modules;</li>
        </ul>
      </li>
      <li>A few stability fixes;</li>
    </ul> 
  </li>    
  <li>
    <p><b>Version 0.5.2</b></p>
    <ul>
      <li>Fixed the bug that rendered reCaptcha unusable;</li>
    </ul> 
  </li>  
  <li>   
    <p><b>Version 0.5.1</b></p>
    <ul>
      <li>Updated reCaptcha to version 2.0 (thanks jce76350 for the headsup);</li>
      <li>Note: in this version reCaptcha is unusable because of a major flaw;</li>
    </ul>
    <p><b>Version 0.5.0</b></p>
    <ul>
      <li>Minor fixes to work with CMSMS 2.0;</li>
      <li>Updated reCaptcha to version 1.11;</li>
      <li>Added a new API method to avoid duplicated inputs with reCaptcha;</li>
      <li>Added a new API method to return the currently used <strong>library name</strong>;</li>
      <li>Fixed all known notices (thanks jce76350);</li>
      <li><strong>Help</strong> and <strong>Change Log</strong> where moved from lang file to separate templates;</li>
      <li>Better organized help;</li>
      <li>Improved security (thanks RolfTj);</li>
      <li>Lack of reCaptch keys just fail to vadidate instead of breaking the page (thanks RolfTj);</li>
    </ul>
  </li>  
  <li>
    <p><b>Version 0.4.6</b> - <em>undocumented</em></p>
  </li>
  <li>
    <p><b>Version 0.4.5</b> - (calguy1000)</p>
    <ul>
      <li>Fixes for CMSMS 1.10</li>
    </ul>
  </li>
  <li>
    <p><b>Version 0.4.2</b> - (calguy1000)</p>
    <ul>
      <li>Fixes a minor notice or two due to changes in PHP behaviour.</li>
    </ul>
  </li>
  <li>
    <p><b>Version 0.4.1</b> - (calguy1000)</p>
    <ul>
      <li>Fixes minor validation issues, and irritating bugs.</li>
    </ul>
  </li>

    <li>
      <p><b>Version 0.3.2</b></p>
      <ul>
        <li>Updated hn_captcha to version 1.5.0</li>
        <li>Added a simple GD based captcha that doesn't use Freetype</li>
      </ul>
    </li>
    <li>
      <p><b>Version 0.3.1</b> (calguy1000)</p>
      <ul>
          <li>Now use the 'Modify Site Preferences' Permission</li>
          <li>Changed the cache directory to tmp/cache</li>
      </ul>
    </li>
    <li>
      <p><b>Version 0.3</b></p>
      <ul>
        <li>Switched from hn_captcha to hn_captcha_X1 which has garbage collection</li>
        <li>Added support for the PhpCaptcha library</li>
        <li>Captcha library specific settings can now be managed from the module admin</li>
        <li>Captcha libraries are autoloaded from the available class files</li>
        <li>Captcha libraries are not loaded untill needed</li>
      </ul>
    </li>
    <li>
      <p><b>Version 0.2.1</b></p>
      <ul>
        <li>Fixes use of the PHP parse_url function in a way that only works on PHP5</li>
      </ul>
    </li>
    <li>
      <p><b>Version 0.2</b></p>
      <ul>
        <li>Fixes for issue with captcha image not showing up on windows servers in some cases.</li>
        <li>Changes to make sure the captcha image is showed correctly when hn_captcha is used and $_SERVER['DOCUMENT_ROOT'] is not set correctly.</li>
        <li>Removed support for the b2evo library. It is derived from hn_captcha and doesn't offer any extra functionality.</li>
        <li>Use an unmodified version of the hn_captcha library (only kept some spelling fixes).</li>
      </ul>
    </li>
    <li>
      <p><b>Version 0.1.1</b></p>
      <ul>
        <li>Prevent a warning that occurs if allow_call_time_pass_reference is set to false in php.ini.</li>
      </ul>
    </li>
    <li>
      <p><b>Version 0.1</b></p>
      <ul>
        <li>Initial Release.</li>
      </ul>
    </li>
</ul>