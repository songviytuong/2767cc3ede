{* Tabs Headers *}
<div id="page_tabs">
  <div id="general">
    General
  </div>    
  <div id="api">
    API
  </div>
  <div id="pear">
    PEAR Text / Captcha
  </div>  
  <div id="new">
    What's New
  </div>       
  <div id="credits">
    Credits
  </div>
</div>
{* Tabs Headers *}

<div class="clearb"></div>

{* Tabs Contents *}
<div id="page_content">

{*************************************************************************************}
{* General *}
  <div id="general_c">
    <h3>What Does This Do?</h3>
    <p>Enables captcha support for use in other modules.</p>
    <p>See <a href="http://www.wikipedia.org/wiki/Captcha">http://www.wikipedia.org/wiki/Captcha</a> for more information about Captcha challenge-response tests.</p>
    <p>
    <strong>Your PHP version must have GD installed with support for JPEGs and TrueType fonts</strong>. For more information, see <a href="http://www.php.net/image/">PHP Image Functions</a>.<br />
    </p>
        
    <h3>Webserver permissions</h3>
    <p>
    The server process must have write access on the folder cache of CMS Madesimple (tmp/cache of your installation folder).<br />
    On some servers (depending on server settings) the umask in the CMS Made Simple Global Settings may have to be changed; try changing it from 022 to 002 if the captcha images are created but not shown.
    </p>
    <h3>How Do I Use It?</h3>
    <div class='warning'>If you use the reCaptcha library, please read the <b>What's New</b> tab for important information regarding the new <b>reCaptcha 2.0</b> use.</div>
    <p>
    This module is not for use directly on a page, it is a tool to be used by other modules. If you are a module developer and want to use Captcha in your module check the API tab.
    </p>
    <hr />
    <p>
    Some implementation examples:<br />
    <a href="http://dev.cmsmadesimple.org/projects/acomments" target="_blank">Comments</a><br />
    <a href="http://dev.cmsmadesimple.org/projects/frontendusers" target="_blank">FrontEndUsers</a><br />
    <a href="http://dev.cmsmadesimple.org/projects/gastbuch" target="_blank">Guestbook</a><br />
    <a href="http://dev.cmsmadesimple.org/projects/questions" target="_blank">Questions</a><br />
    <a href="http://dev.cmsmadesimple.org/projects/formbuilder" target="_blank">Form Builder</a><br />
    </p>
    
  </div>
{*************************************************************************************}
{* API *}
  <div id="api_c">
    <p>If you are a module developer and want to use Captcha in your module:</p>  
     <p>
        To show a captcha image:
        <pre style="display:block;">
        // create a reference to a Captcha module object
        $captcha = $this->getModuleInstance('Captcha'); 
        // show the captcha image
        echo $captcha->getCaptcha();
        </pre>
        To check the user input:
        <pre style="display:block;">
        // create a reference to a Captcha module object
        $captcha = $this->getModuleInstance('Captcha');
        
        // fallback if Captcha module is old
        $test = method_exists($captcha, 'NeedsInputField') ? $captcha->NeedsInputField() : true;
        
        // there us a new method to deal with reCaptcha, and prevent showing an input when using it
        if($test) $smarty->assign( 'input_captcha',$this->CreateInputText($id, 'captcha_input','') );
        
        // check the user input (the checkCaptcha method will return TRUE if $input is correct, FALSE if $input is incorrect)
        // the next line will work regardless we have an input or not when using reCaptcha
        // reCaptcha will use it's own input whereas other libraries will use the module input
        $validated = $captcha->checkCaptcha($params['captcha_input]);
        
        </pre>
     </p> 
  </div>
{*************************************************************************************}
{* PEAR *}
  <div id="pear_c">
    <h3>PEAR Text/Captcha</h3>
        <p>
        This module can make use of PEAR&quot;s Text_CAPTCHA, which will have to be installed on the server.<br />
        Text_CAPTCHA depends on two other PEAR packages, Image_Text and Text_Password. It uses Text_Password to generate the random phrase used in the CAPTCHA test and Image_Text to generate an image file with text in it.<br />
        The process for installing Text_CAPTCHA at the command line is:<br />
        <pre>
        $ pear install Text_Password
        $ pear install Image_Text
        $ pear install --alldeps Text_CAPTCHA
        </pre>
        </p>
  </div>
{*************************************************************************************}  
{* NEW *}
  <div id="new_c">
    <h3>What's New</h3>
    <ul>
      <li>Updated reCaptcha to version 2.0 (thanks jce76350 for the headsup);</li>
      <p>PLEASE NOTE:</p>
      <ul>
          <li>the new reCaptcha is not compatible with the previous 1.x releases;</li>
          <li>the secret keys used by the previous version of reCaptcha are not valid for reCaptcha 2.0, you need to get a pair expecific for reCaptcha 2.0;</li>
          <li>the secret keys have now different labels for reCaptcha 2.0: instead of <b><i>publickey</i></b> and <b><i>privatekey</i></b>, these are now <b><i>site key</i></b> and <b><i>secret</i></b>;</li>
      </ul>

    </ul>
    <div class='information'>
      For more information on the new reCaptcha, please follow these links:
      <ul>
        <li><a href="https://www.google.com/recaptcha/intro/index.html" target="_blank">Introducing the new reCAPTCHA!</a>;</li>
        <li><a href="https://developers.google.com/recaptcha/docs/start" target="_blank">reCAPTCHA Developer's Guide</a>;</li>
      </ul>
    </div>
  </div>
{*************************************************************************************}  
{* NEW *}
  <div id="credits_c">
    <h3>Credits</h3>
    <p>Author and administrator of the original module until 2009: Dick Ittmann.</p>
    <p>Current Project Administrator: Mark Reed (mark).</p>
    <p>Current developer: Fernando Morgado (Jo Morg).</p>
    <br />
    <p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. You must agree to this license before using the module.</p>
    <br />
    <p>Contributors:</p>
    <ul>
      <li>Robert Campbell (Calguy1000): numerous code contributions;</li>
      <li>Nuno Costa: code contributions;</li>
      <li>Jean-Claude Etiemble (jce76350): testing, suggestions and fixes;</li>
      <li>Rolf Tjassens (RolfTj): testing, suggestions and fixes;</li>
      <li>René Helminsen (reneh): testing;</li>
    </ul>
    <p>We apologize for any omissions - notify us, and we'll correct it!</p>
    <h3>Copyright and License</h3>
    <p>Copyright &copy; 2006 - 2013, Dick Ittmann. All Rights Reserved.</p>
    <p>Copyright &copy; 2014, Mark Reed and Fernando Morgado. All Rights Reserved.</p>
    <br />
  </div>
{*************************************************************************************}  
</div>
{* Tabs Contents end*}