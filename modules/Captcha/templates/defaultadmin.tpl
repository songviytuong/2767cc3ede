{$tabs_obj->StartTabsHeaders}

{foreach $tabs_obj->tabs as $tab}
  {$tab->TabHeader}
{/foreach}
{$tabs_obj->EndTabHeaders}

{$tabs_obj->StartTabContent}

  {foreach $tabs_obj->tabs as $tab}  
    {$tab->Start}
      {$tab->FormStart}
      
        {foreach $tab->inputs as $input}
        {* <pre>{$input|print_r:1}</pre>  *}
          
          {if $input->type == 'string'}
            <div class="pageoverflow">
              <p class="pagetext">{$input->label}</p>  
              <p class="pageinput">{$input->data}</p>  
            </div>
          {/if}
          
          {if $input->type == 'dropdown'}
            <div class="pageoverflow">
              <p class="pagetext">{$input->label}:</p>  
              <p class="pageinput">
                {if count($input->data) > 1}
                  {html_options name="{$actionid}{$input->name}" options=$input->data selected=$input->selected}
                {else}
                  {$input->data.{$input->selected}}
                {/if}
              </p>
            </div>  
          {/if}
          
          {if $input->type == 'text'}
            <div class="pageoverflow">
              <p class="pagetext">{$input->label}:</p>  
              <p class="pageinput">
                <input  name="{$actionid}{$input->name}"  value="{$input->data}" size="30" maxlength="255" type="text">
              </p>
            </div>    
          {/if}
                     
          {if $input->type == 'checkbox'}
            <div class="pageoverflow">
              <p class="pagetext">{$input->label}:</p>  
              <p class="pageinput">
                <input type="checkbox" name="{$actionid}{$input->name}"  value="1" {if $input->data}checked{/if}/>
              </p>
            </div>    
          {/if}          
          
          {if $input->type == 'fieldset_start'}
            <fieldset>
            {if !empty($input->label)}
              <legend>{$input->label}</legend>
            {/if}
          {/if}
                    
          {if $input->type == 'fieldset_end'}
            </fieldset> 
          {/if}
          
        {foreachelse}
        {/foreach}

        {$tab->FormSubmit}
      {$tab->FormEnd}
    {$tab->End} 
  {/foreach}

{$tabs_obj->EndTabContent}