<?php
$lang['project'] = 'Captcha';
$lang['msg_changes'] ='Saved Changes';
$lang['friendlyname'] = 'Captcha';
$lang['postinstall'] = 'Post Install Message.';
$lang['postuninstall'] = 'Uninstalled the Captcha module successfully.';
$lang['really_uninstall'] = 'Are you sure you want to uninstall the Captcha module? Other modules might make use of it.';
$lang['uninstalled'] = 'Module Uninstalled.';
$lang['installed'] = 'Module version %s installed.';
$lang['upgraded'] = 'Module upgraded to version %s.';
$lang['moddescription'] = 'Enables captcha support for use by other modules.';

$lang['error'] = 'Error!';
$lang['err_no_gd_support'] = 'Your PHP installation does not have GD support';
$lang['err_no_jpg_support'] = 'Your GD library does not have JPG support';
$lang['err_no_freetype_support'] = 'Your GD library does not have FreeType support';
$lang['admin_title'] = 'Captcha Admin Panel';
$lang['admindescription'] = 'Captcha settings';
$lang['accessdenied'] = 'Access Denied. Please check your permissions.';
$lang['postinstall'] = 'The Captcha module installed successfully.';

$lang['title_general'] = 'General';
$lang['title_cache'] = 'Cache';

$lang['legend_pear'] = 'PEAR Captcha';
$lang['label_enable_pear'] = 'Enable PEAR Text/Captcha (must be installed, see the Captcha module help)';

$lang['legend_active_lib'] = 'Active library';
$lang['label_captchalib_select'] = 'Captcha Library to use';

$lang['msg_active_lib_changed'] = 'Active library changed to %s';
$lang['msg_no_changes'] = 'No changes';
$lang['msg_pear_enabled'] = 'Enabled PEAR Text/Captcha';
$lang['msg_pear_disabled'] = 'Disabled PEAR Text/Captcha';
$lang['msg_pear_disable_while_selected'] = 'PEAR Captcha cannot be disabled while it is being used. <br />Please select another Captcha library to use before disabling PEAR Captcha.';
$lang['msg_pear_unavailable'] = 'PEAR Text/Captcha is not available on this system, check the Captcha module help';

$lang['legend_cache'] = 'Cache';
$lang['label_clear_cache'] = 'Delete all images in the cache directory';
$lang['msg_cache_overview'] = "Number of image files in the cache directory: %s";
$lang['cache_directory'] = 'Cache directory';
$lang['msg_deleted_cache'] = "Deleted %s image files from the cache directory";
$lang['msg_deleted_cache_single'] = "Deleted 1 image file from the cache directory";

$lang['legend_fonts'] = 'Fonts';
$lang['label_font_path'] = 'Font path';
$lang['available_fonts'] = 'Available fonts';

$lang['restore'] = 'Restore to defaults';
$lang['msg_restored_defaults'] = 'Restored values to defaults';

$lang['site_key'] = 'Site key';
$lang['secret'] = 'Secret';

?>
