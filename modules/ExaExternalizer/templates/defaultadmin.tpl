{tab_header name='dashboard' label=$mod->Lang('dashboard_tab_header')}
{tab_header name='setting' label=$mod->Lang('setting_tab_header')}
{tab_header name='donation' label=$mod->Lang('setting_tab_donation')}

{tab_start name='dashboard'}
{include file='module_file_tpl:ExaExternalizer;defaultadmin_tab_dashboard.tpl' scope='root'}

{tab_start name='setting'}
{include file='module_file_tpl:ExaExternalizer;defaultadmin_tab_setting.tpl' scope='root'}

{tab_start name='donation'}
{include file='module_file_tpl:ExaExternalizer;defaultadmin_tab_donation.tpl' scope='root'}

{tab_end}