{form_start}<div class="pageoverflow">
   <p class="pagetext">{$mod->Lang('status')}</p>
   <p class="pageinput">
      {if $setting.status == true}
         {$mod->Lang('status_activated')}
      {else}
         {$mod->Lang('status_deactivated')}
      {/if}
   </p>
</div>{if $setting.status == true}<div class="pageoverflow">   <p class="pagetext">{$mod->Lang('time_left')}</p>   <p class="pageinput">   {$setting.time_left}   {if $setting.time_left <= 1}      {$mod->Lang('minute')}   {else}      {$mod->Lang('minutes')}   {/if}   </p></div>{/if}

<div class="pageoverflow">
   <p class="pageinput">
      {if $setting.status == true}
         <input type="submit" name="{$actionid}form_dashboard" value="{$mod->Lang('status_deactivate')}">         <input type="submit" name="{$actionid}form_dashboard_refresh" value="{$mod->Lang('status_refresh')}">
      {else}
         <input type="submit" name="{$actionid}form_dashboard" value="{$mod->Lang('status_activate')}">
      {/if}
   </p>
</div>{form_end}