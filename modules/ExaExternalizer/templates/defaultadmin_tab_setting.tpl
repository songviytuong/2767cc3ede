{form_start}

<div class="pageoverflow">
   <p class="pagetext">{$mod->Lang('cache_folder')}</p>
   <p class="pageinput">
      {if $setting.status == true}
         {$setting.tmp_path}/{$setting.cache_folder|default:''}
      {else}
         {$setting.tmp_path}/<input type="text" name="{$actionid}cache_folder" value="{$setting.cache_folder|default:''}">
      {/if}
   </p>
</div>

<div class="pageoverflow">
   <p class="pagetext">{$mod->Lang('timeout')}</p>
   <p class="pageinput">
      {if $setting.status == true}
         {$setting.timeout|default:''}
            {if $setting.timeout != 1}
               {$mod->Lang('minutes')}
            {else}
               {$mod->Lang('minute')}
            {/if}
      {else}
         <input type="text" name="{$actionid}timeout" size="4" value="{$setting.timeout|default:''}"> minutes
      {/if}
   </p>
</div>

<div class="pageoverflow">
   <p class="pagetext">{$mod->Lang('chmod')}</p>
   <p class="pageinput">
      {if $setting.status == true}
         {$setting.chmod|default:''}
      {else}
         <input type="text" name="{$actionid}chmod" size="4" value="{$setting.chmod|default:''}">
      {/if}
   </p>
</div>

<div class="pageoverflow">
   <p class="pagetext">{$mod->Lang('stylesheet_extension')}</p>
   <p class="pageinput">
      {if $setting.status == true}
         {$setting.stylesheet_extension|default:''}
      {else}
         <input type="text" name="{$actionid}stylesheet_extension" size="4" value="{$setting.stylesheet_extension|default:''}">
      {/if}
   </p>
</div>

<div class="pageoverflow">
   <p class="pagetext">{$mod->Lang('template_extension')}</p>
   <p class="pageinput">
      {if $setting.status == true}
         {$setting.template_extension|default:''}
      {else}
         <input type="text" name="{$actionid}template_extension" size="4" value="{$setting.template_extension|default:''}">
      {/if}
   </p>
</div>

<div class="pageoverflow">
   <p class="pagetext">{$mod->Lang('udt_extension')}</p>
   <p class="pageinput">
      {if $setting.status == true}
         {$setting.udt_extension|default:''}
      {else}
         <input type="text" name="{$actionid}udt_extension" size="4" value="{$setting.udt_extension|default:''}">
      {/if}
   </p>
</div>

{if $setting.status == false}
<div class="pageoverflow">
   <p class="pageinput">
      <input type="submit" name="{$actionid}form_setting" value="{$mod->Lang('save')}">
   </p>
</div>
{/if}

{form_end}