<?php

namespace ExaExternalizer;

class UserPlugins
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Variables
\* ************************************************************************** */

protected static $fields = [
    'userplugin_id',
    'userplugin_name',
    'code',
    'modified_date'
    ];
    
protected static $folder = "_UDT";









/* ************************************************************************** *\
    Get
\* ************************************************************************** */

public static function get($options=[]) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->count = null;
    $return->record = [];
    
    // Default options
    $default = [
        'fields' => '*',
        'where' => null,
        'order' => null,
        'limit' => null,
        'offset' => null
        ];
      
    // Filter options
    foreach ($options as $key => $val):
        if (!array_key_exists($key, $default)):
            unset($options[$key]);
        endif;
    endforeach;
    $options = array_merge($default, $options);
    
    // Query
    $sql = "SELECT {$options['fields']} FROM " . CMS_DB_PREFIX . "userplugins";
    $sql .= (!empty($options['where'])) ? " WHERE {$options['where']}" : '';
    $sql .= (!empty($options['order'])) ? " ORDER BY {$options['order']}" : '';
    $sql .= (!empty($options['limit'])) ? " LIMIT {$options['limit']}" : '';
    $sql .= (!empty($options['offset'])) ? " OFFSET {$options['offset']}" : '';
    
    $result = $db->Execute($sql);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->count = $result->RecordCount();
    $return->record = $result->GetAll();
    $return->result = true;
    
    return $return;
    
}









/* ************************************************************************** *\
    Set
\* ************************************************************************** */

public static function set($options) {
    
    // Pr�paration des options
    foreach ($options as $key => $val):
        
        // Filtrage
        if (!in_array($key, self::$fields)):
            unset($options[$key]);
            continue;
        endif;

        // Optimisation
        if ($val == ''):
            $options[$key] = NULL;
        endif;
        
        switch ($key):
            case 'userplugin_id':
                $options[$key] = (int) $val;
                break;
            case 'userplugin_name':
                $options[$key] = trim($val);
                break;
        endswitch;
        
    endforeach;
    
    // Mise � jour
    if (empty($options['userplugin_id']) OR $options['userplugin_id'] == 0):
        return false;
    endif;
    
    $return = self::update($options);
    
    return $return;

}









/* ************************************************************************** *\
    Update
\* ************************************************************************** */

protected static function update($options) {
    
    $mod = \cms_utils::get_module('ExaExternalizer');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    $timeStamp = time();
    $timeNow = date("Y-m-d H:i:s",$timeStamp);
    
    // Pr�paration des valeurs
    $options['modified_date'] = (empty($options['modified_date'])) ? $timeNow : $options['modified_date'];

    // Pr�paration du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requ�te
    $query = "
        UPDATE " . CMS_DB_PREFIX . "userplugins
        SET {$set}
        WHERE userplugin_id = {$options['userplugin_id']}
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = $mod->Lang('userplugin_edited');
    
    return $return;
    
}









/* ************************************************************************** *\
    Exportation
\* ************************************************************************** */

public static function export() {
    
    // Variables
    $mod = \cms_utils::get_module('ExaExternalizer');
    $folder = self::$folder;
    $ext = $mod->GetPreference('template_extension', 'txt');

    $res = self::get();
    if ($res->count > 0):

        $item_record = $res->record;

        foreach ($item_record as $item):

            \ExaExternalizer\FileSystem::createFile(
                self::$folder,
                $item['userplugin_name'],
                $ext,
                $item['code'],
                $item['modified_date']
                );

        endforeach;

    endif;
    
}









/* ************************************************************************** *\
   Importer les gabarits
\* ************************************************************************** */
public static function import()
{
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Variables
    //$cache_path = \ExaExternalizer\Cache::getPath();
    //$mod = \cms_utils::get_module('ExaExternalizer');
    //$template_extension = $mod->GetPreference('template_extension');
    //$folder = self::$folder;
    //$folder_path = cms_join_path($cache_path, $folder);


    // Parcourir des items
    $result = self::get();
    if (count($result->record) > 0):

        $item_record = $result->record;

        foreach ($item_record as $item):

            //$filename = cms_join_path($folder_path, $item['userplugin_name'] . '.' . $template_extension);
            //
            //// Si le fichier n'existe pas
            //if (!file_exists($filename)):
            //    continue;
            //endif;
            //
            //// Si le fichier n'a pas �t� modifi�
            //$filetime = @filemtime($filename);
            //if ($filetime <= $item['modified_date']):
            //    continue;
            //endif;
            //
            //// Lecture du contenu
            //$content = '';
            //$filesize = filesize($filename);
            //$fp = fopen($filename, 'r');
            //$content = fread($fp, $filesize);
            //fclose($fp);
            //
            //// Sauvegarde du contenu
            //$result = self::set([
            //    'userplugin_id' => $item['userplugin_id'],
            //    'userplugin_name' => $item['userplugin_name'],
            //    'code' => $content,
            //    'modified_date' => $filetime
            //    ]);
            //
            //// Mise � z�ro du timeout
            //\ExaExternalizer\TimeOut::reset();

        endforeach;

    endif;
    
    $return->result = true;
    
    return $return;
   
}









}?>