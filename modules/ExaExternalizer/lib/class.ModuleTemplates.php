<?php

namespace ExaExternalizer;

class ModuleTemplates
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Variables
\* ************************************************************************** */

public static $fields = [
    'module_name',
    'template_name',
    'content',
    'modified_date'
    ];









/* ************************************************************************** *\
    Get
\* ************************************************************************** */

public static function get($options=[]) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->count = null;
    $return->record = [];
    
    // Default options
    $default = [
        'fields' => '*',
        'where' => null,
        'order' => null,
        'limit' => null,
        'offset' => null
        ];
      
    // Filter options
    foreach ($options as $key => $val):
        if (!array_key_exists($key, $default)):
            unset($options[$key]);
        endif;
    endforeach;
    $options = array_merge($default, $options);
    
    // Query
    $sql = "SELECT {$options['fields']} FROM " . CMS_DB_PREFIX . "module_templates";
    $sql .= (!empty($options['where'])) ? " WHERE {$options['where']}" : '';
    $sql .= (!empty($options['order'])) ? " ORDER BY {$options['order']}" : '';
    $sql .= (!empty($options['limit'])) ? " LIMIT {$options['limit']}" : '';
    $sql .= (!empty($options['offset'])) ? " OFFSET {$options['offset']}" : '';
    
    $result = $db->Execute($sql);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->count = $result->RecordCount();
    $return->record = $result->GetAll();
    $return->result = true;
    
    return $return;
    
}









/* ************************************************************************** *\
    Set
\* ************************************************************************** */

public static function set($options) {
    
    // Pr�paration des options
    foreach ($options as $key => $val):
        
        // Filtrage
        if (!in_array($key, self::$fields)):
            unset($options[$key]);
            continue;
        endif;

        // Optimisation
        if ($val == ''):
            $options[$key] = NULL;
        endif;

        switch ($key):
            case 'module_name':
            case 'template_name':
                $options[$key] = trim($val);
                break;
        endswitch;
        
    endforeach;
    
    // Mise � jour
    if (empty($options['module_name']) OR empty($options['template_name'])):
        return false;
    endif;
    
    $return = self::update($options);
    
    return $return;

}









/* ************************************************************************** *\
    Update
\* ************************************************************************** */

protected static function update($options) {
    
    $mod = \cms_utils::get_module('ExaExternalizer');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    $timeStamp = time();
    $timeNow = date("Y-m-d H:i:s",$timeStamp);
    
    // Pr�paration des valeurs
    $options['modified_date'] = (empty($options['modified_date'])) ? $timeNow : $options['modified_date'];

    // Pr�paration du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requ�te
    $query = "
        UPDATE " . CMS_DB_PREFIX . "module_templates
        SET {$set}
        WHERE
            module_name = '{$options['module_name']}'
        AND
            template_name = '{$options['template_name']}'
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = $mod->Lang('module_template_edited');
    
    return $return;
    
}









/* ************************************************************************** *\
    Exportation
\* ************************************************************************** */

public static function export() {
    
    // Variables
    $folder = '_Modules';
    $mod = \cms_utils::get_module('ExaExternalizer');
    $template_extension = $mod->GetPreference('template_extension');

    // Cr�ation du dossier d'exportation
    \ExaExternalizer\FileSystem::createFolder($folder);
    
    $result = self::get();
    if ($result->count > 0):

        $item_record = $result->record;

        foreach ($item_record as &$item):
            
            $subfolder = cms_join_path($folder, $item['module_name']);
            \ExaExternalizer\FileSystem::createFolder($subfolder);
            \ExaExternalizer\FileSystem::createFile($subfolder, $item['template_name'], $template_extension, $item['content'], $item['modified']);

        endforeach;

    endif;
    
}









/* ************************************************************************** *\
   Importer les gabarits
\* ************************************************************************** */
public static function import()
{
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $mod = \cms_utils::get_module('ExaExternalizer');
    $template_extension = $mod->GetPreference('template_extension');
    $folder = '_Modules';
    $folder_path = cms_join_path($cache_path, $folder);


    // Parcourir chaque gabarit
    $result = self::get();
    if (count($result->record) > 0):

        $item_record = $result->record;

        foreach ($item_record as $item):

            $subfolder_path = cms_join_path($folder_path, $item['module_name']);
            $filename = cms_join_path($subfolder_path, $item['template_name'] . '.' . $template_extension);
            
            // Si le fichier n'existe pas
            if (!file_exists($filename)):
                continue;
            endif;

            // Si le fichier n'a pas �t� modifi�
            $filetime = @filemtime($filename);
            if ($filetime <= $item['modified_date']):
                continue;
            endif;

            // Lecture du contenu
            $content = '';
            $filesize = filesize($filename);
            $fp = fopen($filename, 'r');
            $content = fread($fp, $filesize);
            fclose($fp);

            // Sauvegarde du contenu
            $result = self::set([
                'module_name' => $item['module_name'],
                'template_name' => $item['template_name'],
                'content' => $content,
                'modified_date' => date("Y-m-d H:i:s",$filetime)
                ]);
                
            // Mise � z�ro du timeout
            \ExaExternalizer\TimeOut::reset();

        endforeach;

    endif;
    
    $return->result = true;
    
    return $return;
   
}









}?>