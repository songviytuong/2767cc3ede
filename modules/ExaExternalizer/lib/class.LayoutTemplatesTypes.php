<?php

namespace ExaExternalizer;

class LayoutTemplatesTypes
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Variables
\* ************************************************************************** */

public static $fields = [
    'id',
    'originator',
    'name'
    ];









/* ************************************************************************** *\
    Get
\* ************************************************************************** */

public static function get($options=[]) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->count = null;
    $return->record = [];
    
    // Default options
    $default = [
        'fields' => '*',
        'where' => null,
        'order' => null,
        'limit' => null,
        'offset' => null
        ];
      
    // Filter options
    foreach ($options as $key => $val):
        if (!array_key_exists($key, $default)):
            unset($options[$key]);
        endif;
    endforeach;
    $options = array_merge($default, $options);
    
    // Query
    $sql = "SELECT {$options['fields']} FROM " . CMS_DB_PREFIX . "layout_tpl_type";
    $sql .= (!empty($options['where'])) ? " WHERE {$options['where']}" : '';
    $sql .= (!empty($options['order'])) ? " ORDER BY {$options['order']}" : '';
    $sql .= (!empty($options['limit'])) ? " LIMIT {$options['limit']}" : '';
    $sql .= (!empty($options['offset'])) ? " OFFSET {$options['offset']}" : '';

    $result = $db->Execute($sql);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->count = $result->RecordCount();
    $return->record = $result->GetAll();
    $return->result = true;
    
    return $return;
    
}









}?>