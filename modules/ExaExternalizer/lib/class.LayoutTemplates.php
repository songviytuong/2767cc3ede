<?php

namespace ExaExternalizer;

class LayoutTemplates
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
    Variables
\* ************************************************************************** */

public static $fields = [
    'id',
    'content',
    'modified'
    ];









/* ************************************************************************** *\
    Get
\* ************************************************************************** */

public static function get($options=[]) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->count = null;
    $return->record = [];
    
    // Default options
    $default = [
        'fields' => '*',
        'where' => null,
        'order' => null,
        'limit' => null,
        'offset' => null
        ];
      
    // Filter options
    foreach ($options as $key => $val):
        if (!array_key_exists($key, $default)):
            unset($options[$key]);
        endif;
    endforeach;
    $options = array_merge($default, $options);
    
    // Query
    $sql = "SELECT {$options['fields']} FROM " . CMS_DB_PREFIX . "layout_templates";
    $sql .= (!empty($options['where'])) ? " WHERE {$options['where']}" : '';
    $sql .= (!empty($options['order'])) ? " ORDER BY {$options['order']}" : '';
    $sql .= (!empty($options['limit'])) ? " LIMIT {$options['limit']}" : '';
    $sql .= (!empty($options['offset'])) ? " OFFSET {$options['offset']}" : '';
    
    $result = $db->Execute($sql);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->count = $result->RecordCount();
    $return->record = $result->GetAll();
    $return->result = true;
    
    if (count($return->record) > 0):
    
        foreach ($return->record as $key => $val):
        
            $type_result = \ExaExternalizer\LayoutTemplatesTypes::get([
                'fields' => "id, originator, name",
                'where' => "id = " . $val['type_id'],
                'limit' => 1
                ]);
                
            if ($type_result->count == 1):
                $return->record[$key]['type'] = $type_result->record[0];
            endif;
        
        endforeach;
    
    endif;
    
    return $return;
    
}









/* ************************************************************************** *\
    Set
\* ************************************************************************** */

public static function set($options) {
    
    // Préparation des options
    foreach ($options as $key => $val):
        
        // Filtrage
        if (!in_array($key, self::$fields)):
            unset($options[$key]);
            continue;
        endif;

        // Optimisation
        if ($val == ''):
            $options[$key] = NULL;
        endif;
        
        switch ($key):
            case 'id':
                $options[$key] = (int) $val;
                break;
            case 'content':
            case 'modified':
                $options[$key] = trim($val);
                break;
        endswitch;
        
    endforeach;
    
    // Mise à jour
    if (empty($options['id']) OR $options['id'] == 0):
        return false;
    endif;
    
    $return = self::update($options);
    
    return $return;

}









/* ************************************************************************** *\
    Update
\* ************************************************************************** */

protected static function update($options) {
    
    $mod = \cms_utils::get_module('ExaExternalizer');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    $timeStamp = time();
    $timeNow = date("Y-m-d H:i:s",$timeStamp);
    
    // Préparation des valeurs
    $options['modified'] = (empty($options['modified'])) ? $timeNow : $options['modified'];

    // Préparation du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requête
    $query = "
        UPDATE " . CMS_DB_PREFIX . "layout_templates
        SET {$set}
        WHERE id = {$options['id']}
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = $mod->Lang('template_edited');
    
    return $return;
    
}









/* ************************************************************************** *\
    Exportation
\* ************************************************************************** */

public static function export() {
    
    // Variables
    $folder = '_Templates';
    $mod = \cms_utils::get_module('ExaExternalizer');
    $template_extension = $mod->GetPreference('template_extension');

    // Création du dossier d'exportation
    \ExaExternalizer\FileSystem::createFolder($folder);
    
    $result = self::get();
    if ($result->count > 0):

        $item_record = $result->record;

        foreach ($item_record as &$item):

            $folder_tmp = cms_join_path($folder, $item['type']['originator'], $item['type']['name']);
            \ExaExternalizer\FileSystem::createFile($folder_tmp, $item['name'] . '__' . $item['id'], $template_extension, $item['content'], $item['modified']);

        endforeach;

    endif;
    
}









/* ************************************************************************** *\
   Importer les gabarits
\* ************************************************************************** */
public static function import()
{
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $mod = \cms_utils::get_module('ExaExternalizer');
    $template_extension = $mod->GetPreference('template_extension');
    $folder = '_Templates';
    $folder_path = cms_join_path($cache_path, $folder);


    // Parcourir chaque gabarit
    $result = self::get();
    if (count($result->record) > 0):

        $item_record = $result->record;

        foreach ($item_record as $item):

            $filename = \ExaExternalizer\FileSystem::escapeFilename($item['name']);
            $folder_path_tmp = cms_join_path($folder_path, $item['type']['originator'], $item['type']['name']);
            $filename = cms_join_path($folder_path_tmp, $filename . '__' . $item['id'] . '.' . $template_extension);

            // Si le fichier n'existe pas
            if (!file_exists($filename)):
                continue;
            endif;

            // Si le fichier n'a pas été modifié
            $filetime = @filemtime($filename);
            if ($filetime <= $item['modified']):
                continue;
            endif;

            // Lecture du contenu
            $content = '';
            $filesize = filesize($filename);
            $fp = fopen($filename, 'r');
            $content = fread($fp, $filesize);
            fclose($fp);

            // Sauvegarde du contenu
            $result = self::set([
                'id' => $item['id'],
                'content' => $content,
                'modified' => $filetime
                ]);

            // Mise à zéro du timeout
            \ExaExternalizer\TimeOut::reset();

        endforeach;

    endif;
    
    $return->result = true;
    
    return $return;
   
}









}?>