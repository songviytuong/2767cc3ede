<?php

namespace ExaExternalizer;

class Base
{
   
   
   
   
   
   
   
   
   
/* ************************************************************************** *\
   Exportation
\* ************************************************************************** */
public static function export()
{
   
    $mod = \cms_utils::get_module('ExaExternalizer');
   
    // Exportation des feuilles de style
    \ExaExternalizer\LayoutStylesheets::export();

    // Exportation des gabarits
    \ExaExternalizer\LayoutTemplates::export();
    
    // Exportation des balises utilisateur
    \ExaExternalizer\UserPlugins::export();

    // Exportation des préférences du site
    \ExaExternalizer\SitePrefs::export();
    
    // Exportation des gabarits de modules
    \ExaExternalizer\ModuleTemplates::export();

    // Evènement d'exportation
    \Events::SendEvent($mod->GetName(), 'Export');
   
}





  




/* ************************************************************************** *\
   Importation
\* ************************************************************************** */
public static function import()
{
    
    $mod = \cms_utils::get_module('ExaExternalizer');

    // Importation des feuilles de style
    $result = \ExaExternalizer\LayoutStylesheets::import();

    // Importation des gabarits
    \ExaExternalizer\LayoutTemplates::import();

    // Importation des gabarits de modules
    \ExaExternalizer\ModuleTemplates::import();
    
    // Importation des balises utilisateur
    \ExaExternalizer\UserPlugins::import();
    
    // Importation des préférences du site
    \ExaExternalizer\SitePrefs::import();

    // Evènement d'importation
    \Events::SendEvent($mod->GetName(), 'Import');
   
}









}
?>