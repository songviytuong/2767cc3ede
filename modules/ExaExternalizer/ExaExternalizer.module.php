<?php
class ExaExternalizer extends CMSModule
{
    
    
    
    
    
    
    
    
    
/* ************************************************************************** *\
   Constantes et variables
\* ************************************************************************** */
    
const PERM_USE = 'exaexternalizer_perm_use';
private static $_initialized;









/* ************************************************************************** *\
   Construction
\* ************************************************************************** */

public function __construct() {
    
    parent::__construct();

    spl_autoload_register([$this,'autoload']);
  
    // Vérification de l'initialisation
    if (self::$_initialized):
        return;
    endif;
    self::$_initialized = true;

    // Si le mode de développement est actif
    if ($this->GetPreference('status', false) == true):

        $time_left = \ExaExternalizer\TimeOut::getTimeLeft();

        if ($time_left > 0):
            \ExaExternalizer\Base::import();
        else:
            \ExaExternalizer\Status::change();
        endif;

    endif;

}









/* ************************************************************************** *\
   Fonctions CMS Made Simple
\* ************************************************************************** */
    
public function GetName() {
    return 'ExaExternalizer';
}

public function GetVersion() {
    return '0.6';
}

public function GetAuthor() {
    return 'Jocelyn Lusseau - Exacore';
}

public function GetAuthorEmail() {
    return 'info@exacore.fr';
}

public function GetFriendlyName() {
    return $this->Lang('friendlyname');
}

public function GetAdminDescription() {
    return $this->Lang('admindescription');
}

public function IsPluginModule() {
    return FALSE;
}

public function HasAdmin() {
    return TRUE;
}

public function GetAdminSection() {
    return 'layout';
}

public function VisibleToAdminUser() {
    return $this->CheckPermission(self::PERM_USE);
}

public function GetDependencies() {
    return [];
}

public function GetHelp() {
    return '';
}

public function MinimumCMSVersion() {
    return '2.0.1';
}

public function UninstallPreMessage() {
    return $this->Lang('ask_uninstall');
}









/* ************************************************************************** *\
   Autoloader
\* ************************************************************************** */
    
public function autoload($classname) {

    if (!is_object($this)):
        return false;
    endif;

    $path = $this->GetModulePath().'/lib';
    if (strpos($classname,'\\') !== false):
        $t_path = str_replace('\\','/',$classname);
        if (startswith( $t_path, $this->GetName().'/')):
            $classname = basename($t_path);
            $t_path = dirname($t_path);
            $t_path = substr($t_path,strlen($this->GetName())+1);
            $path = $this->GetModulePath().'/lib/'.$t_path;
        endif;
    endif;

    $fn = $path."/class.{$classname}.php";
    if (file_exists($fn)):
        require_once($fn);
        return true;
    endif;

    return false;

}








    
}
?>