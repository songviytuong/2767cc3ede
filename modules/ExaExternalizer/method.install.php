<?php
if( !defined('CMS_VERSION') ) exit;









/* ************************************************************************** *\
   Permissions
\* ************************************************************************** */

$this->CreatePermission(ExaExternalizer::PERM_USE,'ExaExternalizer - Use');









/* ************************************************************************** *\
   Préférences
\* ************************************************************************** */

$this->SetPreference("status", false);
$this->SetPreference("cache_folder", "externalizer");
$this->SetPreference("timeout", 30);
$this->SetPreference("chmod", "0777");
$this->SetPreference("stylesheet_extension", 'css');
$this->SetPreference("template_extension", 'html');
$this->SetPreference("udt_extension", 'php');









/* ************************************************************************** *\
   Évènements
\* ************************************************************************** */
\Events::CreateEvent($this->GetName(), 'Import');
\Events::CreateEvent($this->GetName(), 'Export');









?>