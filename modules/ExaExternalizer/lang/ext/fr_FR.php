<?php
$lang['friendlyname'] = "ExaExternalizer";
$lang['admindescription'] = "Ce module permet d'utiliser un éditeur de texte
    pour modifier les gabarits, feuilles de styles, balises utilisateurs et
    quelques paramètres du site (Message de maintenance, Metadata, etc.).
    Il exporte ces données dans un dossier temporaire sur le serveur et analyse
    les changements intervenus pour les importer automatiquement.";
$lang['ask_uninstall'] = "Êtes-vous certain(e) de vouloir désinstaller le module ExaExternalizer ?";

$lang['dashboard_tab_header'] = "Tableau de bord";
$lang['status'] = "État";
$lang['status_activated'] = "Actif";
$lang['status_deactivated'] = "Inactif";
$lang['time_left'] = "Temps restant";
$lang['status_activate'] = "Activer";
$lang['status_deactivate'] = "Désactiver";
$lang['status_refresh'] = "Rafraîchir";
$lang['status_changed_activated'] = "ExaExternalizer est actif !";
$lang['status_changed_deactivated'] = "ExaExternalizer est inactif !";
$lang['status_changed_refreshed'] = "ExaExternalizer a été rafraîchi !";

$lang['setting_tab_header'] = "Paramètres";
$lang['cache_folder'] = "Répertoire du cache";
$lang['timeout'] = "Durée d'activation";
$lang['minute'] = "minute";
$lang['minutes'] = "minutes";
$lang['chmod'] = "CHMOD";
$lang['stylesheet_extension'] = "Extension des feuilles de styles";
$lang['template_extension'] = "Extension des gabarits";
$lang['udt_extension'] = "Extension des balises utilisateur";
$lang['save'] = "Enregistrer";
$lang['setting_changed'] = "Paramètres modifiés";

$lang['setting_tab_donation'] = "Don";
$lang['donation_introduction'] = "Ce module est distribué gratuitement et
    continera à l'être.<br/>Cependant, si vous souhaitez effectuer un don du
    montant de votre choix parce que vous estimez que ce module le mérite, je
    vous laisse faire :-)";
    
$lang['sitepref_edited'] = "Préférence modifiée";
$lang['userplugin_edited'] = "Balise utilisateur modifiée";

?>