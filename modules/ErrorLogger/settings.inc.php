<?php
//saved at 2016-11-08 20:03:23
define('MOD_ERROR_LOGGER_INFO',           false);
define('MOD_ERROR_LOGGER_WARNING',        false);
define('MOD_ERROR_LOGGER_ERROR',          true);
define('MOD_ERROR_LOGGER_EXCEPTION',      true);
define('MOD_ERROR_LOGGER_ITEMS_PER_PAGE', 10);
?>