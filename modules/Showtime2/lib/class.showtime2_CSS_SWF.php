<?php

class showtime2_CSS_SWF{
  protected function __construct() {} 

  public static function _Create($show_id){
		$gCms = cmsms();
		$config = $gCms->GetConfig();
		$row = showtime2_data::_Getshowinfo($show_id);
    $mod = cms_utils::get_module('Showtime2');
		
				 $cssstring ="
		#gallery-".$row['show_id']." {margin: auto;}
		#gallery-".$row['show_id']." .gallery-item {float: left; margin-top: 10px; text-align: center;}
		#gallery-".$row['show_id']." img {border: 2px solid #cfcfcf;}
		#gallery-".$row['show_id']." .gallery-caption {margin-left: 0;}";
			
    if ($mod->GetPreference('use_tmp')=='1'){
      $cssfilename = '../tmp/_Showtime2/css/Show_' . $show_id . '.css';
    }
    else {
      $cssfilename = '../uploads/_Showtime2/css/Show_' . $show_id . '.css';
    }
    showtime2_utils::createFile($cssfilename,$cssstring);
		return true;
	}
}     
          
?>
