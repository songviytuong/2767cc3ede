<?php

class showtime2_CSS_NIVO{
  protected function __construct() {} 

  public static function _Create($show_id, $example=false){
    $gCms = cmsms();
    $db = cmsms()->GetDb();
    $config = $gCms->GetConfig();
    $mod = cms_utils::get_module('Showtime2');
    $row = showtime2_data::_Getshowinfo($show_id);
    //set default value h x w if example
    $row['st_width']= ($example)?300:$row['st_width'];
    $row['st_height']=($example)?225:$row['st_height'];

    //Read fontype
    switch (trim($row['st_fonttype'])){
            case 'verdana':
                    $row['st_fonttype']='Verdana, Geneva, sans-serif' ;break;
            case 'courier':
                    $row['st_fonttype']='"Courier New", Courier, monospace' ;break;
            case 'arial':
                    $row['st_fonttype']='Arial, Helvetica, sans-serif' ;break;
            case 'tahoma':
                    $row['st_fonttype']='Tahoma, Geneva, sans-serif' ;break;
            case 'sans':
                    $row['st_fonttype']='_sans, Helvetica, Arial' ;break;
            case 'times':
                    $row['st_fonttype']='"Times New Roman", Times, serif' ;break;
            case 'serif':
                    $row['st_fonttype']='"MS Serif", "New York", serif' ;break;
    }

    

    $cssstring="";
    
    //make slider fixed if not responsive
    if ($row['st_sizing']!="responsive" or $example){
        $cssstring .="
            #slider".$show_id."w{
                width:".$row['st_width']."px;
            }        
            #slider".$show_id."w .nivoSlider, #slider".$show_id."w .nivoSlider img, #slider".$show_id."w .nivo-main-image, #slider".$show_id."w .nivo-slice{
                height:".$row['st_height']."px !important;
                width:".$row['st_width']."px;
            }
            ";
    }
    elseif ($row['st_width'] > 0) {
        $cssstring .="
            #slider".$show_id."w{
                width: ".$row['st_width']."px;
            }
         ";      
    }

    $cssstring .= "
            #slider".$show_id."w .nivoSlider .nivo-caption {
              opacity:".$row['st_captionopacity'].";
              font-size: ".$row['st_textsize']."px;
              font-family: ".$row['st_fonttype']." !important;
              text-align: ".$row['st_commentpos']." !important;
              color: ".$row['st_textcolor'].";
              ".($row['st_showalt']?'background-color: '.$row['st_textbgcolor'].';':'background: none;')."
            }
            #slider".$show_id."w .nivoSlider .nivo-caption h2{
              opacity:".$row['st_captionopacity'].";
              font-size: ".$row['st_titlesize']."px;
              font-family: ".$row['st_fonttype']." !important;
              text-align: ".$row['st_commentpos']." !important;
              color: ".$row['st_textcolor'].";
              ".($row['st_showalt']?'background-color: '.$row['st_textbgcolor'].';':'background: none;')."
            }
         ";
            
    $cssstring .= $mod->GetTemplate('NivoCSS_Show_'.$show_id);
    
    //write if not example
    if (!$example){
            if ($mod->GetPreference('use_tmp')=='1'){
              $cssfilename = '../tmp/_Showtime2/css/Show_' . $show_id . '.css';
            }
            else {
              $cssfilename = '../uploads/_Showtime2/css/Show_' . $show_id . '.css';
            }
            showtime2_utils::createFile($cssfilename,$cssstring);
            return true;
    }else{
      if ($mod->GetPreference('use_tmp')=='1')
        $cssstring = str_replace('url(../','url(../tmp/_Showtime2/',$cssstring);
      else
        $cssstring = str_replace('url(../','url(../uploads/_Showtime2/',$cssstring);
      return $cssstring;
    }
}

}     
     
?>
