<?php
if (!cmsms()) exit;

if(isset($params['show'])){
	$showname=$params['show'];
        
        //see if show_id = numeric
        if (!is_numeric($showname)) {
            $query = "SELECT show_id FROM ".cms_db_prefix()."module_showtime2_name WHERE show_name = ?";
            $showid = $db->GetOne($query,array($showname)); 
        }else{
            $showid = $showname;
        }


        
        $galleryitems="";
	$this->smarty->assign('showid',$showid);
	//see if we have valid showid
        if($showid){
            //read Showinfo
            $showinfo =  showtime2_data::_Getshowinfo($showid);	
            
        
            //include CSS file + theme css file to->  CSSMetadata
            if ($this->GetPreference('use_tmp')=='1'){
              $cssfilename = 'tmp/_Showtime2/css/Show_' . $showid . '.css';
            }
            else {
              $cssfilename = 'uploads/_Showtime2/css/Show_' . $showid . '.css';
            }
            $csstag = '
<!--css for show_'.$showid.'-->
<link rel="stylesheet" type="text/css" href="'.$cssfilename.'" />
';
            if ( empty($this->CSSMetadata) ){
                    $this->CSSMetadata = $csstag;
            }else{
                    $this->CSSMetadata .= $csstag;
            }            
            if ($showinfo){
                    switch($showinfo['st_animationtype']){
                            case "swfobject":
                                    include(dirname(__FILE__).'/function.default_swf.php');
                                    break;
                            case "nivoslider":
                                    include(dirname(__FILE__).'/function.default_nivoslider.php');
                                    break;
                            default:
                                    include(dirname(__FILE__).'/function.default_jQuery.php');
                    }

            }else{
                    echo $this->Lang('changepic_noshow',$showname)."<br />";
            }
         }else{
                    echo $this->Lang('changepic_noshow',$showname)."<br />";
         }
	
}else{
	echo $this->Lang('changepic_noshow2')."<br />";
}//if exist showid

?>