<ul>
<li><strong>Version 3.5.3</strong> - June 2016
<ul>
<li>fixed: unitialzed $temp in showtime2.module.php</li>
<li>changed: do <strong>not</strong> save css-file when changing slides</li>
<li>added: select comment position (left/center/right) in nivoslider</li>
<li>changed: the show-id is now always the next free number</li>
</ul>
<li><strong>Version 3.5.2</strong> - June 2016
<ul>
<li>fixed: broken jquery.js loading</li>
<li>fixed: broken css-file for nivoslider</li>
<li>changed: hide height-field if nivoslider is responsive</li>
<li>updated: jcrop library</li>
<li>added: coordinates to jcrop adminsite</li>
</ul>
<li><strong>Version 3.5.1</strong> - May 2016
<ul>
<li>fixed: broken arrows in jquery-slider</li>
<li>updated: jquery.cycle.all.js</li>
<li>added: buttons on top of the admin-page</li>
<li>fixed: you may use now the same slideshow (except jQuery) twice on a page (necessary for use in a news-article)</li>
<li>changed: check if jquery is already loaded before loading
</ul>
<li><strong>Version 3.5.0</strong> - May 2016
<ul>
<li><strong>Important Note:</strong> I have completely changed the concept of the css-settings for NivoSlider. This new concept is <strong>not compatible</strong> with previous versions, but much more flexible and allows multiple NivoSlider slideshows on the same page with completely different settings! Therefore you <strong>must</strong> open your settings and save them again for each existing NivoSlider slideshow! NivoSlider comes with six themes: the four default themes and two modified themes based on the default theme. These themes are stored in the folder modules/Showtime2/templates/nivoslider/themes. <strong>Do not</strong> modify these themes! They are only initial themes on installation and will be copied to the folder uploads/_Showtime2/nivothemes. These themes are used by Showtime2. You may modify them or add new, customized themes, which will then appear in the theme dropdown list. Each theme can be modified later for a specific show on the settings-page.</li>
<li>changed: you may use several slideshows on the same page now.
  This required a lot of changes, especially to NivoSlider.
  You must save the settings for all NivoSlider-shows again, otherwise these will not work as expected.</li>
<li>changed: moved javascript to the bottom of the page</li>
<li>fixed: include jquery.js only once</li>
<li>fixed: broken compatibility with CMSMS 1</li>
<li>added: option to select tmp instead of uploads folder for css-files and nivothemes</li>
<li>fixed: delete slices from database, if show is deleted</li>
<li>fixed: delete css-template, if show is deleted</li>
<li>fixed: removed obsolete slices from previous shows</li>
<li>fixed: removed obsolete css-templates from previous shows</li>
<li>changed: removed obsolete files</li>
<li>changed: set default to NivoSlider</li>
<li>changed: css-templates-defaults for NivoSlider</li>
<li>changed: recreate missing thumbs on addslices-page</li>
<li>fixed: you may use thumbnails now in NivoSlider to select a slice</li>
<li>added: preview to NivoSlider on edit-page</li>
<li>fixed: recreate watermark-example on update</li>
<li>changed: swfobject.js to version 2.3
<li>changed: forced similiar behaviour of NivoSlider/JQuery/swfobject shows by default</li>
</ul>
</li>
<li><strong>Version 3.4.5</strong> - April 2016
<ul>
<li>removed: event OnPreferenceChange</li>
<li>fixed: showtime_picker -> showtime2_picker in CMSMS 1</li>
<li>added: add watermark to multiple images</li>
<li>added: optional creating backup files when adding watermarks</li>
<li>fixed: tab creation</li>
<li>changed: changed default value from window to transparent for swfobject wmode.</li>
<li>changed: moved css-directory from /modules/Showtime2/templates to /uploads/_Showtime2</li>
<li>fixed: creating css files when changing the show number</li>
<li>removed: apply button on name tab</li>
<li>fixed: german language file</li>
</ul>
<li><strong>Version 3.4.4</strong> - April 2016
<ul>
<li>internal release, removed from the forge</li>
</ul>
<li><strong>Version 3.4.3</strong> - March 2016
<ul>
<li>change: compatibility with CMSMS 1 and 2</li>
<li>fix: english and german language files</li>
<li>change: timing in nivo-slider</li>
<li>fix: minor bugs </li>
</ul>
</ul>
