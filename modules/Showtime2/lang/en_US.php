<?php
//addpictures "  &#039;
$lang['addpicture'] = 'Add pictures';
$lang['addpicture_nonadded'] = 'No pictures added.';
$lang['addpicture_1added'] = 'Picture added.';
$lang['addpicture_added'] = 'Pictures added.';

//addshow
$lang['addshow'] = 'Add show';
$lang['showadded']='Show added.';
$lang['addshow_settings'] = 'Copy settings from previous show&#039;s';
$lang['addshow_default'] = 'use default settings';
$lang['error_nonamegiven']='You need to fill in a name.';

//addslides
$lang['addslides_updatesuccess'] = 'Showtime2 was successfully updated.';
$lang['addslides_bulksuccess'] = 'Bulk action was successfully executed.';
$lang['addslides_bulkconf'] = 'Are you sure you want to execute this bulk action?';
$lang['addslides_bulkwith'] = 'With Selected';
$lang['addslides_moveup'] = 'Move slide up';
$lang['addslides_movedown'] = 'Move slide down';
$lang['addslides_nolink'] = 'no link';
$lang['addslides_changepic'] = 'Change picture for this slide';
$lang['addslides_hide'] = 'Hide this slide';
$lang['addslides_show'] = 'Show this slide';
$lang['addslides_changecomm'] = 'Change slide name and comment';
$lang['addslides_delete'] = 'Delete this slide';
$lang['addslides_addsome'] = 'Add some pictures first.';
$lang['addslides_delete'] = 'delete';
$lang['addslides_active'] = 'active';
$lang['addslides_inactive'] = 'inactive';
$lang['addslides_alink'] = 'asign link';
$lang['addslides_save'] = 'Save your changes!';
$lang['addslides_deletesuccess'] = 'Slide successfully deleted.';
$lang['addslides_crop'] = 'Crop this image';
$lang['addslides_turn_left'] = 'Turn image left';
$lang['addslides_turn_right'] = 'Turn image right';
$lang['addslides_add_watermark'] = 'Add watermark';
$lang['addslides_watermark_added'] = 'Watermark added';
$lang['addslides_image_rotated'] = 'Image rotated';
$lang['Crop_Image'] = 'Crop Image';
$lang['Crop_this_Image'] = 'Crop this image';

//upload tool
$lang['drag_and_drop']= 'You can <strong>drag &amp; drop</strong> files from your desktop on this webpage.<br />';
$lang['press_button'] = 'Press this button when you are done :';
$lang['add_files'] = 'Add files...';
$lang['start_upload'] = 'Start upload';
$lang['cancel_upload'] = 'Cancel upload';
$lang['delete_upload'] = 'Delete selected';
$lang['upload_done'] = 'Upload images done';
$lang['files_uploaded'] = 'You have uploaded %s image(s).';
$lang['newdirname'] = 'Create new directory';
$lang['newdirsuccess'] = 'New directory created';
$lang['newdirfail'] = 'Failed to create new directory';
$lang['ok'] = 'OK';

//headers
$lang['picture']= 'picture';
$lang['order']= 'Order';
$lang['picture_name']= 'Slide name';
$lang['picture_link']= 'Slide link';
$lang['picture_description']= 'Slide comment';
$lang['change_picture']= 'Change<br />picture';
$lang['status']= 'Status';
$lang['edit_picture']= 'Edit picture';

//change comment
$lang['internal_link']='Link for this slide:';
$lang['changecomm_linkimage'] = 'link to this image';
$lang['changecomm_message'] = 'Slide successfully updated.';
$lang['changecomm_nolink'] = 'no link.';
$lang['changecomm_linktitle'] = '<strong>link to this image.</strong><br /> To create link to this slide, please select the nature of the link below.';

//change picture
$lang['changepic_message'] = 'Picture changed.';
$lang['changepicture']= 'Change picture';

//default
$lang['default_addimages'] = 'Add some images first for Slideshow:';
$lang['changepic_noshow'] = 'No show found! <strong>show="%s"</strong> does not exist.';
$lang['changepic_noshow2'] = 'No show found! You need to add &#039;show&#039; parameter : {Showtime2 <strong>show="2"</strong>}';

//defaultadmin
$lang['admin_updated'] = 'The options were successfully updated.';
$lang['admin_taboverview'] = 'Show overview';
$lang['admin_tabgeneral'] = 'General Options';
$lang['admin_tabwatermark'] = 'Watermark Options';
$lang['animationtype'] = 'Animation type';
$lang['edit_animationtype'] = 'Edit Animation type:';
$lang['delete_show'] = 'Delete show';
$lang['admin_swfupload'] = 'jQuery upload: NEW - Upload multiple files at once by ctrl/shift-selecting in dialog.';
$lang['admin_httpupload'] = 'HTTP Upload: Single file upload system (old system) ';
$lang['admin_tag'] = 'Tag to display this Show';
$lang['admin_slidecount'] = 'slide count';
$lang['admin_upmethod'] = 'Upload Method:';
$lang['admin_delete_warning']='you want to delete show: ';
$lang['uploadwatermark'] = 'Upload new watermark image:';
$lang['loadjqueryscripts']='Load jQuery script';

//editshow
$lang['editshow_tabprefs'] = 'Show preferences';
$lang['editshow_taburl'] = 'URL settings';
$lang['editshow_tabtemplate'] = 'Templates';
$lang['editshow_tabname'] = 'Change name';
$lang['editshow_intpage'] = 'internal page';
$lang['editshow_intpage_info'] = 'link to an internal page using Alias depending on your &#039;url_rewriting&#039; settings.';
$lang['editshow_extpage'] = 'external page';
$lang['editshow_extpage_info'] ='Link to an external page like: &#039;http://www.google.be&#039;';
$lang['editshow_image'] = 'image'; 
$lang['editshow_image_info'] = 'link to the original image file';
$lang['editshow_noscale'] = 'No scale';
$lang['editshow_showall'] = 'Show all';
$lang['editshow_exact'] = 'Exact fit (standard)';
$lang['editshow_fit'] = 'Fit but constrain Properties';
$lang['editshow_linko'] = 'Link options';
$lang['editshow_linkloc'] = 'Link location';
$lang['editshow_linktarget'] = 'Link target';
$lang['editshow_self_info'] = 'open links in same frame';
$lang['editshow_blank_info'] = 'open links in a new window';
$lang['editshow_parent_info'] = '(if your using frames) this &#039;ll open links in the previous frame';
$lang['editshow_top_info'] = '(if your using frames) this &#039;ll open links in the top frame';
$lang['editshow_example'] = 'example: (press "Apply button" to refresh)';
$lang['editshow_settings_nav'] = 'Settings navigation bar';
$lang['editshow_choose_presets'] = 'Choose from presets';
$lang['editshow_bar_onoff'] = 'Navigation bar ';
$lang['editshow_border_color'] = 'Nav. <strong>Border color</strong>';
$lang['editshow_border_size'] = 'Nav. <strong>Border size</strong> (in px)';
$lang['editshow_border_rad'] = 'Nav. <strong>Border radius</strong> (in px)';
$lang['editshow_bg_color'] = 'Nav. <strong>Background color</strong>';
$lang['editshow_bg_acolor'] = 'Nav. <strong>Background active color</strong> ';
$lang['editshow_text_onoff'] = '<strong>Show text</strong> ';
$lang['editshow_text_color'] = 'Nav. <strong>Text color</strong> ';
$lang['editshow_button_size'] = 'Nav. <strong>Button size</strong> (in px)';
$lang['editshow_position'] = 'Nav. <strong>Position</strong>';
$lang['editshow_margin'] = 'Nav. <strong>Margin</strong>(in px)';
$lang['editshow_basic'] = 'Basic';
$lang['editshow_width'] = '<strong>Width</strong> (pixels) ';
$lang['editshow_height'] = '<strong>Height</strong> (pixels) ';
$lang['editshow_scaling'] = 'Picture scaling:';
$lang['editshow_slide_options'] = 'Slideshow options';
$lang['editshow_shuffle_onoff'] = '<strong>Shuffle</strong> ';
$lang['editshow_auto_onoff'] = '<strong>Autoplay</strong> ';
$lang['editshow_controls_onoff'] = '<strong>Show controls</strong> ';
$lang['editshow_thumbs_onoff'] = '<strong>Use thumbnails for navigation</strong> ';
$lang['editshow_controls_onoff_warning'] = '(<strong>Caution!</strong> this options off will hide your URL links )';
$lang['editshow_controlshide_onoff'] = '<strong>Show controls on hover</strong> ';
$lang['editshow_transition'] = 'Transition Type';
$lang['editshow_rotation'] = '<strong>Rotation time</strong> The amount of time to show each slide (1 = 1000 milliseconds)';
$lang['editshow_transitiontime'] = '<strong>Transition time</strong> The speed of the transition animation (1 = 1000 milliseconds)';
$lang['editshow_slide_bg'] = 'Slide background color';
$lang['editshow_slide_bg_info'] = '(example: <strong>#000000</strong> or <strong>none</strong> if you want transparent background.)';
$lang['editshow_text_options'] = 'Text display options';
$lang['editshow_show_title'] = 'Show title';
$lang['editshow_show_comment'] = 'Show comment';
$lang['editshow_bg_text'] = 'Show background for text';
$lang['editshow_text_bgcolor'] = 'Text background color';
$lang['editshow_text_color'] = 'Text color';
$lang['editshow_font_type'] = 'Font-family type';
$lang['editshow_title_size'] = '<strong>Title size</strong> in px';
$lang['editshow_comment_size'] = '<strong>Comment size</strong>in px';
$lang['editshow_comment_pos'] = 'Comment position';

$lang['editshow_start_slide'] = '<strong>Start Slide</strong> (0)';
$lang['editshow_pauseonhover'] = '<strong>Pause the slider on Hover</strong> ';
$lang['editshow_thumb_nav'] = '<strong>Enable Thumbnail Navigation</strong> ';

$lang['editshow_nr_slices'] = 'Slices';
$lang['editshow_nr_slices_comment'] = 'The number of slices to use in the "Slice" transitions (eg 15)';

$lang['editshow_box_cols'] = 'Box (Cols x Rows)';
$lang['editshow_box_cols_comment'] = 'The number of columns and rows to use in the "Box" transitions (eg 8 x 4)';

$lang['editshow_nav_controls'] = 'Settings navigation controls';
$lang['editshow_controltypes'] = 'Control types';
$lang['editshow_trans_eas'] = 'Transition Easing';
$lang['editshow_control_left'] = 'left arrow (previous image)';
$lang['editshow_control_fullscr'] = '(<strong>caution!</strong> using this option \'ll hide your url-links)';
$lang['editshow_control_right'] = 'right arrow (next image)';
$lang['editshow_options_updated'] = 'Show options updated';
$lang['editshow_error_showid']= 'This show number is already taken!';

$lang['editshow_theme'] = 'Slideshow theme';
$lang['editshow_theme_comment'] = 'Use a pre-built theme or provide your own styles.';

$lang['editshow_nivo_css'] = 'Nivo CSS settings.';
$lang['editshow_captionopacity'] = 'Caption opacity. (eg 0.5)';
$lang['editshow_sizing'] = 'Slider Sizing';
$lang['editshow_sizing_comment'] = 'Responsive sliders will fill the width of the container (width = 0) or matches the given width.';
$lang['editshow_text_left'] = 'left';
$lang['editshow_text_center'] = 'center';
$lang['editshow_text_right'] = 'right';
$lang['nivo_prev'] = 'Prev';
$lang['nivo_next'] = 'Next';

//browsepictures
$lang['browse_prev_map'] = 'Previous map';
$lang['browse_browse_map'] = 'Browse map';
$lang['browse_current_pic'] = 'Current picture';
$lang['selectall']= ' Select all pictures';
$lang['selectpicture']='Select this picture';
$lang['error_nofilesuploaded']= 'No file uploaded!';
$lang['file_uploaded']= 'Image uploaded';
$lang['selectall'] = 'Select all';
$lang['uploadpicture'] = 'Picture upload';
$lang['uploadimages'] = 'Upload Images';

//watermark
$lang['watermark_trans_warning'] = 'This setting is not working for "PNG" images!';
$lang['watermark_warning'] = 'Options are saved, but we did not found watermark file!';
$lang['watermark_current'] = 'Current watermark';
$lang['watermark_position'] = 'Watermark position';
$lang['watermark_margin'] = 'Watermark margin (in px)';
$lang['watermark_margin_lr'] = 'margin left or right';
$lang['watermark_margin_tb'] = 'margin top or bottom';
$lang['watermark_trans'] = 'Wartermark transparency';
$lang['watermark_trans_100'] = '100 = no transparency';
$lang['watermark_example'] = 'Example: image with watermark.';


//buttons
$lang['EditShow'] = 'Edit Show settings';
$lang['back_to_admin']='Edit shows';
$lang['edit_slides'] = 'Edit slides';
$lang['name'] = 'Name';
$lang['changeshownumber']='Edit Show number';
$lang['upload'] = 'Upload';
$lang['cancel'] = 'Cancel';
$lang['apply'] = 'Apply';
$lang['submit'] = 'Save';
$lang['default']='Reset';

//TinyMCE
$lang['tinymce_description_picker'] = 'Insert Slideshows.';
$lang['tinymce_button_picker'] = 'Add Slideshow';

$lang['animation_info'] = <<<EOT
Choose <strong>&lt;swfobject&gt;</strong>if you want high quality transitions. Ken Burns (pan&zoom) and Full Screen mode.<br />
Choose <strong>jQuery</strong> if you do not like flash but still want different types of transition effects and more navigation controls. (Based on jQuery Cycle Plugin)
EOT;


$lang['friendlyname'] = 'Showtime2 Slideshow';
$lang['postinstall'] = 'Showtime2 - installation complete';
$lang['postuninstall'] = 'Showtime2 - uninstall complete';
$lang['really_uninstall'] = 'Really? Are you sure
you want to uninstall this fine module?';
$lang['uninstalled'] = 'Module Uninstalled.';
$lang['installed'] = 'Module version %s installed.';
$lang['upgraded'] = 'Module upgraded to version %s.';
$lang['use_tmp'] = 'Use tmp instead of uploads folder for css-files';
$lang['create_bak'] = 'Create bak-file when adding watermark';
$lang['moddescription'] = 'Choose among high quality transitions. Ken Burns (pan&amp;zoom) and many other effects. Full Screen mode. (ShowTime2 requires Adobe Flash player 10 or higher.) Demo can be found here: http://www.web2do.be/cms/showtime';

$lang['error'] = 'Error!';
$land['admin_title'] = 'Showtime2 Slideshow Admin Panel';
$lang['admindescription'] = 'This is a showtime module for animated slideshows. • Demo can be viewed here-> "http://www.web2do.be/showtime"';
$lang['accessdenied'] = 'Access Denied. Please check your permissions.';
$lang['welcome_text'] = '<p>Welcome to the Showtime2 Module admin section. Add it to your front-end pages with a {Showtime}</p>';

$lang['help'] = '<h3>What Does This Do?</h3>
<p>


Display images as animated slideshow. <br />
Choose among high quality transitions. Ken Burns (pan&amp;zoom) and many other effects. Full Screen mode.<br />
(ShowTime requires Adobe Flash player 10 or higher.)<br />
Visit Demo website: <a href="http://www.web2do.be/en/cms_modules/Module_Showtime" target="_blank" >ShowTime demo page</a>
<h3>How does it work?</h3>
<ul>
	<li><strong>1)</strong> <img src="[root_url]/modules/Showtime2/images/add_show.png" class="systemicon" alt="Add a show" title="Add a show"> Add show<br />
	&nbsp;&nbsp;&nbsp;&nbsp;Remember each show can hold slides and diferent show options, effects and more.
	<br /><br />
	</li>

	</li>
	<li><strong>2)</strong> <img src="[root_url]/modules/Showtime2/images/add_picture.png" class="systemicon"> Add pictures&nbsp;<br />
	For each picture there &#039;ll be one slide generated which can hold several options like: comment, link, title.
	<br />
<br />
	
	<li><strong>3)</strong> Insert Showtime in your pages.<br />
	If you&#039;r using TinyMCE, you&#039;ll see this new icon (<img src="[root_url]/modules/Showtime2/images/icon_TinyMCE.gif" class="systemicon">) in your WYSIWYG interface. Just click on your desired Slideshow.

	</li>
</ul>
<br />
<h3>Change the look of your Slideshow</h3>
If you click on tab "Module Preferences", you can adjust Basic settings and Slideshow options and Text display options for each slideshow.<br />
 <h3> Basic settings</h3>
<ul>
  <li><strong>width</strong>= Width in pixels</li>
	<li><strong>height</strong>= Height in pixels</li>
	<li><strong>window mode</strong>= window|opaque|transparent|direct|gpu Defines the way the slide show is rendered in your browser.. (<strong>Attention</strong>, opaque and transparent are <strong>not cross-browser compatible</strong>)<br />
using WMode Opaque will cause the flash to appear on the correct Z-index, so that it does not hide menus or other objects, whereas WMode Transparent is always on the top layer. For me the best experience is leave it on "Window"</li>
	<li><strong>picture scaling</strong>= noscale|showall|exactfit|noborder Change the scale mode of images.<br />
&nbsp;&nbsp;&nbsp;&nbsp;NoScale: display images 1:1<br />
&nbsp;&nbsp;&nbsp;&nbsp;ShowAll: scale images proportionally to fit the slide show area<br />
&nbsp;&nbsp;&nbsp;&nbsp;ExactFit: stretch images to exactly fit the slide show area<br />
&nbsp;&nbsp;&nbsp;&nbsp;NoBorder: scale images proportionally to fill the slide show area</li>
</ul>
<h3>Slideshow Options </h3>
<ul>
	<li><strong>transition</strong>= blur|fade|flash|zoomin|zoomout|slidedown|slideup|slideleft|slideright| wipeh|wipev|kenburns|flip|fliph|flipv Transition type.</li>
	<li><strong>shuffle</strong>= on|off Display images in random order.</li>
	<li><strong>autoplay</strong>= on|off Should the slide show start playing automatically?</li>
	<li><strong>show controls</strong>= on|off Display the cursor to control the slideshow.</li>
	<li><strong>transition type</strong>= <em>blur|fade|flash|zoomin|zoomout|slidedown|slideup|slideleft|slideright| wipeh|wipev|kenburns|flip|fliph|flipv&nbsp;Transition type.</em> </li>
	<li><strong>Transition Easing</strong> = prefix.suffix This setting changes the "acceleration curve" of a transition<br />
		&nbsp;&nbsp;&nbsp;&nbsp;prefixes: Linear, Elastic, Bounce, Circular, Cubic, Quadratic, Quintic, Quartic, Sine, Exponential, Back<br />
		&nbsp;&nbsp;&nbsp;&nbsp;suffixes: easeNone, easeIn, easeOut, easeInOut
	</li>
	<li><strong>rotationtime</strong>= Time in seconds to pause between each slide in playback mode</li>
	<li><strong>transitiontime</strong>= Time in seconds for transitions (0 = no transition)</li>
	<li><strong>Slide background color</strong>= #000000 Change the background color. Use wmode=transparent to override bgcolor.</li>
</ul>


<h3>Text display options</h3>
<ul>
  <li><strong>show title</strong>=on|off Display title of an image.</li>
  <li><strong>show comment</strong>=on|off Display caption (description) of an image.</li>
  <li>
   <strong>Show background for text</strong>
    = on|off Display the background color for text window
  </li>
  <li><strong>Text backgroundcolor</strong>= #000000 Change bacground color for text window</li>
  <li>
    <strong>Text color</strong>= #ffffff Change text color.</li>
  <li><strong>Font-family type</strong>= Set Fonttype for text</li>
  <li><strong>Title size</strong>= 16 change title size in px.</li>
  <li><strong>Comment size</strong>= 12 change comment size in px.</li>
</ul>


<h3>Support</h3>
<ul>
  <li>FAQs, extended module help and Troubleshooting can be found in the <a href="http://www.web2do.be/Module_Showtime">www.web2do.be</a>.</li>
  <li>For the latest version of this module or to file a Feature Request or Bug Report, please visit the Module Forge <a href="http://dev.cmsmadesimple.org/projects/showtime2">Showtime module</a>.</li>
  <li>Additional discussion of this module may also be found in the <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>. You are warmly invited to open a new thread if you have not found an answer to your question.</li>
  <li>Lastly, you may have some success emailing the author directly.</li>
</ul>
<h3>Copyright and License</h3>
<p>Copyright &copy; 2009, Peter Orije <a href="mailto:pedrosken@gmail.com">email</a>. All Rights Are Reserved.</p>
<p>Copyright &copy; 2016, Peter Engels <a href="mailto:showtime@acg-bonn.de">email</a>.</p>
<p>This module has been released under the <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. You must agree to this license before using the module.</p>

';

?>
