<?php
//upload tool
$lang['drag_and_drop']= 'Dateien zum Hochladen <strong>hier</strong> ablegen.<br />';
$lang['press_button'] = 'Wenn das Hochladen beendet ist, diesen Button drücken:';
$lang['add_files'] = 'Dateien hinzugügen...';
$lang['start_upload'] = 'Hochladen starten';
$lang['cancel_upload'] = 'Hochladen abbrechen';
$lang['delete_upload'] = 'Ausgewählte löschen';
$lang['upload_done'] = 'Hochladen der Bilder beendet';
$lang['files_uploaded'] = 'Sie haben %s Bilder hochgeladen.';
$lang['newdirname'] = 'Verzeichnis erstellen';
$lang['newdirsuccess'] = 'Verzeichnis erstellt.';
$lang['newdirfail'] = 'Verzeichnis konnte nicht erstellt werden';
$lang['ok'] = 'OK';

$lang['tinymce_description_picker'] = 'Einfaches Einfügen von Slideshows.';
$lang['tinymce_button_picker'] = 'Slideshow hinzufügen';
$lang['addpicture'] = 'Bilder hinzufügen';
$lang['addpicture_nonadded'] = 'Keine Bilder hinzugefügt.';
$lang['addpicture_1added'] = 'Bild hinzugefügt.';
$lang['addpicture_added'] = 'Bilder hinzugefügt.';
$lang['addshow'] = 'Show hinzufügen';
$lang['showadded'] = 'Show hinzugefügt.';
$lang['addshow_settings'] = 'Übernehme die Einstellungen der vorherigen Show&#039;s';
$lang['addshow_default'] = 'Benutze die Standardeinstellungen';
$lang['error_nonamegiven'] = 'Sie müssen einen Namen eintragen.';
$lang['addslides_updatesuccess'] = 'Showtime wurde erfolgreich aktualisiert.';
$lang['addslides_bulksuccess'] = 'Die Massenaktion wurde erfolgreich beendet.';
$lang['addslides_bulkconf'] = 'Sind sich sicher, dass Sie diese Massenaktion durchführen wollen ?';
$lang['addslides_bulkwith'] = 'mit Auswahl';
$lang['addslides_moveup'] = 'Slide nach oben verschieben';
$lang['addslides_movedown'] = 'Slide nach unten verschieben';
$lang['addslides_nolink'] = 'Kein Link';
$lang['addslides_changepic'] = 'Bild für diesen Slide ändern';
$lang['addslides_hide'] = 'Slide verstecken';
$lang['addslides_show'] = 'Slide anzeigen';
$lang['addslides_changecomm'] = 'Slidename und Kommentar ändern';
$lang['addslides_delete'] = 'löschen';
$lang['addslides_addsome'] = 'Bitte erst Bilder hinzufügen.';
$lang['addslides_active'] = 'aktivieren';
$lang['addslides_inactive'] = 'deaktivieren';
$lang['addslides_alink'] = 'Vergeben Sie einen Link';
$lang['addslides_save'] = 'Speichern Sie Ihre Änderungen!';
$lang['addslides_deletesuccess'] = 'Der Slide wurde erfolgreich gelöscht.';
$lang['addslides_crop'] = 'Bild zuschneiden';
$lang['addslides_turn_left'] = 'Bild links drehen';
$lang['addslides_turn_right'] = 'Bild rechts drehen';
$lang['addslides_add_watermark'] = 'Wasserzeichen hinzufügen';
$lang['addslides_watermark_added'] = 'Wasserzeichen hinzugefügt';
$lang['addslides_image_rotated'] = 'Bild wurde gedreht';
$lang['Crop_Image'] = 'Bild zuschneiden';
$lang['Crop_this_Image'] = 'Dieses Bild zuschneiden';
$lang['picture'] = 'Bild';
$lang['order'] = 'Reihenfolge';
$lang['picture_name'] = 'Slidename';
$lang['picture_link'] = 'Slidelink';
$lang['picture_description'] = 'Slidekommentar';
$lang['change_picture'] = 'Bild<br />wechseln';
$lang['status'] = 'Status';
$lang['edit_picture'] = 'Bild bearbeiten';
$lang['internal_link'] = 'Link für diesen Slide:';
$lang['changecomm_linkimage'] = 'Link zu diesem Bild';
$lang['changecomm_message'] = 'Der Slide wurde erfolgreich aktualisiert.';
$lang['changepic_message'] = 'Bild wurde gewechselt.';
$lang['changepicture'] = 'Bild wechseln';
$lang['default_addimages'] = 'Zuerst einige Bilder zur Slideshow hinzufügen:';
$lang['changepic_noshow'] = 'Keine Show gefunden! <strong>show="%s"</strong> existiert nicht.';
$lang['changepic_noshow2'] = 'Keine Show gefunden! Sie müssen hinzufügen &#039;show&#039; parameter : {Showtime2 <strong>show="2"</strong>}';
$lang['admin_updated'] = 'Die Optionen wurden erfolgreich aktualisiert.';
$lang['admin_taboverview'] = 'Showüberblick';
$lang['admin_tabgeneral'] = 'Allgemeine Optionen';
$lang['admin_tabwatermark'] = 'Wasserzeichenoptionen';
$lang['animationtype'] = 'Animationstyp';
$lang['edit_animationtype'] = 'Animationtyp bearbeiten:';
$lang['delete_show'] = 'Show löschen';
$lang['admin_swfupload'] = 'SWFupload: NEU - Laden Sie mehrere Dateien sofort hoch, in dem Sie Ctrl/Shift im Dialogfenster drücken. Flash wird benötigt.';
$lang['admin_httpupload'] = 'HTTP Upload: Einzeldateiuploadsystem (Altes System) ';
$lang['admin_tag'] = 'Kode zum Anzeigen der Show';
$lang['admin_slidecount'] = 'Slidezähler';
$lang['admin_upmethod'] = 'Typ des Uploads:';
$lang['admin_delete_warning'] = 'sie möchten die folgende Show löschen: ';
$lang['uploadwatermark'] = 'Neues Wasserzeichenbild hochladen:';
$lang['loadjqueryscripts'] = 'jQuery-Skript laden';
$lang['editshow_tabprefs'] = 'Showeinstellungen';
$lang['editshow_taburl'] = 'URL Einstellungen';
$lang['editshow_tabname'] = 'Name ändern';
$lang['editshow_intpage'] = 'interne Seite';
$lang['editshow_intpage_info'] = 'Link zu einer internen Seite unter Benutzung eines Alias in Abhängigkeit von Ihren &#039;url_rewriting&#039; Einstellungen.';
$lang['editshow_extpage'] = 'externe Seite';
$lang['editshow_extpage_info'] = 'Link zu einer externen Seite wie: &#039;http://www.google.be&#039;';
$lang['editshow_image'] = 'Bild';
$lang['editshow_image_info'] = 'Link zum Originalbild';
$lang['editshow_noscale'] = 'Keine Skalierung';
$lang['editshow_showall'] = 'Alle anzeigen';
$lang['editshow_exact'] = 'Exakte Anpassung (Standard)';
$lang['editshow_fit'] = 'Anpassung mit beschränkten Eigenschaften';
$lang['editshow_linko'] = 'Linkoptionen';
$lang['editshow_linkloc'] = 'Zielbestimmung für den Link';
$lang['editshow_linktarget'] = 'Linkziel';
$lang['editshow_self_info'] = 'Link im selben Frame öffnen';
$lang['editshow_blank_info'] = 'Link in einem neuen Fenster öffnen';
$lang['editshow_parent_info'] = '(Wenn Sie Frames nutzen) diesen Link im vorherigen Frame öffnen';
$lang['editshow_top_info'] = '(Wenn Sie Frames nutzen) diesen Link im Top-Frame öffnen';
$lang['editshow_example'] = 'Beispiel: (Klicken Sie "Anwenden" zum Aktualisieren)';
$lang['editshow_settings_nav'] = 'Einstellungen Navigationsleiste';
$lang['editshow_choose_presets'] = 'Auswählen aus den Voreinstellungen';
$lang['editshow_bar_onoff'] = 'Navigationsleiste an/aus';
$lang['editshow_border_color'] = 'Nav. <strong>Rahmenfarbe</strong>';
$lang['editshow_border_size'] = 'Nav. <strong>Rahmengröße</strong> (in px)';
$lang['editshow_border_rad'] = 'Nav. <strong>Rahmenradius</strong> (in px)';
$lang['editshow_bg_color'] = 'Nav. <strong>Hindergrundfarbe</strong>';
$lang['editshow_bg_acolor'] = 'Nav. <strong>aktive Hindergrundfarbe</strong> ';
$lang['editshow_text_onoff'] = '<strong>Showtext</strong> an/aus';
$lang['editshow_text_color'] = 'Textfarbe';
$lang['editshow_button_size'] = 'Nav. <strong>Schaltflächengröße</strong> (in px)';
$lang['editshow_position'] = 'Nav. <strong>Position</strong>';
$lang['editshow_margin'] = 'Nav. <strong>Seitenrand</strong>(in px)';
$lang['editshow_basic'] = 'Grundeinstellung';
$lang['editshow_width'] = '<strong>Breite</strong> (Pixel) ';
$lang['editshow_height'] = '<strong>Höhe</strong> (Pixel) ';
$lang['editshow_scaling'] = 'Bildskalierung:';
$lang['editshow_slide_options'] = 'Slideshowoptionen';
$lang['editshow_shuffle_onoff'] = '<strong>Zufallsauswahl</strong> an/aus';
$lang['editshow_auto_onoff'] = '<strong>Automatisches Abspielen</strong> an/aus';
$lang['editshow_controls_onoff'] = '<strong>Kontrollelemente</strong> an/aus';
$lang['editshow_thumbs_onoff'] = '<strong>Miniaturansichten für die Navigation verwenden</strong> ';
$lang['editshow_transition'] = 'Überblendtyp';
$lang['editshow_rotation'] = '<strong>Rotationzeit</strong> (Sekunden)';
$lang['editshow_transitiontime'] = '<strong>Überblendzeit</strong> (Sekunden)';
$lang['editshow_slide_bg'] = 'Slidehindergrundfarbe';
$lang['editshow_slide_bg_info'] = '(Beispiel: <strong>#000000</strong> oder <strong>none</strong> wenn Sie einen transparenten Hindergrund wollen.)';
$lang['editshow_text_options'] = 'Textanzeigeoptionen';
$lang['editshow_show_title'] = 'Titel anzeigen';
$lang['editshow_show_comment'] = 'Kommentar anzeigen';
$lang['editshow_bg_text'] = 'Hindergrund für den Text anzeigen';
$lang['editshow_text_bgcolor'] = 'Texthindergrundfarbe';
$lang['editshow_font_type'] = 'Schrifttypen';
$lang['editshow_title_size'] = '<strong>Titelgröße</strong> in Pixel';
$lang['editshow_comment_size'] = '<strong>Kommentargröße</strong> in Pixel';
$lang['editshow_comment_pos'] = 'Kommentarposition';
$lang['editshow_nav_controls'] = 'Einstellungen Navigationsleiste';
$lang['editshow_controltypes'] = 'Kontrolltypen';
$lang['editshow_trans_eas'] = 'Überblendungen reduzieren';
$lang['editshow_control_left'] = 'linker Pfeil (vorheriges Bild)';
$lang['editshow_control_fullscr'] = '(<strong>Achtung!</strong> Diese Option verbirgt die URL-Links!)';
$lang['editshow_control_right'] = 'rechter Pfeil (nächstes Bild)';
$lang['editshow_options_updated'] = 'Showoptionen akutualisiert';
$lang['editshow_error_showid'] = 'Diese Shownummer existiert bereits!';
$lang['editshow_pauseonhover'] = '<strong>Pausiere die Show bei Mauskontakt</strong> ';
$lang['editshow_controlshide_onoff'] = '<strong>Zeige Steuerungselemente bei Mauskontakt</strong> ';
$lang['editshow_nivo_css'] = 'Nivo CSS Einstellungen.';
$lang['editshow_theme_comment'] = 'Voreingestelltes Theme, oder eigenes wählen.';
$lang['editshow_captionopacity'] = 'Durchsichtigkeit Beschriftung. (z.B. 0.5)';
$lang['editshow_sizing'] = 'Slider Sizing';
$lang['editshow_sizing_comment'] = 'Responsive sliders füllen die Breite des Containers (Breite = 0) oder nehmen die angegebene Breite ein.';
$lang['editshow_nr_slices_comment'] = 'Anzahl an Slices, die in Slice-Übergängen benutzt werden soll. (z.B. 15)';
$lang['editshow_box_cols'] = 'Box (Spalten x Zeilen)';
$lang['editshow_box_cols_comment'] = 'Anzahl von Spalten und Zeilen bei "Box"-Übergängen (z.B. 8 x 4)';
$lang['editshow_controls_onoff_warning'] = '(<strong>Achtung!</strong> Das Abschalten verbirgt die URL-Links!)';
$lang['editshow_text_left'] = 'links';
$lang['editshow_text_center'] = 'zentriert';
$lang['editshow_text_right'] = 'rechts';
$lang['nivo_prev'] = 'Zurück';
$lang['nivo_next'] = 'Vor';

$lang['browse_prev_map'] = 'übergeordnetes Verzeichnis';
$lang['browse_browse_map'] = 'Verzeichnis durchsuchen';
$lang['browse_current_pic'] = 'aktuelle Bild';
$lang['selectall'] = 'alle auswählen';
$lang['selectpicture'] = 'diese Bild auswählen';
$lang['error_nofilesuploaded'] = 'Keine Datei hochgeladen!';
$lang['file_uploaded'] = 'Bild hochgeladen';
$lang['uploadpicture'] = 'Bilder einstellen/hochladen';
$lang['uploadimages'] = 'Hochladen der Bilder';
$lang['newdirname'] = 'Neues Verzeichnis erstellen';
$lang['newdirsuccess'] = 'Neues Verzeichnis erstellt';
$lang['newdirfail'] = 'Verzeichnis konnte nicht erstellt werden.';
$lang['watermark_trans_warning'] = 'Diese Einstellung funktioniert nicht mit "PNG" Bildern!';
$lang['watermark_warning'] = 'Einstellungen wurden gespeichert, aber es wurde kein Wasserzeichenbild gefunden!';
$lang['watermark_onoff'] = 'Wasserzeichen an/aus';
$lang['watermark_current'] = 'aktuelles Wasserzeichen';
$lang['watermark_position'] = 'Wasserzeichenposition';
$lang['watermark_margin'] = 'Wasserzeichenabstand (in px)';
$lang['watermark_margin_lr'] = 'Abstand links oder rechts';
$lang['watermark_margin_tb'] = 'Abstand oben oder unten';
$lang['watermark_trans'] = 'Wasserzeichentransparenz';
$lang['watermark_trans_100'] = '100 = keine Transparenz';
$lang['watermark_example'] = 'Beispiel: Bild mit Wasserzeichen.';
$lang['create_bak'] = 'Erzeuge Sicherungskopie beim Erstellen von Wasserzeichen';
$lang['EditShow'] = 'Show-Einstellungen bearbeiten';
$lang['back_to_admin'] = 'Shows bearbeiten';
$lang['edit_slides'] = 'Slides bearbeiten';
$lang['name'] = 'Name';
$lang['changeshownumber'] = 'Shownummer bearbeiten';
$lang['upload'] = 'Hochladen';
$lang['cancel'] = 'Abbrechen';
$lang['apply'] = 'Anwenden';
$lang['submit'] = 'Sichern';
$lang['default'] = 'Standardeinstellungen übernehmen';
$lang['use_tmp'] = 'tmp Verzeichnis anstelle von uploads für css-Dateien verwenden';
$lang['animation_info'] = 'Wählen Sie <strong>&lt;swfobject&gt;</strong>, wenn Sie hochwertige Überblendungen wollen. Ken Burns (Pan &amp; Zoom) oder Vollbild.<br />
Wählen Sie <strong>jQuery</strong>, wenn Sie kein Flash wollen, aber unterschiedliche Überblendeffekte und mehr Nagivationsmöglichkeiten. (Basierend auf dem jQuery Cycle Plugin)<br/>
<strong>Nivo Slider</strong> bietet zufällige Überblendungen und responsive Slideshows.';
$lang['friendlyname'] = 'Showtime2 Slideshow';
$lang['postinstall'] = 'Showtime2 - Installation fertig';
$lang['postuninstall'] = 'Showtime2 - Deinstallation fertig';
$lang['really_uninstall'] = 'Wirklick ? Sind Sie sicher, dass Sie dieses wunderbare Modul deinstallieren möchten ?';
$lang['uninstalled'] = 'Das Module wurde deinstalliert.';
$lang['installed'] = 'Modulversion %s installiert.';
$lang['upgraded'] = 'Modul aktualisiert auf Version %s.';
$lang['moddescription'] = 'Wählen Sie hochwertige Überblendeffekte. Ken Burns (Pan &amp; Zoom) und viele andere Effekte. Vollbildmodus (ShowTime2 benötigt Adobe Flash Player 10 oder höher.) Eine Demo finde Sie hier: http://www.web2do.be/cms/showtime';
$lang['error'] = 'Fehler!';
$lang['admindescription'] = 'Dies ist ein Showtime Modul für animierte Slideshows. &bull; Eine Demo gibt es hier-> "http://www.web2do.be/showtime"';
$lang['accessdenied'] = 'Zugriff verweigert! Bitte überprüfen Sie Ihre Rechte.';
$lang['welcome_text'] = '<p>Willkommen in der Sowtime Modul Administration. Fügen Sie die Administration den Front-End-Seiten hinzu mit {Showtime}</p>';
$lang['help'] = '
<h3>Was macht dieses Modul ?</h3>

Anzeigen von Bilder als animierte Slideshow. <br />
Wählen Sie hochwertige Überblendungen. Ken Burns (Pan &amp; Zoom) und viele andere Effekte. Vollbildmodus.<br />
(ShowTime2 benötigt Adobe Flash Player 10 oder höher.)<br />
Besuchen Sie die Demo Webseite : <a href="http://www.web2do.be/en/cms_modules/Module_Showtime" target="_blank" >ShowTime Demoseite</a>
<h3>Wie funktioniert es ?</h3>
<ul>
	<li><strong>1)</strong> <img src="[root_url]/modules/Showtime2/images/add_show.png" class="systemicon" alt="Show hinzufügen" title="Show hinzufügen"> Show hinzufügen<br />
	&nbsp;&nbsp;&nbsp;&nbsp;Zur Erinnerung für jede Show kann unterschiedliche Slides, verschiedene Showoptionen, Effekte und mehr beinhalten.
	<br /><br />
	</li>

	</li>
	<li><strong>2)</strong> <img src="[root_url]/modules/Showtime2/images/add_picture.png" class="systemicon"> Bilder hinzufügen<br />
	Für jedes Bild wird ein Slide erstellt, der mehrere Eigenschaften wie : Kommentar, Link, Titel, beinhalten kann.
	<br />
<br />
	<li><strong>3)</strong> Showtime in Ihre Seiten einfügen.<br />
	Wenn Sie TinyMCE verwenden, finden Sie ein neues Icon (<img src="[root_url]/modules/Showtime2/images/icon_TinyMCE.gif" class="systemicon">) in Ihrem WYSIWYG Interface. Wählen Sie hier die gewünschte Slideshow aus.
	
</ul>
<br />
<h3>Ändern Sie das Aussehen Ihrer Show</h3>
Wenn Sie auf den Reiter "Showeinstellungen" klicken können Sie Basiseinstellungen, Slideshowoptionen und Textoptionen für jede Slideshow ändern.<br />
 <h3>Grundeinstellungen</h3>
<ul>
    <li><strong>Breite</strong>= in Pixeln</li>
	<li><strong>Höhe</strong>= in Pixeln</li>
	<li><strong>Window mode</strong>= window|opaque|transparent|direct|gpu definiert die Art der Darstellung in Ihrem Browser.. (<strong>Achtung</strong>, opaque und transparent sind <strong>nicht Cross-Browser kompatibel</strong>)<br />
Die Einstellung WMode Opaque veranlasst Flash den korrekten Z-index einzustellen, damit Menüs oder andere Objekte nicht verdeckt werden. Der WMode Transparent arbeitet ausschließlich auf dem Top-Layer. Nach meiner Erfahrung ist die beste Einstellung "Window"</li>
	<li><strong>Bildskalierung</strong>= noscale|showall|exactfit|noborder Ändern Sie die Bildskalierung der Bilder.<br />
&nbsp;&nbsp;&nbsp;&nbsp;NoScale: zeigt die Bilder 1:1<br />
&nbsp;&nbsp;&nbsp;&nbsp;ShowAll: skaliert die Bilder proportional zur Anpassung an das Slideshow Fenster<br />
&nbsp;&nbsp;&nbsp;&nbsp;ExactFit: streckt die Bilder zur exakten Anpassung an das Slideshow Fenster<br />
&nbsp;&nbsp;&nbsp;&nbsp;NoBorder: skaliert die Bilder proportional zum Ausfüllen des Slideshow Fensters</li>
</ul>
<h3>Slideshowoptionen</h3>
<ul>
	<li><strong>Überblendtyp</strong>= blur|fade|flash|zoomin|zoomout|slidedown|slideup|slideleft|slideright| wipeh|wipev|kenburns|flip|fliph|flipv sind die auswählbare Überblendtypen.</li>
	<li><strong>Zufallsauswahl</strong>= on|off zeigt die Bilder in zufälliger Reihenfolge.</li>
	<li><strong>Automatisches Abspielen</strong>= on|off läßt die Slideshow automatisch starten.</li>
	<li><strong>show controls</strong>= on|off Display the cursor to control the slideshow.</li>
	<li><strong>Überblendungen reduzieren</strong> = prefix.suffix diese Einstellung verändert die "Accelerationskurve" einer Überblendung<br />
		&nbsp;&nbsp;&nbsp;&nbsp;prefixes: Linear, Elastic, Bounce, Circular, Cubic, Quadratic, Quintic, Quartic, Sine, Exponential, Back<br />
		&nbsp;&nbsp;&nbsp;&nbsp;suffixes: easeNone, easeIn, easeOut, easeInOut
	</li>
	<li><strong>Rotationzeit</strong>= Zeit in Sekunden zum Aussetzen zwischen jedem Slide im Playbackmode</li>
	<li><strong>Überblendzeit</strong>= Überblendzeit in Sekunden (0 = keine Überblendung)</li>
	<li><strong>Slidehindergrundfarbe</strong>= #000000 Wechselt die Hindergrundfarbe. Die Einstellung wmode=transparent überschreibt die bgcolor.</li>
</ul>


<h3>Textanzeigeoptionen</h3>
<ul>
  <li><strong>Titel anzeigen</strong>=an|aus zeigt den Titel eines Bildes an.</li>
  <li><strong>Kommentar anzeigen</strong>=an|aus zeigt den Kommentar eines Bildes an.</li>
  <li>
   <strong>Hindergrund für den Text anzeigen </strong>=an|aus zeigt den Hindergrund eines Textfensters an.</li>
  <li><strong>Texthindergrundfarbe</strong>= #000000 wechselt die Hindergrundfarbe eines Textfensters.</li>
  <li><strong>Textfarbe</strong>= #ffffff wechselt die Textfarbe.</li>
  <li><strong>Schrifttypen</strong>= verändert die Schrifttypen.</li>
  <li><strong>Titelgröße</strong>= 16 wechselt die Titelgröße (in Pixel).</li>
  <li><strong>Kommentargröße</strong>= 12 wechselt die Kommentargröße (in Pixel).</li>
</ul>


<h3>Support</h3>
<ul>
  <li>FAQs, erweiterte Modulhilfe und Fehlersuche findet man unter <a href="http://www.web2do.be/Module_Showtime">www.web2do.be</a>.</li>
  <li>Für die aktuelle Version dieses Moduls oder Leistungsmerkmale oder Fehlerreports, bitte das Module Forge aufsuchen <a href="http://dev.cmsmadesimple.org/projects/showtime2">Showtime 2</a>.</li>
  <li>Ergänzende Beiträge zu diesem Modul findet man in den <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>. Sie sind herzlich eingeladen einen neuen Beitrag zu schreiben, wenn Sie keine Antworten auf Ihre Fragen finden.</li>
  <li>Zuletzt haben Sie die Möglichkeit den Autor direkt per Mail zu erreichen.</li>
</ul>
<h3>Copyright und Lizenzen</h3>
<p>Copyright &copy; 2009, Peter Orije <a href="mailto:pedrosken@gmail.com">E-Mail</a>. Alle Rechte gesichert.</p>
<p>Copyright &copy; 2016, Peter Engels <a href="mailto:showtime@acg-bonn.de">E-Mail</a>.</p>
<p>Dieses Modul wurde erstellt unter den <a href="http://www.gnu.org/licenses/licenses.html#GPL">GNU Public License</a>. Sie müssen diese Bedingungen akzeptieren, bevor Sie das Modul benutzen.</p>

';
$lang['qca'] = 'P0-1458450664-1284573084918';
$lang['utma'] = '156861353.632774904.1320245042.1320245042.1320245042.1';
$lang['utmc'] = '156861353';
$lang['utmz'] = '156861353.1320245042.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none)';
$lang['utmb'] = '156861353';
?>