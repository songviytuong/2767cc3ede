<?php
//TinyMCE
$lang['tinymce_description_picker'] = 'Insert Slideshows.';
$lang['tinymce_button_picker'] = 'Add Slideshow';

$lang['addpicture'] = 'Afbeeldingen toevoegen';
$lang['addpicture_nonadded'] = 'Geen afbeeldingen toegevoegd.';
$lang['addpicture_1added'] = 'Afbeelding toegevoegd';
$lang['addpicture_added'] = 'Afbeeldingen toegevoegd';
$lang['addshow'] = 'Show toevoegen';
$lang['showadded'] = 'Show toegevoegd.';
$lang['addshow_settings'] = 'Copieer instellingen van vorige show&#039;s';
$lang['addshow_default'] = 'gebruik standaard instellingen';
$lang['error_nonamegiven'] = 'U moet een naam opgeven.';
$lang['addslides_updatesuccess'] = 'Showtime is aangepast.';
$lang['addslides_bulksuccess'] = 'Bulk actie is uitgevoerd.';
$lang['addslides_bulkconf'] = 'Ben je zeker om deze bulk actie uit te voeren?';
$lang['addslides_bulkwith'] = 'Met geselecteerde';
$lang['addslides_moveup'] = 'Verplaats dia omhoog';
$lang['addslides_movedown'] = 'Verplaats dia omlaag';
$lang['addslides_nolink'] = 'geen link';
$lang['addslides_changepic'] = 'Pas afbeelding aan voor deze dia';
$lang['addslides_hide'] = 'Verberg deze dia';
$lang['addslides_show'] = 'Toon deze dia';
$lang['addslides_changecomm'] = 'Dia en commentaar aanpassen';
$lang['addslides_delete'] = 'verwijderen';
$lang['addslides_addsome'] = 'Voeg eerst afbeeldingen toe.';
$lang['addslides_active'] = 'actief';
$lang['addslides_inactive'] = 'inactief';
$lang['addslides_alink'] = 'Link toevoegen';
$lang['addslides_save'] = 'Aanpassingen opslaan';
$lang['addslides_deletesuccess'] = 'Dia verwijderd';
$lang['addslides_crop'] = 'Deze afbeelding bijsnijden';
$lang['addslides_turn_left'] = 'Linkerkant van afbeelding';
$lang['addslides_turn_right'] = 'Rechterkant van afbeelding';
$lang['addslides_add_watermark'] = 'Watermerk toevoegen';
$lang['Crop_Image'] = 'Afbeelding bijsnijden';
$lang['Crop_this_Image'] = 'Deze afbeelding bijsnijden';
$lang['drag_and_drop'] = 'U kunt <strong>drag &amp; drop</strong> gebruiken van bestanden op uw computer naar deze website.<br />';
$lang['press_button'] = 'Klik op deze knop als u klaar bent:';
$lang['add_files'] = 'Bestanden toevoegen...';
$lang['start_upload'] = 'Start uploaden';
$lang['cancel_upload'] = 'Uploaden annuleren';
$lang['delete_upload'] = 'Verwijder geselecteerden';
$lang['upload_done'] = 'Uploaden van afbeeldingen voltooid';
$lang['files_uploaded'] = 'U heeft %s afbeelding(en) ge&uuml;pload';
$lang['newdirname'] = 'Maak een nieuwe map';
$lang['ok'] = 'OK ';
$lang['picture'] = 'afbeelding';
$lang['order'] = 'Volgorde';
$lang['picture_name'] = 'Dia naam';
$lang['picture_link'] = 'Dia link';
$lang['picture_description'] = 'Dia commentaar';
$lang['change_picture'] = 'Afbeelding<br />aanpassen';
$lang['status'] = 'actief';
$lang['edit_picture'] = 'Bewerk afbeelding';
$lang['internal_link'] = 'Link voor deze dia:';
$lang['changecomm_linkimage'] = 'Link naar deze afbeelding';
$lang['changecomm_message'] = 'Dia aangepast.';
$lang['changecomm_nolink'] = 'geen link';
$lang['changecomm_linktitle'] = '<strong>link naar deze afbeelding.</strong><br /> Om een link te maken naar deze slide, kunt u de nature van de link hieronder kiezen.';
$lang['changepic_message'] = 'Afbeelding aangepast.';
$lang['changepicture'] = 'Afbeelding aanpassen';
$lang['default_addimages'] = 'Voeg eerst afbeeldingen toe voor deze diashow:';
$lang['changepic_noshow'] = 'Geen show gevonden! <strong>show=&quot;%s&quot;</strong> bestaat niet.';
$lang['changepic_noshow2'] = 'Geen show gevonden! U moet de &#039;show&#039; parameter toevoegen: {Showtime <strong>show=&quot;2&quot;</strong>}';
$lang['admin_updated'] = 'De instellingen zijn aangepast.';
$lang['admin_taboverview'] = 'Show overzicht';
$lang['admin_tabgeneral'] = 'Algemene opties';
$lang['admin_tabwatermark'] = 'Opties Watermerk';
$lang['animationtype'] = 'Animatie type';
$lang['edit_animationtype'] = 'Bewerk animatie type:';
$lang['delete_show'] = 'Verwijder show';
$lang['admin_swfupload'] = 'SWFupload: Nieuw - Meerdere bestanden tegelijkertijd opladen. Gebruik ctrl/shift toets voor selectie, Flash noodzakelijk.';
$lang['admin_httpupload'] = 'HTTP Upload: Bestanden &eacute;&eacute;n per &eacute;&eacute;n opladen (oud system) ';
$lang['admin_tag'] = 'Markering om Show te tonen';
$lang['admin_slidecount'] = 'Aantal dia&#039;s';
$lang['admin_upmethod'] = 'Oplaad methode:';
$lang['admin_delete_warning'] = 'Wil je deze show verwijderen?';
$lang['uploadwatermark'] = 'Nieuw watermerk afbeelding opladen:';
$lang['loadjqueryscripts'] = 'Laad jQuery script';
$lang['editshow_tabprefs'] = 'Toon instellingen';
$lang['editshow_taburl'] = 'URL instellingen';
$lang['editshow_tabtemplate'] = 'Sjablonen';
$lang['editshow_tabname'] = 'Naam aanpassen';
$lang['editshow_intpage'] = 'interne pagina';
$lang['editshow_intpage_info'] = 'Link naar een interne pagina met Alias, is afhankelijk van je &#039;url_rewriting&#039; instellingen.';
$lang['editshow_extpage'] = 'externe pagina';
$lang['editshow_extpage_info'] = 'Link naar een externe pagina zoals: &#039;http://www.google.be&#039;';
$lang['editshow_image'] = 'afbeelding';
$lang['editshow_image_info'] = 'link naar de originele afbeelding';
$lang['editshow_noscale'] = 'Geen verschaling';
$lang['editshow_showall'] = 'Toon alles';
$lang['editshow_exact'] = 'Precies passen (standard)';
$lang['editshow_fit'] = 'passen maar behoud verhoudingen';
$lang['editshow_linko'] = 'Link opties';
$lang['editshow_linkloc'] = 'Link lokaties';
$lang['editshow_linktarget'] = 'Link doel';
$lang['editshow_self_info'] = 'open links in hetzelfde frame';
$lang['editshow_blank_info'] = 'open links in een nieuw venster';
$lang['editshow_parent_info'] = '(als u frames gebruikt) dit zal links openenen in vorig frame.';
$lang['editshow_top_info'] = '(als u frames gebruikt) dit zal links openen in de hoofd frame';
$lang['editshow_example'] = 'voorbeeld: (druk &quot;Aanpassen button&quot; voor update)';
$lang['editshow_settings_nav'] = 'Instellingen navigatie balk';
$lang['editshow_choose_presets'] = 'Kies van vooraf ingestelde';
$lang['editshow_bar_onoff'] = 'Navigatie balk aan/uit';
$lang['editshow_border_color'] = 'Nav. <strong>Rand kleur</strong>';
$lang['editshow_border_size'] = 'Nav. <strong>Rand dikte</strong> (in px)';
$lang['editshow_border_rad'] = 'Nav. <strong>Rand radius</strong> (in px)';
$lang['editshow_bg_color'] = 'Nav. <strong>Achtergrond kleur</strong>';
$lang['editshow_bg_acolor'] = 'Nav. <strong>Actieve achtergrond kleur</strong> ';
$lang['editshow_text_onoff'] = '<strong>Toon tekst</strong> aan/uit';
$lang['editshow_text_color'] = 'Tekst kleur';
$lang['editshow_button_size'] = 'Nav. <strong>knop grootte</strong> (in px)';
$lang['editshow_position'] = 'Nav. <strong>Plaats</strong>';
$lang['editshow_margin'] = 'Nav. <strong>Marge</strong>(in px)';
$lang['editshow_basic'] = 'Basis';
$lang['editshow_width'] = '<strong>Breedte</strong> (pixels) ';
$lang['editshow_height'] = '<strong>Hoogte</strong> (pixels) ';
$lang['editshow_scaling'] = 'Afbeelding verhouding:';
$lang['editshow_slide_options'] = 'Diashow options';
$lang['editshow_shuffle_onoff'] = '<strong>Willikeurige volgorde</strong> aan/uit';
$lang['editshow_auto_onoff'] = '<strong>Automatish afspelen</strong> aan/uit';
$lang['editshow_controls_onoff'] = '<strong>Toon navigatie</strong> aan/uit';
$lang['editshow_controls_onoff_warning'] = '(<strong>Waarschuwing!</strong> deze opties verbergen uw URL-links )';
$lang['editshow_controlshide_onoff'] = '<strong>Laat controls zien bij hover</strong> ';
$lang['editshow_transition'] = 'Overgangs type';
$lang['editshow_rotation'] = '<strong>Rotatietijd</strong> (seconden)';
$lang['editshow_transitiontime'] = '<strong>Overgangstijd</strong> (seconden)';
$lang['editshow_slide_bg'] = 'Dia achtergrond kleur';
$lang['editshow_slide_bg_info'] = '(voorbeeld: <strong>#000000</strong> of <strong>geen</strong> indien u transparante achtergrond wenst.)';
$lang['editshow_text_options'] = 'Tekst opties';
$lang['editshow_show_title'] = 'Toon titel';
$lang['editshow_show_comment'] = 'Toon commentaar';
$lang['editshow_bg_text'] = 'Toon achtergrond voor tekst';
$lang['editshow_text_bgcolor'] = 'Tekst achtergrond kleur';
$lang['editshow_font_type'] = 'Lettertype ';
$lang['editshow_title_size'] = '<strong>Titel grootte</strong> in px';
$lang['editshow_comment_size'] = '<strong>Commentaar grootte</strong>in px';
$lang['editshow_comment_pos'] = 'Commentaar positie';
$lang['editshow_start_slide'] = '<strong>Start Slides</strong> (0)';
$lang['editshow_pauseonhover'] = '<strong>Pauzeer de slider bij een hover</strong> ';
$lang['editshow_thumb_nav'] = '<strong>Schakel tumbnail navigatie in</strong> ';
$lang['editshow_nr_slices'] = 'Het  aantal slides om te gebruiken in de &quot;Slice&quot; overgangen (e.g. 15)';
$lang['editshow_box_cols'] = 'Het aantal kolommen en rijen die worden gebruikt in de box transitie (e.g. 8x4)';
$lang['editshow_nav_controls'] = 'Instellingen navigatie knoppen';
$lang['editshow_controltypes'] = 'Navigatie types';
$lang['editshow_trans_eas'] = 'Overgangs types';
$lang['editshow_control_left'] = 'linkse pijl (vorige afbeeldingn)';
$lang['editshow_control_fullscr'] = '(<strong>caution!</strong> using this option &#039;ll hide your url-links)';
$lang['editshow_control_right'] = 'rechtse pijl (volgende dia)';
$lang['editshow_options_updated'] = 'Show opties aangepast';
$lang['editshow_error_showid'] = 'Dit Show nummer is reeds in gebruik!';
$lang['editshow_theme'] = 'Thema slideshow';
$lang['editshow_caption_css'] = 'Caption CSS instellingen.';
$lang['editshow_captionopacity'] = 'Caption zichtbaarheid. (eg 0.5)';
$lang['browse_prev_map'] = 'vorige folder';
$lang['browse_browse_map'] = 'Verken folder';
$lang['browse_current_pic'] = 'Huidige afbeelding';
$lang['selectall'] = 'Selecteer alles';
$lang['selectpicture'] = 'Selecteer deze afbeelding';
$lang['error_nofilesuploaded'] = 'Geen bestand opgeladen!';
$lang['file_uploaded'] = 'Afbeelding opgeladen';
$lang['uploadpicture'] = 'Afbeelding opladen';
$lang['uploadimages'] = 'Afbeeldingen opladen';
$lang['watermark_trans_warning'] = 'Deze instelling werkt niet voor &quot;PNG&quot; afbeeldignen!';
$lang['watermark_warning'] = 'Instellingen opgeslagen, maar we vonden geen watermerk bestand!';
$lang['watermark_current'] = 'Huidige watermerk';
$lang['watermark_position'] = 'Watermerk positie';
$lang['watermark_margin'] = 'Wartermerk marge (in px)';
$lang['watermark_margin_lr'] = 'marge links of rechts';
$lang['watermark_margin_tb'] = 'marge boven of onder';
$lang['watermark_trans'] = 'Wartermerk transparantie';
$lang['watermark_trans_100'] = '100 = geen transparantie';
$lang['watermark_example'] = 'Voorbeeld: afbeelding met watermerk.';
$lang['EditShow'] = 'Bewerk Show instellingen';
$lang['back_to_admin'] = 'Bewerk Shows';
$lang['edit_slides'] = 'Bewerk dia&#039;s';
$lang['name'] = 'Naam';
$lang['changeshownumber'] = 'Bewerk Shownummer';
$lang['upload'] = 'Opladen';
$lang['cancel'] = 'Annuleer';
$lang['apply'] = 'Aanpassen';
$lang['submit'] = 'Opslaan';
$lang['default'] = 'Plaats standaard';
$lang['animation_info'] = 'Kies <strong><swfobject></strong> als u hoge kwalitatieve overgangen wil, zoals: Ken Burns (pan&amp;zoom) en volledig schermvullend.<br />
Kies <strong>jQuery</strong> als u liever geen flash wil maar toch handige overgangen en meer navigatie opties. (gebaseerd op jQuery Cycle Plugin)';
$lang['friendlyname'] = 'Showtime Diashow';
$lang['postinstall'] = 'Showtime - installatie volledig';
$lang['postuninstall'] = 'Showtime - verwijdering volledig';
$lang['really_uninstall'] = 'Bent u zeker?
u wilt deze module verwijderen?';
$lang['uninstalled'] = 'Module verwijderd.';
$lang['installed'] = 'Module versie %s installeerd.';
$lang['upgraded'] = 'Module upgrade naar versie %s.';
$lang['moddescription'] = 'Kies voor hoge kwaliteits overgangen, zoals: Ken Burns (pan&amp;zoom) en nog veel meer leuke effecten plus een optie voor &quot;Full Screen&quot; mode. Demo kan je hier terugvinden: http://www.web2do.be/cms/showtime';
$lang['error'] = 'Fout!';
$lang['admindescription'] = 'This is a showtime module for annimated slideshow. &bull; Demo can be viewed here-> &quot;http://www.web2do.be/showtime&quot;';
$lang['accessdenied'] = 'Toegang geweigerd. AUB controleer uw rechten.';
$lang['welcome_text'] = '<p>Welkom in de Module administratie afdeling. Voeg deze module toe aan uw pagina&#039;s dor het plaatsen van {Showtime}</p>';
$lang['qca'] = 'P0-1262823020-1307408963171';
$lang['utma'] = '156861353.1115859297.1333390516.1333390516.1333390516.1';
$lang['utmz'] = '156861353.1333390516.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none)';
$lang['utmb'] = '156861353';
$lang['utmc'] = '156861353';
?>