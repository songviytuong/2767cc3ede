<?php
if (!cmsms()) exit;

$showid = (isset($params['showid']) ? $params['showid'] : '');
if ($showid == '')
	$this->Redirect($id, 'defaultadmin', $returnid);

if (isset($params['cancel'])) 
	$this->Redirect($id, 'defaultadmin', $returnid);
if (! $this->CheckPermission('Use Showtime2')) {
  return $this->DisplayErrorPage($id, $params, $returnid,$this->Lang('accessdenied'));
}

$query = "DELETE FROM ".cms_db_prefix()."module_showtime2_name WHERE show_id = ?";
$db->Execute($query, array($showid));

// delete pictures
$query = "DELETE FROM ".cms_db_prefix()."module_showtime2 WHERE show_id = ?";
$db->Execute($query, array($showid));

//delete css file
$this->DeleteTemplate("NivoCSS_Show_".$showid);
if ($this->GetPreference('use_tmp')=='1'){
  $cssfilename = '../tmp/_Showtime2/css/Show_' . $showid . '.css';
}
else {
  $cssfilename = '../uploads/_Showtime2/css/Show_' . $showid . '.css';
}
if(file_exists($cssfilename)){
	unlink($cssfilename);
}



//Update search index
$module =& $this->GetModuleInstance('Search');
if ($module != FALSE)
  {
    $module->DeleteWords($this->GetName(), $showid, 'show');
  }
	$this->Redirect($id, 'defaultadmin', $returnid);


?>