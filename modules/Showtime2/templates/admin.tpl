{literal}
<script type="text/javascript" src="../modules/Showtime2/templates/jquery/jquery.1.9.0.min.js"></script>

<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function($) {
  if ($('.pagemcontainer')){
    $('.pagemcontainer').delay(2500);
    $('.pagemcontainer').slideToggle(800);
  }
$("input[type=text]").focus(function(){
    this.select();
});

});


</script>
{/literal}

{if isset($message)}
<div class="pagemcontainer">
    <p class="pagemessage">{$message}</p>
  </div>
{/if}

{$tabheader}

{$starttab}
{$startshowoverview}
{$LinkAddshow}
        <table class="pagetable" cellspacing="0">
{if $show_nivo_note=='1'}        
<div style="-moz-border-radius:5px 5px 5px 5px; background:none repeat scroll 0 0 #EFEFEF; border:1px solid #D9D9D9; margin:15px 0 0; padding:5px; float:left;">
<h2>Important Note</h2>
<p></p>
<p>This version manages templates for NivoSlider in a way, which is <strong>not compatible</strong> with previous releases! Therefore you must open the settings-tab for each NivoSlider-Show and check your settings and then save the settings again!</p>
<p></p>
</div>
{/if}
            <thead>
                <tr>
                    <th class="pagew25">{$th_name}</th>
                    <th class="pagew25">{$th_animationtype}</th>
                    <th class="pagew25">{$th_tag}</th>
                    <th class="pagew25 pagepos">{$th_slidecount}</th>
                    <th class="pageicon">{$th_editshow}</th>
                    <th class="pageicon">{$th_editslides}</th>
                    <th class="pageicon">{$th_deleteshow}</th>
                </tr>
            </thead>
            <tbody>
                {$showlist}
            </tbody>

        </table>
{if $isCMSMS2!='1'}
{$endtab}
{/if}
{if $can_use_pref}
{$starttaboptions}
{literal}
<style type="text/css">
div.pageoverflow img {vertical-align:middle;}
</style>
{/literal}
  {$startform}
  <div class="pageoverflow">
    <p class="pagetext">{$admin_upmethod}<br /><br /></p>
    <p class="pageinput">{$uploadmethode}</p>
        <p class="pageinput">{$load_jQuery_scripts}{$jQuery_info}</p><br />
        <p class="pageinput">{$use_tmp}{$use_tmp_info}</p><br />
  </div>
  
    <div class="pageoverflow">
    <p class="pagetext">{$submit}</p>
  </div>
   
  {$endform}
{if $isCMSMS2!='1'}
{$endtab}
{/if}


{$starttabwatermark}

<table cellpadding="0" width="100%">
<tr><td valign="top">
<div id="FORMuploadMethode">
    {$upload_startform}
    <div class="pageoverflow">
      <p class="pagetext">{$prompt_browse}</p>
      <p class="pageinput">{$input_browse}&nbsp;{$upload_submit}</p>
      <br /><br />
    </div>
    {$endform}
</div>

  {$startform}
  <div class="pageoverflow">
    <p class="pagetext">{$watermark_current_info}</p>
    <p class="pageinput"><img src="{$currentwatermark}" /></p>
        <br />
  </div>
  <div class="pageoverflow">
    <p class="pageinput">{$watermark_bak}{$create_bak}</p><br />
    <p class="pagetext">{$watermark_position_info}</p>
    <p class="pageinput">{$watermark_pos}</p>
         <br />
  </div>
    
  <div class="pageoverflow">
    <p class="pagetext">{$watermark_margin_info}</p>
    <p class="pageinput">{$watermark_padding_x} ({$watermark_margin_lr})</p>
    <p class="pageinput">{$watermark_padding_y} ({$watermark_margin_tb})</p>
        <br />
  </div>

  <div class="pageoverflow">
    <p class="pagetext">{$watermark_trans_info}</p>
    <p class="pageinput">{$watermark_transparant} ({$watermark_trans_100})</p>
        <p class="pageinput"><span style="color:#bb2222;">{$watermark_trans_warning}</span></p>
        <br />
  </div>
  
    <div class="pageoverflow">
    <p class="pagetext">{$watermark_submit}</p>
  </div>
    {$endform}
</td>

<td valign="top">
  <div class="pageoverflow">
    <p class="pagetext">{$watermark_example}</p>
    <p class="pageinput"><img src="{$example_image}" /></p>
        <br />
  </div>
</td></tr></table>
   
  
{$endtab}
{/if}

{$endtabcontent}