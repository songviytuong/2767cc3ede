<!-- generated by showtime Module start -->
    <div id="slider{$showid}w" class="slider-wrapper theme-{$show.st_theme}{$showid}" >
        <div id="slider{$showid}{$ext}" class="nivoSlider" >
        {foreach from=$items item=entry}
            {if $entry->picture_url == ""}
            <img src="{$entry->picture}"  alt="" data-thumb="{$entry->thumbnail}" title="{if {$show.st_showtext}}<h2>{$entry->picture_name}</h2>{/if}{if ($show.st_showcomment)}{if $entry->comment <> ""}{$entry->comment}{else}{$entry->picture_name}{/if}{/if}" />
            {else}
            <a href="{$entry->picture_url}" target="{$entry->picture_url_target}">
            <img src="{$entry->picture}"  alt="" data-thumb="{$entry->thumbnail}" title="{if {$show.st_showtext}}<h2>{$entry->picture_name}</h2>{/if}{if ($show.st_showcomment)}{if $entry->comment <> ""}{$entry->comment}{else}{$entry->picture_name}{/if}{/if}" />
            </a>
            {/if}
        {/foreach}
        </div>
    </div>
 <!-- generated by showtime Module end -->
