{literal}
<style type="text/css">
div.pageoverflow img {vertical-align:middle;}
</style>
{/literal}

{$StartTabH}
{$Tabh_02}
{$Tabh_01}
{$Tabh_03}
{$EndTabH}

{$StartTabC}
{$FormStart}
{$StartTab_gen}
    {$editslides}{$addpicture}{$backimage}
    <br /><br />
  <div class="pageoverflow">
    
    <p class="pagetext">{$nametext}:</p>
    <p class="pageinput">{$nameinput}</p>
        <br />
     <p class="pagetext">{$shownumber}:</p>
    <p class="pageinput">{$shownumberinput}</p>
       
        {$tab}
  </div>
  <div class="pageoverflow">
    <p class="pagetext">&nbsp;</p>
    <p class="pageinput">{$submit}{$cancel}</p>
  </div>
{if $isCMSMS2!='1'}
{$EndTab}
{/if}
{if $permission=="1"}

  {$StartTab_link}
    {$editslides}{$addpicture}{$backimage}

        <table class="form-table" rowpadding="40" width="100%">    
            <tr><td colspan="3" style="border-bottom:#777777 dashed thin;" height="20"></td></tr>
            <tr>            
                <td valign="top"><p><strong>{$linkoptions}</strong></p></td>
                <td valign="top"><br />
                    {$st_link}
                    <label for="st_link"><strong>{$linkloc}</strong></label><br /><br />
                    <p><strong>{$internalpage}</strong> 
                      = {$internalpage_info}</p>
                    <p><strong>{$externalpage}</strong> = {$externalpage_info}</p>
                    <p><strong>{$imagepage}</strong> = {$imagepage_info}</p>
                </td>
                <td valign="top"><br />
                    {$st_target}
                    <label for="st_target"><strong>{$linktarget}</strong></label><br /><br />
                    <p><strong>_self</strong> = {$self_info}</p>
                    <p><strong>_blank</strong> = {$blank_info}</p>
                    <p><strong>_parent</strong> = {$parent_info}</p>       
                    <p><strong>_top</strong> = {$top_info}</p>
                </td>
            </tr>
            <tr><td colspan="3" style="border-bottom:#777777 dashed thin;" height="20"></td></tr>
            
            <tr>
              <td colspan="4">
                {$submit}    {$default}    {$cancel}    {$apply}    
                </td>
            </tr>

        </table>
{if $isCMSMS2!='1'}
      {$EndTab}
{/if}
  {$StartTab_pref}
    {$editslides}{$addpicture}{$backimage}
    <table class="form-table" rowpadding="40" width="100%">            
    <tr><td colspan="3" style="border-bottom:#777777 dashed thin;" height="10px;"></td></tr>
    <tr>
      <td colspan="4">
        {$submit}    {$default}    {$cancel}    {$apply} 
        </td>
    </tr>
    <tr height="20">
        <td valign="top"><p><strong style="margin: 0pt; padding: 0pt;">{$basic}</strong></p></td>
        <td valign="top"" style="padding-right:10px"><br />
        <div class="pageoverflow">
            
            <p><label for="st_sizing"><strong>{$label_sizing}</strong></label><br />
            {$st_sizing}<br />
            {$label_sizing_comment}</p>                    
            
            <div id="dimensions">
            <p><br />{$st_width}
            <label for="st_width">{$label_width}</label></p>               	
            {if $stt_sizing=='fixed'}
            <p>{$st_height}
            <label for="st_height">{$label_height}</label></p>
            {/if}
            <br />
            </div>

            
            <p><label for="st_theme"><strong>{$label_theme}</strong></label><br /><br />
            {$st_theme}<br />
            {$label_theme_comment}</p>
            
            
        </div>
        </td>
        <td valign="top" width="40%"><br />
         <div class="pageoverflow">
            <p><strong>{$label_animationtype}:</strong></p>
            <p>{$animation_info}</p>
            <p>{$edit_animationtype}</p>
        </div>
       </td>

        <td valign="top" colspan="6">

        {$example_info}<br /><br />
      <style type="text/css">
        {literal}
        #nav_settings {-moz-border-radius:5px 5px 5px 5px; background:none repeat scroll 0 0 #EFFFFF; border:1px solid #D9D9D9;  padding:15px; max-width:270px;}
        #nav_settings img{vertical-align:middle;}
        .n25_settings img{vertical-align:middle;}
        
        #presetbutton {
          background:url("../modules/Showtime2/images/bg_glass.png") repeat-x scroll 50% 50% #E6E6E6;
          border:1px solid #D3D3D3;
          color:#555555;
          font-weight:normal;
          -moz-border-radius:4px 4px 4px 4px;
        }
          #presetbutton p {
          display:block;
          font-size:1em;
          padding:0.5em 0.5em 0.5em 0.7em;
          cursor:pointer;
          margin:0;
          font-weight:bold;
          text-align:center;
        }
        #presetlist {
          -moz-border-radius:0 0 5px 5px;
          background-color:#FFFFFF;
          border:1px solid #CCCCCC;
          padding:5px;
          display:none;
        }
      
        {/literal}
      {$stt_cssstring}
      </style>
<link rel='stylesheet' type='text/css' href='../modules/Showtime2/templates/nivoslider/nivo-slider.css' />
<script type="text/javascript">
if( typeof(jQuery) == "undefined") {
  document.write('<script type="text/javascript" src="../modules/Showtime2/templates/jquery/jquery-1.9.0.min.js"><\/script>');
}
</script>      
<script type="text/javascript" src="../modules/Showtime2/templates/nivoslider/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="../modules/Showtime2/templates/farbtastic/farbtastic.js"></script>
<link rel="stylesheet" href="../modules/Showtime2/templates/farbtastic/farbtastic.css" type="text/css" />
      <script type="text/javascript">
      {literal}
        $(document).ready(function(){
        $(window).load(function() {
            $('#slider{/literal}{$showidvalue}{literal}').nivoSlider({ {/literal}
            effect: '{$stt_transition}', // Specify sets like: 'fold,fade,sliceDown'
            slices: {$stt_slices}, // For slice animations
            boxCols: {$stt_box_x}, // For box animations
            boxRows: {$stt_box_y}, // For box animations
            animSpeed: {$stt_transitiontime}, // Slide transition speed
            pauseTime: {$stt_time}, // How long each slide will show
            startSlide: {$stt_start_slide}, // Set starting Slide (0 index)
            directionNav: {$stt_showcontrols}, // Next & Prev navigation
            directionNavHide: {$stt_controlshide}, // Only show on hover
            controlNav: {$stt_navbut}, // 1,2,3... navigation
            controlNavThumbs: {$stt_thumb_nav}, // Use thumbnails for Control Nav
            controlNavThumbsFromRel: false, // Use image rel for thumbs
            keyboardNav: false, // Use left & right arrows
            pauseOnHover: true, // Stop animation while hovering
            manualAdvance:{if ($stt_autoplay=='false')}true,{else}false,{/if} // Force manual transitions
            captionOpacity: {$stt_captionopacity}, // Universal caption opacity
            prevText: '{$nivo_prev}', // Prev directionNav text
            nextText: '{$nivo_next}', // Next directionNav text
            randomStart: {$stt_shuffle}, // Start on a random slide
            {literal}
            beforeChange: function(){}, // Triggers before a slide transition
            afterChange: function(){}, // Triggers after a slide transition
            slideshowEnd: function(){}, // Triggers after all slides have been shown
            lastSlide: function(){}, // Triggers when last slide is shown
            afterLoad: function(){} // Triggers when slider has loaded
          });
        });

      {/literal}
{literal}
            
        $('#colorpicker_textbgcolor').farbtastic('#m1_st_textbgcolor');
                $('#m1_st_textbgcolor').focus( function(){$('#colorpicker_textbgcolor').show();});
                $('#m1_st_textbgcolor').blur( function(){$('#colorpicker_textbgcolor').hide();});
        $('#colorpicker_textcolor').farbtastic('#m1_st_textcolor');
        $('#m1_st_textcolor').focus( function(){$('#colorpicker_textcolor').show();});
                $('#m1_st_textcolor').blur( function(){$('#colorpicker_textcolor').hide();});
            });


      {/literal}
            </script>

        <div id="slider{$showidvalue}w" class="slider-wrapper theme-{$stt_theme}{$showidvalue}" float= "left">
            <div id="slider{$showidvalue}" class="nivoSlider">
                 <img src="../modules/Showtime2/images/slide1.jpg"  alt="" title="{if $stt_showtext=='true'}<h2>title here</h2>{/if}{if $stt_showcomment=='true'}Comment here Lorem ipsum dolor sit amet.{/if}" data-thumb="../modules/Showtime2/images/thumb_slide1.jpg"  />
                 <img src="../modules/Showtime2/images/slide2.jpg"  alt="" title="{if $stt_showtext=='true'}<h2>title here</h2>{/if}{if $stt_showcomment=='true'}Comment here Lorem ipsum dolor sit amet.{/if}" data-thumb="../modules/Showtime2/images/thumb_slide2.jpg" />
                 <img src="../modules/Showtime2/images/slide3.jpg"  alt="" title="{if $stt_showtext=='true'}<h2>title here</h2>{/if}{if $stt_showcomment=='true'}Comment here Lorem ipsum dolor sit amet.{/if}" data-thumb="../modules/Showtime2/images/thumb_slide3.jpg" />
                 <img src="../modules/Showtime2/images/slide4.jpg"  alt="" title="{if $stt_showtext=='true'}<h2>title here</h2>{/if}{if $stt_showcomment=='true'}Comment here Lorem ipsum dolor sit amet.{/if}" data-thumb="../modules/Showtime2/images/thumb_slide4.jpg" />
                 <img src="../modules/Showtime2/images/slide5.jpg"  alt="" title="{if $stt_showtext=='true'}<h2>title here</h2>{/if}{if $stt_showcomment=='true'}Comment here Lorem ipsum dolor sit amet.{/if}" data-thumb="../modules/Showtime2/images/thumb_slide5.jpg" />
            </div>
        </div>
        </td>
        </tr>
            <tr><td colspan="3" style="border-bottom:#777777 dashed thin;" height="20"></td></tr>
            
            <tr height="20">            
                <td valign="top"><p><strong>{$label_slide_options}</strong></p></td>
                <td valign="top" style="padding-right:10px"><br />
                    <p><label for="st_transition"><strong>{$label_transition}:</strong>  </label><br />
                    {$st_transition}</p>                  
                    <br /> 
                    
                    <p><label for="st_nr_slices"><strong>{$label_nr_slices}</strong></label><br />
                    {$st_slices}<br />
                    {$label_nr_slices_comment}</p>
                    <br />

                    <p><label for="st_box_cols"><strong>{$label_box_cols}</strong></label><br />
                    {$st_box_x} x {$st_box_y}<br />                   
                    {$label_box_cols_comment}</p>
                    <br />
                    
                    <p><br />{$st_rotatetime}<label for="st_rotatetime">{$label_rotation}</label></p>  
                    
                    <p>{$st_transitiontime} <label for="st_transitiontime">{$label_transitiontime} </label></p>	                      

                     <p>{$st_start_slide}<label for="st_start_slide">{$label_start_slide}</label></p>                    
                </td>
                <td valign="top">

                    <p>{$st_navbut}<label for="st_navbut"><strong>{$bar_onoff}</strong></label></p>
                    {if ($stt_navbut=='true')}
                    <p>{$st_thumb_nav}<label for="st_thumb_nav">{$thumbs_onoff}</label></p>
                    {/if}
                    <p>{$st_showcontrols}<label for="st_showcontrols">{$controls_onoff}</label>&nbsp;&nbsp;&nbsp;
                    {$showcontrols_images}</p> 
                    
                     <p>{$st_pauseonhover}<label for="st_pauseonhover">{$label_pauseonhover}</label></p>
                     
                    <p>{$st_autoplay}<label for="st_autoplay">{$auto_onoff}</label></p>
                    
                    <p>{$st_controlshide}<label for="controlshide">{$controlshide_onoff}</label></p>

                    <p>{$st_shuffle}<label for="st_shuffle">{$shuffle_onoff}</label></p>
              </td>
                
            </tr>
            <tr><td colspan="3" style="border-bottom:#777777 dashed thin;" height="20"></td></tr>
            <tr>            
                <td valign="top"><p><strong>{$text_options}</strong></p></td>
                <td valign="top" style="padding-right:10px">
                    <br />
                    {$st_showtext}
                    <label for="st_showtext"><strong>{$label_show_title}</strong></label><br />
                    {$st_showcomment}
                    <label for="st_showcomment"><strong>{$label_show_comment}</strong></label><br />
                    {$st_showalt}
                    <label for="st_showalt"><strong>{$label_bg_text}</strong></label>
                    <br /><br />                 	
                    <p>{$st_textbgcolor}<label for="st_textbgcolor"><strong>{$label_text_bgcolor}</strong> </label></p>
                    <div id="colorpicker_textbgcolor" style="background:#F9F9F9;position:absolute;display:none;"></div>
                    <p>{$st_textcolor}
                    <label for="st_textcolor"><strong>{$label_text_color}</strong> </label></p>
                    <div id="colorpicker_textcolor" style="background:#F9F9F9;position:absolute;display:none;"></div>
                    <br /><br />
                    
                    {$st_captionopacity}
                    <label for="st_captionopacity"><strong>{$label_captionopacity}</strong></label><br /><br />

                </td>
                <td valign="top"><br />
                    <p><label for="st_font_type"><strong>{$label_font_type}:</strong> </label><br /><br />

                    {$st_fonttype}</p>
                    <br />
                    <p>{$st_titlesize}<label for="st_titlesize">{$label_title_size}</label></p>
                    <p>{$st_textsize}<label for="st_textsize">{$label_comment_size}</label></p>
                    <p>{$st_commentpos}<label for="st_commentpos"><strong>{$label_comment_pos}</strong></label></p>
                </td>
                
                <td valign="top" colspan="6">
                  <label for="st_nivo_css"><strong>{$label_nivo_css}</strong></label><br />
                    {$template_input}
                </td>
                
            </tr>

            <tr><td colspan="3" style="border-bottom:#777777 dashed thin;" height="20"></td></tr>
            <tr>
              <td colspan="4">
                {$submit}    {$default}    {$cancel}    {$apply} 
                </td>
            </tr>
        </table>
{$EndTab}
{/if}
{$hidden}{$hidden2}{$FormEnd}
{if $isCMSMS2!='1'}
{$EndTabC}
{/if}