
{literal}

    <script src="../modules/Showtime2/jq_crop/js/jquery.Jcrop.min.js"></script>
    <link rel="stylesheet" href="../modules/Showtime2/jq_crop/css/jquery.Jcrop.min.css" type="text/css" />
    <link rel="stylesheet" href="../modules/Showtime2/jq_upload/bootstrap.css">

    <script language="Javascript">
      $(function(){
        $('#cropbox').Jcrop({
          aspectRatio: {/literal}{$st_width}/{$st_height}{literal},
          setSelect: [ {/literal}{$x_pos}, {$y_pos}, {$x_dim}, {$y_dim} {literal}],
          onSelect: updateCoords,
          onChange: updateCoords
        })
        $('[name*=_x]').val({/literal}{$x_pos}{literal});
        $('[name*=_y]').val({/literal}{$y_pos}{literal});
        $('[name*=_w]').val({/literal}{$x_dim}{literal});
        $('[name*=_h]').val({/literal}{$y_dim}{literal});

      });

      function updateCoords(c){
        $('[name*=_x]').val(c.x);
        $('[name*=_y]').val(c.y);
        $('[name*=_w]').val(c.w);
        $('[name*=_h]').val(c.h);
        
      };

    </script>

    <div class="well">

    <h1>{/literal}{$title_crop}</h1>

    <!-- This is the image we're attaching Jcrop to -->
    <img src="{$imgtocrop}?{$filemtime}" id="cropbox" />

    <!-- This is the form that our event handler fills -->

          {$FormStart}
            {$hidden}
            <br />
            <table>
            <tr>
              <td>
            {$coordinates_x}
              </td>
              <td>
            {$coordinates_y}
              </td>
            </tr>
            <tr>
              <td>
            {$coordinates_w}
              </td>
              <td>
            {$coordinates_h}
              </td>
            </tr>
            </table>
            {$submit}{$cancel}
          {$FormEnd}      
  </div>
