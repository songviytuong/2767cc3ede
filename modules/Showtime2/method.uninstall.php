<?php
#-------------------------------------------------------------------------
# Module: Showtime
# Moduledemo's URI: http://www.web2do.be/cms/showtime
# Description:Display images as animated slideshow. 
# Choose among several smooth transition effects. ShowTime requires Adobe Flash player 10 or higher.
#
# Version: 1.0
# author: Peter Orije
# Author URI: http://www.web2do.be
/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
# Module based on Wordpress plugin from: Paul Schroeder URI: http://youtag.lu/showtime-wp-plugin/
#-------------------------------------------------------------------------

if (!cmsms()) exit;

$db =& $gCms->GetDb();
if ($this->GetPreference("use_tmp")=='1'){
  $cssfolder = '../tmp/_Showtime2/';}
else {
  $cssfolder = '../uploads/_Showtime2/';}

// remove the database table
$dict = NewDataDictionary( $db );
$sqlarray = $dict->DropTableSQL( cms_db_prefix()."module_showtime2" );
$dict->ExecuteSQLArray($sqlarray);
$sqlarray = $dict->DropTableSQL( cms_db_prefix()."module_showtime2_name" );
$dict->ExecuteSQLArray($sqlarray);

// remove the sequence
$db->DropSequence( cms_db_prefix()."module_showtime2_seq" );
$db->DropSequence( cms_db_prefix()."module_showtime2_name_seq" );

// remove all preferences
$this->RemovePreference();

// remove all templates
$this->DeleteTemplate();

// remove the permissions
$this->RemovePermission('Use Showtime2');
$this->RemovePermission('Use Showtime2 Prefs');


// remove the event
//$this->RemoveEvent( 'OnShowtimePreferenceChange' );
$this->RemoveEventHandler( 'Core', 'ContentPostRender' );

// delete css-folder
showtime2_utils::removeFolder($cssfolder,true);

// put mention into the admin log
$this->Audit( 0, $this->Lang('friendlyname'), $this->Lang('uninstalled'));


?>