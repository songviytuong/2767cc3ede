<?php
#-------------------------------------------------------------------------
# Showtime 2
# Version 3.5.0
# (c) 2016 by Peter Engels <showtime@acg-bonn.de>
# The module's homepage is: http://dev.cmsmadesimple.org/projects/showtime2/
#
# A fork of: Showtime (c) 2010-2014 by Peter Orije
#-------------------------------------------------------------------------
# Module: Showtime
# Moduledemo's URI: http://www.web2do.be/cms/showtime
# Description:Display images as animated slideshow. 
# Choose among several smooth transition effects. ShowTime requires Adobe Flash player 10 or higher.
#
# Version: 1.0
# author: Peter Orije
# Author URI: http://www.web2do.be
/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
# Module based on Wordpress plugin from: Paul Schroeder URI: http://youtag.lu/showtime-wp-plugin/
#-------------------------------------------------------------------------

class Showtime2 extends CMSModule{
    function GetName(){                         return 'Showtime2';}
    function GetFriendlyName(){                 return $this->Lang('friendlyname');}
    function GetVersion(){                      return '3.5.3';}
    function GetHelp() {           
            $gCms = cmsms();
            $config = $this->GetConfig();
            $help = str_replace('[root_url]',$config["root_url"], $this->Lang('help'));
            return $help;
    }
    function GetAuthor() {                      return 'Peter Engels';}
    function GetAuthorEmail()  {                return 'showtime@acg-bonn.de';}
    /*
    function GetChangeLog()  {                  return $this->Lang('changelog');}
    */
    function GetChangeLog() {
      return file_get_contents(dirname(__FILE__) . '/changelog.inc');
    }

    function IsPluginModule() {                 return true;}
    function HasAdmin() {                       return true;}
    function GetAdminSection() {                return 'content';}
    function GetAdminDescription(){             return $this->Lang('admindescription');}
    function VisibleToAdminUser() {             return $this->CheckPermission('Use Showtime2');}
    function CheckAccess($perm = 'Use Showtime2'){return $this->CheckPermission($perm);}
    function GetDependencies() {                return array();}
    function MinimumCMSVersion()  {             return "1.9";}

    function DisplayErrorPage($id, &$params, $return_id, $message=''){
            $this->smarty->assign('title_error', $this->Lang('error'));
            $this->smarty->assign('message', $message);
            echo $this->ProcessTemplate('error.tpl');
    }
	
    function GetEventDescription ( $eventname ){return;}// $this->Lang('event_info_'.$eventname );}
    function GetEventHelp ( $eventname ){       return;}// $this->Lang('event_help_'.$eventname );}
    function InstallPostMessage(){              return $this->Lang('postinstall');}
    function UninstallPostMessage(){            return $this->Lang('postuninstall');}
    function UninstallPreMessage(){             return $this->Lang('really_uninstall'); }
	
    function SetParameters(){
            $this->RegisterModulePlugin();
            $this->RestrictUnknownParams();
            $this->CreateParameter('show', '', 'Showtime id');
            $this->SetParameterType('show',CLEAN_STRING);
    }

    /**
    * DoEvent methods
    */
    function DoEvent( $originator, $eventname, &$params ){
      if ($originator == 'Core' && $eventname == 'ContentPostRender'){
        $side_pos = stripos($params["content"],"</head");
        if (isset($this->NivoMetadata)){
          $temp = "
<!--LOAD SHOWTIME NivoCSS-->
<link rel='stylesheet' type='text/css' href='modules/Showtime2/templates/nivoslider/nivo-slider.css' />
";
        if( $side_pos !== FALSE){
                $params["content"] = substr($params["content"], 0, $side_pos) .  
                $temp . substr($params["content"], $side_pos);
        }}
        $side_pos = stripos($params["content"],"</head");
        if( $side_pos !== FALSE && isset($this->CSSMetadata)){
                $params["content"] = substr($params["content"], 0, $side_pos) .  
                $this->CSSMetadata . substr($params["content"], $side_pos);
        }
        $side_pos = strripos($params["content"],"</body");
        if (isset($this->SWFMetadata)){
          $temp = "
<!--LOAD SHOWTIME swfobject-->
<script type='text/javascript' src='modules/Showtime2/showtime/swfobject/swfobject.js'></script>
";
        if( $side_pos !== FALSE){
                $params["content"] = substr($params["content"], 0, $side_pos) .  
                $temp . substr($params["content"], $side_pos);
        }}
        $side_pos = strripos($params["content"],"</body");
        if( $side_pos !== FALSE && isset($this->SWFMetadata)){
                $params["content"] = substr($params["content"], 0, $side_pos) .  
                $this->SWFMetadata . substr($params["content"], $side_pos);
        }
        $side_pos = strripos($params["content"],"</body");
        if (($this->GetPreference("load_jQuery_scripts")) and (isset($this->JQMetadata) or isset($this->NivoMetadata))){
          $temp = "
<!-- LOAD SHOWTIME jQuery -->          
<script type='text/javascript'>
function versionCompare(a, b) {
    var i, diff;
    var regExStrip0 = /(\.0+)+$/;
    var segmentsA = a.replace(regExStrip0, '').split('.');
    var segmentsB = b.replace(regExStrip0, '').split('.');
    var l = Math.min(segmentsA.length, segmentsB.length);

    for (i = 0; i < l; i++) {
        diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
        if (diff) {
            return diff;
        }
    }
    return segmentsA.length - segmentsB.length;
}

if ((typeof jQuery == 'undefined') || (versionCompare(jQuery.fn.jquery,'1.7.0')<0))
{
  document.write('<script type=\"text/javascript\" src=\"modules/Showtime2/templates/jquery/jquery-1.9.0.min.js\"><\/script>');
}
</script>
";
        if (isset($this->NivoMetadata)){
          $temp .= "
<script type='text/javascript' src='modules/Showtime2/templates/nivoslider/jquery.nivo.slider.pack.js'></script>
";}
        if (isset($this->JQMetadata)){
          $temp .= "
<script type='text/javascript' src='modules/Showtime2/templates/jquery/jquery.cycle.all.min.js' ></script>
";}
        if( $side_pos !== FALSE){
                $params["content"] = substr($params["content"], 0, $side_pos) .  
                $temp . substr($params["content"], $side_pos);
        }}
        $side_pos = strripos($params["content"],"</body");
        if( $side_pos !== FALSE && isset($this->JQMetadata)){
                $params["content"] = substr($params["content"], 0, $side_pos) .  
                $this->JQMetadata . substr($params["content"], $side_pos);
        }
        $side_pos = strripos($params["content"],"</body");
        if( $side_pos !== FALSE && isset($this->NivoMetadata)){
                $params["content"] = substr($params["content"], 0, $side_pos) .  
                $this->NivoMetadata . substr($params["content"], $side_pos);
        }

      }
    }


    /**
    * Register TinyMCE plugin
    */

    function RegisterTinyMCEPlugin(){
            $gCms = cmsms();
            $config = $this->GetConfig();
            $db =& $this->GetDB();
            $plugin1 = "
    tinymce.create('tinymce.plugins.picker', {
            createControl: function(n, cm) {
                    switch (n) {
                            case 'picker':
                                    var c = cm.createMenuButton('picker', {
                                            title : 'Add Showtime2',
                                            image : '".$config["root_url"]."/modules/Showtime2/images/icon_TinyMCE.gif',
                                            icons : false
                                    });
                                    c.onRenderMenu.add(function(c, m) {
                    ";

                            $query = 'SELECT * FROM '.cms_db_prefix().'module_showtime2_name';
                            $dbresult = $db->Execute($query);	
                            if ( $dbresult && $dbresult->RecordCount() > 0 ){			
                                    while( $dbresult && ($row = $dbresult->FetchRow()) ){
                                            $showname=addcslashes($row['show_name'],"\\\'\"&\n\r<>"); 
                                            $plugin1 .= "
                                                    m.add({title : 'Showtime2: ".$showname." (".$row['st_animationtype'].")', onclick : function() {
                                                            tinyMCE.activeEditor.execCommand('mceInsertContent', false, '{Showtime2 show=\'".$row['show_id']."\'}');
                                                    }});
                                                    m.addSeparator();
                                            ";
                                    }
                            }else{
                                            $plugin1 .= "
                                                    m.add({title : 'Add Show first in Showtime2!', onclick : function() {
                                                            tinyMCE.activeEditor.execCommand('mceInsertContent', false, '');
                                                    }});
                                                    m.addSeparator();
                                            ";

                            }

                    $plugin1 .= "
                                    });
                                    // Return the new menu button instance
                                    return c;
                    }

                    return null;
            }
    });
            ";

            return array(array('picker',$plugin1,'Showtime2: (add also [showtime2_picker,] to your Profiles toolbars)'));
    }
  
	/**
	 * Register TinyMCE plugin for CMSMS 2.x
	 */
	function HasCapability($capability, $params=array()) {
		return ($capability=='WYSIWYGItems');
	}

	function GetWYSIWYGBtnName()
	{
		return 'module_showtime2';
	}

	function _buildList(array $elements) {
		$list = array();
		foreach ($elements as $element) {
			$item = array();
  		$item['menu_text'] = $element['show_name'];
			$item['content'] = "{Showtime2 show='".$element['show_id']."'}";  
			$list[] = $item;
		}
		return $list;
	}

	function GetWYSIWYGBtn($wysiwyg_module)
	{  
		$shows = showtime2_utils::GetShows();
		$items = $this->_buildList($shows);
    
		if ($wysiwyg_module == 'TinyMCE')
		{
			$obj = new tinymce_modulemenu;
			$obj->module_name = $this->GetName();
			$obj->button_name = $this->GetWYSIWYGBtnName();
			$obj->title = $this->Lang('tinymce_button_picker');
			$obj->tooltip = $this->Lang('tinymce_description_picker');
			$obj->icon = 'image';
			$obj->image = $this->GetModuleURLPath()."/images/icon_TinyMCE.gif";
			$obj->menu_section = 'insert';
			$obj->menu_entries = $items;

			return $obj;
		}

		return false;
	}	
} //end class

?>
