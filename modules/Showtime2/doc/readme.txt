Showtime 3.4 -> Showtime2

IMPORTANT!

If you are using Showtime 3.4 do NOT uninstall this module before installing Showtime2! If Showtime 3.4 is still installed, Showtime2 will copy the database-tables and css-files for its own use. After installing Showtime2 you can deinstall and remove Showtime, all your slideshows will be present in Showtime2. But don't forget to change your tags in the sides using Showtime!

Change

{Showtime show='1'}

to

{Showtime2 show='1'}

Showtime2 3.4.x -> Showtime2 3.5.0

I have completely changed the concept of the css-settings for nivoslider. This new concept is not compatible with previous versions, but much more flexible and allows multiple NivoSlider slideshows on the same page! Therefore you must open your settings and save them again for each NivoSlider slideshow! Nivoslider comes with six themes: the four default themes and two modified themes based on the default theme. These themes are stored in the folder modules/Showtime2/templates/nivoslider/themes. Do not modify these themes! They are only initial themes on installation and will be copied to the folder uploads/_Showtime2/nivothemes. These themes are used by Showtime2. You may modify them or add new, customized themes, which will then appear in the theme dropdown-list. Each theme can be modified later for a specific show on the settings-page.

Have fun!