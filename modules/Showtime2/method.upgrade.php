<?php
#-------------------------------------------------------------------------
# Module: Showtime
# Moduledemo's URI: http://www.web2do.be/cms/showtime
# Description:Display images as animated slideshow. 
# Choose among several smooth transition effects. ShowTime requires Adobe Flash player 10 or higher.
#
# Version: 1.0
# author: Peter Orije
# Author URI: http://www.web2do.be
/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
# Module based on Wordpress plugin from: Paul Schroeder URI: http://youtag.lu/showtime-wp-plugin/
#-------------------------------------------------------------------------
if (!cmsms()) exit;
require_once('lib/class.showtime2_utils.php');
$current_version = $oldversion;
$db =& $this->GetDb();
$taboptarray = array( 'mysql' => 'TYPE=MyISAM' );
$dict = NewDataDictionary( $db );
$this->SetPreference('show_nivo_note','0');
switch($current_version)
{
 case "3.4":   
   $current_version = "3.4.1";

 case "3.4.1":   
   $current_version = "3.4.2";
  
 case "3.4.2":   
   $current_version = "3.4.3";
        
 case "3.4.3":   
   $current_version = "3.4.5";
  // remove obsolete files
  @unlink('../modules/Showtime2/templates/nivoslider/.DS_STORE');
  @unlink('../modules/Showtime2/templates/nivoslider/dark/.DS_STORE');
  @unlink('../modules/Showtime2/showtime/error_log');
  $this->SetPreference("watermark_bak", '1');
  $this->RemoveEvent( 'OnShowtimePreferenceChange' );
  // create css-directory
  mkdir('../uploads/_Showtime2/css/',0755,true);
  showtime2_utils::createFile('../uploads/_Showtime2/css/index.html',"<!-- dummy index.html -->");
  copy('../uploads/_Showtime2/css/index.html','../uploads/_Showtime2/index.html');
  if (!file_exists('../uploads/index.html'))
    {copy('../uploads/_Showtime2/css/index.html','../uploads/index.html');}
  $old = '../modules/Showtime2/templates/css/';
  $new = '../uploads/_Showtime2/css/';
  showtime2_utils::moveFolder($old,$new);

  case "3.4.4": 
    if ($current_version=="3.4.4"){
      $current_version = "3.4.5";
      // remove obsolete files
      @unlink('../modules/Showtime2/templates/nivoslider/.DS_STORE');
      @unlink('../modules/Showtime2/templates/nivoslider/dark/.DS_STORE');
      @unlink('../modules/Showtime2/showtime/error_log');
      // create css-directory
      mkdir('../uploads/_Showtime2/css/',0755,true);
      showtime2_utils::createFile('../uploads/_Showtime2/css/index.html',"<!-- dummy index.html -->");
      copy('../uploads/_Showtime2/css/index.html','../uploads/_Showtime2/index.html');
      if (!file_exists('../uploads/index.html'))
        {copy('../uploads/_Showtime2/css/index.html','../uploads/index.html');}
      $old = '../modules_custom/Showtime2/css/';
      $new = '../uploads/_Showtime2/css/';
      showtime2_utils::moveFolder($old,$new,true);
      // delete css-folder
      @unlink('../modules_custom/Showtime2/index.html');
      rmdir('../modules_custom/Showtime2');
      $handle=opendir ('../modules_custom');
      if (!empty($handle)){
        $i = 0;
        while ( $file = readdir( $handle ) )
        {
        $i++;
        if ($file=='index.html'){$i--;}
         if ( $i > 2 )
          {
          break;
          }
        }
        closedir($handle);
        if ($i<=2) {
        showtime2_utils::removeFolder('../modules_custom');}}
    }    
  case "3.4.5": 
    $current_version = "3.5.0";
    $this->SetPreference('show_nivo_note','1');
    $pref = $this->GetPreference("use_tmp");
    if (empty($pref)){
      $this->SetPreference("use_tmp", '0');}
    // remove all templates
    $this->DeleteTemplate();
    // remove slides for non existing shows
    $query = "SELECT show_id FROM ".cms_db_prefix()."module_showtime2_name";
    $output=array();
    $result = $db->Execute($query);
    if( $result && $result->RecordCount() > 0 ){
      while ($row = $result->FetchRow()){
        $output[$row['show_id']] = $row;}
      $whereclause = ' where show_id not in ('.implode(',',array_keys($output)).')';
      $query = "DELETE FROM ".cms_db_prefix()."module_showtime2".$whereclause;
      $result = $db->Execute($query);
    }
    // remove watermark-example in order to create a fresh one
    @unlink('../modules/Showtime2/images/watermark_example_new.jpg');

    // remove obolete files  
    @unlink('../modules/Showtime2/license.txt');
    @unlink('../modules/Showtime2/doc/changelog.txt');
    @unlink('../modules/Showtime2/moduleinfo.ini');
    // remove obolete jquery and nivoslider files  
    @unlink('../modules/Showtime2/templates/orig_jQuery_css.tpl');
    @unlink('../modules/Showtime2/templates/jquery/jquery-1.7.1.min.js');
    @unlink('../modules/Showtime2/templates/jquery/jquery-1.4.2.js');
    @unlink('../modules/Showtime2/templates/jquery/jquery.js');
    @unlink('../modules/Showtime2/showtime/swfobject/swfobject-org.js');
    @unlink('../modules/Showtime2/templates/nivoslider/jquery-1.9.0.min.js');
    @unlink('../modules/Showtime2/templates/nivoslider/jquery.nivo.slider.pack_old.js');
    @unlink('../modules/Showtime2/templates/nivoslider/jquery.nivo.slider_old.js');
    @unlink('../modules/Showtime2/templates/nivoslider/arrows.png');
    @unlink('../modules/Showtime2/templates/nivoslider/bullets.png');
    @unlink('../modules/Showtime2/templates/nivoslider/default_arrows.png');
    @unlink('../modules/Showtime2/templates/nivoslider/default_bullets.png');
    @unlink('../modules/Showtime2/templates/nivoslider/loading.gif');
    @unlink('../modules/Showtime2/templates/nivoslider/nivo_arrows.png');
    @unlink('../modules/Showtime2/templates/nivoslider/nivo_bullets.png');
    @unlink('../modules/Showtime2/templates/nivoslider/nivo_controlnav.png');
    @unlink('../modules/Showtime2/templates/nivoslider/orman_arrows.png');
    @unlink('../modules/Showtime2/templates/nivoslider/orman_bullets.png');
    @unlink('../modules/Showtime2/templates/nivoslider/orman_slider.png');
    @unlink('../modules/Showtime2/templates/nivoslider/pascal_bullets.png');
    @unlink('../modules/Showtime2/templates/nivoslider/pascal_controlnav.png');
    @unlink('../modules/Showtime2/templates/nivoslider/pascal_slider.png');
    @unlink('../modules/Showtime2/templates/nivoslider/theme_default.jpg');
    @unlink('../modules/Showtime2/templates/nivoslider/theme_nivo.jpg');
    @unlink('../modules/Showtime2/templates/nivoslider/theme_orman.jpg');
    @unlink('../modules/Showtime2/templates/nivoslider/theme_pascal.jpg');
    showtime2_utils::removeFolder('../modules/Showtime2/templates/nivoslider/bar');
    showtime2_utils::removeFolder('../modules/Showtime2/templates/nivoslider/dark');
    showtime2_utils::removeFolder('../modules/Showtime2/templates/nivoslider/default');
    showtime2_utils::removeFolder('../modules/Showtime2/templates/nivoslider/light');
    if ($this->GetPreference("use_tmp")){
    showtime2_utils::copyFolder('../modules/Showtime2/templates/nivoslider/themes/','../tmp/_Showtime2/nivothemes/',true);}
    else{
    showtime2_utils::copyFolder('../modules/Showtime2/templates/nivoslider/themes/','../uploads/_Showtime2/nivothemes/',true);}

 case "3.5.0":   
   $current_version = "3.5.1";
   $this->RemovePreference("hide_donation");
   @unlink('../modules/Showtime2/images/donation-button.png');
 
 case "3.5.1":   
   $current_version = "3.5.2";
   @unlink('../modules/Showtime2/jq_crop/js/jquery.min.js');

 case "3.5.2":   
  $current_version = "3.5.3";

}

// put mention into the admin log
$this->Audit( 0, 
        $this->Lang('friendlyname'), 
        $this->Lang('upgraded', $this->GetVersion()));

//note: module api handles sending generic event of module upgraded here
?>