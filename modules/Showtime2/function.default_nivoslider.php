<?php
  //first some calculations and settings
  $showinfo['st_rotatetime'] = $showinfo['st_rotatetime']  * 1000;
  $showinfo['st_transitiontime'] = $showinfo['st_transitiontime'] * 1000;			
  $showinfo['st_showcontrols'] = ($showinfo['st_showcontrols'])? 'true':'false';// Next Prev navigation
  $showinfo['st_controlshide'] = ($showinfo['st_controlshide'])? 'true':'false';// Only show on hover
  $showinfo['st_navbut'] = ($showinfo['st_navbut'])? 'true':'false';// 1,2,3... navigation
  $showinfo['st_thumb_nav'] = ($showinfo['st_thumb_nav'])? 'true':'false';// Use thumbnails for Control Nav
  $showinfo['st_shuffle'] = ($showinfo['st_shuffle'])? 'true':'false';// Start on a random slide
  $showinfo['st_autoplay'] = ($showinfo['st_autoplay'])? 'false':'true';// Start on a random slide
  $showinfo['st_pauseonhover'] = ($showinfo['st_pauseonhover'])? 'true':'false';//

  $this->smarty->assign('show',$showinfo);
        
  //read pictures or slides from database
  $query = 'SELECT picture_id, picture_name, picture_url, picture_url_nature, thumbnail_path, picture_url_target, picture_path, comment FROM '.
    cms_db_prefix().'module_showtime2 WHERE active AND show_id = ? ORDER BY picture_number';	

  $result=$db->Execute($query,array($showid));
  $reccount= $result->RecordCount();			
  if ($reccount<>"0"){ //see if there are images
    $counter = 0;
    while(!$result->EOF()){
      $pic=$result->fields;
      if (!empty($pic['picture_url_nature'])&& $pic['picture_url_nature']<>"none"){
        switch ($pic['picture_url_nature']){
          case "internal page":
            $pic['picture_url'] = showtime2_utils::CreatePictureLink($pic['picture_url']);
            break;
          case "external page":
            $pic['picture_url']=$pic['picture_url'];
            break;
          case "image":
            $pic['picture_url'] = $config['image_uploads_url'].$pic['picture_path'];
            break;					
        }
      }


      $onerow = new stdClass();
      $onerow->picture_id = $pic['picture_id'];
      $onerow->slide_id=$counter;
      $onerow->picture_name = $pic['picture_name'];
      $onerow->picture_url = $pic['picture_url'];
      $onerow->picture_url_target = $pic['picture_url_target'];
      $onerow->thumbnail = $config['image_uploads_url'].$pic['thumbnail_path'];
      $onerow->picture= $config['image_uploads_url'] . $pic['picture_path'];
      $onerow->comment = $pic['comment'];
      $entryarray[] = $onerow;
      $counter ++;
      $result->MoveNext();
    }//end while


    $smarty->assign('items', $entryarray);

    $st_time = $showinfo['st_rotatetime']+$showinfo['st_transitiontime'];
    $template_metadata = "
<!--LOAD SHOWTIME nivoslider-->
<script type='text/javascript'>
      $(document).ready(function(){
      $(window).load(function() {
          $('#slider".$showid."').nivoSlider({
          effect: '".$showinfo['st_transition']."', // Specify sets like: 'fold,fade,sliceDown'
          slices: ".$showinfo['st_slices'].", // For slice animations
          boxCols: ".$showinfo['st_box_x'].", // For box animations
          boxRows: ".$showinfo['st_box_y'].", // For box animations
          animSpeed: ".$showinfo['st_transitiontime'].", // Slide transition speed
          pauseTime: ".$st_time.", // How long each slide will show
          startSlide: ".$showinfo['st_start_slide'].", // Set starting Slide (0 index)
          directionNav: ".$showinfo['st_showcontrols'].", // Next Prev navigation
          directionNavHide: ".$showinfo['st_controlshide'].", // Only show on hover
          controlNav: ".$showinfo['st_navbut'].", // 1,2,3... navigation
          controlNavThumbs: ".$showinfo['st_thumb_nav'].", // Use thumbnails for Control Nav
          controlNavThumbsFromRel: false, // Use image rel for thumbs
          keyboardNav: true, // Use left & right arrows
          pauseOnHover: ".$showinfo['st_pauseonhover'].", // Stop animation while hovering
          manualAdvance: ".$showinfo['st_autoplay'].", // Force manual transitions
          captionOpacity: ".$showinfo['st_captionopacity'].", // Universal caption opacity
          prevText: '".$this->Lang('nivo_prev')."', // Prev directionNav text
          nextText: '".$this->Lang('nivo_next')."', // Next directionNav text
          randomStart: ".$showinfo['st_shuffle'].", // Start on a random slide
          beforeChange: function(){}, // Triggers before a slide transition
          afterChange: function(){}, // Triggers after a slide transition
          slideshowEnd: function(){}, // Triggers after all slides have been shown
          lastSlide: function(){}, // Triggers when last slide is shown
          afterLoad: function(){} // Triggers when slider has loaded
        });
      });
    });
</script>
<!--END SHOWTIME-->
";
    
    if (empty($this->NivoMetadata) ){
      $this->NivoMetadata = $template_metadata;
      $smarty->assign('ext', '');    }
    else {
      if (strpos($this->NivoMetadata,'slider'.$showid)){
      $template_metadata = str_replace('slider'.$showid,'slider'.$showid.'a',$template_metadata);
      $smarty->assign('ext', 'a');}
      else     $smarty->assign('ext', '');
      
      $this->NivoMetadata .= $template_metadata;
    }
    
    //finaly process template
    echo $this->ProcessTemplate('showtime_nivo.tpl');


   }else{ //No images found
    echo  "<br>".$this->Lang('default_addimages'). $showid ."<br>";
   }//if $reccount <> 0
  $result->Close();
  //End Read the pictures



?>