<?php
/* --------------------------------------------------------------------------------------------------------
  Build example in Admin panel
-----------------------------------------------------------------------------------------------------------*/
global $CMS_VERSION;

  //set textoptions
  switch (trim($showinfo['st_fonttype'])){
    case 'verdana':
      $stt_fonttype='Verdana, Geneva, sans-serif' ;break;
    case 'courier':
      $stt_fonttype='"Courier New", Courier, monospace' ;break;
    case 'arial':
      $stt_fonttype='Arial, Helvetica, sans-serif' ;break;
    case 'tahoma':
      $stt_fonttype='Tahoma, Geneva, sans-serif' ;break;
    case 'sans':
      $stt_fonttype='_sans, Helvetica, Arial' ;break;
    case 'times':
      $stt_fonttype='"Times New Roman", Times, serif' ;break;
    case 'serif':
      $stt_fonttype='"MS Serif", "New York", serif' ;break;
  }
  $this->smarty->assign('stt_fonttype',$stt_fonttype);
  $this->smarty->assign('stt_rotatetime',$showinfo['st_rotatetime'] * 1000);
  $this->smarty->assign('stt_transitiontime',$showinfo['st_transitiontime'] * 1000);
  $this->smarty->assign('stt_time',($showinfo['st_transitiontime']+$showinfo['st_rotatetime'])*1000);
  $this->smarty->assign('stt_transition',$showinfo['st_transition']);
  $this->smarty->assign('stt_scale',$showinfo['st_scale']);
  $this->smarty->assign('stt_wmode',$showinfo['st_wmode']);
  $this->smarty->assign('stt_commentpos',$showinfo['st_commentpos']);
  $this->smarty->assign('stt_bgcolor',$showinfo['st_bgcolor']);
  $this->smarty->assign('showidvalue',$showid);		
  $this->smarty->assign('stt_easeFunc',$showinfo['st_easeFunc'].$showinfo['st_ease']);
  $this->smarty->assign('stt_autoplay',$showinfo['st_autoplay']);
  $this->smarty->assign('stt_showcontrols',$showinfo['st_showcontrols']);
  $this->smarty->assign('stt_controlshide',$showinfo['st_controlshide']);
  $this->smarty->assign('stt_captionopacity',$showinfo['st_captionopacity']);
  $this->smarty->assign('stt_showcontrolssub',$showinfo['st_showcontrolssub']);
  $this->smarty->assign('stt_textbgcolor',($showinfo['st_showalt']=='true')?$showinfo['st_textbgcolor']:'none');
  $this->smarty->assign('stt_showtext', $showinfo['st_showtext']);
  $this->smarty->assign('stt_showcomment',$showinfo['st_showcomment']);
  $this->smarty->assign('stt_showalt',$showinfo['st_showalt']);
  $this->smarty->assign('stt_textcolor',$showinfo['st_textcolor']);
  $this->smarty->assign('stt_textsize',$showinfo['st_textsize']);
  $this->smarty->assign('stt_titlesize',$showinfo['st_titlesize']);
  $this->smarty->assign('stt_shuffle',$showinfo['st_shuffle']);
  $this->smarty->assign('stt_navbut',$showinfo['st_navbut']);
  
  $this->smarty->assign('stt_theme',$showinfo['st_theme']);
  $this->smarty->assign('stt_slices',$showinfo['st_slices']);
  $this->smarty->assign('stt_box_x',$showinfo['st_box_x']);
  $this->smarty->assign('stt_box_y',$showinfo['st_box_y']);
  $this->smarty->assign('stt_thumb_nav',($showinfo['st_thumb_nav']=='true'?'true':'false'));
  $this->smarty->assign('stt_start_slide',$showinfo['st_start_slide']);
  $this->smarty->assign('stt_pauseonhover',$showinfo['st_pauseonhover']);
  $this->smarty->assign('stt_sizing',$showinfo['st_sizing']);

  //$this->smarty->assign('st_com_opacity',$showinfo['st_com_opacity']);



/* --------------------------------------------------------------------------------------------------------
  Build preferences in Admin panel
-----------------------------------------------------------------------------------------------------------*/


  /* --------------------------------------------------------------------------------------------------------
  BASIC SETTINGS
  -----------------------------------------------------------------------------------------------------------*/	
  $this->smarty->assign('basic',$this->Lang('editshow_basic'));
  $this->smarty->assign('isCMSMS2',version_compare($CMS_VERSION,'2.0','>='));

  // width and height settings	
  $this->smarty->assign('st_width',$this->CreateInputText($id,'st_width',$showinfo['st_width'],5,5));
  $this->smarty->assign('label_width',$this->Lang('editshow_width'));
  $this->smarty->assign('st_height',$this->CreateInputText($id,'st_height',$showinfo['st_height'],5,5));
  $this->smarty->assign('label_height',$this->Lang('editshow_height'));



  $smarty->assign('edit_animationtype', 
    $this->CreateInputRadioGroup($id,"st_animationtype",
    array(
        '<img src="../modules/Showtime2/images/nivoslider_icon.jpg" />'=>"nivoslider",
        '<img src="../modules/Showtime2/images/jQuery_icon.jpg" />'=>"jQuery",
        '<img src="../modules/Showtime2/images/swfobject_icon.jpg" />'=>"swfobject"),
    'nivoslider','','<br /><br /><br />'));
  $this->smarty->assign('label_animationtype',$this->Lang('animationtype'));
  $this->smarty->assign('animation_info',$this->Lang('animation_info'));


  /* --------------------------------------------------------------------------------------------------------
  SLIDESHOW SETTINGS
  -----------------------------------------------------------------------------------------------------------*/	
        //slider Sizing
  $keyvalues=array('Responsive'=>'responsive','Fixed Size'=>'fixed');
  $this->smarty->assign('st_sizing',$this->CreateInputDropdown($id, 'st_sizing',$keyvalues,-1,$showinfo['st_sizing'], $addttext='id="sizingpuldown"'));
  $this->smarty->assign('label_sizing',$this->Lang('editshow_sizing'));
        $this->smarty->assign('label_sizing_comment',$this->Lang('editshow_sizing_comment'));


  //Nivoslider theme settings
  $keyvalues=array();
  if ($this->GetPreference('use_tmp')=='1'){
    $path = '../tmp/_Showtime2/nivothemes/';}
  else {
    $path = '../uploads/_Showtime2/nivothemes/';}
  $allfiles = scandir($path);
  foreach ($allfiles as $file) {
    if ($file!='.' && $file!='..' && is_dir($path.$file)){
      $keyvalues[$file] = $file;}}
  $this->smarty->assign('st_theme',$this->CreateInputDropdown($id, 'st_theme',$keyvalues,-1,$showinfo['st_theme'], $addttext=''));
    $this->smarty->assign('label_theme',$this->Lang('editshow_theme'));
    $this->smarty->assign('label_theme_comment',$this->Lang('editshow_theme_comment'));
        
    
        
        // SLIDESHOW OPTIONS
  $this->smarty->assign('label_slide_options',$this->Lang('editshow_slide_options'));

        // Transition type
  $keyvalues=array('Random'=>'random','Fade'=>'fade','Fold'=>'fold','Slice Down'=>'sliceDown','Slice down (Left)'=>'sliceDownLeft','Slice Up'=>'sliceUp',
    'Slice up (Left)'=>'sliceUpLeft','Slice Up/Down'=>'sliceUpDown','Slice Up/Down (left)'=>'sliceUpDownLeft','Slide in (Right)'=>'slideInRight',
    'Slide in (Left)'=>'slideInLeft','Box Random'=>'boxRandom','Box Rain'=>'boxRain','Box Rain (Reverse)'=>'boxRainReverse','Box Rain Grow'=>'boxRainGrow',
    'Box Rain Grow (Reverse)'=>'boxRainGrowReverse');
  $this->smarty->assign('st_transition',$this->CreateInputDropdown($id, 'st_transition',$keyvalues,-1,$showinfo['st_transition'], $addttext=''));
  $this->smarty->assign('label_transition',$this->Lang('editshow_transition'));
    
        // Number Slices
  $this->smarty->assign('st_slices',$this->CreateInputText($id,'st_slices',$showinfo['st_slices'],5,5));
  $this->smarty->assign('label_nr_slices',$this->Lang('editshow_nr_slices'));
  $this->smarty->assign('label_nr_slices_comment',$this->Lang('editshow_nr_slices_comment'));
        
        // Box (Cols x Rows)
  $this->smarty->assign('st_box_x',$this->CreateInputText($id,'st_box_x',$showinfo['st_box_x'],5,5));
  $this->smarty->assign('st_box_y',$this->CreateInputText($id,'st_box_y',$showinfo['st_box_y'],5,5));
  $this->smarty->assign('label_box_cols',$this->Lang('editshow_box_cols'));
  $this->smarty->assign('label_box_cols_comment',$this->Lang('editshow_box_cols_comment'));
        

  // Transition and rotation timers
  $this->smarty->assign('st_rotatetime',$this->CreateInputText($id,'st_rotatetime',$showinfo['st_rotatetime'],5,5));
  $this->smarty->assign('st_transitiontime',$this->CreateInputText($id,'st_transitiontime',$showinfo['st_transitiontime'],5,5));
  $this->smarty->assign('label_rotation',$this->Lang('editshow_rotation'));
  $this->smarty->assign('label_transitiontime',$this->Lang('editshow_transitiontime'));
        
        
        
  // random setting
  $this->smarty->assign('st_shuffle',$this->CreateInputCheckbox($id,'st_shuffle','true',$showinfo['st_shuffle']));
  $this->smarty->assign('shuffle_onoff',$this->Lang('editshow_shuffle_onoff'));
  
  // Autoplay setting
  $this->smarty->assign('st_autoplay',$this->CreateInputCheckbox($id,'st_autoplay','true',$showinfo['st_autoplay']));
  $this->smarty->assign('auto_onoff',$this->Lang('editshow_auto_onoff'));

  // Show navigation controls
  $this->smarty->assign('st_showcontrols',$this->CreateInputCheckbox($id,'st_showcontrols','true',$showinfo['st_showcontrols']));
  $this->smarty->assign('showcontrols_images', '<img src="../modules/Showtime2/templates/jquery/images/prev_20.png" />
    <img src="../modules/Showtime2/templates/jquery/images/next_20.png" />');
  $this->smarty->assign('controls_onoff',$this->Lang('editshow_controls_onoff'));
  $this->smarty->assign('thumbs_onoff',$this->Lang('editshow_thumbs_onoff'));
  $this->smarty->assign('nivo_prev',$this->Lang('nivo_prev'));
  $this->smarty->assign('nivo_next',$this->Lang('nivo_next'));

  // Show controls on hover
  $this->smarty->assign('st_controlshide',$this->CreateInputCheckbox($id,'st_controlshide','true',$showinfo['st_controlshide']));
  $this->smarty->assign('controlshide_onoff',$this->Lang('editshow_controlshide_onoff'));



        
        
        

  // Turn navigation on off
  $this->smarty->assign('st_navbut',$this->CreateInputCheckbox($id,'st_navbut','true',$showinfo['st_navbut']));
  $this->smarty->assign('bar_onoff',$this->Lang('editshow_bar_onoff'));


  // Start position Slideshow
  $this->smarty->assign('st_start_slide',$this->CreateInputText($id,'st_start_slide',$showinfo['st_start_slide'],5,5));
  $this->smarty->assign('label_start_slide',$this->Lang('editshow_start_slide'));

  // Pause on hover setting
  $this->smarty->assign('st_pauseonhover',$this->CreateInputCheckbox($id,'st_pauseonhover','true',$showinfo['st_pauseonhover']));
  $this->smarty->assign('label_pauseonhover',$this->Lang('editshow_pauseonhover'));

  // Show thumbnail navigation
  $this->smarty->assign('st_thumb_nav',$this->CreateInputCheckbox($id,'st_thumb_nav','true',$showinfo['st_thumb_nav']));
  $this->smarty->assign('label_thumb_nav',$this->Lang('editshow_thumb_nav'));



  /* --------------------------------------------------------------------------------------------------------
  Caption options
  -----------------------------------------------------------------------------------------------------------*/	
  $this->smarty->assign('text_options',$this->Lang('editshow_text_options'));
  
  //show title
  $this->smarty->assign('st_showtext',$this->CreateInputCheckbox($id,'st_showtext','true',$showinfo['st_showtext']));
  $this->smarty->assign('label_show_title',$this->Lang('editshow_show_title'));

  //show comment
  $this->smarty->assign('st_showcomment',$this->CreateInputCheckbox($id,'st_showcomment','true',$showinfo['st_showcomment']));
  $this->smarty->assign('label_show_comment',$this->Lang('editshow_show_comment'));

  //show background text
  $this->smarty->assign('st_showalt',$this->CreateInputCheckbox($id,'st_showalt','true',$showinfo['st_showalt']));
  $this->smarty->assign('label_bg_text',$this->Lang('editshow_bg_text'));

  //text background color
  $this->smarty->assign('st_textbgcolor',$this->CreateInputText($id,'st_textbgcolor',$showinfo['st_textbgcolor'],7,7));
  $this->smarty->assign('label_text_bgcolor',$this->Lang('editshow_text_bgcolor'));

  //text color
  $this->smarty->assign('st_textcolor',$this->CreateInputText($id,'st_textcolor',$showinfo['st_textcolor'],7,7));
  $this->smarty->assign('label_text_color',$this->Lang('editshow_text_color'));

  // Caption opacity
  $this->smarty->assign('st_captionopacity',$this->CreateInputText($id,'st_captionopacity',$showinfo['st_captionopacity'],5,5));
  $this->smarty->assign('label_captionopacity',$this->Lang('editshow_captionopacity'));
  
  //fonttype
  $keyvalues=array();
  $keyvalues['Verdana, Geneva, sans-serif']='verdana';
  $keyvalues['"Courier New", Courier, monospace']='courier';
  $keyvalues['Arial, Helvetica, sans-serif']='arial';
  $keyvalues['Tahoma, Geneva, sans-serif']='tahoma';
  $keyvalues['_sans, Helvetica, Arial']='sans';
  $keyvalues['"Times New Roman", Times, serif']='times';
  $keyvalues['"MS Serif", "New York", serif']='serif';  
  $this->smarty->assign('st_fonttype',
    $this->CreateInputDropdown($id, 'st_fonttype',$keyvalues,-1,$showinfo['st_fonttype'], $addttext=''));
  $this->smarty->assign('label_font_type',$this->Lang('editshow_font_type'));

  //title and comment size
  $keyvalues=array('8'=>'8','9'=>'9','10'=>'10','12'=>'12','14'=>'14','16'=>'16','18'=>'18','20'=>'20','22'=>'22','24'=>'24','26'=>'26','28'=>'28','30'=>'30','32'=>'32','34'=>'34','36'=>'36','38'=>'38','40'=>'40','42'=>'42','44'=>'44','46'=>'46','48'=>'48','50'=>'50','52'=>'52','54'=>'54','56'=>'56','58'=>'58','60'=>'60','65'=>'65','70'=>'70','75'=>'75','80'=>'80');
  
  $this->smarty->assign('st_titlesize',
    $this->CreateInputDropdown($id, 'st_titlesize',$keyvalues,-1,$showinfo['st_titlesize'], $addttext=''));
    
  $this->smarty->assign('st_textsize',
    $this->CreateInputDropdown($id, 'st_textsize',$keyvalues,-1,$showinfo['st_textsize'], $addttext=''));
  $this->smarty->assign('label_title_size',$this->Lang('editshow_title_size'));
  $this->smarty->assign('label_comment_size',$this->Lang('editshow_comment_size'));

  $keyvalues=array($this->Lang('editshow_text_left')=>'left',$this->Lang('editshow_text_center')=>'center',$this->Lang('editshow_text_right')=>'right');
  $this->smarty->assign('st_commentpos',
    $this->CreateInputDropdown($id, 'st_commentpos',$keyvalues,-1,$showinfo['st_commentpos'], $addttext=''));
  $this->smarty->assign('label_comment_pos',$this->Lang('editshow_comment_pos'));
/*
  //comment postion
  $smarty->assign('st_commentpos', 
    $this->CreateInputRadioGroup($id,"st_commentpos",
    array('<img src="../modules/Showtime2/images/comments_top.png" />&nbsp;&nbsp;&nbsp;&nbsp;'=>"top",
        '<img src="../modules/Showtime2/images/comments_right.png" />&nbsp;&nbsp;&nbsp;&nbsp;'=>"right",
        '<img src="../modules/Showtime2/images/comments_bottom.png" />&nbsp;&nbsp;&nbsp;&nbsp;'=>"bottom",
        '<img src="../modules/Showtime2/images/comments_left.png" />&nbsp;&nbsp;&nbsp;&nbsp;'=>"left"),
    $showinfo['st_commentpos'],'','&nbsp;'));
  $this->smarty->assign('label_comment_pos',$this->Lang('editshow_comment_pos'));
*/
  
  //edit nivo_css
  //check if have alredy CSS template for this show
  $nivo_css = $this->GetTemplate("NivoCSS_Show_".$showid);
  if (!$nivo_css){ //get default
      $nivo_css = showtime2_utils::GetNivoCSS($showinfo['st_theme'],$showid);
      $this->SetTemplate('NivoCSS_Show_'.$showid,$nivo_css);}

  $this->smarty->assign('label_nivo_css',$this->Lang('editshow_nivo_css'));
  $this->smarty->assign('template_input',	$this->CreateTextArea(false,$id,$nivo_css,'nivo_css','','nivo_css','','','20','','','','style="width:300px; height:450px;"'));

  //pass this for example
  $cssstring = showtime2_CSS_NIVO::_Create($showid,true);
  $this->smarty->assign('stt_cssstring',$cssstring);
?>