<div id="list" class="scrollable"><ul>
{foreach from=$entries item='item'}
  <li>
    <span class="classname"}{$item.name}</span>
    {if $item.obj->has_settings()}
    <a class="edit_link">{$mod->Lang('edit')}</a>
  </li>
{/foreach}
</ul></div>

{foreach from=$entries item='item'}
{if $item.obj->has_settings()}
  <div class="scrollable" id="{$item.classname}">
    <div class="header"><a class="back_link">{$mod->Lang('back')}</a></div>
    <fieldset>
      <legend>{$mod->Lang('settings_for')}: {$item.name}</legend>
      {foreach from=$item.obj->get_controls() item='control'}
        <div class="pageoverflow">
          <p class="pagetext">{$control[0]}</p>
          <p class="pageinput">{$control[1]}</p>
        </div>
      {/foreach}
    </fieldset>
  </div>
{/if}
{/foreach}