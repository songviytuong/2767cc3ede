{$formstart}
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('enable_cache')}:</p> 
  <p class="pageinput">
    {cge_yesno_options prefix=$actionid name='usecache' selected=$usecache}
    <br/>
    {$mod->Lang('info_cache')}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext"></p>
  <p class="pageinput"> 
    <input type="submit" name="{$actionid}submit" value="{$mod->Lang('submit')}"/>
    <input type="submit" name="{$actionid}clearcache" value="{$mod->Lang('clear_cache')}" onclick="return confirm('{$mod->Lang('ask_clear_cache')}');"/>
  </p>
</div>
{$formend}