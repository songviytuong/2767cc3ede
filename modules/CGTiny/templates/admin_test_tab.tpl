{$formstart}
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('url')}:</p>
  <p class="pageinput">
    <input type="text" name="{$actionid}url" value="{$url|default:''}" size="80" maxlength="255"/>
    <br/>
    {$mod->Lang('info_url')}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext"></p>
  <p class="pageinput">
    <input type="submit" name="{$actionid}test_submit" value="{$mod->Lang('submit')}"/>
  </p>
</div>
{if isset($test_result)}
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('result')}:</p>
  <p class="pageinput">{$test_result}</h4>
</div>
{/if}
{$formend}