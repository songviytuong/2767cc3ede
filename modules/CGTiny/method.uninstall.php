<?php
if( !isset($gCms) ) exit;

$db = $this->GetDb();
$dict = NewDataDictionary( $db );

$sqlarray = $dict->DropTableSQL( cms_db_prefix()."module_cgtiny_services" );
$dict->ExecuteSQLArray($sqlarray);

$sqlarray = $dict->DropTableSQL( cms_db_prefix()."module_cgtiny_cache" );
$dict->ExecuteSQLArray($sqlarray);

?>