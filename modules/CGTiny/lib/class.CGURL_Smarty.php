<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20011 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This projects homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGURL_Smarty
{
    public static function init()
    {
        $smarty = cmsms()->GetSmarty();
        $smarty->register_function('cgtiny',array('CGURL_Smarty','cgtiny'));
    }


    public static function cgtiny($params,&$smarty)
    {
        if( !isset($params['src']) ) return;
        $src = trim($params['src']);
        unset($params['src']);

        $service = '';
        if( isset($params['service']) ) {
            $service = trim($params['service']);
            unset($params['service']);
        }

        if( $src == '::HERE::' ) $src = cge_url::current_url();

        $usecache = TRUE;
        if( isset($params['nocache']) && $params['nocache'] ) {
            $usecache = FALSE;
            unset($params['nocache']);
        }

        // get remaining params.
        $url = $src;
        $assign = null;
        if( isset($params['assign']) ) {
            $assign = $params['assign'];
            unset($params['assign']);
        }
        if( count($params) ) {
            if( strpos($src,'?') === FALSE ) {
                $url .= '?';
            }
            else {
                $url .= '&';
            }
            $url .= cge_array::implode_with_key($params);
        }

        $short_url = CGURL_Shortener::short($url,$usecache,$service);

        if( $assign ) {
            $smarty->assign(trim($params['assign']),$short_url);
            return;
        }
        return $short_url;
    }
} // end of class

#
# EOF
#
?>