<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20011 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGURL_Service_Manager
{
  protected function __construct() {} // can't instantiate this puppy.

  public static function list_all_services()
  {
    $mask = dirname(__FILE__).'/class.CGURL_Service_*php';
    $tmp = glob($mask);
    $files = array();
    for( $i = 0; $i < count($tmp); $i++ ) {
        $class = str_replace('.php','',str_replace('class.','',basename($tmp[$i])));
        if( $class == get_class() ) continue; // don't load this class.
        if( !class_exists($class) ) continue; // force an autoload.
        $files[] = $class;
    }

    return $files;
  }


  public static function update_services()
  {
    $all_services = self::list_all_services();

    $db = cmsms()->GetDb();

    $query = 'SELECT classname FROM '.cms_db_prefix().'module_cgtiny_services ';
    $available_services = $db->GetCol($query);

    $query = 'INSERT INTO '.cms_db_prefix().'module_cgtiny_services (service,classname,sorting,active) VALUES (?,?,?,?)';
    $num = count($available_services);
    foreach( $all_services as $service ){
        if( is_array($available_services) && count($available_services) && in_array($service,$available_services) ) {
            // no need ot touch this one.
            continue;
        }

        ++$num;
        $obj = new $service;
        $name = $obj->get_name();
        $dbr = $db->Execute($query,array($name,$service,$num,1));
      }
  }

} // end of class

#
# EOF
#
?>