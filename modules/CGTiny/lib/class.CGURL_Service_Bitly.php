<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20011 by Robert Campbell 
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
# 
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGURL_Service_Bitly extends CGURL_Service
{
  private $_login;
  private $_apikey;

  public function get_name()
  {
    return 'Bit.ly';
  }


  public function has_settings()
  {
    return TRUE;
  }


  public function store_settings($params)
  {
    $mod = cge_utils::get_module('CGTiny');
    $keys = array('bitly_login','bitly_apikey');
    $res = FALSE;
    foreach( $keys as $key )
      {
	if( isset($params[$key]) )
	  {
	    $mod->SetPreference($key,$params[$key]);
	    $res = TRUE;
	  }
      }
    return $res;
  }


  public function get_controls()
  {
    $this->authenticate();
    $mod = cge_utils::get_module('CGTiny');
    return array(
		 array($mod->Lang('bitly_login'),
		       $mod->CreateInputText('m1_','bitly_login',$this->_login,40,40)),
		 array($mod->Lang('bitly_apikey'),
		       $mod->CreateInputText('m1_','bitly_apikey',$this->_apikey,40,40))
		 );
  }


  public function is_short( $url ) {
    return stristr( $url, 'bit.ly/' );
  }


  public function authenticate()
  {
    $mod = cge_utils::get_module('CGTiny');
    if( is_object($mod) )
      {
	$this->_login = $mod->GetPreference('bitly_login');
	$this->_apikey = $mod->GetPreference('bitly_apikey');
	return TRUE;
      }
    return FALSE;
  }


  public function shorten($url)
  {
    if( !$this->authenticate() ) return $url;

    $data = array('version'=>'json',
		  'format'=>'json',
		  'login'=>$this->_login,
		  'apiKey'=>$this->_apikey,
		  'longUrl'=>$url);
    $result = cge_http::post('http://api.bit.ly/shorten',$data);
    if( !$result ) return $url;
    $result = json_decode($result);
    if( !$result->errorCode )
      {
	foreach( $result->results as $detail )
	  {
	    return $detail->shortUrl;
	  }
      }
    else
      {
	// todo: audit this.
	return $url;
      }
  }

  public function lengthen($url)
  {
    if( !$this->authenticate() ) return $url;

    $data = array('version'=>'json',
		  'format'=>'json',
		  'login'=>$this->_login,
		  'apikey'=>$this->_apikey,
		  'shortUrl'=>$url);
    $result = cge_http::post('http://api.bit.ly/expand',$data);
    if( !$result ) return $url;
    $result = json_decode($result);
    if( !$result->errorCode )
      {
	foreach( $result->results as $detail )
	  {
	    return $detail->longUrl;
	  }
      }
    else
      {
	// todo: audit this.
	return $url;
      }
  }
}

#
# EOF
#
?>