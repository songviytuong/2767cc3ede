<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20011 by Robert Campbell 
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
# 
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGURL_Service_googl extends CGURL_Service
{
  private $_apikey;
  private $_url = 'https://www.googleapis.com/urlshortener/v1/url';

  public function get_name()
  {
    return 'goo.gl';
  }

  public function has_settings()
  {
    return TRUE;
  }

  public function store_settings($params)
  {
    $mod = cge_utils::get_module('CGTiny');
    $keys = array('googl_apikey');
    $res = FALSE;
    foreach( $keys as $key )
      {
	if( isset($params[$key]) )
	  {
	    $mod->SetPreference($key,$params[$key]);
	    $res = TRUE;
	  }
      }
    return $res;
  }
  
  
  public function get_info()
  {
    $mod = cge_utils::get_module('CGTiny');
    return $mod->Lang('googl_info');
  }


  public function get_controls()
  {
    $this->authenticate();
    $mod = cge_utils::get_module('CGTiny');
    return array(
		 array($mod->Lang('googl_apikey'),
		       $mod->CreateInputText('m1_','googl_apikey',$this->_apikey,40,40))
		 );
  }


  public function authenticate()
  {
    $mod = cge_utils::get_module('CGTiny');
    if( is_object($mod) )
      {
	$this->_apikey = $mod->GetPreference('googl_apikey');
	return TRUE;
      }
    return FALSE;
  }


  public function is_short( $url )
  {
    return stristr( $url, 'goo.gl/' );
  }


  public function shorten( $url )
  {
    // authentication is optional (but recommended)
    //if( !in_array('curl',get_loaded_extensions()) ) return $url;

    $this->authenticate();

    $data = array('longUrl'=>$url);
    $data = json_encode($data);

    $turl = $this->_url;
    if( $this->_apikey )
      {
	$turl .= '?key='.$this->_apikey;
      }

    cge_http::reset();
    $http = cge_http::get_http();
    $http->addRequestHeader('Content-Type: application/json');
    $http->setMethod('POST');
    $http->setParams($data);
    $response = $http->execute($turl);
    if( !$response ) return $url;

    $response = json_decode($response);
    if( !is_object($response) || !isset($response->id) ) return $url;

    return $response->id;
  }


  public function lengthen( $url )
  {
    $this->authenticate();

    $turl = $this->_url;
    $turl .= '?shortURL='.urlencode($url);
    if( $this->_apikey )
      {
	$turl .= '&key='.$this->_apikey;
      }
    $response = cge_http::get($turl);
    if( !$response ) return $url;

    $response = json_decode($response);
    if( !is_array($response) || !isset($response['longUrl']) || !isset($response['status']) ) return $url;
    if( $response['status'] != 'OK' ) return $url;

    return $response['longUrl'];
  }
} // end of class
#
# EOF
#
?>