<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20011 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This projects homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGURL_Shortener
{
  private static $_cache = array();

  protected static function cache_enabled()
  {
    $mod = cge_utils::get_module('CGTiny');
    return $mod->GetPreference('cache_enabled',0);
  }


  public static function get_services($all = false,$full = false)
  {
    $db = cmsms()->GetDb();
    $query = 'SELECT service,classname FROM '.cms_db_prefix().'module_cgtiny_services';
    if( $full ) $query = 'SELECT * FROM '.cms_db_prefix().'module_cgtiny_services';
    if( !$all ) $query .= ' WHERE active = 1';
    $query .= ' ORDER BY sorting ASC';
    $data = $db->GetArray($query);
    return $data;
  }


  protected static function cache_short($url)
  {
    // look for this thing in the memory cache
    if( isset(self::$_cache[$url]) ) return self::$_cache[$url]['u_short'];

    // not in memory cache... try db cache
    $db = cmsms()->GetDb();
    $query = 'SELECT * FROM '.cms_db_prefix().'module_cgtiny_cache
               WHERE u_long = ? OR originator = ?';
    $tmp = $db->GetArray($query,array($url,cge_url::current_url()));
    if( !is_array($tmp) ) {
        // nope... not there either.
        return;
    }

    // we got something.... update the memory cache
    self::$_cache = array_merge(self::$_cache,cge_array::to_hash($tmp,'u_long'));

    if( isset(self::$_cache[$url]) ) {
        // found it.
        return self::$_cache[$url]['u_short'];
    }

    // still nothing found.
    return;
  }


  protected static function cache_long($url)
  {
    // look for the damned thing the long way in the cache
    foreach( self::$_cache as $key => $rec ) {
        if( $rec['u_short'] == $url ) return $key;
    }

    // not in memory cache... try db cache
    $db = cmsms()->GetDb();
    $query = 'SELECT * FROM '.cms_db_prefix().'module_cgtiny_cache
               WHERE u_short = ? OR originator = ?';
    $tmp = $db->GetArray($query,array($url,cge_url::current_url()));
    if( !is_array($tmp) ) {
        // nope... not there either.
        return;
    }

    // we got something.... update the memory cache
    self::$_cache = array_merge(self::$_cache,cge_array::to_hash($tmp,'u_long'));

    for( $i = 0; $i < count($tmp); $i++ ) {
        if( $tmp[$i]['u_short'] == $url ) return $tmp[$i]['u_long'];
    }

    // still couldn't find it.
    return;
  }


  protected static function cache_store($long,$short)
  {
      global $CMS_ADMIN_PAGE;
      if( isset($CMS_ADMIN_PAGE) ) return;

      $db = cmsms()->GetDb();
      $query = 'INSERT INTO '.cms_db_prefix().'module_cgtiny_cache
                (u_long,u_short,originator) VALUES (?,?,?)';
      $db->Execute($query,array($long,$short,cge_url::current_url()));
  }


  public static function short($url,$use_cache = TRUE,$service = '')
  {
    if( $use_cache ) {
        $mod = cge_utils::get_module('CGTiny');
        $use_cache = $mod->GetPreference('usecache',1);
    }

    $orig_url = $url;
    if( $use_cache ) {
        // see if we can get a short url from cache
        $url = self::cache_short($url);
        if( $url ) return $url;
    }

    // get it from the services.
    $services = self::get_services();
    if( !is_array($services) || count($services) == 0 ) return $url;

    if( $service != '' ) {
        // we want a specific service.
        if( !startswith($service,'CGURL_Service_') ) {
            $service = 'CGURL_Service_'.$service;
        }

        $services_t = array();
        foreach( $services as $rec ) {
            if( $rec['classname'] == $service ) {
                $services_t[] = $rec;
                break;
            }
        }
        if( count($services_t) ) $services = $services_t;
    }

    // foreach service.
    foreach( $services as $rec ) {
        if( !isset($rec['classname']) ) continue;
        $class = $rec['classname'];
        $obj = new $class;
        $short_url = $obj->shorten($orig_url);
        if( $short_url != $orig_url ) {
            // we got one from the service.
            $mod = cge_utils::get_module('CGTiny');
            $mod->Audit('',$class,'short_url: '.$short_url);
            if( $use_cache ) self::cache_store($orig_url,$short_url);
            return $short_url;
        }
    }

    // url could not be shortened.
    return $url;
  }


  public static function long($url,$use_cache = TRUE)
  {
    $orig_url = $url;
    if( $use_cache ) {
        // see if we can get a long url from cache
        $url = self::cache_long($url);
        if( $url ) return $url;
    }

    // get it from services.
    $services = self::get_services();
    if( !is_array($services) || count($services) == 0 ) return $url;

    foreach( $services as $class ) {
        $obj = new $class;
        $url = $obj->lengthen($url);
        if( $url != $orig_url ) return $url;
    }
    return $url;
  }


  public static function get_service($url)
  {
    $services = self::get_services();
    if( !is_array($services) || count($services) == 0 )
      return;

    foreach( $services as $class ) {
        $obj = new $class;
        if( $obj->is_short($url) ) return $class;
    }
  }
}

#
# EOF
#
?>