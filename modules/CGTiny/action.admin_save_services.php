<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20011 by Robert Campbell 
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
# 
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE
if( !isset($gCms) ) exit;
if( !$this->CheckPermission('Modify Site Preferences') ) exit;

$this->SetCurrentTab('services');

$services = CGURL_Shortener::get_services(true,true);
$services = cge_array::to_hash($services,'classname');
$query = 'UPDATE '.cms_db_prefix().'module_cgtiny_services SET sorting = ?, ACTIVE = ?
           WHERE classname = ?';
$sorting = 0;
foreach( $params as $key => $value ) {
  if( !startswith($key,'CGURL_Service_') ) {
    continue;
  }
  if( !endswith($key,'_active') ) {
    continue;
  }

  $classname = substr($key,0,strlen('_active')*-1);
  if( !isset($services[$classname]) ) continue;
  if( isset($services[$classname]['saved']) ) continue;    
  $active = $value;

  $obj = new $classname;
  $obj->store_settings($params);
  $services[$classname]['sorting'] = $sorting;
  $services[$classname]['active'] = $active;
  $dbr = $db->Execute($query,array($sorting++,$active,$classname));
  if( !$dbr )
    {
      debug_display($db->sql.'<br/>'.$db->ErrorMsg());
    }
  $services[$classname]['saved'] = 1;
}
$this->SetMessage($this->Lang('msg_services_updated'));
$this->RedirectToTab($id);

#
# EOF
#
?>