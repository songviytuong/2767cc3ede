<ul>
<li>Version 1.0 -- March 2011 -- Initial Release</li>
<li>Version 1.0.1 -- March 2011
  <ul>
    <li>Minor bug fix to the assign parameter.</li>
  </ul>
</li>
<li>Version 1.0.1 -- March 2011
  <ul>
    <li>Upgrade routine now searches for new services.</li>
    <li>Adds Yourls service.</li>
  </ul>
</li>
<li>Version 1.1
 <ul>
   <li>Numerous minor improvements.</li>
 </ul>
</li>
<li>Version 1.2
 <ul>
   <li>Adds HasCapability and shorten_url methods.</li>
   <li>Adds Notifier capability.</li>
   <li>Fixes is.gd (API change) (1.2.5)</li>
   <li>Minor fixes to specifying a service. (1.2.7).</li>
 </ul>
</li>
</ul>
