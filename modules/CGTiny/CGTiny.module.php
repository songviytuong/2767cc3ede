<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGTiny (c) 20012 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow shortening URLS.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE

class CGTiny extends CGExtensions
{
  public function GetName() { return get_class($this); }
  public function InitializeFrontend() { CGURL_Smarty::init(); }
  public function GetDependencies() { return array('CGExtensions'=>'1.45'); }
  function GetFriendlyName() { return $this->Lang('friendlyname'); }
  function GetVersion() { return '1.2.7'; }
  function MinimumCMSVersion() { return '1.12'; }
  function GetHelp() { return $this->Lang('help'); }
  function GetAuthor() { return 'calguy1000'; }
  function GetAuthorEmail() { return 'calguy1000@gmail.com'; }
  function GetChangeLog() { return @file_get_contents(__DIR__.'/changelog.html.inc'); }
  function IsPluginModule() { return TRUE; }
  function GetAdminDescription() { return $this->Lang('moddescription'); }
  function HasAdmin() { return TRUE; }
  function AllowAutoInstall() { return FALSE; }
  function AllowAutoUpgrade() { return FALSE; }
  function VisibleToAdminUser() { return $this->CheckPermission('Modify Site Preferences'); }
  function InstallPostMessage() { return $this->Lang('postinstall'); }
  function UninstallPostMessage() { return $this->Lang('postuninstall'); }

  function GetHeaderHtml() {
    $fn = $this->GetModuleURLPath().'/lib/jquery.tablednd_0_5.js';
    $output = '<script type="text/javascript" src="'.$fn.'"></script>'."\n";
    return $output;
  }

  public function HasCapability($capability, $params = array())
  {
    if( $capability == 'shorten_url' ) return TRUE;
    return FALSE;
  }

  public function shorten_url($url)
  {
    return CGURL_Shortener::short($url);
  }

  public function shorten_urls_in_text($text)
  {
    $fn = function($matches) {
      if( isset($matches[0]) ) {
	$mod = cms_utils::get_module(MOD_CGTINY);
	return $mod->shorten_url($matches[0]);
      }
      return $matches[0];
    };

    $regex ='@((https?://)([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.\,]*(\?\S+)?)?)*)@';
    return preg_replace_callback($regex,$fn,$text);
  }
}

#
# EOF
#
?>
