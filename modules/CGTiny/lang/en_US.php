<?php
#A
$lang['active'] = 'Active';
$lang['ask_clear_cache'] = 'Are you sure you want to clear all entries from the cache?';

#B
$lang['back'] = 'Back';
$lang['bitly_login'] = 'Login';
$lang['bitly_apikey'] = 'API Key';

#C
$lang['clear_cache'] = 'Clear Cache';

#D

#E
$lang['edit'] = 'Edit';
$lang['enable_cache'] = 'Enable Caching';

#F
$lang['friendlyname'] = 'Calguys URL Shorting Tool';

#G
$lang['googl_apikey'] = 'Google API Key';

#H
$lang['help'] = <<<EOT
<h3>What Does This Do?</h3>
<p>This module provides a convenient way of dynamically creating short URLS via a variety of URL shortening services, and interacting with them on your CMSMS Website.</p>
<h3>Features</h3>
<ul>
  <li>Supports a variety of URL Shortening Services.</li>
  <li>Supports caching to minimize external requests and maximize performance.</li>
  <li>Supports cascading of services (if one fails, the next in the list will be tried)</li>
  <li>Flexible Interface</li>
</ul>
<h3>How do I use it</h3>
  <p>The simplest way to use this module is to just use the tag in your page template, a module template, a GCB, or a content block.  like: <code style="color: blue;">{cgtiny src='http://www.cmsmadesimple.org'}></code>.  This will loop through the available services attempting to retrieve a short URL, when a result is returned it is cached and displayed on the page.   This output could then be used when generating a link, or supplied to other services, like facebook, or twitter.</p>
  <p>Some services (like bit.ly) require that you register in order to use their service (and also take advantage of advanced features).   You may need to provide the CGTiny module with information that will allow it to shorten urls on your behalf.</p>
<h3>Smarty Functions</h3>
<ul>
  <li><code style="color: blue;">{cgtiny src=&lt;string&gt; [service=&lt;string&gt;] [nocache=&lt;bool&gt;] [...] [assign=&lt;string&gt;]}</code>
    <p>Parameters:</p>
    <ul>
    <li><code style="color: blue;">src=&lt;string&gt;</code> - <em>Required</em> - Specify the URL to shorten.   As a convenience the string ::HERE:: can be used to specify the current page url.</li>
    <li><code style="color: blue;">service=&lt;string&gt;</code> - (optional) - Specify a specific service to use. Currently available services include isgd, tinyurl, bitly, and googl.</li>
    <li><code style="color: blue;">nocache=&lt;bool&gt;</code> - (optional) - Disable usage of the cache for this call.</li>
    <li><code style="color: blue;">assign=&lt;string&gt;</code> - (optional) - Specify the name of a smarty variable that will receive the results.</li>
    <li>Any other parameters supplied to the tag are appended to the URL before shortening is requested.<br/>
       i.e: <code>{cgtiny src='::HERE::' utm_campaign='MyCampaign' utm_source='twitter' utm_media='tweet'}</code><br/><br/>
    <li><strong>Hint:</strong> One way to use this module is in conjunction with the custom fields of the News, CGBlog or other modules to allow administrators to automatically select the campaign (and other parameters) to include on the tag, and then to hardcode the parameters for the medium and source.  i.e if in News you have a custom field called &quot;campaign&quot; you could do something like the code below in your news detail template  to automatically generate a facebook like button::<br/>
        <code>{cgtiny src=\$entry->canonical utm_campaign=\$entry->fieldsbyname.campaign->value utm_media='social' utm_source='facebook' assign='fburl'}{cgfb_like href=\$fburl}</code>
    </ul>
  </li>
</ul>
<h3>Support</h3>
<p>This module does not include commercial support. However, there are a number of resources available to help you with it:</p>
<ul>
<li>For the latest version of this module, FAQs, or to file a Bug Report or buy commercial support, please visit calguy\'s
module homepage at <a href="http://calguy1000.com">calguy1000.com</a>.</li>
<li>Additional discussion of this module may also be found in the <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>.</li>
<li>The author, calguy1000, can often be found in the <a href="irc://irc.freenode.net/#cms">CMS IRC Channel</a>.</li>
<li>Lastly, you may have some success emailing the author directly.</li>  
</ul>
<h3>Copyright and License</h3>
<p>Copyright &copy; 2008, Robert Campbel <a href="mailto:calguy1000@cmsmadesimple.org">&lt;calguy1000@cmsmadesimple.org&gt;</a>. All Rights Are Reserved.</p>
<p>This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.</p>
<p>However, as a special exception to the GPL, this software is distributed
as an addon module to CMS Made Simple.  You may not use this software
in any Non GPL version of CMS Made simple, or in any version of CMS
Made simple that does not indicate clearly and obviously in its admin 
section that the site was built with CMS Made simple.</p>
<p>This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
Or read it <a href="http://www.gnu.org/licenses/licenses.html#GPL">online</a></p>
EOT;

#I
$lang['info_cache'] = 'If Caching is enabled the system will attempt to use information cached in the database where possible, and any results successfullyr eturned from an external service will be cached in the database';
$lang['info_url'] = 'Copy and paste a url into this field to test the shortening functonality';

#J

#K

#L

#M
$lang['moddescription'] = 'A Simple, Flexible Extendable URL Shortinging Module';
$lang['modified'] = 'Modified';
$lang['msg_cachecleared'] = 'All records in the cgtiny cache have been cleared';
$lang['msg_services_updated'] = 'Service Settings Updated';
$lang['msg_settingschanged'] = 'Settings have been changed';

#N
$lang['name'] = 'Name';

#O

#P
$lang['postinstall'] = 'The CGTiny module has been installed, and is ready for use. You can configure additional shortening services in the modules admin panel';
$lang['postuninstall'] = 'The CGTiny module, and all configuration data has been removed.  Any pages that use this modules tags may now be generating errors';

#Q

#R
$lang['result'] = 'Result';

#S
$lang['save'] = 'Save';
$lang['services'] = 'Services';
$lang['settings'] = 'Settings';
$lang['settings_for'] = 'Settings For';
$lang['submit'] = 'Submit';

#T
$lang['test'] = 'Test';

#U
$lang['url'] = 'URL';

#V

#W

#X

#Y
$lang['yourls_description'] = 'This option supports shortening URLS via a private yourls server.   You need to specify the url to the yourls server, and your unique admin signature';
$lang['yourls_signature'] = 'yourls API signature';
$lang['yourls_url'] = 'URL to yourls server';

#Z
?>