{if empty($item)}
    {$tab_header=$mod->Lang('add')}
{else}
    {$tab_header=$mod->Lang('edit')}
{/if}

{tab_header name='edit' label=$tab_header}
{tab_start name='edit'}

{form_start item_id=$item.id|default:''}

<div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('name')}</p>
    <p class="pageinput">
        <input type="text" name="{$actionid}name" value="{$item.name|default:''}">
    </p>
</div>

<div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('content')}</p>
    <p class="pageinput">
        {cms_textarea prefix=$actionid name=content value=$item.content|default:'' enablewysiwyg=false}
    </p>
</div>

{if !empty($item.date_creation)}
<div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('date_creation')}</p>
    <p class="pageinput">
        {$item.date_creation}
    </p>
</div>
{/if}

{if !empty($item.date_modification)}
<div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('date_modification')}</p>
    <p class="pageinput">
        {$item.date_modification}
    </p>
</div>
{/if}

<div class="pageoverflow">
    <p class="pagetext">{$mod->Lang('status')}</p>
    <p class="pageinput">
        <input type="hidden" name="{$actionid}status" value="0">
        <input type="checkbox" name="{$actionid}status" value="1" {if !empty($item) AND $item.status == true}checked{/if}>
    </p>
</div>

<div class="pageoverflow">
    <p class="pageinput">
        <input type="submit" name="{$actionid}submit_save" value="{$mod->Lang('save')}">
        <input type="submit" name="{$actionid}submit_apply" value="{$mod->Lang('apply')}">
        <input type="submit" name="{$actionid}submit_cancel" value="{$mod->Lang('cancel')}">
    </p>
</div>

{form_end}
{tab_end}