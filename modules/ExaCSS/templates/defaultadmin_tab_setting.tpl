<fieldset>

    <legend>
        {$mod->Lang('preprocessor')}
    </legend>
    
    {form_start}
    
    <div class="pageoverflow">
       <p class="pagetext">{$mod->Lang('preprocessor')}</p>
       <p class="pageinput">
          <select name="{$actionid}preprocessor">
            {foreach $preprocessor_list as $preprocessor}
                <option value="{$preprocessor.alias}" {if $setting.preprocessor == $preprocessor.alias}selected{/if}>{$preprocessor.name}</option>
            {/foreach}
          </select>
       </p>
    </div>

    <div class="pageoverflow">
       <p class="pageinput">
          <input type="submit" name="{$actionid}submit_preprocessor" value="{$mod->Lang('save')}">
       </p>
    </div>

    {form_end}
    
</fieldset>

<fieldset>

    <legend>
        Importation
    </legend>
    
    {form_start}
    
    <div class="pageoverflow">
       <p class="pagetext">{$mod->Lang('file_to_import')}</p>
       <p class="pageinput">
          <input type="file" name="{$actionid}file">
       </p>
    </div>

    <div class="pageoverflow">
       <p class="pageinput">
          <input type="submit" name="{$actionid}submit_import" value="{$mod->Lang('import')}">
       </p>
    </div>

    {form_end}
    
</fieldset>

{if $setting.exaexternalizer_available}
<fieldset>

    <legend>
        {$mod->Lang('exaexternalizer')}
    </legend>
    
    {form_start}
    
    <div class="pageoverflow">
       <p class="pagetext">{$mod->Lang('exaexternalizer_compatibility')}</p>
       <p class="pageinput">
            <input type="hidden" name="{$actionid}exaexternalizer_compatibility" value="0">
            <input type="checkbox" name="{$actionid}exaexternalizer_compatibility" value="1" {if $setting.exaexternalizer_compatibility == true}checked{/if}>
       </p>
    </div>

    <div class="pageoverflow">
       <p class="pageinput">
          <input type="submit" name="{$actionid}submit_exaexternalizer" value="{$mod->Lang('save')}">
       </p>
    </div>

    {form_end}
    
</fieldset>
{/if}