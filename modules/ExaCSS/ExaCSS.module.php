<?php
class ExaCSS extends CMSModule
{
    
    
    
    
    
    
    
    
    
/* ************************************************************************** *\
   Constantes et variables
\* ************************************************************************** */
    
const PERM_USE = 'exacss_perm_use';









/* ************************************************************************** *\
   Construction
\* ************************************************************************** */

public function __construct() {
    
    parent::__construct();

    spl_autoload_register([$this,'autoload']);

}









/* ************************************************************************** *\
   Fonctions CMS Made Simple
\* ************************************************************************** */
    
public function GetName() {
    return 'ExaCSS';
}

public function GetVersion() {
    return '2.0.3';
}

public function GetAuthor() {
    return 'Jocelyn Lusseau - Exacore';
}

public function GetAuthorEmail() {
    return 'info@exacore.fr';
}

public function GetFriendlyName() {
    return $this->Lang('friendlyname');
}

public function GetAdminDescription() {
    return $this->Lang('admindescription');
}

public function IsPluginModule() {
    return true;
}

public function HasAdmin() {
    return true;
}

public function GetAdminSection() {
    return 'layout';
}

public function VisibleToAdminUser() {
    return $this->CheckPermission(self::PERM_USE);
}

public function GetDependencies() {
    return [];
}

public function GetHelp() {
    return '';
}

public function MinimumCMSVersion() {
    return '2';
}

public function UninstallPreMessage() {
    return $this->Lang('ask_uninstall');
}

public function GetHeaderHTML() {
    $tpl = '<script type="text/javascript" src="' . $this->GetModuleURLPath() . '/js/jquery.tablednd.0.8.min.js"></script>';
    return $tpl ;
}

public function InitializeFrontend() {
    $this->RegisterModulePlugin();
}









/* ************************************************************************** *\
    Évènements
\* ************************************************************************** */

public function DoEvent($originator, $eventname, &$params) {
    
    if ($originator == 'ExaExternalizer' && $eventname == 'Export'):
        \ExaCSS\ExaExternalizer::export();
    endif;
    
    if ($originator == 'ExaExternalizer' && $eventname == 'Import'):
        \ExaCSS\ExaExternalizer::import();
    endif;
    
}









/* ************************************************************************** *\
   Autoloader
\* ************************************************************************** */
    
public function autoload($classname) {

    if (!is_object($this)):
        return false;
    endif;

    $path = $this->GetModulePath().'/lib';
    if (strpos($classname,'\\') !== false):
        $t_path = str_replace('\\','/',$classname);
        if (startswith( $t_path, $this->GetName().'/')):
            $classname = basename($t_path);
            $t_path = dirname($t_path);
            $t_path = substr($t_path,strlen($this->GetName())+1);
            $path = $this->GetModulePath().'/lib/'.$t_path;
        endif;
    endif;

    $fn = $path."/class.{$classname}.php";
    if (file_exists($fn)):
        require_once($fn);
        return true;
    endif;

    return false;

}








    
}
?>