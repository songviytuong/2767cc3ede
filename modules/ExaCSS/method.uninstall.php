<?php
if( !defined('CMS_VERSION') ) exit;









/* ************************************************************************** *\
   Permissions
\* ************************************************************************** */

$this->RemovePermission(ExaCSS::PERM_USE);









/* ************************************************************************** *\
   Préférences
\* ************************************************************************** */

$this->RemovePreference();









/* ************************************************************************** *\
   Évènements
\* ************************************************************************** */

$this->RemoveEventHandler('ExaExternalizer', 'Export');
$this->RemoveEventHandler('ExaExternalizer', 'Import');









/* ************************************************************************** *\
   Suppression des tables
\* ************************************************************************** */

$db = $this->GetDb();
$dict = NewDataDictionary($db);

$table = [
    "module_exacss_preprocessor",
    "module_exacss_item"
    ];
    
foreach ($table as $item):
    $sqlarray = $dict->DropTableSQL(CMS_DB_PREFIX.$item);
    $dict->ExecuteSQLArray($sqlarray);
endforeach;









?>