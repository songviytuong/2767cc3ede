<?php
$lang['friendlyname'] = "ExaCSS";
$lang['admindescription'] = "Ce module permet d'éditer le CSS avec un préprocesseur au choix.";
$lang['ask_uninstall'] = "Êtes-vous certain(e) de vouloir désinstaller le module ExaCSS ?";

$lang['dashboard_tab_header'] = "Tableau de bord";

$lang['setting_tab_header'] = "Paramètres";

$lang['setting_tab_donation'] = "Don";
$lang['donation_introduction'] = "Ce module est distribué gratuitement et
    continera à l'être.<br/>Cependant, si vous souhaitez effectuer un don du
    montant de votre choix parce que vous estimez que ce module le mérite, je
    vous laisse faire :-)";
    
$lang['no_result_for_this_id'] = "Aucun résultat pour cet ID !";
$lang['preprocessor_error'] = "Erreur d'exécution du préprocesseur !";
$lang['preprocessor'] = "Préprocesseur";
$lang['file_to_import'] = "Fichier à importer";

$lang['item_list_empty'] = "Aucun item";
$lang['preprocessor_changed'] = "Préprocesseur modifié";

$lang['add'] = "Ajouter";
$lang['item_add'] = "Ajouter un item";
$lang['item_added'] = "Item ajouté";
$lang['item_edited'] = "Item édité";
$lang['item_deleted'] = "Item supprimé";
$lang['item_sort_edited'] = "Ordre de l'item modifié";
$lang['edit'] = "Éditer";
$lang['name'] = "Nom";
$lang['content'] = "Contenu";
$lang['date_creation'] = "Date de création";
$lang['date_modification'] = "Date de modification";
$lang['status'] = "État";
$lang['status_change'] = "Changer l'état";
$lang['export'] = "Exporter";
$lang['delete'] = "Supprimer";
$lang['sort_save'] = "Enregistrer l'ordre";

$lang['save'] = "Enregistrer";
$lang['apply'] = "Appliquer";
$lang['cancel'] = "Annuler";
$lang['import'] = "Importer";

$lang['item_delete_confirm'] = "Êtes-vous sûr(e) de vouloir supprimer cet item ?";

$lang['error_file_transfer'] = "Erreur lors du transfert du fichier";
$lang['error_date_modification_search'] = "Erreur dans la recherche de la date de dernière modification !";
$lang['error_file_not_xml'] = "Le fichier n'est pas au format XML !";
$lang['error_file_not_exacss_xml'] = "Le fichier n'est pas un fichier XML exporté par ExaCSS !";
$lang['error_during_importation'] = "Erreur lors de l'importation !";

$lang['import_success'] = "Importation réalisée avec succès !";

$lang['exaexternalizer'] = "ExaExternalizer";
$lang['exaexternalizer_not_available'] = "ExaExternalizer non disponible !";
$lang['exaexternalizer_compatibility'] = "Compatibilité avec ExaExternalizer";
$lang['setting_saved'] = "Paramètre enregistré";

?>