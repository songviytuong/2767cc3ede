<?php
$lang['friendlyname'] = "ExaCSS";
$lang['admindescription'] = "This module allows you to edit CSS with the CSS preprocessor of your choice.";
$lang['ask_uninstall'] = "Are you sure want to uninstall ExaCSS ?";

$lang['dashboard_tab_header'] = "Dashboard";

$lang['setting_tab_header'] = "Settings";

$lang['setting_tab_donation'] = "Donation";
$lang['donation_introduction'] = "This module is distributed for free and
    continue to be.<br/>However, if you want to make a donation of any amount
    of your choice because you feel that this module merit, I'll let you do
    :-)";
    
$lang['no_result_for_this_id'] = "No results for this ID!";
$lang['preprocessor_error'] = "Preprocessor Runtime Error!";
$lang['preprocessor'] = "Preprocessor";
$lang['file_to_import'] = "File to import";

$lang['item_list_empty'] = "No item";
$lang['preprocessor_changed'] = "Preprocessor changed";

$lang['add'] = "Add";
$lang['item_add'] = "Add an item";
$lang['item_added'] = "Item added";
$lang['item_edited'] = "Item edited";
$lang['item_deleted'] = "Item deleted";
$lang['item_sort_edited'] = "Order modified";
$lang['edit'] = "Edit";
$lang['name'] = "Name";
$lang['content'] = "Content";
$lang['date_creation'] = "Creation";
$lang['date_modification'] = "Modification";
$lang['status'] = "Status";
$lang['status_change'] = "Change status";
$lang['export'] = "Export";
$lang['delete'] = "Delete";
$lang['sort_save'] = "Change order";

$lang['save'] = "Save";
$lang['apply'] = "Apply";
$lang['cancel'] = "Cancel";
$lang['import'] = "Import";

$lang['item_delete_confirm'] = "Are you sure (e) want to delete this item?";

$lang['error_file_transfer'] = "Error transferring file";
$lang['error_date_modification_search'] = "Error in search of the last modification date!";
$lang['error_file_not_xml'] = "The file is not in XML format!";
$lang['error_file_not_exacss_xml'] = "The file is not an XML file exported by ExaCSS!";
$lang['error_during_importation'] = "Error while importing!";

$lang['import_success'] = "Importation completed successfully!";

$lang['exaexternalizer'] = "ExaExternalizer";
$lang['exaexternalizer_not_available'] = "ExaExternalizer not available!";
$lang['exaexternalizer_compatibility'] = "Compatibility with ExaExternalizer";
$lang['setting_saved'] = "Setting saved";

?>