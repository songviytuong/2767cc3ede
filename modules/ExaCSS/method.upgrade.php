<?php
if( !defined('CMS_VERSION') ) exit;









/* ************************************************************************** *\
   Variables
\* ************************************************************************** */

$db = $this->GetDb();
$dict = NewDataDictionary($db);
$current_version = $oldversion;









/* ************************************************************************** *\
   Switcher
\* ************************************************************************** */

switch($current_version):
    
    case '1.0':
	case '1.0.1':
	case '1.0.2':
        upgrade_1_1();
        
    case '1.1':
	case '1.1.1':
	case '1.2':
    case '1.2.1':
    case '1.2.2':
    case '1.3':
        upgrade_1_3_1();
         
    case '1.3.1':
    case '1.4':
        upgrade_2_0();
    
    case '2.0':
    case '2.0.1':
    case '2.0.2':
    case '2.0.3':
        break;
    
endswitch;









/* ************************************************************************** *\
   Upgrade 1.1
\* ************************************************************************** */

function upgrade_1_1() {
    
    // Ajout du préprocesseur scssphp
    $result = \ExaCSS\Preprocessor::set([
        'name' => 'scssphp',
        'alias' => 'scssphp',
        'url' => 'http://leafo.net/scssphp/',
        'sort' => '2',
        'status' => '1'
        ]);
    
    // Ajout du préprocesseur CSS-Crush
    $result = \ExaCSS\Preprocessor::set([
        'name' => 'CSS-Crush',
        'alias' => 'css-crush',
        'url' => 'http://the-echoplex.net/csscrush/',
        'sort' => '3',
        'status' => '1'
        ]);
    
    // Fin de la mise à jour
    $current_version = "1.1";

}









/* ************************************************************************** *\
   Upgrade 1.3
\* ************************************************************************** */

function upgrade_1_3() {
    
    // Récupération du préprocesseur lessphp (leafo)
    $result = \ExaCSS\Preprocessor::get([
        'where' => "alias = 'lessphp'"
        ]);
        
    $lessphp_leafo = $result->result[0];
    
    // Mise à jour du préprocesseur lessphp (leafo)
    $result = \ExaCSS\Preprocessor::set([
        'id' => $lessphp_leafo['id'],
        'name' => "lessphp (leafo)",
        'alias' => "lessphp_leafo"
        ]);

    $preprocessor_active = $this->GetPreference('preprocessor', '');
    if ($preprocessor_active == 'lessphp'):
        $this->SetPreference('preprocessor', 'lessphp_leafo');
    endif;

    // Ajout du préprocesseur Less.php (oyejorge)
    $result = \ExaCSS\Preprocessor::set([
        'name' => "Less.php (oyejorge)",
        'alias' => "lessphp_oyejorge",
        'url' => "http://lessphp.gpeasy.com/",
        'sort' => "4",
        'status' => "1"
        ]);

    // Fin de la mise à jour
    $current_version = "1.3";

}









/* ************************************************************************** *\
   Upgrade 2.0
\* ************************************************************************** */

function upgrade_2_0() {
    
    // Récupération des préprocesseurs
    $result = \ExaCSS\Preprocessor::get();
    $preprocessor_list = $result->record;
    
    // Suppression des préprocesseurs
    foreach ($preprocessor_list as $preprocessor):
    
        $result = \ExaCSS\Preprocessor::delete($preprocessor['id']);
    
    endforeach;
    
    // Installation du préprocesseur None
    $result = \ExaCSS\Preprocessor::set([
        'name' => "None",
        'alias' => "",
        'url' => "",
        'sort' => "0",
        'status' => "1"
        ]);
    
    // Installation du préprocesseur Less.php (oyejorge)
    $result = \ExaCSS\Preprocessor::set([
        'name' => "Less.php (oyejorge)",
        'alias' => "lessphp_oyejorge",
        'url' => "http://lessphp.gpeasy.com/",
        'sort' => "1",
        'status' => "1"
        ]);

    // Installation du préprocesseur scssphp (Leafo)
    $result = \ExaCSS\Preprocessor::set([
        'name' => "scssphp (Leafo)",
        'alias' => "scssphp_leafo",
        'url' => "http://leafo.github.io/scssphp/",
        'sort' => "1",
        'status' => "1"
        ]);
    
    // Mise à jour du préprocesseur actif si nécessaire
    $preprocessor = $this->GetPreference('preprocessor', '');
    switch ($preprocessor):
        case 'lessphp_leafo':
            $this->SetPreference('preprocessor', 'lessphp_oyejorge');
            break;
        case 'scssphp':
            $this->SetPreference('preprocessor', 'scssphp_leafo');
            break;
        case '':
            $this->SetPreference('preprocessor', '');
            break;
    endswitch;
    
    // Création de la table item
    $fields = "
        id I KEY AUTO,
        name C(255),
        content X2,
        sort I,
        date_creation " . CMS_ADODB_DT . ",
        date_modification " . CMS_ADODB_DT . ",
        status L
        ";
    $sqlarray = $dict->CreateTableSQL(CMS_DB_PREFIX."module_exacss_item",$fields,['mysql' => 'TYPE=MyISAM']);
    $dict->ExecuteSQLArray($sqlarray);

    // Copie des mixins vers les items
    $query = "
        INSERT ".CMS_DB_PREFIX."module_exacss_item
        SELECT * FROM ".CMS_DB_PREFIX."module_exacss_mixin
        ";
    $db->Execute($query);
    
    // Suppression des mixins
    $sqlarray = $dict->DropTableSQL(CMS_DB_PREFIX.'module_exacss_mixin');
    $dict->ExecuteSQLArray($sqlarray);
    
    // Copie des variables vers les items
    $query = "
        INSERT ".CMS_DB_PREFIX."module_exacss_item
        SELECT * FROM ".CMS_DB_PREFIX."module_exacss_variable
        ";
    $db->Execute($query);
    
    // Suppression des variables
    $sqlarray = $dict->DropTableSQL(CMS_DB_PREFIX.'module_exacss_variable');
    $dict->ExecuteSQLArray($sqlarray);
    
    // Copie des stylesheets vers les items
    $query = "
        INSERT ".CMS_DB_PREFIX."module_exacss_item
        SELECT * FROM ".CMS_DB_PREFIX."module_exacss_stylesheet
        ";
    $db->Execute($query);
    
    // Suppression des stylesheets
    $sqlarray = $dict->DropTableSQL(CMS_DB_PREFIX.'module_exacss_stylesheet');
    $dict->ExecuteSQLArray($sqlarray);
    
    // Suppression des permissions
    $this->RemovePermission();
    
    // Ajout de la permission
    $this->CreatePermission(ExaCSS::PERM_USE,'ExaCSS - Use');
    
    // Fin de la mise à jour
    $current_version = "2.0";
    
}









?>