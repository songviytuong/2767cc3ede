<?php

namespace ExaCSS;

class Preprocessor
{
    
    
    
    
    
    
    
    
    
/* ************************************************************************** *\
    Variables
\* ************************************************************************** */

public static $fields = [
    'id',
    'name',
    'alias',
    'url',
    'sort',
    'status'
    ];









/* ************************************************************************** *\
    Get
\* ************************************************************************** */

public static function get($options) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->count = null;
    $return->record = [];
    
    // Default options
    $default = [
        'fields' => '*',
        'where' => null,
        'order' => null,
        'limit' => null,
        'offset' => null
        ];
      
    // Filter options
    foreach ($options as $key => $val):
        if (!array_key_exists($key, $default)):
            unset($options[$key]);
        endif;
    endforeach;
    $options = array_merge($default, $options);
    
    // Query
    $sql = "SELECT {$options['fields']} FROM " . CMS_DB_PREFIX . "module_exacss_preprocessor";
    $sql .= (!empty($options['where'])) ? " WHERE {$options['where']}" : '';
    $sql .= (!empty($options['order'])) ? " ORDER BY {$options['order']}" : '';
    $sql .= (!empty($options['limit'])) ? " LIMIT {$options['limit']}" : '';
    $sql .= (!empty($options['offset'])) ? " OFFSET {$options['offset']}" : '';
    
    $result = $db->Execute($sql);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->count = $result->RecordCount();
    $return->record = $result->GetAll();
    $return->result = true;
    
    return $return;
    
}









/* ************************************************************************** *\
    Set
\* ************************************************************************** */

public static function set($options) {
    
    // Préparation des options
    foreach ($options as $key => $val):
        
        // Filtrage
        if (!in_array($key, self::$fields)):
            unset($options[$key]);
            continue;
        endif;

        // Optimisation
        if ($val == ''):
            $options[$key] = NULL;
        endif;
        
        switch ($key):
            case 'id':
            case 'sort':
                $options[$key] = (int) $val;
                break;
            case 'name':
            case 'status':
                $options[$key] = trim($val);
                break;
        endswitch;
        
    endforeach;
    
    // Insertion ou mise à jour
    if (!empty($options['id']) AND $options['id'] > 0):
        $return = self::update($options);
    else:
        $return = self::insert($options);
    endif;
    
    return $return;

}









/* ************************************************************************** *\
    Insert
\* ************************************************************************** */

protected static function insert($options) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->insertId = null;
    
    // Vérification des valeurs
    if (empty($options['name'])):
        $return->message = "No name!";
        return $return;
    endif;
    
    if (empty($options['alias'])):
        $return->message = "No alias!";
        return $return;
    endif;
    
    // Préparation des valeurs
    $options['sort'] = (empty($options['sort'])) ? $timeStamp : $options['sort'];
    
    // Préparation du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requête
    $query = "
        INSERT INTO " . CMS_DB_PREFIX . "module_exacss_preprocessor
        SET {$set}
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->insertId = $db->Insert_ID();
    $return->message = "Préprocesseur ajouté";
    
    return $return;
    
}









/* ************************************************************************** *\
    Update
\* ************************************************************************** */

protected static function update($options) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Préparation du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requête
    $query = "
        UPDATE " . CMS_DB_PREFIX . "module_exacss_preprocessor
        SET {$set}
        WHERE id = {$options['id']}
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = "Item édité";
    
    return $return;
    
}









/* ************************************************************************** *\
    Delete
\* ************************************************************************** */

public static function delete($item_id) {

    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Requête
    $query = "
        DELETE FROM " . CMS_DB_PREFIX . "module_exacss_preprocessor
        WHERE id = ?
        ";
    
    $result = $db->Execute($query, [$item_id]);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = "Préprocesseur supprimé";
    
    return $return;

}









/* ************************************************************************** *\
    Execute
\* ************************************************************************** */

public static function execute() {
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    $mod = \cms_utils::get_module('ExaCSS');
    $preprocessor = $mod->GetPreference('preprocessor', '');
    
    $content = '';
    
    // Récupération des items
    $result = \ExaCSS\Item::get([
        'where' => "status = true",
        'order' => "sort ASC"
        ]);
        
    if (!$result OR $result->result == false):
        $return->message = 'Erreur inconnue';
        return $return;
    endif;
   
    $item_list = $result->record;

    // Concaténation des items
    if ($result->count > 0):
        
        foreach ($item_list as $item):
        
            $content .= $item['content'] . "\n\n";
        
        endforeach;
        
    endif;
    
    // Exécution de Smarty
    $content = self::smarty($content);

    // Exécution du préprocesseur
    switch ($preprocessor):
        case "lessphp_oyejorge":
            $content = self::lessphp_oyejorge($content);
            break;
        case "scssphp_leafo":
            $content = self::scssphp_leafo($content);
            break;
        default:
            break;
    endswitch;
    
    // Minification du contenu
    $content = self::minify($content);
    
    // Suppression du cache ExaCSS
    $result = \ExaCSS\Cache::delete();
    
    // Ecriture du cache CSS
    $result = \ExaCSS\Cache::write($content);
    
    $return->result = true;
    
    return $return;
    
}









/* ************************************************************************** *\
    Smarty
\* ************************************************************************** */

protected static function smarty($content) {
    
    $smarty = cmsms()->GetSmarty();
    
    $smarty->left_delimiter = '[[';
    $smarty->right_delimiter = ']]';
    $tmp = $smarty->force_compile;
    $smarty->force_compile = 1;
    $content = $smarty->fetch('string:'.$content);
    $smarty->force_compile = $tmp;
    $smarty->left_delimiter = '{';
    $smarty->right_delimiter = '}';
    
    return $content;
    
}









/* ************************************************************************** *\
    Minify
\* ************************************************************************** */

protected static function minify($content) {
    
    // Suppression des préfixes http(s)
    $content = preg_replace('#https?:#', '', $content);
    
	// Suppression des commentaires
	$content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);

	// Suppression des tabulations, espaces multiples, retours à la ligne, etc.
	$content = str_replace(array("\r\n", "\r", "\n", "\t"), ' ', $content);
	$content = preg_replace("![ ]+!", " ", $content);

	// Suppression des derniers espaces inutiles
	$content = str_replace(array(' { ',' {','{ '), '{', $content);
	$content = str_replace(array(' } ',' }','} '), '}', $content);
	$content = str_replace(array(' : ',' :',': '), ':', $content);
	$content = str_replace(array(' ; ',' ;','; '), ';', $content);
    $content = trim($content);
	
	return $content ;
    
}









/* ************************************************************************** *\
    lessphp_oyejorge
\* ************************************************************************** */

protected static function lessphp_oyejorge($content) {
    
    $mod = \cms_utils::get_module('ExaCSS');
    
    try {
        require_once($mod->GetModulePath().'/extra/lessphp_oyejorge/Less.php');
        $parser = new \Less_Parser();
        $parser->parse($content);
        $content = $parser->getCss();
    }
    catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
    
    return $content;
    
}









/* ************************************************************************** *\
    lessphp_oyejorge
\* ************************************************************************** */

protected static function scssphp_leafo($content) {
    
    $mod = \cms_utils::get_module('ExaCSS');
    
    try {
        require_once($mod->GetModulePath().'/extra/scssphp_leafo/scss.inc.php');
        $scss = new \Leafo\ScssPhp\Compiler();
        $content = $scss->compile($content);
    }
    catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
    
    return $content;
    
}









}
?>