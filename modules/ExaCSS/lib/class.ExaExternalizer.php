<?php

namespace ExaCSS;

class ExaExternalizer
{
    
    
    
    
    
    
    
    

/* ************************************************************************** *\
    Export
\* ************************************************************************** */

public static function export() {
    
    // Test de la pr�sence d'ExaExternalizer
    $exaexternalizer_installed = \cms_utils::module_available('ExaExternalizer');

    if ($exaexternalizer_installed == false):
        return;
    endif;
    
    $mod = \cms_utils::get_module('ExaCSS');

    // R�cup�ration des items
    $result = \ExaCSS\Item::get();
    $item_list = $result->record;

    $folder = $mod->GetName();
    $ExaExternalizer = \cms_utils::get_module('ExaExternalizer');
    $stylesheet_extension = $ExaExternalizer->GetPreference('stylesheet_extension');

    // Cr�ation du dossier d'exportation
    \ExaExternalizer\FileSystem::createFolder($folder);

    foreach ($item_list as &$item):

        \ExaExternalizer\FileSystem::createFile($folder, $item['name'] . '__' . $item['id'], $stylesheet_extension, $item['content'], strtotime($item['date_modification']));

    endforeach;
    
}









/* ************************************************************************** *\
   Importer les items
\* ************************************************************************** */
public static function import()
{
    
    // Test de la pr�sence d'ExaExternalizer
    $exaexternalizer_installed = \cms_utils::module_available('ExaExternalizer');

    if ($exaexternalizer_installed == false):
        return;
    endif;
    
    $mod = \cms_utils::get_module('ExaCSS');
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Variables
    $cache_path = \ExaExternalizer\Cache::getPath();
    $ExaExternalizer = \cms_utils::get_module('ExaExternalizer');
    $stylesheet_extension = $ExaExternalizer->GetPreference('stylesheet_extension');
    $folder = $mod->GetName();
    $folder_path = cms_join_path($cache_path, $folder);


    // Parcourir chaque feuille de styles
    $result = \ExaCSS\Item::get();
    if (count($result->record) > 0):

        $item_record = $result->record;

        foreach ($item_record as $item):

            $filename = \ExaExternalizer\FileSystem::escapeFilename($item['name']);
            $filename = cms_join_path($folder_path, $filename . '__' . $item['id'] . '.' . $stylesheet_extension);

            // Si le fichier n'existe pas
            if (!file_exists($filename)):
                continue;
            endif;

            // Si le fichier n'a pas �t� modifi�
            $filetime = @filemtime($filename);
            if ($filetime <= strtotime($item['date_modification'])):
                continue;
            endif;

            // Lecture du contenu
            $content = file_get_contents($filename);

            // Sauvegarde du contenu
            $result = \ExaCSS\Item::set([
                'id' => $item['id'],
                'content' => $content,
                'date_modification' => date("Y-m-d H:i:s", $filetime)
                ]);

            // Mise � z�ro du timeout
            \ExaExternalizer\TimeOut::reset();

        endforeach;

    endif;
    
    $return->result = true;
    
    return $return;
   
}









}
?>