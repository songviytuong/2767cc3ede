<?php

namespace ExaCSS;

class Item
{
    
    
    
    
    
    
    
    
    
/* ************************************************************************** *\
    Variables
\* ************************************************************************** */

public static $fields = [
    'id',
    'name',
    'content',
    'sort',
    'date_creation',
    'date_modification',
    'status'
    ];









/* ************************************************************************** *\
    Get
\* ************************************************************************** */

public static function get($options=[]) {
    
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->count = null;
    $return->record = [];
    
    // Default options
    $default = [
        'fields' => '*',
        'where' => null,
        'order' => null,
        'limit' => null,
        'offset' => null
        ];
      
    // Filter options
    foreach ($options as $key => $val):
        if (!array_key_exists($key, $default)):
            unset($options[$key]);
        endif;
    endforeach;
    $options = array_merge($default, $options);
    
    // Query
    $sql = "SELECT {$options['fields']} FROM " . CMS_DB_PREFIX . "module_exacss_item";
    $sql .= (!empty($options['where'])) ? " WHERE {$options['where']}" : '';
    $sql .= (!empty($options['order'])) ? " ORDER BY {$options['order']}" : '';
    $sql .= (!empty($options['limit'])) ? " LIMIT {$options['limit']}" : '';
    $sql .= (!empty($options['offset'])) ? " OFFSET {$options['offset']}" : '';
    
    $result = $db->Execute($sql);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->count = $result->RecordCount();
    $return->record = $result->GetAll();
    $return->result = true;
    
    return $return;
    
}









/* ************************************************************************** *\
    Set
\* ************************************************************************** */

public static function set($options=[]) {
    
    // Préparation des options
    foreach ($options as $key => $val):
        
        // Filtrage
        if (!in_array($key, self::$fields)):
            unset($options[$key]);
            continue;
        endif;

        // Optimisation
        if ($val == ''):
            $options[$key] = NULL;
        endif;
        
        switch ($key):
            case 'id':
            case 'sort':
                $options[$key] = (int) $val;
                break;
            case 'name':
            case 'status':
                $options[$key] = trim($val);
                break;
        endswitch;
        
    endforeach;
    
    // Insertion ou mise à jour
    if (!empty($options['id']) AND $options['id'] > 0):
        $return = self::update($options);
    else:
        $return = self::insert($options);
    endif;
    
    return $return;

}









/* ************************************************************************** *\
    Insert
\* ************************************************************************** */

protected static function insert($options) {
    
    $mod = \cms_utils::get_module('ExaCSS');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    $return->insertId = null;
    
    $timeStamp = time();
    $timeNow = date("Y-m-d H:i:s",$timeStamp);
    
    // Vérification des valeurs
    if (empty($options['name'])):
        $return->message = "No name!";
        return $return;
    endif;
    
    // Préparation des valeurs
    $options['sort'] = (empty($options['sort'])) ? $timeStamp : $options['sort'];
    $options['date_creation'] = (empty($options['date_creation'])) ? $timeNow : $options['date_creation'];
    $options['date_modification'] = (empty($options['date_modification'])) ? $timeNow : $options['date_modification'];

    // Préparation du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requête
    $query = "
        INSERT INTO " . CMS_DB_PREFIX . "module_exacss_item
        SET {$set}
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->insertId = $db->Insert_ID();
    $return->message = $mod->Lang('item_added');
    
    return $return;
    
}









/* ************************************************************************** *\
    Update
\* ************************************************************************** */

protected static function update($options) {
    
    $mod = \cms_utils::get_module('ExaCSS');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    $timeStamp = time();
    $timeNow = date("Y-m-d H:i:s",$timeStamp);
    
    // Préparation des valeurs
    $options['date_modification'] = (empty($options['date_modification'])) ? $timeNow : $options['date_modification'];

    // Préparation du SET
    $set = '';
    foreach ($options as $key => $val):
        $set .= $key . ' = ?';
        end($options);
        if ($key != key($options)):
            $set .= ', ';
        endif;
    endforeach;
    
    // Requête
    $query = "
        UPDATE " . CMS_DB_PREFIX . "module_exacss_item
        SET {$set}
        WHERE id = {$options['id']}
        ";
    
    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = $mod->Lang('item_edited');
    
    return $return;
    
}









/* ************************************************************************** *\
    Delete
\* ************************************************************************** */

public static function delete($item_id) {

    $mod = \cms_utils::get_module('ExaCSS');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Requête
    $query = "
        DELETE FROM " . CMS_DB_PREFIX . "module_exacss_item
        WHERE id = ?
        ";
    
    $result = $db->Execute($query, [$item_id]);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = $mod->Lang('item_deleted');
    
    return $return;

}









/* ************************************************************************** *\
    Sort
\* ************************************************************************** */

public static function sort($item_id, $sort) {
    
    $mod = \cms_utils::get_module('ExaCSS');
    $db = \cms_utils::get_db();
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Vérification
    if (!is_numeric($item_id) OR !is_numeric($sort)):
        $return->message = "Erreur lors de la tentative de modification de l'ordre de l'item !";
        return $return;
    endif;

    // Requête
    $query = "
        UPDATE " . CMS_DB_PREFIX . "module_exacss_item
        SET
            sort = ?
        WHERE
            id = ?
        ";
        
    $options = [
        $sort,
        $item_id
        ];

    $result = $db->Execute($query, $options);
    
    if (!$result):
        $return->message = $db->ErrorMsg();
        return $return;
    endif;
    
    $return->result = true;
    $return->message = $mod->Lang('item_sort_edited');
    
    return $return;
    
}









/* ************************************************************************** *\
    Import
\* ************************************************************************** */

public static function import($file) {
    
    $mod = \cms_utils::get_module('ExaCSS');
    
    $return = new \stdClass();
    $return->result = false;
    $return->message = '';
    
    // Vérification d'erreur sur le transfert
    if ($file['error'] != 0):
        $return->message = $mod->Lang('error_file_transfer') . " (" . $file['error'] . ")";
        return $return;
    endif;
    
    // Détermine s'il s'agit d'un fichier XML
	$xml = new \DomDocument() ;
	$xml_load = @$xml->load($file['tmp_name']);
	
	if ($xml_load == false):
		$return->message = $mod->Lang('error_file_not_xml');
        return $return;
	endif;
	
	// Détermine s'il s'agit bien d'un fichier XML d'ExaCSS
	$sxml = simplexml_import_dom($xml);
	if (!isset($sxml->name) OR
		!isset($sxml->content) OR
		!isset($sxml->sort) OR
		!isset($sxml->date_creation) OR
		!isset($sxml->date_modification) OR
		!isset($sxml->status)):
		$return->message = $mod->Lang('error_file_not_exacss_xml');
        return $return;
	endif;
	
	// Importation
	$result = self::set([
        'name' => (string) $sxml->name,
        'content' => (string) $sxml->content,
        'sort' => (int) $sxml->sort,
        'date_creation' => (string) $sxml->date_creation,
        'date_modification' => (string) $sxml->date_modification,
        'status' => (int) $sxml->status
        ]);
	
	if ($result->result == false):
		$return->message = $mod->Lang('error_during_importation');
        return $return;
	endif;

    $return->message = $mod->Lang('import_success');
	$return->result = true;
    
    return $return;
    
}









}
?>