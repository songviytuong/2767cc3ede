<?php

namespace ExaCSS;

class Cache
{
    
    
    
    
    
    
    
    
    
/* ************************************************************************** *\
    Get filename
\* ************************************************************************** */

protected static function getFilename() {
    
    $mod = \cms_utils::get_module('ExaCSS');
    $preprocessor = $mod->GetPreference('preprocessor', '');
    $last_modification = self::getLastModification();
    
    $filename = 'exacss_'.md5($preprocessor.$last_modification).'.css';
    
    return $filename;
    
}









/* ************************************************************************** *\
    Get path
\* ************************************************************************** */

protected static function getPath() {
    
    $config = cmsms()->GetConfig() ;

    $path = $config['css_path'];
    
    return $path;
    
}









/* ************************************************************************** *\
    Get URL
\* ************************************************************************** */

public static function getUrl() {
    
    $config = cmsms()->GetConfig();
    
    $mod = \cms_utils::get_module('ExaCSS');
    $preprocessor = $mod->GetPreference('preprocessor', '');
    $last_modification = self::getLastModification();
    
    $url = $config['css_url'];
    $url = preg_replace('#^https?:#', '', $url);
    $filename = self::getFilename($preprocessor, $last_modification);
    
    return $url . $filename;
    
}









/* ************************************************************************** *\
    Get last modification
\* ************************************************************************** */

public static function getLastModification() {
    
    $mod = \cms_utils::get_module('ExaCSS');
    $last_modification = NULL ;

    $result = \ExaCSS\Item::get([
        'fields' => "MAX(date_modification) last_modification",
        'where' => "status = true"
        ]) ;

    if (!$result):
        throw new Exception($mod->Lang('error_date_modification_search'));
    elseif ($result->result == false):
        throw new Exception($result->message);
    endif;

    $last_modification = $result->record[0]['last_modification'];
    
    return $last_modification;
    
}
    
    
    
    
    
    
    
    
    
/* ************************************************************************** *\
    Is cached ?
\* ************************************************************************** */

public static function isCached() {

    $return = false;

    $filename = self::getFilename();
    $path = self::getPath();

    if (file_exists(cms_join_path($path,$filename))):
        $return = true;
    endif;
    
    return $return;
    
}









/* ************************************************************************** *\
    Write
\* ************************************************************************** */

public static function write($content) {
    
    $return = false;
    
    $path = self::getPath();
    $filename = self::getFilename();
    
    if (!empty($content)):
		$fh = fopen(cms_join_path($path,$filename),'w');
		fwrite($fh, $content);
		fclose($fh);
	endif;
    
    $return = true;
    
    return $return;
    
}









/* ************************************************************************** *\
    Delete
\* ************************************************************************** */

public static function delete() {
    
    $return = false;
    
    $path = self::getPath();

    $files = glob(cms_join_path($path,'exacss_*'));
    if (count($files) > 0):
        foreach ($files as $f):
            unlink($f);
        endforeach;
    endif;
    
    $return = true;
    
    return $return;
    
}









}
?>