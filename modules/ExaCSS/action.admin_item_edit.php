<?php
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission(ExaCSS::PERM_USE) ) return;









/* ************************************************************************** *\
   Retour du formulaire d'�dition
\* ************************************************************************** */

if (!empty($params['submit_cancel'])):

    $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');

endif;

if (!empty($params['submit_save']) OR !empty($params['submit_apply'])):

    $params['id'] = (empty($params['item_id'])) ? null : $params['item_id'];
    $result = \ExaCSS\Item::set($params);

    if ($result->result == false):
        $this->SetError($result->message);
        $this->RedirectToAdminTab();  
    else:
        $this->SetMessage($result->message);
        if (!empty($params['submit_save'])):
            $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
        else:
            if (empty($params['item_id'])):
                $params['item_id'] = $result->insertId;
            endif;
            $this->RedirectToAdminTab('edit', ['item_id' => $params['item_id']], 'admin_item_edit');
        endif;
    endif;

endif;









/* ************************************************************************** *\
   R�cup�ration de l'item
\* ************************************************************************** */

if (!empty($params['item_id'])):

    $result = \ExaCSS\Item::get([
        'where' => "id = {$params['item_id']}"
        ]);
        
    if ($result->result AND $result->count == 1):
        
        $item = $result->record[0];
        $smarty->assign('item', $item);
        
    endif;

endif;









/* ************************************************************************** *\
   Affichage du template
\* ************************************************************************** */

$tpl = $smarty->CreateTemplate($this->GetTemplateResource('admin_item_edit.tpl'),null,null,$smarty);
$tpl->display();









?>