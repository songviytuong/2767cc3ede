<?php
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission(ExaCSS::PERM_USE) ) return;









/* ************************************************************************** *\
    Récupération de l'item
\* ************************************************************************** */

if (!isset($params['item_id'])):
	$this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
endif;

$result = \ExaCSS\Item::get([
    'where' => "id = {$params['item_id']}"
    ]);

$item = $result->record[0];









/* ************************************************************************** *\
    Préparation du fichier XML
\* ************************************************************************** */

$xml_filename = 'ExaCSS_'.munge_string_to_url($item['name']).'_'.munge_string_to_url($item['date_modification']).'.xml';









/* ************************************************************************** *\
    Envoi du fichier XML
\* ************************************************************************** */

$xml = new DOMDocument('1.0', 'utf-8');

$xml_content = array() ;
$root = $xml->appendChild($xml->createElement('root'));
$xml_content['name'] = $root->appendChild($xml->createElement('name', $item['name']));
$xml_content['content'] = $root->appendChild($xml->createElement('content'));
$xml_content['content']->appendChild($xml->createCDATASection($item['content']));
$xml_content['sort'] = $root->appendChild($xml->createElement('sort', $item['sort']));
$xml_content['date_creation'] = $root->appendChild($xml->createElement('date_creation', $item['date_creation']));
$xml_content['date_modification'] = $root->appendChild($xml->createElement('date_modification', $item['date_modification']));
$xml_content['status'] = $root->appendChild($xml->createElement('status', $item['status']));
$xml->formatOutput = true ;

while(@ob_end_clean());
header('Content-Description: File Transfer');
header('Content-Type: application/force-download');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 10 Jul 1990 05:00:00 GMT");
header('Content-Disposition: attachment; filename='.$xml_filename);
echo $xml->saveXML();
exit;









?>