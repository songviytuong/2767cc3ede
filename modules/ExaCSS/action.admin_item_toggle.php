<?php
if( !defined('CMS_VERSION') ) exit;
if( !$this->CheckPermission(ExaCSS::PERM_USE) ) return;









/* ************************************************************************** *\
   Vérification de l'ID
\* ************************************************************************** */

if (empty($params['item_id'])):
    $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
endif;









/* ************************************************************************** *\
   Récupération de l'item
\* ************************************************************************** */

$result = \ExaCSS\Item::get([
    'where' => "id = {$params['item_id']}",
    'limit' => 1
    ]);

if ($result->result == false):
    $this->SetError($result->message);
    $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
elseif ($result->count != 1):
    $this->SetError($this->Lang('no_result_for_this_id'));
    $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
endif;

$item = $result->record[0];









/* ************************************************************************** *\
   Bascule de l'item
\* ************************************************************************** */

$options['id'] = $item['id'];
$options['status'] = ($item['status'] == true) ? false : true;

$result = \ExaCSS\Item::set($options);

if ($result->result == false):
    $this->SetError($result->message);
   $this->RedirectToAdminTab('dashboard', '', 'defaultadmin');
endif;

$this->SetMessage($result->message);
$this->RedirectToAdminTab('dashboard', '', 'defaultadmin');









?>