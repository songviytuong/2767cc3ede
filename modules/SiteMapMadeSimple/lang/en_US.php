<?php

# A
$lang['addedit_xml'] = 'Add/Edit XML Template';
$lang['allow_hidden_pages'] = 'Show hidden pages in the sitemap?';
$lang['always'] = 'Always';
$lang['automatic'] = 'Automatic';

# C
$lang['change_frequency'] = 'Change Frequency';

# D
$lang['daily'] = 'Daily';
$lang['default_templates'] = 'Prototype Templates';
$lang['default_xml_template'] = 'Prototype XML Template';
$lang['dynamic_update'] = 'Dynamically update the static xml file when content is changed? <em>(applies only if outputting a static sitemap.xml)</em>';

# E
$lang['error_permissiondenied'] = 'Permission Denied';

# F
$lang['friendlyname'] = 'SiteMap Made Simple';

# G
$lang['generate'] = 'Generate Now';

# H
$lang['hourly'] = 'Hourly';

# I
$lang['info'] = 'Info';
$lang['info_default_templates'] = 'This form allows you to adjust the text that will be displayed when you click \'New Template\'.  Adjusting this text will have no immediate effect on any display.';
$lang['info_priority_field'] = <<<EOT
Specify the name of a content property name <em>(i.e: extra1)</em> that holds a priority value for each content page.   If this property is not set for that page, or is not an integer number between 1 and 10, then the default (depth based) mechanism will be used for calculating the page priority.  A value of 1 indicates a higher priority than 10.  This allows site administrators to manually adjust the priority of each page.
EOT;
$lang['info_sitemapurl'] = 'This is the url that you should submit to the search engine to assist them in automatically indexing your site';

# M
$lang['moddescription'] = 'A simple way to generate a google sitemap for your CMS Made Simple website';
$lang['monthly'] = 'Monthly';

# N
$lang['never'] = 'Never';

# P
$lang['postinstall'] = 'The SiteMap Made Simple module has successfully been installed.  Please set the appropriate preferences in the modules admin section';
$lang['postuninstall'] = 'The SiteMap Made Simple module has successfully been removed';
$lang['preferences'] = 'Preferences';
$lang['preuninstall'] = 'Are you really sure you want to uninstall this module?';
$lang['priority_field'] = 'Content property to use in calculating the priority field';

# S
$lang['save'] = 'Save Sitemap';
$lang['sitemap'] = 'SiteMap';
$lang['static_sitemap'] = 'Generate a static sitemap.xml file? <em><strong>Note:</strong> This requires that the httpd process have write access to the sitemap.xml file in the CMS installation root. (or the ability to create the file if it does not already exist)</em>';
$lang['submit'] = 'Submit';

# T
$lang['templates'] = 'Templates';

# U
$lang['url'] = 'URL';

# W
$lang['weekly'] = 'Weekly';

# X
$lang['xml_templates'] =  'XML Templates';

# Y
$lang['yearly'] = 'Yearly';

?>
