<?php
$lang['addedit_xml'] = 'XML-sjabloon toevoegen/bewerken';
$lang['allow_hidden_pages'] = 'Toon verborgen pagina&#039;s in de sitemap?';
$lang['always'] = 'Altijd';
$lang['automatic'] = 'Automatisch';
$lang['change_frequency'] = 'Verander de frequentie';
$lang['daily'] = 'Dagelijks';
$lang['default_templates'] = 'Standaardsjablonen';
$lang['default_xml_template'] = 'Standaard XML-sjabloon';
$lang['dynamic_update'] = 'Het statische XML-bestand dynamisch bijwerken als het is veranderd? <em>(alleen van toepassing bij uitvoer van een statische sitemap.xml)</em>';
$lang['error_permissiondenied'] = 'Toegang geweigerd.';
$lang['friendlyname'] = 'SiteMap Made Simple ';
$lang['hourly'] = 'Ieder uur';
$lang['info'] = 'Info ';
$lang['info_default_templates'] = 'Met dit formulier kunt u de tekst aanpassen die wordt getoond als u op &#039;Nieuw sjabloon&#039; klikt. Aanpassen van deze tekst heeft geen direct effect op ieder scherm.';
$lang['info_priority_field'] = 'Benoem een naam van een content &#039;extra pagina veld&#039; <em>(i.e: extra1)</em> die de priority waarde bevat.   
Als deze waarde niet is benoemd voor een bepaalde pagina of geen heel getal tussen de 1 en de 10 bevat, dan zal de standaard methode worden toegepast voor het bepalen van de pagina prioriteit.  
Een waarde 1 geeft een hogere prioriteit dan 10.
Dit geeft websitebeheerders de mogelijkheid handmatig de prioriteit van iedere pagina in te stellen.  ';
$lang['info_sitemapurl'] = 'Dit is de URL die u moet sturen naar de zoek-engine om hem te helpen bij het automatisch indexeren van uw website.';
$lang['moddescription'] = 'Een eenvoudige manier om een Google-sitemap van uw CMS Made Simple website te genereren';
$lang['monthly'] = 'Maandelijks';
$lang['never'] = 'Nooit';
$lang['postinstall'] = 'De SiteMap Made Simple-module is ge&iuml;nstalleerd. Stel de benodigde voorkeuren in via het beheerpaneel van de module';
$lang['postuninstall'] = 'De SiteMap Made Simple-module is verwijderd';
$lang['preferences'] = 'Voorkeuren';
$lang['preuninstall'] = 'Weet u zeker dat u deze module wilt deinstalleren?';
$lang['priority_field'] = 'Te gebruiken Content &#039;extra pagina veld&#039; bij het bepalen van de waarde voor het &#039;priority&#039; veld in de sitemap';
$lang['save'] = 'Sitemap opslaan';
$lang['sitemap'] = 'Sitemap';
$lang['static_sitemap'] = 'Moet een statische sitemap.xml bestand worden gegenereerd? <em><strong>Merk op:</strong> Dit vereist dat het httpd-proces schrijfrechten heeft op het sitemap.xml bestand in de CMS-installatiemap. (of de mogelijkheid dit bestand aan te maken als het nog niet bestaat)</em>';
$lang['submit'] = 'Verstuur';
$lang['templates'] = 'Sjablonen';
$lang['url'] = 'Url';
$lang['weekly'] = 'Wekelijks';
$lang['xml_templates'] = 'XML-sjablonen';
$lang['yearly'] = 'Jaarlijks';
?>
