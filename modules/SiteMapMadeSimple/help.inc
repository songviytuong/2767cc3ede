<h3>What Does It Do?</h3>
<p>This module generates a sitemap xml file suitable for submitting to google or other compatible search engines.  The sitemap is generated dynamically on a per request basis, and therefore will always represent the current status of your site.</p>
<h3>How Do I Use it?</h3>
<p>Simply installing the module will allow you to access a certain url to see the XML file.  This module will work better if pretty urls are enabled in the config.php (either using the internal_pretty_urls or by setting up mod_rewrite).</p>
<h3>Notes:</h3>
<ul>
<li><p>The change frequency generated in this module is dependant upon the last modified date of the pages.</p></li>
<li><p>The priority of the page is dependant upon the page depth.  The lower a page is in the hierarchy, the lower its priority. The 'default' page is given a priority of 1.0</p></li>
</ul>
<h3>References:</h3>
<p>See <a href="http://www.sitemap.org">www.sitemap.org</a></p>
<h3>Support</h3>
<p>This module does not include commercial support. However, there are a number of resources available to help you with it:</p>
<ul>
<li>For the latest version of this module, FAQs, or to file a Bug Report or buy commercial support, please visit the CMS Made simple website, and the development forge at: cmsmadesimple.org.</li>
<li>Additional discussion of this module may also be found in the CMS Made Simple Forums.</li>
<li>The author, calguy1000, can often be found in the CMS IRC Channel.</li>
<li>Lastly, you may have some success emailing the author directly.</li>
</ul>
<p>As per the GPL, this software is provided as-is. Please read the text of the license for the full disclaimer.</p>
<p>Copyright &copy; 2008, Robert Campbel <a href="mailto:calguy1000@cmsmadesimple.org">&lt;calguy1000@cmsmadesimple.org&gt;</a>. All Rights Are Reserved.</p>
<p>This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.</p>
<p>However, as a special exception to the GPL, this software is distributed
as an addon module to CMS Made Simple.  You may not use this software
in any Non GPL version of CMS Made simple, or in any version of CMS
Made simple that does not indicate clearly and obviously in its admin 
section that the site was built with CMS Made simple.</p>
<p>This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
Or read it <a href="http://www.gnu.org/licenses/licenses.html#GPL">online</a></p>