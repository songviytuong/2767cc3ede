<?php
#-------------------------------------------------------------------------
# Module: SiteMapMadeSimple - A module to easily create a sitemap for
#         google maps, and other search engines.
#
# Version: 1.0, calguy1000 <calguy1000@hotmail.com>
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/skeleton/
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin 
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
if( !isset($gCms) ) exit();

$url = $this->create_url('cntnt01','default',ContentOperations::get_instance()->GetDefaultContent());
$config = $gCms->GetConfig();
$static_sitemap = $this->GetPreference('static_sitemap',1);

if( $static_sitemap ) {
  $url = $config['root_url'].'/sitemap.xml';
}
else if( isset($config['assume_mod_rewrite']) && $config['assume_mod_rewrite'] === true ) {
  $url = $config['root_url'].'/sitemap.xml/1';
}
else if( isset($config['internal_pretty_urls']) && $config['internal_pretty_urls'] == true ) {
  $url = $config['root_url'].'/index.php/sitemap.xml/1';
}

$fn = cms_join_path($config['root_path'],'sitemap.xml');
if( isset($params['sitemap_generate']) ) {
  $parms = array();
  $xmlfile = $this->GenerateSitemap($parms);

  if( $static_sitemap ) {
    $f = @fopen($fn,'w');
    @fwrite($f,$xmlfile);
    @fclose($f);
  }
}
else if( file_exists($fn) && is_readable($fn) ) {
  $xmlfile = file_get_contents($fn);
}

$smarty->assign('xmlfile',$xmlfile);
$smarty->assign('formstart',$this->CGCreateFormStart($id,'defaultadmin'));
$smarty->assign('formend',$this->CreateFormEnd());
$smarty->assign('url',$url);
$smarty->assign('prompt_url',$this->Lang('url'));
$smarty->assign('info_sitemapurl',$this->Lang('info_sitemapurl'));
$smarty->assign('prompt_sitemap',$this->Lang('sitemap'));
echo $this->ProcessTemplate('info_tab.tpl');

// EOF
?>