{$formstart}
<div class="pageoverflow">
  <p class="pagetext">{$prompt_allow_hidden}:</p>
  <p class="pageinput">{$input_allow_hidden}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_change_freq}:</p>
  <p class="pageinput">{$input_change_freq}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$mod->Lang('priority_field')}:</p>
  <p class="pageinput">
    <input type="text" name="{$actionid}priority_field" size="20" maxlength="50" value="{$priority_field}"/>
    <br/>{$mod->Lang('info_priority_field')}
  </p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_static_sitemap}:</p>
  <p class="pageinput">{$input_static_sitemap}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">{$prompt_dynamic_update}:</p>
  <p class="pageinput">{$input_dynamic_update}</p>
</div>
<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput">{$submit}</p>
</div>
{$formend}
