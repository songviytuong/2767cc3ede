{$formstart}
<div class="pageoverflow">
  <p class="pagetext">{$prompt_url}:</p>
  <p class="pageinput">{$url}<br/>{$info_sitemapurl}</p>
</div>

<div class="pageoverflow">
  <p class="pagetext">{$prompt_sitemap}:</p>
  <p class="pageinput"><textarea rows="10" cols="50" readonly="readonly">{$xmlfile|default:''}</textarea></p>
</div>

<div class="pageoverflow">
  <p class="pagetext">&nbsp;</p>
  <p class="pageinput"><input type="submit" name="{$actionid}sitemap_generate" value="{$mod->Lang('generate')}"/></p>
</div>

{$formend}