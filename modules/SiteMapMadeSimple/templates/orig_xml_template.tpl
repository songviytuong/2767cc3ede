<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.google.com/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.9 http://www.google.com/schemas/sitemap/0.9/sitemap.xsd">{strip}
{foreach $output as $page}
<url>
  <loc>{$page->url}</loc>
  <lastmod>{$page->date|date_format:"%Y-%m-%d"}</lastmod>
  <priority>{$page->priority}</priority>
  <changefreq>{$page->frequency}</changefreq>
</url>
{/foreach}
{/strip}</urlset>