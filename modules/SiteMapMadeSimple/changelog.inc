<ul>
<li>1.0 - November 2007
<p>Initial Release</p></li>
<li>1.0.1 - November 2007
<p>Bugfixes</p></li>
<li>1.0.2 - November 2007
<p>Bugfixes</p></li>
<li>1.1 - November 2007
<p>Adds the ability for static xml file output, traps the page edit/delete events.</p></li>
<li>1.1.1 - November 2007
<p>Bugfixes</p></li>
<li>1.1.2 - March 2008
<p>Remove PHP5 specific code.</p></li>
<li>1.1.3 - March 2008
<p>Add IsAdminOnly().</p></li>
<li>1.1.3 - March 2008
<p>Fix minor error in caching.</p></li>

<li>1.2 - February 2009
  <ul>
    <li>Added a &quot;Save Sitemap&quot; button.</li>
    <li>Added a means by which the priority of a page could be adjusted manually.</li>
  </ul>
</li>

<li>1.2.1 - March 2009
  <ul>
    <li>Exclude internal page links.</li>
  </ul>
</li>
<li>1.2.2 - October 2011
  <ul>
    <li>Updates for CMSMS 1.10.</li>
  </ul>
</li>
<li>1.2.4 - October 2011
  <ul>
    <li>Fixes.</li>
  </ul>
</li>
<li>1.2.6 - March 2012
  <ul>
    <li>Now requires CMSM 1.10.3 (min)</li>
    <li>Give the content type to smarty.</li>
  </ul>
</li>
<li>1.2.7 - January, 2013
  <ul>
    <li>Minor bug fixes.</li>
    <li>Move help and changelog out of lang file.</li>
    <li>Now require CMSMS 1.11.3 (minimum).</li>
  </ul>
</li>
<li>1.2.7 - January, 2013
  <ul>
    <li>Updates to modern standards (should work with CMSMS 2.0)</li>
    <li>Now require CMSMS 1.11.9</li>
  </ul>
</li>
</ul>
