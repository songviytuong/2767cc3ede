<?php
#-------------------------------------------------------------------------
# Module: Cart2- A simple example frontend form module
# Version: 1.0, calguy1000 <calguy1000@cmsmadesimple.org>
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# This project's homepage is: http://www.cmsmadesimple.org
# The module's homepage is: http://dev.cmsmadesimple.org/projects/skeleton/
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------

//
// EXCEPTION
//
class cart2_Exception extends cg_exception {}
class cart2_QOHException extends cart2_Exception {}

$cgextensions = cms_join_path($gCms->config['root_path'],'modules','CGEcommerceBase','CGEcommerceBase.module.php');
if( !is_readable( $cgextensions ) ) {
  echo '<h1><font color="red">ERROR: The CGEcommerceBase module could not be found.</font></h1>';
  return;
}
require_once($cgextensions);

final class Cart2 extends CGEcommerceBase
{
  private $_items;
  private $_calculated;
  private $_subtotal;
  private $_total;
  private $_weight;
  private $_numitems;
  private $_key;

  public function __construct()
  {
    parent::__construct();

    $this->_key = md5(__FILE__);
    $this->_items = array();
    $this->_calculated = 0;
    $this->_subtotal = 0;
    $this->_total = 0;
    $this->_weight = 0;
    $this->_numitems = 0;
  }

  public function GetName() { return 'Cart2'; }
  public function GetVersion() { return '1.2.2'; }
  public function MinimumCMSVersion() { return "1.12.1"; }
  public function GetFriendlyName() { return $this->Lang('friendlyname'); }
  public function GetAuthor() { return 'calguy1000'; }
  public function GetAuthorEmail() { return 'calguy1000@cmsmadesimple.org'; }
  public function GetChangeLog() { return @file_get_contents(__DIR__.'/changelog.inc'); }
  public function IsPluginModule() { return true; }
  public function HasAdmin() { return true; }
  public function GetAdminSection() { return 'ecommerce'; }
  public function GetAdminDescription() { return $this->Lang('moddescription'); }
  public function AllowAutoInstall() { return FALSE; }
  public function AllowAutoUpgrade() { return FALSE; }
  public function LazyLoadFrontend() { return TRUE; }
  public function LazyLoadAdmin() { return TRUE; }
  public function InstallPostMessage() { return $this->Lang('postinstall'); }
  public function UninstallPostMessage() { return $this->Lang('postuninstall'); }
  public function UninstallPreMessage() { return $this->Lang('really_uninstall'); }
  public function GetDependencies() { return array('CGExtensions'=>'1.53.1', 'CGEcommerceBase'=>'1.6.2'); }

  public function VisibleToAdminUser() {
    return $this->CheckPermission('Modify Templates') || $this->CheckPermission('Modify Site Preferences');
  }

  public function InitializeFrontend()
  {
    $this->RegisterModulePlugin();
    $this->RestrictUnknownParams();
    $this->SetParameterType('addtocarttemplate',CLEAN_STRING);
    $this->SetParameterType('mycarttemplate',CLEAN_STRING);
    $this->SetParameterType('supplier',CLEAN_STRING);
    $this->SetParameterType('sku',CLEAN_STRING);
    $this->SetParameterType('price',CLEAN_STRING);
    $this->SetParameterType('product',CLEAN_STRING);
    $this->SetParameterType('viewcarttemplate',CLEAN_STRING);
    $this->SetParameterType('viewcartpage',CLEAN_STRING);
    $this->SetParameterType('hideform',CLEAN_INT);
    $this->SetParameterType('destpage',CLEAN_STRING);
    $this->SetParameterType(CLEAN_REGEXP.'/cart_.*/',CLEAN_STRING);
  }

  public function Initializeadmin()
  {
    $this->CreateParameter('addtocarttemplate','',$this->Lang('help_addtocarttemplate'));
    $this->CreateParameter('mycarttemplate','',$this->Lang('help_mycarttemplate'));
    $this->CreateParameter('supplier','Products',$this->Lang('help_supplier'));
    $this->CreateParameter('sku','Products',$this->Lang('help_sku'));
    $this->CreateParameter('price','',$this->Lang('help_price'));
    $this->CreateParameter('product','Products',$this->Lang('help_product'));
    $this->CreateParameter('viewcarttemplate','',$this->Lang('help_viewcarttemplate'));
    $this->CreateParameter('viewcartpage','',$this->Lang('help_viewcartpage'));
    $this->CreateParameter('hideform','0',$this->Lang('help_hideform'));
    $this->CreateParameter('destpage','',$this->Lang('help_destpage'));
    $this->CreateParameter('action','default',$this->Lang('help_action'));
  }

  /* ================================= */
  /* == BEGIN NON STANDARD FUNCTIONS = */
  /* ================================= */
  public function IsCartModule() { return true; }
  public function SupportsShippingInfo() { return FALSE; }

  /**
   * Erase the entire contents of the cart.
   *
   * @internal
   * @ignore
   */
  public function EraseCart($adddata = '')
  {
    if( isset($_SESSION[$this->_key]) ) {
      cg_ecomm_cart::on_cart_adjusted('before',$adddata);
      unset($_SESSION[$this->_key]);
      $this->_items = array();
      $this->_calculated = 0;
      cg_ecomm_cart::on_cart_adjusted('after',$adddata);
    }
  }


  /**
   * Calculate and store (temporarily) any cart metadata
   *
   * @internal
   * @ignore
   */
  protected function _calculate_cart_metadata($force = 0)
  {
    $gCms = cmsms();
    $smarty = $gCms->GetSmarty();
    class_exists('cg_ecomm_productinfo'); // force the product info stuff to be loaded.

    if( $this->_calculated == 1 ) return;
    if( count($this->_items) == 0 || $force == 1) $this->_expand_cart();

    $smarty->assign('currencysymbol',cg_ecomm::get_currency_symbol());
    $smarty->assign('weightunits',cg_ecomm::get_weight_units());

    $this->_weight = 0;
    $this->_subtotal = 0;
    $this->_total = 0;
    $this->_numitems = 0;
    for( $i = 0; $i < count($this->_items); $i++ ) {
      $obj =& $this->_items[$i];

      // adjust metadata.
      $this->_subtotal += $obj->get_unit_price() * $obj->get_quantity();
      $this->_total += $obj->get_item_total();
      $this->_weight += $obj->get_unit_weight() * $obj->get_quantity();
      $this->_numitems += $obj->get_quantity();
    }

    $this->_calculated = 1;
  }


  /**
   * Return the contents of the named basket.
   * This module does not support more than one basket.
   *
   * @param string The basket name
   * @param integer The feu uid
   * @return array of cg_ecomm_cartitem objects.
   */
  public function &GetBasketItems($name,$feu_uid = -1)
  {
    return $this->GetItems();
  }

  /**
   * Return shipping information and meta data about a basket
   * This module only supports one basket
   *
   * @param string The basket name
   * @param integer The FEU userid
   * @return mixed Hash containing address information, subtotal, total, weight values
   */
  public function GetBasketDetails($name,$feu_uid = -1)
  {
    $this->_calculate_cart_metadata();
    $result = array();
    $result['cart_name'] = $this->Lang('cart');
    $result['dest_first_name'] = '-- not entered --'; // what's this for?
    $result['subtotal'] = $this->_subtotal;
    $result['total'] = $this->_total;
    $result['weight'] = $this->_weight;
    return $result;
  }


  /**
   * Return an array of basket names.
   * This module only supports one basket.
   *
   * @param integer the FEU User id.
   * @return array of strings
   */
  public function GetBasketNames($feu_uid='')
  {
    // this module does not support naming baskets.
    return array($this->Lang('cart'));
  }


  /**
   * Get the number of baskets currently created
   * This module only supports one basket.
   *
   * @return integer
   */
  public function GetNumBaskets()
  {
    return 1;
  }


  /**
   * Return the number of entries in the cart (not including quantities)
   *
   * @return integer
   */
  public function GetNumItems()
  {
    $this->_calculate_cart_metadata();
    return count($this->_items);
  }


  /**
   * Get the subtotal of the cost of all of the items in the cart
   * taking into account quantities, and any price adjustments
   * but not including any taxes or shipping costs.
   *
   * @return float
   */
  public function GetTotal()
  {
    $this->_calculate_cart_metadata();
    return $this->_total;
  }


  /**
   * Get the total weight of the order. Unit of weight is determined by the products module
   *
   * @return float
   */
  public function GetTotalWeight()
  {
    $this->_calculate_cart_metadata();
    return $this->_weight;
  }


  /**
   * Expand the cart
   *
   * @internal
   * @ignore
   */
  protected function _expand_cart()
  {
    $this->_items = array();
    if( !isset($_SESSION[$this->_key]) ) return;

    $this->_items = unserialize(base64_decode($_SESSION[$this->_key]));
    $this->_calculated = 0;
    $this->_subtotal = 0;
    $this->_total = 0;
    $this->_weight = 0;
    $this->_numitems = 0;
  }


  /**
   * Collapse the cart
   *
   * @internal
   * @ignore
   */
  protected function _collapse_cart($items = array())
  {
    if( count($items) == 0 ) $items =& $this->_items;

    $data = base64_encode(serialize($items));
    $_SESSION[$this->_key] = $data;
  }


  /**
   * Add an item to the named cart
   *
   * Note: This module ignores the basket_name parameter as there is only one basket.
   *
   * @param cg_ecomm_cartitem The cart item to add
   * @param string Optional basket name (if the basket does not exist it will be created)
   * @return boolean
   */
  public function AddCartItem(cg_ecomm_cartitem $obj,$basket_name = '')
  {
    $this->_expand_cart();

    $msg = '';
    $res = cg_ecomm_cart::check_cartitem_valid($this->_items,$obj);
    if( !$res ) throw new CmsException('Attempt to add invalid cart item to the cart');

    // set the summary for the item. if it doesn't have one.
    $product = null;
    if( $obj->get_type() == $obj::TYPE_PRODUCT ) {
        $product = cg_ecomm::get_product_info($obj->get_source(),$obj->get_product_id());
        if( $obj->get_summary() == '' ) {
            $obj->set_summary(cg_ecomm_cart::calculate_cartitem_summary($product,$obj->get_attributes(),$obj));
        }
    }

    // here we check the policy on adding quantities or adding a new item.
    $added = false;
    $idx = null;
    for( $i = 0; $i < count($this->_items); $i++ ) {
        $one =& $this->_items[$i];

        if( $one->compare($obj) ) {
            // they are the same item.
            // increase the quantity and play with the unit discount if any.

            // check if we have this many available.
            $quantity = $one->get_quantity()+$obj->get_quantity();
            if( $product && $this->GetPreference('watch_qoh') ) {
                $avail_quantity = $product->get_qoh_by_sku($obj->get_sku());
                if( $quantity > $avail_quantity ) {
                    throw new cart2_QOHException($this->Lang('error_quantity_notavailable2',$obj->get_quantity(),$avail_quantity));
                }
            }

            $one->set_quantity($one->get_quantity()+$obj->get_quantity());
            $discount = min($one->get_unit_discount(),$obj->get_unit_discount());
            $one->set_unit_discount($discount);
            $idx = $i;
            $added = true;
            break;
        }
    }

    if( !$added ) {
      cg_ecomm_cart::before_add_cartitem($obj);
      $idx = count($this->_items);
      $this->_items[] = $obj;
    }

    $this->_calculated = 0;
    $this->_collapse_cart();

    // note this could be done by having a cart base class
    // or by letting the cg_ecomm stuff handle all the cart stuff
    cg_ecomm_cart::on_cart_item_added($obj);
    return $idx;
  }

  /**
   * Get all of the items in the cart
   *
   * @return mixed array of cart items, or null
   */
  public function &GetItems()
  {
    $this->_calculate_cart_metadata();
    return $this->_items;
  }

  /**
   * Set all items into the cart
   *
   * @param array of cg_ecomm_cartitem objects
   */
  public function SetItems($items)
  {
    if( is_array($items) ) {
      cg_ecomm_cart::on_cart_adjusted('before');
      $this->_items = $items;
      $this->_calculated = 0;
      $this->_collapse_cart();
      cg_ecomm_cart::on_cart_adjusted('after');
    }
  }

  /**
   * Return the HTML form for adding an item to the cart
   *
   * @param hash of form parameters
   * @return string
   */
  public function get_addtocart_form($params)
  {
    @ob_start();
    $this->DoActionBase('default','test',$params);
    $tmp = @ob_get_contents();
    @ob_end_clean();
    return $tmp;
  }

  /**
   * Check if an item with the same source and product id exists in the cart
   *
   * @param string The supplier module name
   * @param integer The product id
   * @param null ignored
   * @return boolean
   */
  public function check_itemid_exists($source,$product,$extra = null)
  {
    if( !$source ) return FALSE;
    if( $product <= 0 ) return FALSE;

    $this->_expand_cart();
    for( $i = 0; $i < count($this->_items); $i++ ) {
      $item =& $this->_items[$i];
      if( $item->get_source() == $source && $item->get_product_id() == $product ) return TRUE;
    }
    return FALSE;
  }


  /**
   * Check if an item with the specified SKU exists in the cart.
   *
   * @param string The supplier module name
   * @param string The SKU
   * @param null ignored
   * @return boolean
   */
  public function check_sku_exists($source,$sku,$extra = null)
  {
    if( !$source ) return FALSE;
    if( !$sku ) return FALSE;

    $this->_expand_cart();
    for( $i = 0; $i < count($this->_items); $i++ ) {
      $item =& $this->_items[$i];
      if( $item->get_source() == $source && $item->get_sku() == $sku ) return TRUE;
    }
    return FALSE;
  }

} // class

#
# EOF
#
?>
