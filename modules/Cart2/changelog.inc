<ul>
  <li>Version 1.0.5
    <p><strong>NOTE: REQUIRES PHP 5.3.x</strong></p>
    <p>Initial public release.</li>
  </li>
  <li>Version 1.0.7
    <p>Fix minor issue where Promotions is required.</p>
    <p>Registers static plugin on install.</p>
  </li>
  <li>Version 1.1.x
    <ul>
      <li>Bug fixes.</li>
      <li>Adds ability to override price per sku (not options)</li>
      <li>Adds support for extended promotions.</li>
    </ul>
  </li>
  <li>Version 1.2.x
    <br/>
    <p class="warning"><strong>Note:</strong> Templates have changed, and we are now using smarty scopes.  There may be compatibility problems with your templates and logic.</p>
    <ul>
      <li>Adds functionality for restricting purchases based on qoh data:
        <ul>
	  <li>Depends on a preference (off by default)</li>
   	  <li>Checks the product (or product option) for qoh after submit and will not accept the cart item if quantity > qoh</li>
  	  <li>Checks the items already in the cart after submit and will not accept the cart item if total quantity > qoh</li>
  	  <li>Hide options from the dropdown if qoh < 1</li>
	</ul>
	</li>
      <li>Adds ability to override price per sku (not options)</li>
      <li>Adds support for extended promotions.</li>
    </ul>
  </li>
</ul>
