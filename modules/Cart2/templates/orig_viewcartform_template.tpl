{* viewcartform template *}
<div class="viewcartform">
{* if the smarty variable orders_simpleviewcart is not set,
   then don't provide a form for adjusting quantities
*}
{if !isset($cartitems) || count($cartitems) == 0 }
  <div class="alert alert-warning">Your cart is empty</div>
{else}

{if isset($formstart) && !isset($orders_simpleviewcart)}{$formstart}{/if}
<table class="table" width="100%">
  <thead>
    <tr>
      <th>{$Cart2->Lang('sku')}</th>
      <th>{$Cart2->Lang('summary')}</th>
      <th>{$Cart2->Lang('quantity')}</th>
      <th>{$Cart2->Lang('unit_price')}</th>
      <th>{$Cart2->Lang('unit_discount')}</th>
      <th>{$Cart2->Lang('total')}</th>
      <th width="1%">{$Cart2->Lang('remove')}</th>
    </tr>
  </thead>
  <tbody>
  {foreach from=$cartitems item='oneitem'}
    <tr>
      <td>{$oneitem->sku}</td>
      <td>{$oneitem->summary}</td>
      <td>
         {if $oneitem->type != 1 || !isset($oneitem->quantity_box)}
           {$oneitem->quantity}
         {else}
           {$oneitem->quantity_box}
         {/if}
      </td>
      <td>{$currencysymbol}{$oneitem->unit_price|number_format:2}</td>
      <td>{$currencysymbol}{$oneitem->unit_discount|number_format:2}</td>
      <td>{$currencysymbol}{$oneitem->item_total|number_format:2}</td>
      <td>{if isset($oneitem->remove_box)}{$oneitem->remove_box}{/if}</td>
    </tr>
  {/foreach}
  </tbody>
  <tfoot>
    <tr>
      <td colspan="5" align="right">{$Cart2->Lang('total_weight')}:</td>
      <td>{$cartweight|number_format:2}{$weightunits}</td>
      <td></td>
    </tr>
    <tr>
      <td colspan="5" align="right">{$Cart2->Lang('subtotal')}:<br>
          <em>({$Cart2->Lang('infosubtotal')})</em>
      </td>
      <td>{$currencysymbol}{$carttotal|number_format:2}</td>
      <td></td>
    </tr>
    {if isset($formstart) && !isset($orders_simpleviewcart)}
    <tr>
      <td colspan="7">
        <input type="submit" name="{$submit_name}" value="{$submit_text}"/>
        <input type="submit" name="{$actionid}cart_empty_cart" value="{$Cart2->Lang('empty_cart')}"/>
      </td>
    </tr>
    {/if}
  </tfoot>
</table>

{if isset($formstart) && !isset($orders_simpleviewcart)}{$formend}{/if}
{/if}
</div>
