<?php

final class cart2_utils
{
  private function __construct() {}

  public static function get_option_text(cg_ecomm_productinfo $product,cg_ecomm_productinfo_option $opt,
					  $percent,$discount,$price)
  {
    $mod = cms_utils::get_module('Cart2');
    $smarty = \CmsApp::get_instance()->GetSmarty();
    $tpl = $mod->GetPreference('option_template');
    if( $tpl == '' ) return;

    $smarty->assign($mod->GetName(),$mod);
    $smarty->assign('mod',$mod);
    $smarty->assign('percent',$percent);
    $smarty->assign('discount',$discount);
    $smarty->assign('price',$price);
    $smarty->assign('product',$product);
    $smarty->assign('opt',$opt);
    $smarty->assign('currency_symbol',cg_ecomm::get_currency_symbol());
    $smarty->assign('currency_code',cg_ecomm::get_currency_code());
    $smarty->assign('weight_units',cg_ecomm::get_weight_units());

    return $smarty->fetch('string:'.$tpl);
  }

  public static function adjust_offer($supplier_mod,cg_ecomm_promotion_match $offer)
  {
    // this function should be in the match object.

    // ideally supplier mod should be in the offer.
    $newoffer = new stdClass;
    $newoffer->discount = null;
    $newoffer->percent = null;
    $newoffer->free_productid = null;
    $newoffer->free_productsku = null;
    $newoffer->parent = $offer->get_cart_idx();
    $newoffer->promo_id = $offer->get_promo();

    switch( $offer->get_type() ) {
    case $offer::OFFER_DISCOUNT:
      // subtract the discount frome product value... maybe.
      $discount = (float)$offer->get_val();
      if( $discount > 0 ) $discount *= -1;
      $newoffer->discount = $discount;
      break;

    case $offer::OFFER_PERCENT:;
      $percent = (float)$offer->get_val();
      if( $percent < 0 ) $percent *= -1;
      if( $percent > 1 ) $percent /= 100.0;
      $newoffer->percent = $percent;
      break;

    case $offer::OFFER_PRODUCTID:
      $newoffer->free_productid = $offer->get_val();
      break;

    case $offer::OFFER_PRODUCTSKU:
      // test the value to see if it matches a single sku.
      $tmp = explode(',',$offer->get_val());
      if( count($tmp) == 1 ) {
	// if sku does not contain wildchard chars.
	$tmp[0] = trim($tmp[0]);
	if( strpos($tmp[0],'*') === FALSE && strpos($tmp[0],'[') === FALSE  && strpos($tmp[0],'?') == FALSE ) {
	  // if we can get this product by sku, then it's valid.
	  $tproduct = cg_ecomm::get_product_by_sku($supplier_mod,$tmp[0]);
	  if( is_object($tproduct) ) {
	    //$my_offer->free_productsku = $offer->get_val(); bad
	    $newoffer->free_productsku = $offer->get_val();
	  }
	  else {
	    audit('',$this->GetName(),'offer says sku '.$offer->get_val().' is free, but we could not find it');
	  }
	}
      }
      break;

    default:
      // ignore anything else
    }

    return $newoffer;
  }

} // end of class
?>