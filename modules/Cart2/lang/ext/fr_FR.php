<?php
$lang['addedit_addtocart_template'] = 'Ajouter / &Eacute;diter le gabarit Ajouter au panier (AddToCart)';
$lang['addedit_mycartform_template'] = 'Ajouter / &Eacute;diter le gabarit Mon panier (MyCart)';
$lang['addedit_viewcartform_template'] = 'Ajouter / &Eacute;diter le gabarit Voir le panier (ViewCart)';
$lang['addtocart_destpage'] = 'Page o&ugrave; rediriger l&#039;utilisateur apr&egrave;s l&#039;ajout au panier';
$lang['addtocart_templates'] = 'Gabarits Ajouter au panier (AddToCart)';
$lang['add_to_cart'] = 'Ajouter &agrave; mon panier';
$lang['amount'] = 'Montant';
$lang['default_addtocart_template'] = 'Gabarit Ajouter au panier par d&eacute;faut';
$lang['default_mycartform_template'] = 'Gabarit Mon panier par d&eacute;faut';
$lang['default_viewcartform_template'] = 'Gabarit Voir le panier par d&eacute;faut';
$lang['default_templates'] = 'Gabarits par d&eacute;faut';
$lang['delete'] = 'Supprimer';
$lang['description'] = 'Description&nbsp;';
$lang['empty_cart'] = 'Supprimer tous les articles';
$lang['error_cartpolicy_additem'] = 'Impossible d&#039;ajouter cet article dans le panier. Veuillez consulter les r&egrave;gles du site concernant la gestion du panier';
$lang['error_invalidparam'] = 'Un param&egrave;tre fourni a une valeur non correcte&nbsp;: %s';
$lang['error_missingparam'] = 'Un param&egrave;tre requis est manquant&amp;nbps;: %s';
$lang['error_nosuchproduct'] = 'Aucune information trouv&eacute;e sur le produit&nbsp;: %s';
$lang['free_product'] = 'Article gratuit';
$lang['friendlyname'] = 'Panier2';
$lang['help_option_template'] = '<h3>Option Template:</h3>
<p>This template is used for generating the dropdown list in the add-to-cart form for items <em>(usually products)</em> That contain options.  It generates a simple one line display.</p>
<p>Available variables include:</p>
<table border=&quot;1&quot; cellpadding=&quot;3&quot; style=&quot;margin-top: 0.5em;&quot;>
  <thead>
    <tr>
      <th>Name:</th>
      <th>Description:</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>$currency_symbol</td>
      <td>The currency symbol. <em>(i.e: $)</em></td>
    </tr>
    <tr>
      <td>$currency_code</td>
      <td>The currency code. <em>(i.e: USD)</em></td>
    </tr>
    <tr>
      <td>$product</td>
      <td>An object representing product information. See the cg_ecomm_productinfo class for the list of available methods.</td>
    </tr>
    <tr>
      <td>$option</td>
      <td>An object representing the option information. See the cg_ecomm_productinfo_option class for the list of available methods.</td>
    </tr>
    <tr>
      <td>$discount</td>
      <td>The value of any discount applied to this option via promotion matching. <em>(May be null or empty.)</em></td>
    </tr>
    <tr>
      <td>$percent</td>
      <td>The percentage discount applied to this option via promotion matching. <em>(May be null or empty.)</em></td>
    </tr>
  </tbody>
</table>';
$lang['help_action'] = 'Specifies the behaviour of the module.  Possible values are &#039;default&#039;,&#039;mycart&#039;, and &#039;viewcart&#039;.<br/><ul><li>default: displays a form with an &#039;add to cart&#039; button to allow adding a specific product to the cart.  This mode requires the &#039;product&#039; parameter be supplied.</li><li>mycart: Displays a form that displays the number of items in the cart, and a &#039;checkout&#039; button.</li><li>viewcart: Displays a detailed form of the contents of the cart, including a current total, and allows deleting items from the cart.</li></ul>';
$lang['infosubtotal'] = 'C&#039;est un sous-total. Les taxes et les frais de transports sont calcul&eacute;s au moment de la finalisation de la commande';
$lang['lbl_productsummarytemplate'] = 'Gabarit de sommaire de produit';
$lang['moddescription'] = 'Un module de panier simple';
$lang['mycartform_templates'] = 'Gabarits du formulaire Mon panier';
$lang['my_cart'] = 'Mon panier';
$lang['no'] = 'Non';
$lang['none'] = 'Aucun';
$lang['number'] = 'Nombre';
$lang['number_of_items'] = 'Nombre d&#039;articles';
$lang['postinstall'] = 'Le module Panier a bien &eacute;t&eacute; install&eacute;';
$lang['postuninstall'] = 'Le module Panier a &eacute;t&eacute; d&eacute;sinstall&eacute;';
$lang['preferences'] = 'Pr&eacute;f&eacute;rences';
$lang['price'] = 'Prix';
$lang['product_id'] = 'Id produit';
$lang['product_summary'] = 'Gabarit de sommaire de produit';
$lang['prompt_option_template'] = 'Gabarit d&#039;option d&#039;ajout au panier';
$lang['quantity'] = 'Quantit&eacute;';
$lang['really_uninstall'] = '&Ecirc;tes-vous s&ucirc;r de vouloir d&eacute;sinstaller ce module ?';
$lang['remove'] = 'Supprimer';
$lang['sku'] = 'UGS';
$lang['submit'] = 'Envoyer';
$lang['subtotal'] = 'Sous-total';
$lang['summary'] = 'Sommaire';
$lang['shipping'] = 'Livraison';
$lang['shipping_module'] = 'Module de livraison';
$lang['total'] = 'Total&nbsp;';
$lang['total_weight'] = 'Poids total';
$lang['unit_price'] = 'Prix unitaire';
$lang['viewcartform_templates'] = 'Gabarits du formulaire ViewCart';
$lang['warn_default_templates'] = 'Ce formulaire contr&ocirc;le ce qui est affich&eacute; au d&eacute;part quand vous cliquez sur &quot;Ajouter un nouveau gabarit&quot; dans l&#039;onglet correspondant. La modification du contenu dans cette zone d&#039;&eacute;dition n&#039;aura pas d&#039;effet imm&eacute;diat sur votre site.';
$lang['weight'] = 'Poids';
$lang['yes'] = 'Oui';
$lang['yousave'] = 'Vous sauvegardez';
$lang['qca'] = 'P0-483686814-1251229608352';
?>