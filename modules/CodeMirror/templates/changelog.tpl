<ul>
  <li>5.0.1 - Fixed apply buttons (autosave to textarea onchange), fixed a url issue, (thanks Matthieu) and added replacing css syntax with sass/less</li>
  <li>5.0 - Updated to CM 5.20.2 and fixed module to cmsms-2</li>
  <li>4.4 - Updated to CM 4.4</li>
  <li>4.0 - Updated to CM 4.0</li>
  <li>3.2.0 - Updated to CM 3.20</li>
  <li>3.0.2 - Updated to CM 3.02</li>
  <li>3.0.0 - Updated to CM 3.0-final. Possible fix to admins on https. Fixed so that editing UDT's are getting highlighted</li>
  <li>2.3.2 - Updated to CM 2.31</li>
  <li>2.3.1 - Apply-button in edit css are fixed, templates still have core bug :(</li>
  <li>2.3.0 - Reimplemented linewrapping, automated theme detection</li>
  <li>2.2.3 - Updated to CM 2.2.3</li>
  <li>2.2.2 - Updated to CM 2.2.1</li>
  <li>2.2.1 - Updated to CM 2.2.0, added tab-width setting</li>
  <li>2.2.0 - Updated to CM 2.15, added optional fullscreen mode</li>
  <li>2.1.2 - Updated to CM 2.12, cleaner way of saving editor content to textarea</li>
  <li>2.1.0 - Updated to CM 2.1.1, once again change testmode... </li>
  <li>2.0.1 - Updated to CM 2.0.1</li>
  <li>2.0 - Updated to CM 2.0 and reworked just about everything to make this work</li>
  <li>0.1.9 - Updated to CM 0.93</li>
  <li>0.1.8 - Updated to CM 0.9, added option on how to handle TAB</li>
  <li>0.1.7 - Updated to CM 0.80</li>
  <li>0.1.6 - Updated to CM 0.70, Added option to match parenthesis</li>
  <li>0.1.5 - Updated to CM 0.66, removed a strange \n char from headeroutput, made tabbing work</li>
  <li>0.1.4 - Fixed CPU hogging problems by rewriting testarea-code</li>
  <li>0.1.3 - Updated to CM 0.65, added option to toggle wordwrap</li>
  <li>0.1.2 - Updated to CM 0.63</li>
  <li>0.1.1 - Updated to CM 0.62, fixed examples and probably functionality as well</li>
  <li>0.1.0 - Initial release </li>
</ul>