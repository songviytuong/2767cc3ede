<?php
$lang['friendlyname'] = 'CodeMirror syntaxhighlighting editor';
$lang['postinstall'] = 'CodeMirror-modulen installerades utan n&aring;gra problem';
$lang['needpermission'] = '&#039;You need the &#039;%s&#039; permission to perform that function.';
$lang['settingstab'] = 'Inst&auml;llningar';
$lang['savesettings'] = 'Spara inst&auml;llningar';
$lang['settingssaved'] = 'Inst&auml;llningar sparades';
$lang['htmltest'] = 'HTML syntax test';
$lang['csstest'] = 'CSS syntax test';
$lang['phptest'] = 'PHP syntax test';
$lang['jstest'] = 'JavaScript syntax test';
$lang['examplesyntax'] = 'Syntax att testa med';
$lang['change'] = '&Auml;ndra...';
$lang['showlinenumbers'] = 'Visa radnummer';
$lang['textwrapping'] = 'Wrap long lines';
$lang['help'] = '<strong>What does this module do?</strong>
<br/>
The CodeMirror module allows you to edit content, templates and stylesheets using a syntaxhighlighting editor inside your browser.
It can both replace a wysiwyg-module, and act standalone for template/stylesheet editing (only supported in CMSms 1.1+)
<br/><br/>
<strong>How do I use this module?</strong>
You simply install it, and choose it in your admin user preferences. Note, that if you use CMSms 1.1 or above you can
select CodeArea both as a WYSIWYG and as a syntax highlighting editor.';
$lang['qca'] = 'P0-1231872325-1269359321195';
$lang['utma'] = '156861353.1269777603.1269359342.1269359342.1269359342.1';
$lang['utmb'] = '156861353.1.10.1269359342';
$lang['utmc'] = '156861353';
$lang['utmz'] = '156861353.1269359342.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=cmsms';
?>