<?php
$lang['friendlyname'] = 'CodeMirror syntaksfremhevingsredigerer';
$lang['postinstall'] = 'CodeMirror modulen ble installert';
$lang['needpermission'] = 'Du trenger &#039;%s&#039; tillatelse for &aring; utf&oslash;re denne funksjonen.';
$lang['settingstab'] = 'Innstillinger';
$lang['savesettings'] = 'Lagre innstillinger';
$lang['settingssaved'] = 'Innstillinger lagret';
$lang['htmltest'] = 'HTML syntaks test';
$lang['csstest'] = 'CSS syntaks test';
$lang['phptest'] = 'PHP syntaks test';
$lang['jstest'] = 'JavaScript syntakstest';
$lang['showlinenumbers'] = 'Vis linjenummer';
$lang['allowfullscreen'] = 'Tillat fullskjermsmodus (F11)';
$lang['textwrapping'] = 'Tekstbryting';
$lang['automatchparens'] = 'Automatiske paranteser';
$lang['tabhandling'] = 'Hvordan skal trykk av TAB-tasten h&aring;nteres';
$lang['tabdefault'] = 'La nettleseren avgj&oslash;re';
$lang['tabindent'] = 'Lag innrykk';
$lang['tabspaces'] = 'Sett inn mellomrom';
$lang['tabshift'] = 'Shift til h&oslash;yre, shift-TAB sender til venstre';
$lang['tabsize'] = 'St&oslash;rrelse p&aring; faner';
$lang['cssadditions'] = 'CodeMirror-css tillegg.';
$lang['theme'] = 'Tema';
$lang['help'] = '<strong>What does this module do?</strong>
<br/>
The CodeMirror module allows you to edit content, templates and stylesheets using a syntaxhighlighting editor inside your browser.
It can both replace a wysiwyg-module, and act standalone for template/stylesheet editing (only supported in CMSms 1.1+)
<br/><br/>
<strong>How do I use this module?</strong>
You simply install it, and choose it in your admin user preferences. Note, that if you use CMSms 1.1 or above you can
select CodeArea both as a WYSIWYG and as a syntax highlighting editor.';
$lang['qca'] = 'P0-536849115-1307983495210';
$lang['utma'] = '156861353.150519287.1381072773.1381072773.1381078439.2';
$lang['utmz'] = '156861353.1381072773.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)';
$lang['utmb'] = '156861353';
$lang['utmc'] = '156861353';
?>