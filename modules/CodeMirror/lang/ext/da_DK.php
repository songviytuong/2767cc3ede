<?php
$lang['friendlyname'] = 'CodeMirror - en syntaks-fremh&aelig;vende editor';
$lang['postinstall'] = 'CodeMirror modulet blev korrekt installeret';
$lang['needpermission'] = '&#039;You need the &#039;%s&#039; permission to perform that function.';
$lang['settingstab'] = 'Indstillinger';
$lang['savesettings'] = 'Gem indstillinger';
$lang['settingssaved'] = 'Indstillingerne blev gemt';
$lang['htmltest'] = 'HTML syntaks test';
$lang['csstest'] = 'CSS syntaks test';
$lang['phptest'] = 'PHP syntaks test';
$lang['jstest'] = 'JavaScript syntaks test';
$lang['examplesyntax'] = 'Syntaks der skal testes med';
$lang['change'] = 'Skift...';
$lang['showlinenumbers'] = 'Vis linienumre';
$lang['textwrapping'] = 'Ombryd lange linier';
$lang['automatchparens'] = 'Vis matchende parenteser';
$lang['tabhandling'] = 'How to handle pressing the TAB key';
$lang['tabdefault'] = 'Let the browser decide';
$lang['tabindent'] = 'Do indentation';
$lang['tabspaces'] = 'Insert spaces';
$lang['tabshift'] = 'Shift to the right, shift-TAB shifts to left';
$lang['cssadditions'] = 'Tilf&oslash;jelser til CodeMirror-css.';
$lang['help'] = '<strong>Hvad g&oslash;r dette modul?</strong>
<br/>
CodeMirror-modules giver dig mulighed for at redigere indhold, skabeloner og stylesheets ved hj&aelig;lp af syntaks-fremh&aelig;vende redigering i din browser.
<br/><br/>
<strong>Hvordan bruger jeg dette modul?</strong>
Du installerer det og v&aelig;lger det i dine bruger indstillinger for adminsitrationen. Bem&aelig;rk at du kan v&aelig;lge at benytte det b&aring;de som syntaks fremh&aelig;ver og WYSIWYG.';
$lang['qca'] = '1229601790-85243197-43260713';
$lang['utma'] = '156861353.389684394.1301142140.1301254697.1301435256.3';
$lang['utmz'] = '156861353.1301142140.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)';
$lang['utmb'] = '156861353';
$lang['utmc'] = '156861353';
?>