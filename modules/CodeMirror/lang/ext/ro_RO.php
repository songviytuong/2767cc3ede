<?php
$lang['friendlyname'] = 'Editorul CodeMirror cu evidentiator de sintaxa';
$lang['postinstall'] = 'Modulul CodeMirror a fost instalat cu succes';
$lang['needpermission'] = 'Aveti nevoie de permisiunea &#039;%s&#039; pentru aceasta functie.';
$lang['settingstab'] = 'Setari';
$lang['savesettings'] = 'Salvare setari';
$lang['settingssaved'] = 'Setarile au fost salvate';
$lang['htmltest'] = 'test sintaxa HTML';
$lang['csstest'] = 'test sintaxa CSS';
$lang['phptest'] = 'test sintaxa PHP';
$lang['jstest'] = 'test sintaxa JavaScript';
$lang['examplesyntax'] = 'Syntax to test cu';
$lang['change'] = 'Schimba';
$lang['showlinenumbers'] = 'Afisare numere de linie';
$lang['textwrapping'] = 'Strange liniile lungi';
$lang['automatchparens'] = 'Autopotrivire paranteze';
$lang['help'] = '<strong>Ce face acest modul?</strong>
<br/>
Modulul CodeMirror va permite sa editati continut, template-uri si stylesheet-uri folosind un editor cu evidentiator de sintaxa din browser.
Poate inlocui atat un modul wysiwyg, si sa functioneze de sine statator pentru editare de template/stylesheet.
<br/><br/>
<strong>Cum se folosese acest modul?</strong>
Instalati-l, si alegeti-l in preferintele de utilizator din admin.';
$lang['qca'] = 'P0-1838215043-1282807533934';
$lang['utmz'] = '156861353.1282807972.1.2.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=cmsms';
$lang['utma'] = '156861353.1191873817.1282807534.1282807534.1282807534.1';
$lang['utmc'] = '156861353';
$lang['utmb'] = '156861353.12.9.1282807615368';
?>