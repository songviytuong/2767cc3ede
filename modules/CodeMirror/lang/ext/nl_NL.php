<?php
$lang['friendlyname'] = 'CodeMirror Syntaxhighlighting Editor';
$lang['postinstall'] = 'De CodeMirror Module is ge&iuml;nstalleerd';
$lang['needpermission'] = 'U heeft &#039;%s&#039; rechten nodig om deze functie te gebruiken.';
$lang['settingstab'] = 'Instellingen';
$lang['savesettings'] = 'Instellingen opslaan';
$lang['settingssaved'] = 'Instellingen opgeslagen';
$lang['htmltest'] = 'HTML Syntax Test';
$lang['csstest'] = 'CSS Syntax Test';
$lang['phptest'] = 'PHP Syntax Test';
$lang['jstest'] = 'JavaScript Syntax Test';
$lang['showlinenumbers'] = 'Regelnummers tonen';
$lang['allowfullscreen'] = 'Sta fullscreen modus toe (F11)';
$lang['textwrapping'] = 'Lange regels afbreken';
$lang['automatchparens'] = 'Automatisch bij elkaar horende tags kleuren';
$lang['tabhandling'] = 'Wat te doen bij het indrukken van TAB toets';
$lang['tabdefault'] = 'Laat de browser het bepalen';
$lang['tabindent'] = 'Maak regel inspringing';
$lang['tabspaces'] = 'Voeg spaties toe';
$lang['tabshift'] = 'Verplaatst naar rechts, shift-TAB verplaatst naar links';
$lang['tabsize'] = 'Afmetingen van de tabs';
$lang['cssadditions'] = 'Aanpassingen aan de CodeMirror-css.';
$lang['theme'] = 'Theme ';
$lang['help'] = '<strong>What does this module do? </strong>
<br />
The CodeMirror module allows you to edit content, templates and stylesheets using a syntaxhighlighting editor inside your browser.
It can both replace a wysiwyg-module, and act standalone for template/stylesheet editing (only supported in CMSms 1.1+)
<br/><br/>
<strong>How do I use this module?</strong>
You simply install it, and choose it in your admin user preferences. Note, that if you use CMSms 1.1 or above you can
select CodeArea both as a WYSIWYG and as a syntax highlighting editor.';
$lang['utma'] = '156861353.1528414206.1368110210.1368110210.1368110210.1';
$lang['utmz'] = '156861353.1368110210.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none)';
$lang['utmb'] = '156861353';
$lang['utmc'] = '156861353';
?>