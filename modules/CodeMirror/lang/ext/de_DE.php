<?php
$lang['friendlyname'] = 'CodeMirror - Editor mit Syntaxhervorhebung';
$lang['postinstall'] = 'Das CodeMirror-Modul wurde installiert.';
$lang['needpermission'] = 'Sie ben&ouml;tigen die &#039;%s&#039;-Berechtigung, um diese Funktion auszuf&uuml;hren.';
$lang['settingstab'] = 'Einstellungen';
$lang['savesettings'] = 'Einstellungen speichern';
$lang['settingssaved'] = 'Einstellungen gespeichert';
$lang['htmltest'] = 'HTML-Syntax-Test';
$lang['csstest'] = 'CSS-Syntax-Test';
$lang['phptest'] = 'PHP-Syntax-Test';
$lang['jstest'] = 'JavaScript-Syntax-Test';
$lang['showlinenumbers'] = 'Zeilennummern anzeigen';
$lang['allowfullscreen'] = 'Vollbildmodus erlauben (F11)';
$lang['automatchparens'] = 'Automatische Klammer-Erkennung';
$lang['tabhandling'] = 'Was soll beim Dr&uuml;cken der TAB-Taste passieren';
$lang['tabdefault'] = 'Browser entscheiden lassen';
$lang['tabindent'] = 'Einr&uuml;cken';
$lang['tabspaces'] = 'Leerzeichen einf&uuml;gen';
$lang['tabshift'] = 'Shift nach rechts, Shift-TAB verschiebt sich nach links';
$lang['tabsize'] = 'Gr&ouml;&szlig;e der Registerkarten';
$lang['cssadditions'] = 'Zus&auml;tze zur CodeMirror-CSS.';
$lang['theme'] = 'Theme ';
$lang['help'] = '<h3>Was macht dieses Modul?</h3>
<p>Das CodeMirror-Modul ist ein Editor mit Syntaxhervorhebung, mit dem Sie die Inhalte, die Templates und die Stylesheets bearbeiten k&ouml;nnen. Das Modul kann sowohl die WYSIWYG-Editoren ersetzen als auch separat zum Bearbeiten von Templates und Stylesheets verwendet werden (unterst&uuml;tzt ab CMSMS-Version 1.1).</p>
<h3>Wie wird es eingesetzt?</h3>
<p>Installieren Sie das Modul und aktivieren Sie es anschlie&szlig;end in den benutzerspezifischen Einstellungen.</p>
<p>Beachten Sie, dass Sie ab CMSMS-Version 1.1 und h&ouml;her CodeMirror sowohl als WYSIWYG-Editor als auch als Editor mit Syntaxhervorhebung ausw&auml;hlen k&ouml;nnen.</p>';
$lang['utma'] = '156861353.1244232381.1326832795.1326832795.1326832795.1';
$lang['utmz'] = '156861353.1326832795.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none)';
$lang['utmc'] = '156861353';
$lang['utmb'] = '156861353';
?>