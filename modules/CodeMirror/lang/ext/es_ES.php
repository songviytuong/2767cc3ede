<?php
$lang['friendlyname'] = 'Editor de resaltado de sintaxis CodeMirror';
$lang['postinstall'] = 'El m&amp;oacute;dulo CodeMirror fue satisfactoriamente instalado';
$lang['needpermission'] = '&#039;Se necesita el permiso de &#039;%s&#039; para realizar esta funci&amp;oacute;n.';
$lang['settingstab'] = 'Ajustes';
$lang['savesettings'] = 'Salvar ajustes';
$lang['settingssaved'] = 'Ajustes guardados';
$lang['htmltest'] = 'Prueba de sintaxis HTML';
$lang['csstest'] = 'Prueba de sintaxis CSS';
$lang['phptest'] = 'Prueba de sintaxis PHP';
$lang['examplesyntax'] = 'Syntaxis usada para probar';
$lang['change'] = 'Cambiar...';
$lang['showlinenumbers'] = 'Mostar el n&amp;uacute;mero de l&amp;iacute;neas';
$lang['textwrapping'] = 'Auto-ajustar lineas largas';
$lang['help'] = '<strong>Qu&amp;eacute; hace este m&amp;oacute;dulo?</strong>
<br/>
El m&amp;oacute;dulo CodeMirror le permite editar contenido, plantillas y hojas de estilo usando un editor de resaltado de sintaxis dentro de su navegador.
Este tambiu&amp;eacute;n puede  reemplazar el m&amp;oacute;dulo de wysiwyg, y actuar solo para edici&amp;oacute;n de plantillas/hojas de estilo template/stylesheet  (solo soportado en CMSms 1.1+)
<br/><br/>
<strong>C&amp;oacute;mo uso este m&amp;oacute;dulo?</strong>
Usted simplemente lo instala, y lo selecciona en las preferecias de la administraci&amp;oacute;n. Note que, si usted usa CMSms 1.1 o superior a este usted puede seleccionar CodeMirror junto con WYSIWYG para el resaltado de sintaxis.';
$lang['qca'] = 'P0-2096054370-1259803615939';
$lang['utma'] = '156861353.238606854.1259804368.1267652459.1267659773.57';
$lang['utmz'] = '156861353.1267652459.56.28.utmccn=(referral)|utmcsr=forum.cmsmadesimple.org|utmcct=/index.php/topic,19929.0.html|utmcmd=referral';
$lang['utmc'] = '156861353';
$lang['utmb'] = '156861353';
?>