<?php
$lang["friendlyname"]="CodeMirror syntaxhighlighting editor";

$lang["postinstall"]="The CodeMirror module was sucessfully installed";
$lang["needpermission"]="'You need the '%s' permission to perform that function.";

$lang["settingstab"]="Settings";
$lang["savesettings"]="Save settings";
$lang["settingssaved"]="Settings saved";

$lang["htmltest"]="HTML syntax test";
$lang["csstest"]="CSS syntax test";
$lang["phptest"]="PHP syntax test";
$lang["jstest"]="JavaScript syntax test";
//$lang["examplesyntax"]="Syntax to test with";
//$lang["change"]="Change...";

$lang["showlinenumbers"]="Show linenumbers";
$lang["allowfullscreen"]="Allow fullscreen mode (F11)";

$lang["textwrapping"]="Wrap long lines";
$lang["automatchparens"]="Automatch parenthesis";
$lang["tabhandling"]="How to handle pressing the TAB key";
$lang["tabdefault"]="Let the browser decide";
$lang["tabindent"]="Do indentation";
$lang["tabspaces"]="Insert spaces";
$lang["tabshift"]="Shift to the right, shift-TAB shifts to left";
$lang["tabsize"]="Size of tabs";

$lang["cssadditions"]="Additions to the CodeMirror-css.";

$lang["theme"]="Theme";

$lang["dontreplacecss"]="Don't replace";
$lang["cssreplace"]="Replace CSS editing with";

$lang["help"]="<strong>What does this module do?</strong>
<br/>
The CodeMirror module allows you to edit content, templates and stylesheets using a syntaxhighlighting editor inside your browser.
It can both replace a wysiwyg-module, and act standalone for template/stylesheet editing (only supported in CMSms 1.1+)
<br/><br/>
<strong>How do I use this module?</strong>
You simply install it, and choose it in your admin user preferences. Note, that if you use CMSms 1.1 or above you can
select CodeMirror both as a WYSIWYG and as a syntax highlighting editor.";
?>
