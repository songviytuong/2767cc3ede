$(document).ready(function(){
    ajaxCart.overrideButtonsInThePage();
    $(document).on('click', '.block_cart_collapse', function(e){
        e.preventDefault();
        ajaxCart.collapse();
    });
    $(document).on('click', '.block_cart_expand', function(e){
        e.preventDefault();
        ajaxCart.expand();
    });
});

var ajaxCart = {
    overrideButtonsInThePage : function(){
        //for every 'add' buttons...
        $(document).on('click', '.ajax_add_to_cart_button', function(e){
            console.log('ajax_add_to_cart_button');
            var idProduct =  $(this).data('id-product');
            if ($(this).prop('disabled') != 'disabled'){
                ajaxCart.add(idProduct, null, false, this);
            }
        });
        
        
    },

    // try to expand the cart
    expand : function(){
        console.log('expand');
    },

    // try to collapse the cart
    collapse : function(){
        console.log('collapse');
    },
    // Fix display when using back and previous browsers buttons
    refresh : function(){
        console.log('refresh');
    },
    // Update the cart information
    updateCartInformation : function (jsonData, addedFromProductPage){
        console.log('updateCartInformation');
    },
    // close fancybox
    updateFancyBox : function (){},
    // add a product in the cart via ajax
    add : function(idProduct, idCombination, addedFromProductPage, callerElement, quantity, whishlist){
        console.log('add:'+idProduct);
    }
}